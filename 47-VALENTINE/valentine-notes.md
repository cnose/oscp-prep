# VALENTINE | 10.10.10.79

## Initial Enumeration

    sudo nmap -v -T4 -sV -sC -oN nmap-initial 10.10.10.79
    
### Open Ports and Their Services

* 22 - SSH - OpenSSH 5.9p1 Debian
* 80 - HTTP - Apache 2.2.22
* 443 - HTTPS - Apache 2.2.22

## Services Enumeration

### 22 - OpenSSH 5.9p1

Kind of an older version of SSH here. Even so, the only weaknesses are username enumeration and an SFTP vuln. Might come back here if I get creds or if all else fails.

### 80/443 - Apache 2.2.22

There is a cute (and somehow familiar) image on both pages:

![](img/port-80_443.png)

I grabbed the image to do forensics if I am forced to.

Gobuster finds a `/dev/` directory with some files in it:

![](img/dev.png)

One is what appears to be dev notes, referring to encoding and decoding:

![](img/notes.png)

The other appears to be some kind of cipher text, maybe in hex?

![](img/hype-key.png)

Meanwhile, as gobuster churns away, it finds directories and php files related to encoding and decoding, and an `/omg` directory:

![](img/gobuster.png)

Using an [online tool](https://wiremask.eu/tools/binary-translator/) and correctly guessing it was hexadecimal, the code turns out to be a private SSH key:

![](img/ssh-key.png)

I still don't have a username though. And running the hash through john with the rockyou wordlist (I tried using it to login with username 'hype' and it asked for a passphrase) didn't come up with any hits.

Playing around with the encoder.php and I can see it is just encoding strings in base64.

I feel like I have some but not all of the pieces here. And the [image](enum/omg.jpg) is bothering me because I know I have seen it before. Because it is the fucking [HEARTBLEED](https://heartbleed.com) logo.

### Heartbleed

It is a vulnerability in openssl 1.0.1 and 1.0.2-beta that allows an attacker to read protected parts of memory, leading to information disclosure such as personal details, user credentials or even the encryption keys themselves.

I can test for the vuln using nmap, since they have a script:

![](img/nmap-heartbleed.png)

So the server is vulnerable! Let's check searchsploit for any POCs:

![](img/searchsploit.png)

## Foothold

I've grabbed one of the python-based heartbleed exploits, let's hope I've not chosen a dud.

It tests for the vulnerability, and spits out some of the illicitly-obtained memory. I had to run it a few times to get anything interesting:

![](img/hb-decode.png)

It is a base64-encoded string...

![](img/believe.png)

I guess that may be the passphrase for the SSH key. Let's try:

![](img/hype-shell.png)

And I'm in!

### Flag

![](img/hype-flag.png)

## User Enumeration

First thing I did was read the user's `.bash_history` file and I see that they were running a Tmux session:

![](img/history.png)

And there is a `.tmux.conf` in the user's home folder, but it is owned by root, which makes me think that the tmux session is a root session. Looking closer at the history, we see a `/.devs` directory, which is not normal - it is owned by the root user and the hype group. I think I'll be coming back to this for privesc, but I should do my due diligence so I'll run linpeas on the machine before moving forward.

### linpeas

* Old ass kernel - vulnerable to several kernel exploits:

![](img/kernel.png)

* Tmux running as root:

![](img/tmux.png)

### Linux exploit suggester

This kernel (3.2.0.23-generic) is vulnerable to DirtyCOW and another one called 'perf_swevent' (a very detailed explainer is [here](http://timetobleed.com/a-closer-look-at-a-recent-privilege-escalation-bug-in-linux-cve-2013-2094/)).

OK, so now we've found some potential vectors, namely the Tmux session and a couple of kernel exploits.

## Privilege Escalation

### Tmux

It is a terminal multiplexer that allows for multiple terminal sessions, as well as detaching said sessions for later resumption. This system has a tmux session in progress, running as root. I _think_ I just need to attach to the session.

    tmux -S /.devs/dev_sess
    
![](img/root-shell.png)

### Dirty COW

OK, now that we have root in what I think was the intended way, I'm going to try the Dirty COW exploit and see if I can get root that way. This is an exploit that tries to abuse faster processors by inducing a race condition in the Copy On Write function (not sure of that's the right word). Basically it allows an attacker to take a root-owned (in this case, SUID binary /usr/bin/passwd) file and write to it when it was actually trying to write to a copy of it. A great explainer video is [here](https://www.youtube.com/watch?v=kEsshExn7aE).

I grabbed one of the C files from the exploitdb archive (there are several types of dirtycow), and then sent it over to the victim machine and compiled it there.

Then I executed the exploit and waited and waited and waited and I think it hanged. So I tried another method of the exploit, which overwrites `/etc/passwd` with a new root user:

![](img/dirtycow.png)

OK, that one worked. It is noisy as all hell though.

### Flag

![](img/root-flag.png)

### Behind the Curtain

In the root user's home dir is a script - `curl.sh` - that is responsible for placing the base64-encoded passphrase into memory, so that we can read it using the heartbleed vuln. Normally, you would have to run the exploit constantly, and juicy data would slowly bleed out.

![](img/curl.png)

## Conclusion

A fun box and a good intro to the heartbleed bug. I remember when this bug was disclosed as I think it was one of the first to have a flashy campaign behind it - the logo, the website etc. I guess it's a good way to get people to take it seriously.

Steps taken:

* find base64-encoded private ssh key on server
* discover server is vulnerable to heartbleed
* use heartbleed to read sensitive memory, decoding the SSH passphrase, login as user
* attach to already running Tmux session that is running as root, or
* use DirtyCOW exploit to get a root shell
