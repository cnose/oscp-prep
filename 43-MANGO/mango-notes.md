# MANGO | 10.10.10.162

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.162
    
### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.6p1 Ubuntu
* 80 - HTTP - Apache 2.4.29 Ubuntu
* 443 - HTTPS - Apache 2.4.29 Ubuntu

## Services Enumeration

### 22 - OpenSSH 7.6p1

Nothing to see here. If I get credentials I'll try them here.

### 80/443 - HTTP(S)

Port 80 is forbidden, but port 443 gives us a convincing google clone:

![](img/port-443.png)

The 'Analytics' link at the top right actually goes somewhere:

![](img/analytics.png)

Also, in the nmap scan there is a proper web address that I should probably add to my `/etc/hosts` file:

![](img/common-name.png)

When I do that, the service on port 80 redirects to a login page:

![](img/mango-login.png)

Everyone by now knows that the mango refers to MongoDB, a database system, so I went looking for weaknesses. Searchsploit had a couple of RCEs but I was looking for a bypass.

Mongo uses the NoSQL database language so I looked for NoSQL bypass mongo and found a [page](https://www.imperva.com/blog/nosql-ssji-authentication-bypass/) that detailed how it could be done:

![](img/NOSQL-bypass-method.png)

In BurpSuite I modified the GET request to contain the '[$ne]' characters in the correct spot, and sent it along. I followed the redirect and ended up at 'home.php' so that's something:

![](img/home.png)

There is a vuln there. But being out of my depth I turn to a good resource first: [hacktricks](https://book.hacktricks.xyz/pentesting-web/nosql-injection) is an invaluable resource to have at hand when you are lost, like I currently am.

But there is a nice python-based brute forcing script that I can modify and use to find usernames and passwords! Pretty handy!

![](img/mongobrute.png)

## Foothold

Now it is time to move to SSH. Trying the admin credentials do not work, but mango's do:

![](img/mango-shell.png)

The flag is not readable by the mango user, so let's quickly try that admin password again, this time using `su`:

![](img/admin-shell.png)

Well that did it.

### User Flag

![](img/user-flag.png)

## User Enumeration

OK, now that we are admin, let's see what we are allowed to do.

Can't run sudo :(

I brought linpeas, pspy and linux exploit suggester over to enumerate the box.

Linpeas found an unusual SUID/SGID binary:

![](img/suid.png)

OK so I always check [GTFObins](https://gtfobins.github.io/) when I find an unusual SUID binary, and I got lucky because otherwise I have no idea what jjs is or how to use it.

I might be able to use this binary to spawn a root shell; failing that, it could be used to read a privileged file, like root.txt.

## Privilege Escalation

I went for the gold right away and tried to use the SUID commands from GTFO, but it resulted in an unresponsive shell, though it does appear to be a new /bin/sh. But I can't type anything into the terminal. So the other option is simply to read the root.txt file using the other commands:

    echo 'var BufferedReader = Java.type("java.io.BufferedReader");
    var FileReader = Java.type("java.io.FileReader");
    var br = new BufferedReader(new FileReader("/root/root.txt"));
    while ((line = br.readLine()) != null) { print(line); }' | jjs
    
Once I put all that in, I was able to display the root flag:

![](img/root-flag.png)

## Conclusion

A bit anti-climactic, but I guess I could have used the file write commands also found on GTFO to write my SSH key into the `/root/authorized_keys` file and then log in via SSH as root.

Actually, let's do that. The commands are [here](https://gtfobins.github.io/gtfobins/jjs/#file-write), and I just need to choose the correct location and input my public ssh key and I have a nice stable bonafide secure shell:

![](img/root-shell.png)

Steps taken:

* Discover mango login page
* Use NoSQL bruteforce exploit to enumerate users and extract passwords
* Gain access via SSH
* Escalate to higher priv user using credentials
* Escalate to root via SUID binary
