# DELIVERY | 10.10.10.222

## Enumeration

    sudo nmap -v -T4 -sCV -oN nmap-initial 10.10.10.222

### Ports and Services

![nmap scan](img/nmap-scan.png)

A more thorough scan revealed a service running on port 8065 as well.

#### 22 - SSH - OpenSSH 7.9p1

This may become useful later on if I stumble upon credentials.

#### 80 - HTTP - nginx 1.14.2

![main page](img/main-page.png)

There is a link to a subdomain (helpdesk.delivery.htb), so I'll need to add an entry to the `/etc/hosts` file before moving any further.

![helpdesk portal](img/helpdesk.png)

Clicking on the 'contact us' link on the main homepage has some info about accessing the 'MatterMost' portal (which is the service on port 8065), but only if I can get a valid '@delivery.htb' email address.

Running dirsearch on the helpdesk subdomain finds another login portal, this time it appears it's meant for helpdesk staff:

![helpdesk staff login](img/scp-login.png)

I opened a helpdesk ticket, and the interesting thing is I am given another email address once I do so. The email is my ticket number as username with the domain of '@delivery.htb'. I can use that email to sign up on the 'MatterMost' portal:

![ticket created](img/ticket-created.png)

## Exploitation via leaked credentials? 

Not really sure what to call this, as it is a bit convoluted in the sense that there is maybe some kind of misconfiguration. Doesn't seem that realistic to me, but what do I know?

### Exploit Method

Basically I will use that ticket-created email address to sign up for a MatterMost account, and then for some reason the confirmation email will get put into the help message. I honestly don't know why.

But here, I will sign up for MatterMost and see what comes back:

![link](img/confirmation-link.png)

So there is the link I can use to confirm my email. After pasting the URL into the browser I am in, and I am brought to the company chat:

![chat](img/chat.png)

Lots of good info in there. I have credentials to the staff login area (they do work), but I am more interested in getting on the machine, so let's try them on SSH:

![user shell](img/user-shell.png)

#### Flag

![user flag](img/user-flag.png)

## Privilege Escalation - maildeliverer > root

OK, so the chat message gave us some hints as to what I should be looking out for: hashes that I can crack. Looking at `/etc/passwd` shows me that there is probably MYSQL running on this box, so maybe I need to start there to find hashes.

![/etc/passwd](img/etc-passwd.png)

In the `/opt` folder is a mattermost directory with the binary and config files. I found what appear to be mysql credentials in a file named 'config.json':

![/opt/mattermost/config/config.json](img/config-json.png)

Time to use these creds to see if I can grab hashes out of the DB. Let's see what DB I am even looking for:

```bash
mysql -u mmuser -h 127.0.0.1 -p -e 'show databases;'
```

![databases](img/databases.png)

So in this instance I connected by typing `mysql -u mmuser -D mattermost -p`, and then I typed `show tables;` to see the list of tables:

![table listing](img/tables.png)

The command `select Email,Password from Users;` got me the info I think I am supposed to be looking for:

![hashes](img/hashes.png)

### Exploit method

OK so the chat message said I would have to use rules to find the correct password, but a pretty big hint that it is some variant of "PleaseSubscribe!" A good place to start in regards to hashcat rules is the "best64" rule. So I did that with the following command (after appending "PleaseSubscribe!" to the fasttrack wordlist):

```bash
hashcat -m 3200 hash ~/Documents/fasttrack.txt -r /usr/share/docs/hashcat/rules/best64.rule -O
```

And pretty quickly I had a hit:

![hash cracked](img/cracked.png)

All in all, a pretty simple variation. I tried to get in via SSH, but no dice. Instead, I just did a `su -` to escalate to root.

![root shell](img/root-shell.png)

#### root flag

![root flag](img/root-flag.png)

## Conclusion

The foothold was baffling to me, I still don't understand how that can happen unless you specifically set it up to do that. Like, it doesn't seem logical that sending an email would.... wait. There's a note from the box creator in the root dir:

![note.txt](img/note.png)

Holy shit. I stand corrected. It is actually possible to for this to happen, and it has, on several occasions. Mind blown.

The privesc was super easy once I figured out the correct mysql commands to view the hashes.

**Steps taken:**

* Use [TicketTrick](https://medium.com/intigriti/how-i-hacked-hundreds-of-companies-through-their-helpdesk-b7680ddc2d4c) exploit to gain access to internal chat app, get credentials
* Escalate to root by grabbing hashes from mysql database and cracking them.
