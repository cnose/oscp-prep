# CASCADE | 10.10.10.182

## Initial Enumeration

    sudo nmap -v -T4 -sV -sC -oN nmap-initial 10.10.10.182

### Open Ports and Services

* 53 - DOMAIN - Microsoft DNS 6.1.7601 (Windows Server 2008 R2 SP1)
* 88 - kerberos-sec
* 135 - MSRPC
* 139/445 - SMB
* 389 - LDAP
* 636 - LDAPS
* 3268 - LDAP
* 3269 - LDAPS
* 5985 - WINRM
* 49154 and up - MSRPC

It is a Domain Controller. Hostname cascade.local, CASC-DC1.

## Services Enumeration

### 135 - MSRPC

Running 'enum4linux-ng' revealed a wealth of information on this DC, returning a list of users and groups. I ran the list of users through kerbrute and 11 out of the 14 (not counting the Guest account) came back as valid, so I separated them into different lists.

### 53 - DOMAIN

I tried to do a zone transfer with dig but it wasn't having it. I ran nslookup and there was an interesting output:

![nslookup](img/nslookup.png)

Pinging that server (10.10.10.183) returns nothing so it's probably some artifact or whatever.

### 139/445 - SMB

There are probably loads of shares on the other side of this, but I don't have a valid login just yet. I'll have to return to this later.

### 389 etc - LDAP

Like running enum4linux, running 'windapsearch' gave a wealth of information on the domain. I wanted to see if there was more information on the users. And there turned out to be. In the dump of info I gathered, one user, 'r.thompson', had an item at the end of his listing that was not there for anyone else:

![legacy password](img/legacypwd.png)

That is simply base64 encoded, so that password turns out to be 'rY4n5eva' - yes really.

## Foothold via legacy credentials

I used crackmapexec to try to see how I could use the creds I now have. They work on SMB, LDAP. SMB is the more likely vector for grabbing more information.

### Back to SMB

    smbmap -u 'r.thompson' -p 'rY4n5eva' -d cascade.local -H cascade.local
    
This showed me that Ryan does not have write access to any shares, but he does have read access, and there is one notable share in the list:

![SMB shares](img/smbmap.png)

I connected to the share and grabbed all I could (`recurse ON, prompt OFF, mget *`) but it turned out I only had access to the 'IT' subfolder, was locked out of anywhere else.

![file tree](img/filetree.png)

Most of it was innocuous, a couple of references to temporary admin accounts (that has the same password as the real admin account), a log indicating the 'arksvc' is the service which controls the AD Recycle Bin (and that credentials from the TempAdmin account might be in there). In the dcdiag.log there is a reference to a failed SPN creation for the WSMAN service, which is the winrm port (5985).

I hit paydirt in the 's.smith' folder because there is a registry entry for a VNC server installation. There is an encoded password in there, and I remember that it is actually possible to decrypt these.

![vnc password hex](img/smithvncpass.png)

I grabbed a decoder from [here](https://github.com/trinitronx/vncpasswd.py) (python 2 warning) and ran it against the hex:

![vnc password plain](img/smithpass.png)

So now I have new credentials. Let's see what Steve can access.

He has access to a different SMB share, one called "Audit":

![Audit](img/smithauditread.png)

This folder has an exe called "CascAudit" which is obviously for auditing the domain, but it also has a DB file which probably has some sensitive info on it. I can use sqlitebrowser to look at it:

![sqlite browse](img/dbview.png)

Inside there is a table listing deleted users, and an LDAP table that includes perhaps encrypted/encoded creds ('BQO5l5Kj9MdErXx6Q6AGOw==') for the arksvc account - it looked like it was base64 but decoding it is just gibberish so I'm not sure about that. But the real great thing is that this user can log in via winrm!

![winrm pwn](img/smithwinrmpwn.png)

So let's do that so that we have a real shell on the machine.

![smith shell](img/smith-shell.png)

### Flag

![smith flag](img/smith-flag.png)

## System Enumeration

The usual suspects did not turn up anything - winpeas, WindowsEnum, and even bloodhound did not pull up much. That means that I need to go back and look at the share that this user was a part of. The information in the database is right there, I just need to figure out how to use it. The "CascAudit.exe" that is in the share could be how I move further. It is a 32-bit .NET executable, so unfortunately I can't debug it in gdb (I think). I had to spend over an hour figuring out how to install a working version of ILSpy, which can debug .NET files. I just didn't want to have to use Windows.

This program is limited however, as I can use it to see the decompiled code, but I won't be able to use it to add a breakpoint and run the function which will decrypt the password for me. But anyway, in the 'MainModule' of the program is the function to decrypt that base64 encrypted password. It uses a hardcoded string (which is 'c4scadek3y654321'), an IV ('1tdyjCbY1Ix49842', found in CascCrypto.dll) and the AES Mode (CBC). To be honest, in the ILSpy code there was no reference to the AES Mode (for me it said only 'Mode 1') so I grabbed that info from a writeup. In fact, this entire section is beyond me so I'm using a writeup.

This is what the MainModule looks like:

![MainModule](img/mainmodule.png)

And this is what the relevant part of CascCrypto.dll looks like for me:

![Decrypt function](img/decryptstring.png)

I can use a cool [web-based](https://gchq.github.io/CyberChef/) tool to combine different methods of encoding/decoding and encrytping/decrypting into "recipes". It is useful in this case because we have both base64 encoding and AES encryption.

In this case the "recipe" is taking a base64 string and decrypting it with AES using the provided string and IV and mode:

![CyberChef Recipe](img/recipe.png)

Then in the next pane I just input the base64 string and hit the "BAKE!" button (cute):

![Baked](img/output.png)

So now I have the plaintext of arksvc's password: 'w3lc0meFr31nd'. Time to see what this service account can access and do.

This user has similar SMB access as the r.thompson user:

    cme winrm -u arksvc -p w3lc0meFr31nd -d cascade.local cascade.local
    
![arksvc pwned](img/arkpwn.png)

### Enumerating arksvc

This user has an additional group membership, that of the "AD Recycle Bin" group:

![arksvc whoami /all](img/arkinfo.png)

Let's remember that there was a user called "TempAdmin" that had the same password as the real admin. This user was deleted, but as the arksvc user I may be able to recover the password.

After a little [research](https://adamtheautomator.com/active-directory-recycle-bin/) I found how to use this account to start to recover information about deleted objects:

    `Get-ADObject -Filter 'isDeleted -eq $true -and Name -like "*DEL:*"' –IncludeDeletedObjects`
    
At the bottom was an entry for the TempAdmin user:

![TempAdmin](img/tempadmin.png)

Appending `-Property *` to the end of that above command gives me all the info on all the deleted objects, and lo and behold there is another 'cascadeLegacyPwd' in there: YmFDVDNyMWFOMDBkbGVz.

![Legacy Password for TempAdmin](img/templegacypwd.png)

But let's remember that is in base64, so let's decode it:

    echo YmFDVDNyMWFOMDBkbGVz | base64 -d
    
And we have... baCT3r1aN00dles. Not a bad password I must say.

## Privilege Escalation via admin credentials in AD Recycle Bin

    cme smb -u administrator -p baCT3r1aN00dles -d cascade.local cascade.local --shares
    
![admin pwned smb](img/adminpwnedsmb.png)

I can dump all the hashes by using secretsdump now:

    secretsdump.py -dc-ip cascade.local -just-dc -history cascade.local/administrator@cascade.local
    
![hashdump](img/hashdump.png)

OK time to get a shell. I'm just going to use Evil-WinRM here:

    evil-winrm -i cascade.local -u administrator
    
![admin shell](img/admin-shell.png)

And for good measure here it is using psexec from Impacket:

    psexec.py administrator@cascade.local

![SYSTEM shell](img/root-shell.png)

### Flag

![root flag](img/root-flag.png)

## Conclusion

This was quite a challenge, and I can feel the effort the box creator put into this in order to make it feel as real as possible. Most of the other AD-style machines I've tackled were a bit lax in security, and were full of misconfigurations which made them easier to exploit. For this box, the users were very locked down and in the correct groups with the correct permissions. However, it still goes to show that even in those situations there can be lapses. In this case, it was a legacy password that was visible through LDAP that started it all, and a legacy password in a deleted user the finished it.

**Steps taken:**

* Enumerate users through RPC/Enum4linux/kerbrute
* Further enumerate users via LDAP with windapsearch, find base64 encoded user password, gain foothold
* Enumerate SMB to find encrypted VNC user password, decrypt it as key is known, move laterally
* Further enumerate SMB to find sqlite3 db with encoded & encrypted password
* Decompile and debug "CascAudit.exe" program to extract info about it's decrypt function, decrypt password, move laterally
* Use new privs to recover information about a deleted user that shared password as administrator, privesc with password


