# ARCTIC | 10.10.10.11

I already completed this box as a part of the [Windows Privilege Escalation For Beginners Course on Udemy](https://www.udemy.com/share/102YD8B0sYeVlXRQ==/).

The writeup can be found [here](https://gitlab.com/cnose/wpe-capstone/-/tree/master/1-ARCTIC)
