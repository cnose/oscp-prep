# BEEP | 10.10.10.7

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.7

### Open Ports and Their Services

* 22 - SSH - OpenSSH 4.3
* 25 - SMTP - Postfix smtpd
* 80 - HTTP - Apache 2.2.3
* 110 - POP3 - Cyrus pop3d 2.3.7
* 111 - RPCBIND
* 143 - IMAP - Cyrus imapd 2.3.7
* 443 - HTTPS
* 993 - IMAPS - Cyrus imapd
* 995 - POP3 - Cyrus pop3d
* 3306 - MYSQL
* 4445 - upnotifyd?
* 10000 - HTTPS - MiniServ 1.570 webmin

## Service Enumeration

I ran the nmap vuln script on all the ports above and there were a whole slew of results. This machine uses very outdated versions of the services running. Even SSH has several vulnerabilities (though most of them are DOS attacks).

Even though are are several vectors I bet many of them are too sophisticated for me to pull off properly (the SSL attacks especially), so my bet is that I need to exploit something more simple to gain initial access.

### 22 - SSH

![](img/ssh.png)

This is pretty much an ancient version of SSH that has several vulnerabilities. One of them is a local privesc involving hijacking another user's X-session. Searchsploit has username enumeration and an authenticated command execution vuln, so there is nothing we can feasibly use here to gain access unless we have creds.

### 25 - SMTP

This mailserver could be vulnerable to username enumeration via the VRFY flag, but that is not the lowest hanging fruit, so we'll let it be for now.

### 80 - HTTP

The webpage is a login screen, powered by Elastix. There are a few potential vulnerabilities there, depending on what version is running (unknown at this time):

![](img/elastix-ss.png)

The webserver running on the port, Apache 2.2.3, is out of date and also has several potential vulnerabilities that could be exploitable:

![](img/ss-port-80.png)

Running gobuster on this address gives back a ton of accessible directories. Like, a lot. A few of them have login screens and correspond with the web servers that are running on this machine. I'm sure most of these are red herrings and/or rabbit holes, but at this point I am not sure where to to start.

![](img/gobuster-80.png)

Update: even though these directories are accessible, the ones that did not have login screens were basically empty. I hunted for config files but none of them loaded.

### 110/143/993/995 - Mail servers

These are probably not the attack vector, but there are login portals for these that I can try to bruteforce in the browser if it comes to that.

### 111 - RPCBIND

I tried to login with a null session but it didn't work, so I'm moving on.

### 4445 - ??

Legit have no idea what the hell is running on there, a google search just brings up UPNOTIFYP, so I'm gonna pass on this one.

### 10000 - MINISERV 1.570 WebMin

My gut says this is the easiest way in. Either this one or the Elastix on port 80.

Unfortunately searchsploit only shows a RCE from Metasploit for this exact version, but I have found one that is in the range (version 1.5), so I have copied that to my working directory, along with two potentials for the elastix page.

## Exploitation

### Elastix RCE.py

This looks to be a pretty simple Remote Command Execution exploit, for this version of Elastix. It executes a perl reverse shell back to my machine - I just need to fill in the correct info:

![](img/elastix-rce.png)

So, let's spin up a listener on port 6969 and run this thing:

    rlwrap nc -nvlp 6969

Wow, so this is funny. The version of SSL used is so old it is no longer supported by python, so I am unable to use this particular exploit.

I even tried just making the request myself without the additional layer of python, and I was not able to get a connection back, so looks like this route is out.

### Elastix LFI.pl

Annoyingly, this exploit ran, but said it was not successful. So I just grabbed the URL that was inside the script and pasted it into my browser (replacing the actual address) and I think I hit paydirt:

![](img/LFI.png)

There is a password in there, and I was able to use it to login to the FreePBX panel:

![](img/pbxpanel.png)

And the Elastix panel:

![](img/elastix-panel.png)

And the vtiger CRM:

![](img/vtiger-crm.png)

As well as the portal for monitoring calls/voicemail:

![](img/callmonitor.png)

The only others that I could not get into were the webmail and the Webmin on port 10000 (in fact I was locked out!)

And there might have been an additional credential on that page I accessed with the LFI exploit: 'passw0rd'.

### SSH

So, what can I do with these credentials besides what I've done already? I guess I can try logging into SSH with it:

Actually I can't do that as the SSH server is unable to use or send the correct key exchange method:

![](img/ssh-exchange.png)

Yikes. I'm scratching my head here. So I went searching for a way to connect via SSH, which is the only way that I know of to get a shell in this situation.  
That led me to [here](https://www.openssh.com/legacy.html), which has as an example one of the key exchange algorithms I am working with.

So I was able to log in as the root user and own this box by typing:

    ssh -v -oKexAlgorithms=+diffie-hellman-group1-sha1 root@10.10.10.7  

And I was greeted with the root shell:

![](img/root-shell.png)

### Flags

![](img/user-flag.png)
![](img/root-flag.png)

## Conclusion

Actually a difficult box for me, due to the versions of things being so out of date that my modern machine was complaining about them, and the fact that there was so much crap to sift through in regards to directories and services and files. But I learned to just be methodical and patient, as well as to look for ways to deal with compatibility issues so that I could get the job done. 

Steps taken:

* Enumerate all web services and try potential exploits until a Local File Inclusion exploit worked, which disclosed some credentials
* Use the credentials to log into the machine as the root user
* Learn about legacy SSH servers and the old timey key exchange algorithms they use.
