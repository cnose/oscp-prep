# SAUNA | 10.10.10.175

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.175

### Ports and Services

#### 53 - DOMAIN - Simple DNS Plus

I can check for other domains using dig, but I will move on for now and come back if I am stuck. 

#### 80 - HTTP - Microsoft IIS 10.0

This is the website of an awful bank (all banks are awful).

![port 80](img/port-80.png)

I'm running gobuster on this while I move on.

#### 88 - KERBEROS

Looks like this is also a Domain Controller, which means I get to use Active Directory attacks.

I can use a tool called kerbrute to enumerate users/passwords/etc, but I will go through other less intrusive options first.

#### 135 - MSRPC

I can connect with a null session, but could not find a command I could run. I prefer to let 'enum4linux-ng' do this kind of thing anyway. In this case, RPC seems locked up tight.

#### 139/445 - SMB

Since this is a DC, SMB signing is in effect. Trying a null session gets me nowhere. Of course I'll come back to this if I get some creds.

#### 389/636/3268/3269 - LDAP

I had luck on a previous box while enumerating LDAP. Since so far I have been striking out with other methods, I thought I would get something from this, but no - nothing came of it.

#### 5895 - winrm

Nice to know we have a Winrm service running. If I could just get some usernames that would help a lot.

### Usernames

From the `/about.html` page, I did grab a theoretical group of users, and it appears that the maker has at least put effort in that portion of the site since there is a joke about only having one security manager:

![Meet the team](img/team.png)

I compiled a list from these names, using two of the most common formats for usernames:

![userlist](img/userlist.png)

#### Kerbrute

With a potential list of valid users, I can try to find out which are actually real:

    kerbrute userenum --dc egotistical-bank.local -d egotistical-bank.local -v userlist
    
Right away I got a hit, and a surprise that the valid user does not have pre-auth required, so I got a hash as well:

![kerbrute](img/kerbrute.png)

However, the last time I used this tool the hash did not work for me in hashcat. Luckily I can grab it using another tool called 'GetNPUsers.py' by Impacket.

    GetNPUsers.py -no-pass -dc-ip egotistical-bank.local -request -format hashcat egotistical-bank.local/fsmith
    
This results in a different hash, but it is of a type I have successfully loaded into hashcat before. So now I log into my cracking box and run the proper hashcat command to try to crack this with rockyou.txt:

    hashcat -m 18200 hash ~/Documents/rockyou.txt
    
In about one second it has found the password - 'Thestrokes23':

![cracked password](img/cracked.png)

### Enumerating with fresh creds

I used 'enum4linux-ng' again and this time I received much more information like users, groups, password policies, and SMB shares.

![Enum4linux output](img/enumusers.png)

Now I have two more users to enumerate - hsmith and a service account - svc_loanmgr.

Running the 'GetNPUsers.py' with the new user list did not garner anything, but the other command - 'GetUserSPNs.py' (which is for getting service tickets) - did get something: a TGS hash for hsmith:

![hsmith hash](img/hsmithhash.png)

So I cracked that hash over on my cracker, and guess what? It is the exact same password as fsmith: Thestrokes23.

But it turns out the password is expired, according to output from crackmapexec:

![password expired](img/expired.png)

### Foothold via Evil-WinRM

Before I take the time to puzzle about hsmith's expired password, I should note that I can get a shell as fsmith through winRM, as demonstrated by this crackmapexec output:

![fsmith pwned](img/fsmith-pwn.png)

![fsmith shell](img/fsmith-shell.png)

And here is the flag:

![user flag](img/user-flag.png)

## Privilege Escalation - fsmith > svc_loanmgr

Running winpeas on the box found autologon credentials:

![Autologon creds](img/loanmgr-pw.png)

But my eagle eyes see that the default username is different from the one we found already - loanmanager vs loanmgr - so before I get too excited I should see if the password works at all.

![svc_loanmgr pwn](img/loanmgr-pwn.png)

That was pretty straightforward.

## Privilege Escalation - svc_loanmgr > Administrator

Using winpeas with this user did not net me any new information. In fact, using the typical Windows enumeration scripts and techniques, I could not really find any difference between the two accounts I have owned. Since this is a Domain Controller, it is time to spend more effort enumerating that side of things.

### Domain Enumeration with Bloodhound

This tool is awesome. It uses visuals to show you the relationships between different parts of the domain. When you can look at it all from above like this, things tend to become more clear. So let's get to it.

I uploaded the ingestor (SharpHound.exe) using evil-WinRMs built-in upload function (a godsend when it works), and ran the following command to grab as much info as I could about the domain:

    ./sh.exe -c All -d egotistical-bank.local --ldapusername fsmith --ldappassword Thestrokes23
    
That puts everything in a zip file, so I just downloaded that file to my machine using the download function, then dragged n dropped it into bloodhound.

The first thing I do when I load up a new analysis is I search for the users I have owned, and mark them as such.

Then I run through some of the top queries to see if there are easy wins. There are not - most of the queries turn up empty. I clicked on the 'Node Info' for the svc_loanmgr user and looked more closely. On the section called 'Outbound Control Rights' I clicked on the heading 'First Degree Object Control':

![First degree object control](img/objectcontrol.png)

This user has two privileges over the domain that, taken together, can allow me to own the whole thing. 

![GetChanges](img/getchanges.png)

With both 'GetChanges' and 'GetChangesAll', I am able to use this user's creds to perform a DCSync attack using the Impacket tool called 'secretsdump' - I can grab all the hashes for all users (including the Administrator), and then can either crack the hashes, or just pass the hashes to privesc.

### secretsdump

There is another way to do this DCSync attack, using mimikatz, but I prefer the impacket tools as they are simpler and can be run remotely:

    secretsdump.py -dc-ip 10.10.10.175 -just-dc egotistical-bank.local/svc_loanmgr@egotistical-bank.local
    
Now I have the keys to the kingdom:

![hashdump](img/hashdump.png)

For the record, I tried cracking the Admin hash with hashcat, the rockyou wordlist, and different rules, but I was not able to find the password. Oh well. At least I am still able to do a pass the hash attack to win.

![admin shell](img/admin-shell.png)

And here is the prize:

![root flag](img/root-flag.png)

## Conclusion

Another fun Active Directory box. I really like doing these ones, and since most of the world out there uses AD, the fact that I enjoy these is a good sign. I like how I could not have moved forward without bloodhound, but that it also didn't lay out the red carpet for the privesc - you still had to pay attention and look at everything.

**Steps taken:**

* Find potential usernames on website
* Use GetNPUsers to find user with pre-auth disabled, get hash, crack hash, login as user
* Discover autologon creds for service account, login as service account
* Use Bloodhound to discover service account effectively has DCSync rights
* Use secretsdump to grab Administrator hash
* Use pass the hash attack to login as Administrator

