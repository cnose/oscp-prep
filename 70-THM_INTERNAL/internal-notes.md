# INTERNAL

> TryHackMe

## nmap scan results

* 22 - SSH - OpenSSH 7.6p1 Ubuntu
* 80 - HTTP - Apache 2.4.29

![port 80 default web page](img/port-80-default.png)

## gobuster scan results

    gobuster dir -u http://internal.thm -t 50 -x php,html,txt -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories-lowercase.txt | tee gobuster.log

![directories found](img/gobuster.png)

## wpscan results

* Outdated version of Wordpress
* No vulnerable plugins found
* One user only - admin

I did an additional scan to try to bruteforce the password using the rockyou wordlist. Often this is not possible or practical as there are some configurations that only allow a few incorrect login attempts before blocking the IP or locking out the account.

    wpscan --url http://internal.thm/blog -t 25 -U admin -P /home/user/dox/wordlists/rockyou.txt

![Wordpress admin creds found](img/wpadmin-password.png)

![Dashboard Access](img/dashboard.png)

The admin has a hidden post containing credentials, but admin is the only user. Could try them on the phpmyadmin page, but the format for username is '<name>@localhost' so it's not likely.

![More creds found](img/will-creds.png)

![Access Denied](img/denied.png)

The wpscan results noted the theme (twentyseventeen), and themes tend to be php code and can be modified. Replacing the php code in the 404.php page with code for a reverse shell can work.

![php reverse shell](img/php-shell.png)

## Exploitation - upload of reverse shell

After setting up a netcat listener, I curl the 404.php page location to get a connection:

```bash
curl http://internal.thm/blog/wp-content/themes/twentyseventeen/404.php
```
![shell access](img/whoami.png)

## Enumeration

There is one regular user, and also a mysql user:

![cat /etc/passwd](img/users.png)

As the www-data user, I cannot enumerate the user's home directory as the permissions are set well.

I check the 'wp-config.php' file in the `/var/www/html/wordpress` dir to find the DB credentials:

![MYSQL db creds](img/db-creds.png)

These creds do allow access to phpmyadmin:

![phpmyadmin dashboard](img/phpmyadmin.png)

### linpeas results

* there are a few things running on localhost, internally accessible (one is the mysql):

![netstat](img/netstat.png)

* aubreanna is in the 'adm' group, which means she can read logs:

![aubreanna group memberships](img/aubreanna-groups.png)

* aubreanna is also running a jenkins instance, perhaps on port 8080:

![jenkins](img/jenkins.png)

In the `/opt` folder is a text file with aubreanna's credentials:

![aubreanna creds](img/wpsave.png)

I am able to login via SSH for a more stable session:

![aubreanna shell](img/aubreanna-shell.png)

A text file in the user's home folder confirms that a jenkins instance is running on port 8080, but on a different IP which looks like a docker instance, installed via snap.

![jenkins txt](img/jenkins-txt.png)

This can be accessed using an SSH tunnel with the following command:

```bash
ssh -L 8080:172.17.0.2:8080 aubreanna@internal.thm
```

Navigating to `127.0.0.1:8080` brings me to the local Jenkins instance.

### Bruteforcing with Hydra

Hydra is a login bruteforcing tool that I learned can give false negatives if you don't throttle the threads from 16 (down to 6 worked for me).

```bash
hydra 127.0.0.1 -s 9001 -f -V http-post-form "/j_acegi_security_check:j_username=admin&j_password=^PASS^&from=%2F&Submit=Sign+in:Invalid username or password" -l admin -P /home/user/dox/wordlists/rockyou.txt -I -t 6
```

That got me the admin credentials for the Jenkins instance.

![jenkins creds](img/jenkins-creds.png)

![jenkins dashboard](img/jenkins-dashboard.png)

Using the script console I am able to use a small command to get a reverse shell connection:

![Groovy reverse shell](img/groovyshell.png)

![shell in a docker instance](img/docker.png)

I am now the jenkins user, but within the docker instance.

Luckily, they used the same technique to hide creds on the docker system, in `/opt`:

![root creds](img/root-creds.png)

### privesc

I can use these credentials to login as root via SSH:

![root shell](img/root-shell.png)

## Recommendations

* Client needs to re-think their practice of leaving credentials in plaintext on their production machine.
* Additionally, client should have an IP or account lockout policy for incorrect password attempts in order to deter bruteforcing. They should institute this on both the external wordpress instance and the internal Jenkins instance.
* Webapp passwords should be more complex and contain more characters. Avoid dictionary words, unless it is a passphrase.
