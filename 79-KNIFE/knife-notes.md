# KNIFE |

## Enumeration

```bash
sudo nmap -v -T4 -sCV -oN nmap-initial <IP>
sudo nmap -v -T4 -sUV --top-ports=20 -oN nmap-udp <IP>
```

### Ports and Services

![nmap scan](img/Pasted%20image%2020210522161005.png)

#### 22 - SSH - OpenSSH 8.2p1 Ubuntu

Nothing to see here just yet.

#### 80 - HTTP - Apache 2.4.41

![homepage port 80](img/Pasted%20image%2020210522121216.png)

A website for a medical company or a hospital I guess. There are no links on the webpage at all, and viewing the source does not show anything of note, except perhaps for this mention of CSS injection:

![CSS Injection](img/Pasted%20image%2020210522124344.png)

Or is it this???

![window.console](img/Pasted%20image%2020210522134607.png)


## Foothold via backdoored PHP

Nope! It is none of those things! It is an imaginary scenario in which PHP itself was backdoored. This nearly happened but some eagle-eyed people on the repo [caught it in time](https://www.bleepingcomputer.com/news/security/phps-git-server-hacked-to-add-backdoors-to-php-source-code/).

But somehow we were supposed to imagine that it did happen and there are websites out there using the backdoored version that was never released. Basically if you use BurpSuite and add a misspelled "User-Agentt:" to the web request (or I guess you could use curl), as long as it begins with "zerodium", you can execute arbitrary commands on the webserver.

### Exploit Method

Here is me running the command 'id':

![RCE in backdoored PHP](img/Pasted%20image%2020210522143941.png)

Let's try getting a reverse shell. I had to go down a long list of different types of rev shell to get it working, eventually the Netcat OpenBSD one worked for me:

![Reverse shell command](img/Pasted%20image%2020210522153232.png)

![user shell](img/Pasted%20image%2020210522153310.png)

#### user flag

![user flag](img/Pasted%20image%2020210522153404.png)

## Privilege Escalation - james > root

There is one other user besides root with a shell, but they don't appear to be a real user, so I'll be looking for how to escalate to root.

### Enumeration

**sudo -l**  
![sudo -l](img/Pasted%20image%2020210522153805.png)

Let's have a look at this knife binary with `file /usr/bin/knife`:

![file /usr/bin/knife](img/Pasted%20image%2020210522153929.png)

OK so it's a symlink to something in the `/opt/` folder. I'll just run it asking for help - `./knife --help`. There are a lot of things in here but I am looking for an easy win, some way to execute commands:

![knife exec](img/Pasted%20image%2020210522154312.png)

### Exploit method

From looking up the documentation of this tool, it uses ruby, so I thought I needed to try to get a reverse shell using ruby. That was not working so I thought to check how to get ruby to execute commands, and it is simple: `ruby -e 'exec "/bin/bash"'`. So I ran the knife binary as root and tried this:

![root shell](img/Pasted%20image%2020210522160046.png)

#### root flag

![root flag](img/Pasted%20image%2020210522160303.png)

## Conclusion

I didn't really enjoy this one at all. I suppose you could argue it is like real life because there is a parallel universe in which nobody noticed that PHP was backdoored, and then everyone would have a grand old time getting into tons of websites or whatever, but maybe I'm just making excuses for not being quick about this one. The privesc took a few minutes of reading documentation and then looking up how to execute bash in ruby. I guess this was an easy but frustrating box.

**Steps taken:**

* Find nothing at all on website (let this be a lesson for later), google "PHP exploit" until finding info about potential backdoor
* Make a guess that the page was running an unreleased version with the backdoor, get code execution and reverse shell
* Escalate to root using third party app running as root to execute bash in a ruby-like console.
