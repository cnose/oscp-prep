# BRAINFUCK | 10.10.10.17

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.17
    
### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.2p2 Ubuntu
* 25 - SMTP - postfix smtpd
* 110 - POP3 - Dovecot pop3d
* 143 - IMAP - Dovecot impad
* 443 - HTTPS - nginx 1.10.0

## Services Enumeration

### 22 - OpenSSH

I'll leave this until I need it later.

### 25 - SMTP

A mail server. Now I gotta figure out the best way to connect to it. The nmap scan suggested I could enumerate usernames using this.

### 110/143 - POP3/IMAP

Part of the mailserver which I am sure I'll have to come back to.

### 443 - HTTPS - nginx 1.10.0

I noticed two domain names for the IP address, so I added them to my `/etc/hosts` file, since just going to the IP address brings me to a default nginx page.

![](img/etc-hosts.png)

#### Wordpress

The domain 'brainfuck.htb' is a wordpress site, so after running gobuster on it I'll also do a wpscan.

![](img/wordpress.png)

That blog post there gives me an idea of the username I'm looking to attack.

The wpscan finds a vulnerable plugin called 'WP Support Plus', and searchsploit has a few potentials for attacking it - the one I want the is privesc, which lets me log in as any user without knowing the password:

![](img/plugin-vulns.png)

It is an exploit that I host locally, but it makes a POST request and you just tell it what user you are and it gives you the auth_cookie.

![](img/wp-privesc.png)

Then, I refresh the wordpress page and I am logged in as admin:

![](img/wp-admin.png)

A quick way to a shell is if as admin you can modify the php of any files on the site - but the php files of the themes are not writable, so there must be another way to gain a foothold.

There is an email plugin on the system called "Easy SMTP" and on the config page, there is the user's (orestis) email password, obfuscated in the usual way. But I can unmask that by opening the dev console and the plaintext password is right there:

![](img/wp-creds.png)

#### Email access

Now I am able to access the user's email account, and I find more credentials in one of the emails, this time for the 'sup3rs3cr3t' forum that is on the other domain:

![](img/forum-creds.png)

#### Forum Access

Before logging in, there is only one visible forum post here:

![](img/forum.png)

Logging in reveals secret posts:

![](img/secret-posts.png)

This user is a douche and kind of an asshole, and has a real obnoxious forum signature, but it will be helpful in moving forward.

![](img/scrambled.png)

The scrambled text from the user's posts have a common text at the end of them, which is obviously the user's inane forum sig; I can see it changes from post to post, which means we're dealing with a kinda complicated cipher.

This was above my paygrade, and I had steered clear of any boxes that use cryptography so far, so I am taking this as a learning experience and I will hold on to the tools and resources and methods I learn here. I went to [this website](http://rumkin.com/tools/cipher/) and learned that the cipher used is a "Keyed Vigenere", but that I needed the key first. I used the "One Time Pad" on that website to figure out the key. You do that by putting in the scrambled text with the corresponding plaintext, and a key emerges:

![](img/key.png)

So the key is 'fuckmybrain' - nice. Now I am able to decode the URL in the admin message, which I could tell was a SSH private key:

![](img/ssh-key.png)


It has a passphrase, but I am able to crack that in john, after converting the key to a hash that john can read:

![](img/passphrase.png)

### Foothold via SSH

So, finally, we have usable creds to gain a foothold on the system:

![](img/user-shell.png)

### Flag

![](img/user-flag.png)

## User Enumeration

There are a number of interesting files in the user's home directory:

![](img/orestis-dir.png)

**encrypt.sage**

![](img/encrypt.png)

Some kind of 'sage' script that takes the root.txt file and then does some magic to scramble it, and then writes it to 'output.txt', as well as writing what to me looks like random numbers to 'debug.txt'.

**output.txt**

![](img/output.png)

This is the contents of root.txt, encrypted somehow. Obviously I need to decrypt it to get the hash.

**debug.txt**

![](img/debug.png)

I copied this over to my machine, so it is easier to see that this is three separate numbers.

Taken all together, it is now clear that I could have given this a good college try. But I was too intimidated and sought guidance. Going back to the sage file, we see a few variables, which are the result of complicated maths that I will probably never understand: p,q,n and e. With these I could've google searched one of the fancy maths expressions and I would have discovered this was RSA encryption. Then breadcrumbs lead one to find a [usable decryption algorithm](https://crypto.stackexchange.com/questions/19444/rsa-given-q-p-and-e). With this fancy math script thing, I should be able to decrypt the code.

## "Privilege Escalation" via RSA decryption attack

I need to replace some things in the script, namely inputting the numbers I have been given in the correct spots. 'ct' in the script is the actual encrypted root.txt contents that are in 'output.txt'.

Running it gives me another slew of digits:

![](img/decrypt.png)

pt means 'plaintext', and I would have had no idea what to do here if not for Ippsec's video. I can convert this number to hex, and then decode it, after that I will have the root flag contents:

![](img/hextoascii.png)

And that is the flag. It is not really a privilege escalation though. But there is another way, and we can get more root out of it.

## Real Privilege Escalation via lxd Group Membership

I like this one more because you can use it to get a real root shell on the system. This is one I will want to keep up my sleeve because I swear I have already done boxes in which the unprivileged user is in the lxd group.

[This page](https://www.hackingarticles.in/lxd-privilege-escalation/) is a great explainer on how to pull this off. It's pretty easy, and basically involves you using the user's lxd privs to launch a linux container with a lightweight and basic OS (in this case alpine linux), then mount the root dir of the host OS, and give yourself full privileges. Once that is done, enter the container via a shell, and you have full root privileges over the mounted filesystem. From there, you can simply read the flag, or even better you can copy bash to a temp directory and make it SUID. Then exit the container and execute that copy of bash and you are root. So here are the steps.

First, on your own machine, clone the apline linux container builder repo:

    git clone https://github.com/saghul/lxd-alpine-builder.git
    
Then, execute the builder script and you'll end up with a compressed archive:

    sudo ./build-alpine
    
![](img/archive.png)

Next, transfer the .gz file to the victim machine and then use these commands to import, initialize and setup and run the container:

    lxc image import ./<tar.gz file> --alias <give it an alias>
    lxc init <alias> <another name> -c security.privileged=true
    lxc config device add <name> <choose a disk name> disk source=/ path=/mnt/root recursive=true
    lxc start <name>
    lxc exec <name> /bin/sh
    
You will see that you are now root, but of the container you created, not of the brainfuck machine:

![](img/lxd-root.png)

But that doesn't really matter, because you mounted the brainfuck machine's root directory and have full privs over it. But instead of simply reading the flag, I am going to give myself a real root shell on the brainfuck machine.

    cd /mnt/root
    cp bin/bash tmp/bash && chmod +s tmp/bash
    
Notice that I do not have a slash before bin or tmp, because I am in `/mnt/root` of brainfuck via the container. If I use the leading slash my user will copy bash from the container, which I don't want to do. Then I make the copy of bash SUID. After that I can exit the container by typing `exit`.

Here is the SUID bash in the `/tmp` dir of brainfuck:

![](img/bash.png)

And now I just need to `/tmp/bash -p` (the -p preserves the SUID perms):

![](img/root-shell.png)

And the flag is mine:

![](img/root-flag.png)

## Conclusion

At the time of the release of this box, it was rated 'Insane' difficulty by Hack the Box, but apparently nowadays it is considered moderate difficulty. For me that is a bit sobering, but I have a few boxes under my belt now, and I have been documenting as much as I can, so hopefully I can apply myself to use what I have learned so that I can do that thing that the OSCP people say ad nauseum: "try harder". Once I am completely done with the series of boxes I have to do, I want to see if I can actually do some of the newer ones, ones that have no available writeups.

Steps taken:

* Enumerate wordpress instance to find vulnerable plugin
* Log in as admin via plugin exploit
* Unmask email password in SMTP plugin options page
* Find forum credentials in compromised email account
* Decode encrypted chat to find URL to SSH private key
* Use RSA decrypting attack to read contents of root.txt, or
* Escalate privileges via lxd group membership
