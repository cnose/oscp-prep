# FRIENDZONE | 10.10.10.123

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.123
    
### Open Ports and Their Services

* 21 - FTP - vsftpd 3.0.3
* 22 - SSH - OpenSSH 7.6p1 Ubuntu
* 53 - DOMAIN - ISC BIND 9.11.3
* 80 - HTTP - Apache 2.4.29 Ubuntu
* 139/445 - SMB - Samba 4.7.6 Ubuntu
* 443 - HTTPS

## Services Enumeration

### 21 - vsftpd 3.0.3

This appears to be a non-vulnerable version of vsftpd, as all the way up to 3.0.2 there was a security bypass vuln. Additionally, version 2.3.4 was famous for briefly containing a backdoor. This version, however, does not seem to be vulnerable to anything; anonymous login is also disabled.

### 22 - OpenSSH 7.6p1

It is what looks to be a standard SSH setup; I'll need valid creds to use this service.

### 53 - ISC Bind 9.11.3

This DNS service is vulnerable to DOS but that is not a way in to the system since it just stops the service from working. Moving on...

### 80 - Apache 2.4.29

This looks to be a webpage for some kind of escape room software? Not really sure. Running a gobuster query finds a /wordpress and robots.txt file, but they are there for trolling purposes mostly.

### 443 - Apache 2.4.29 ssl

Here is where it gets a bit interesting. The nmap scan says 'not found' when scanning the IP, but with the scan we get domain name info. So after adding 'friendzone.red' to my /etc/hosts file I am able to connect to a page by going to that text URL. When I check the source I get a boring CTF-like clue:

![](img/fz-source.png)

So I follow that and I find a brief message with some alphanumeric characters:

![](img/js-dir.png)

I don't think it is a hash (`hashid` came up empty) so maybe it is a password, so I'll save it for later. Let's move on to SMB.

### 139/445 - Samba 4.7.6

Running `smbmap -h 10.10.10.123` shows me there are some readable (and writable) shares:

![](img/smbmap.png)

Using `smbclient` to connect and navigate, I grab the `creds.txt` file that is just sitting there in the \\general directory, and confirm that yes the \\Development share is indeed writable. The creds file has creds!

![](img/creds.png)

### Back to 53

Since trying the creds on FTP, SSH and SMB didn't work I had to go back to doing DNS stuff, which I am pretty weak at. I looked up how to do basic DNS enumeration and used the `host` command to do so.
    
    host -t axfr friendzone.red 10.10.10.123
    
![](img/host.png)

These are the kind of results I need. Let's try them all! (I had to add the subdomains to the hosts file entry, and then had to make sure I was browsing via https and then click through the warnings to get through)

`uploads.friendzone.red` has a simple uploader that says you can upload only images, but I tried both an image and then a php file and they both resulted in very similar success messages. Can't seem to find where the files went though.

![](img/uploads.png)

`hr.friendzone.red` was a 404 on port 443 and was the original homepage on port 80.

`administrator1.friendzone.red` gave me a login page, so I used the creds on the login form:

![](img/login.png)

So I went to `dashboard.php` and this is what was there:

![](img/dashboard.png)

Hmmm. Let's follow exactly what it says there and see what we get. So I add `?image_id=a.jpg&pagename=timestamp` to the URL and I get another annoying troll back:

![](img/nelson.png)

And if I check the `/images` dir I do not see any of the files I uploaded there. In the response we see a timestamp which changes each time, implying there is a script running. So that could mean we might have a 'local file inclusion' vuln here, as I may be able to get the page to run a malicious reverse shell script. The only place where I know I can upload is the 'Development' share over SMB, and I guess the path is `/etc/Development` as there is a clue in the SMB enumeration.

Let's see if I can get a reverse shell this way.

## Exploitation

First, I upload the php reverse shell I modified previously:

![](img/smb-shell.png)

Next I need to make sure I have a netcat listener running on my end:

    nc -nvvlp 6969
    
And finally, I need to point the dashboard.php page towards my shell by modifying the URL:

    https://administrator1.friendzone.red/dashboard.php?image_id=b.jpg&pagename=/etc/Development/rev.php
    
Nothing happened. Finally I realized the page was adding `.php` to the end on it's own, so I modified my modification by omitting the extension:

![](img/www-data-shell.png)

It worked!

### User Flag

I was able to read the flag even though I am only the www-data user:

![](img/user-flag.png)

## User Enumeration

OK, time to see what kind of damage I can do as the 'www-data' user. I wanted to find out where the SMB shares lived, and I did find `general` and `Development` in `/etc` as I expected, but `Files` was not there. Maybe it is in the `/root` directory? 

While poking around I found a real basic python script at `/opt/server_admin/reporter.py` - it is unfinished, can be run by root and calls 'os.system' which if I could modify and run it could call a root shell for me. I have a feeling I'll be coming back to it once I gather more info.

Linpeas picks up that there is a mail server running locally on port 25, as well as something on port 953. I'll have to find out what that is.

Additionally, root login is allowed over SSH, which is a security concern and maybe my route to owning the box if I can get creds.

The 'files' share I was looking for might be `/etc/sambafiles` which was also found by linpeas.

And finally, there is a potentially exploitable SUID binary in `/usr/sbin/exim4` to check out.

Running pspy shows me that exim4 is indeed running:

![](img/exim4.png)

As well as a cron for that 'reporter.py' script I found previously:

![](img/cron-reporter.png)

Since linpeas didn't go into enough detail with some files I checked out everything in the www folder - there are several directories. I found a password in the `mysql_data.conf` file - 'Agpyu12!0.213$' - which is for the user friend, so might as well try that one out:

![](img/friend-shell.png)

And now I am 'friend'. Let's continue with enumeration under this user.

I cannot run `sudo` on the machine, so there's that. I checked if the password works for SSH and it does, no pubkey needed.

As this user I still cannot modify the reporter python script - the only thing I can think to do is to create a malicious 'os' module, as the script calls that. Maybe that's the privesc? After looking back through my linpeas output (I ran it again as the new user) I do see that 'os.py' is _writable_ by me. So this must be the way!

## Privilege Escalation

### Python Library Hijacking

Since I can write to the python os module that is located at `/usr/lib/python2.7`, I can modify it to call a reverse shell, or to copy bash to a temporary directory and give it an SUID of 0. It is a real long file so I will copy the original first and work on the copy.

So I decide to just append a reverse shell command at the bottom of the module since I don't know if everything will break if I delete anything from it (probably yes). Search for "Payloadsallthethings python reverse shell" to find a suitable command to append.

Then I spin up another netcat listener and hope for the best:

![](img/root-shell.png)

And it worked! Pretty cool!

### Flag

![](img/root-flag.png)

## Conclusion

I like that I keep learning with each box. With this one I required some help and did take some research shortcuts, to cut down on the trial and error. But learning another LFI exploit was cool, and it was nice to have my memory refreshed regarding [python library hijacks](https://rastating.github.io/privilege-escalation-via-python-library-hijacking/). I was daunted at first until I realized I could just append the command at the bottom.

Steps taken:

* Through SMB enumeration, find creds
* Through DNS enumeration, find admin login page, use creds
* Through vigorous enumeration, discover LFI vuln and upload & execute php reverse shell
* Through enumeration, find creds in a config file, they are reused for the valid user
* Through enumeration find a writable python library, add reverse shell code to the end to get a root shell.

Enumeration, enumeration, enumeration!
