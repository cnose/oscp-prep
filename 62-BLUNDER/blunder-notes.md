# BLUNDER | 10.10.10.191

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.191
    
### Ports and Services

* 80 - HTTP - Apache 2.4.41 Ubuntu

The homepage looks like a personal blog:

![homepage](img/port-80.png)

I used gobuster on the address to search for other directories:

    gobuster dir -u http://10.10.10.191 -t 100 -x php,html,txt -w /usr/share/seclists/Discovery/Web-Content/directory-list-2.3-medium.txt | tee gobuster.log
    
And I found a 'todo.txt' note:

![/todo.txt](img/todo.png)

The `/admin` page is a login screen:

![/admin](img/admin.png)

I ran a searchsploit query for 'bludit' and there were a number of hits:

![searchsploit bludit](img/searchsploit-bludit.png)

I think there is a specific order that I need to do this. The directory traversal exploits seem to be a bit misnamed, as they would have me upload a malicious file that is a php reverse shell. But it requires me to be authenticated. The name of the Authentication Bruteforce Bypass basically tells me that there is some sort of brute force blocker in place. None of the leads in the gobuster output give me possible credentials, although I have found via google that 'admin' is the default username. Another possible username is 'fergus' since it is mentioned in the todo.txt file. Since this blog has a lot of words in it I will use cewl to grab them and make a wordlist and use the bypass exploits to see if I can bruteforce the credentials.

## Exploitation - Brute Force bypass

**Cewl command:** 
    cewl 10.10.10.191 -m 8 > wordlist.txt
    
**Exploit command:** 
    ./bludbrute.py -l http://10.10.10.191/admin/login.php -u users.txt -p wordlist.txt
    
Luckily, fergus' password is actually in the text on the website - not sure how often that happens IRL but it worked in this case. It is "RolandDeschain":

![fergus password](img/fergus-pass.png)

### Foothold via malicious file upload

One of the exploits from searchsploit should work here. Bludit does a check on uploaded files to make sure it is the correct extension; if it is, then the file is moved to the anticipated location, but if it fails, the file stays in the known temporary location. Then I can access it and get a reverse shell.

![exploit code](img/bluditexploit.png)

I changed a couple things here; the IP address, username + password. I also changed the name of the .png file.

I tried to use msfvenom to create the reverse php payload, but it resulted in a very limited shell, so instead I just copied the pentestmonkey 'php-reverse-shell' but changed the extension to .png. I also changed the IP and port in the code so it would connect back to me.
    
I also had to create an '.htaccess' file with the following code inside it:

    `RewriteEngine off
    AddType application/x-httpd-php .png`
    
This allows the server to act as if PNG files are actually PHP, and execute them. Incredibly, the server allows us to upload an .htaccess file, which it really shouldn't.

Now I run the exploit - `python3 exploit.py`:

![Exploit complete](img/exploitdone.png)

Let's check to see if the malicious file is there:

![sneaky.png in /temp](img/sneaky.png)

OK, now I need to spin up a netcat listener - `nc -nvvlp 6969` - and navigate to the file. 

![www-data shell](img/www-shell.png)

Let's upgrade this thing before moving any further:

    which python3
    python3 -c 'import pty;pty.spawn("/bin/bash")'
    <CTRL-z>
    tput lines && tput cols
    stty raw -echo
    fg
    export SHELL=bash
    export TERM=xterm-256color
    stty rows 25 columns 125
    
## Privilege Escalation - www-data > Hugo

The flag is not readable by my user. There are two regular users on this machine - hugo and shaun. The user flag is in hugo's home folder, so I either have to become hugo or else go straight to root.

I noticed a directory at `/ftp` which is nonstandard:

![FTP dir](img/ftp.png)

Inside were a number of files, and a note:

![note.txt](img/note.png)

Not totally sure what that's about yet, but I grabbed a gzipped config file and a pdf from the dir using netcat as I couldn't open them on the machine:

![ftp files](img/ftpfiles.png)

The "config" file is a compressed .wav file, that makes a buzzing sound. The pdf is a manual for a specific digital camera. I think the info is to lead me towards a code embedded in the sound file, which I must say is a bit CTF-like. But let's leave this for a bit while I chase down something I noticed when I ran linpeas.

![Two versions of Bludit](img/2bludits.png)

There are two versions of the CMS that was used. Digging into the 3.9.2 (which is the one that was running I think) there is a users.php file (in `/bl-content/databases`) which turns out holds all the user info, including encrypted passwords:

![users.php](img/adminhash.png)

`hashid` says they are SHA-1 hashes, and there are no hits on crackstation. And I also know that the password is not likely in the website text since I tried that route already. But let's check out the same file, but in the other Bludit install:

![Hugo](img/hugo.png)

There we see hugo's info in there. I'll grab the hash and try crackstation again:

![Hugo's password](img/hugopass.png)

Ouch, that's very basic. Let's see if he also uses it for his user account on this box:

![Hugo shell](img/hugo-shell.png)

Yep.

### Flag

![user flag](img/user-flag.png)

## Sudo -l - Hugo > root

OK let's continue with privesc, from this account. Knowing the password could be a huge asset.

![sudo -l output](img/sudo-l.png)

When I saw this, it set off alarm bells. So I had to check the sudo version to make sure I was not off base:

![Sudo -V](img/sudo-version.png)

OK, so I'm not sure if the box creator meant it this way, but I can fully escalate to root here. Now, the output from `sudo -l` means I can execute /bin/bash as any user besides root. So I could type `sudo -u shaun /bin/bash` and I would then be the shaun user. But if the version of sudo is prior to 1.8.28 (it is 1.8.25p1), and if the permissions are set as they are here for hugo, then there is an exploit that allows me to become root. A short explainer is [here](https://resources.whitesourcesoftware.com/blog-whitesource/new-vulnerability-in-sudo-cve-2019-14287).

So without further ado, let me type `sudo -u#-1 /bin/bash`:

![root shell](img/root-shell.png)

### Flag

![root flag](img/root-flag.png)

## Conclusion

I don't know how realistic it is for a CMS user to have their password as a word in one of their blog posts, but it is here. I suppose it can be a useful technique when you are doing HTB, but I think a real engagement would require a huge list, gleaned from previous data leaks. The FTP thing was probably a rabbithole and I'm glad I didn't get too wrapped up in it. Often when I do enumeration with linpeas I do see the version of sudo in red but so many times that has not panned out that I pretty much ignored it - besides it didn't matter from the POV of the www-data user anyway.

**Steps taken:**

* Make wordlist from blog posts on webpage
* Bruteforce password from wordlist, gain access
* Use directory traversal vuln to upload and execute reverse php payload, get shell
* Find encrypted password in CMS database directory, get lucky finding decrypt, move laterally into new user
* Use CVE-2019-14287 to escalate to root user
