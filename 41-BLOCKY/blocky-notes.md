# BLOCKY | 10.10.10.37

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN enum/nmap-initial 10.10.10.37
    
### Open Ports and Their Services

* 21 - FTP - ProFTPD 1.3.5a
* 22 - SSH - OpenSSH 7.2p2 Ubuntu
* 80 - HTTP - Apache 2.4.18
* 8192 (closed) - SOPHOS
* 25565 - minecraft

## Services Enumeration

### 21 - proFTPD 1.3.5a

The version 1.3.5 (not sure if all variants) is vulnerable to an unathenticated copy exploit, according to searchsploit:

![](img/searchsploit-ftp.png)

I could use it to copy files I should not be able to access, or with the python exploit I could leverage it to actually execute commands. Let's check out the other services before getting ahead of ourselves here.

### 22 - OpenSSH 7.2p2

Your typical SSH setup. If I get valid credentials I might try to log in with them here.

### 80 - Apache 2.4.18

This version is vulnerable to a local privesc that may be useful later, though I do know it relies on waiting for a service to restart apache which is impractical on Hack the Box.

The website is a wordpress blog, and there are quite a few accessible directories according to my gobuster query:

![](img/gobuster.png)

And since it is a wordpress site, the people behind wordpress have made a handy vulnerability scanner called 'wpscan', so we can use that to see what it can find:

    wpscan -v -e --url http://10.10.10.37 | tee wpscan.txt
    
It told me the blog is running an out of date (and insecure) version of Wordpress, and it also found a sole user - 'notch' - expected as this page is a minecraft clone.

We have two login screens to pay attention to: one is for the blog at `/wp-login.php` and the other is a 'phpadmin' page, at that location.

The `/plugins` directory has two interesting files that I will take a closer look at:

![](img/plugins.png)

BlockyCore.jar has a file called 'BlockyCore.class' that contains what appears to be database credentials:

![](img/blockycore-creds.png)

(I installed 'jad' after this so I could better view extracted `.class` files.)

 ### 25565 - minecraft
 
 Completing the minecrafty theme of this box, there is a minecraft server running on the appropriate port. I don't think I really need to enumerate this one.
 
 ## Foothold
 
I tried that password on both SSH and on the phpmyadmin page, and I gained access both ways; the SSH vector has the username 'notch' and the phpmyadmin has 'root':

![](img/user-shell.png)

![](img/phpmyadmin.png)

### User Flag

![](img/user-flag.png)

## Privilege Escalation

'notch' has full sudo privs:

![](img/sudo-l.png)

So to become root all I need to do is `sudo bash`:

![](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

This was an easy box, that tries to hinder you with a flood of intel. It's not the minecraft server that is vulnerable, nor is it any part of wordpress; the way in is merely due to an inexperienced person who put sensitive files online, and we just looked through them to find the credentials.

Steps taken:

* find credentials left unprotected in open /plugins directory
* use creds to log in via SSH
* Escalate to root via `sudo bash`
