# ACTIVE | 10.10.10.100

## Initial Enumeration

    sudo nmap -v -T4 -sV -sC -oN nmap-initial 10.10.10.100
    
### Open Ports and Services

* 53 - domain - Microsoft DNS
* 88 - kerberos-sec
* 135 - MSRPC
* 139/445 - SMB
* 389 - LDAP
* 464 - kpasswd5
* 593 - ncacn_http
* 636 - tcpwrapped
* 3268 - LDAP
* 3269 - tcpwrapped
* 5722 - MSRPC
* 9389 - mc-mmf - .NET Message Framing
* 49152-49158 - msrpc

## Services Enumeration

### 88 - kerberos-sec

I can use 'kerbrute' to try enumerating usernames, but it would be best to have some kind of idea of how the usernames are structured, so I'll come back to this if I get something or if I don't, as a last resort.

### 135 - MSRPC

I was allowed a null session, but pretty much had ACCESS_DENIED across the board for any queries I typed in myself. 'enum4linux-ng' had better luck and could list all the shares on the machine:

![](img/enum4linuxshares.png)

There is a non-standard share called 'Replication' there.

### 139/445 - SMB

The 'Replication' share has a directory called 'active.htb' (like the hostname) so we can assume it contains files regarding the entire domain (and this machine is a Domain Controller).

What I am really looking for is a 'Groups.xml' file, which could have an encrypted password in it. Thing is, Microsoft accidentally leaked the key for this years ago and the passwords can be made plaintext.

Sure enough, buried far in the share is the file:

![](img/groups-xml.png)

So I get it and exit.

![](img/cpassword.png)

'cpassword' is what I am looking for, and there is also a username of 'SVC_TGS' which is a service account.

I will use 'gpocrack' from the blackarch repo to crack the password:

![](img/cracked.png)

Note that there is a year in the password (2k18), I should be aware that a newer password with a more recent year might need to replace that.

![](img/crackmap.png)

But as you can see, this password still works. Now I have valid credentials for a system service user.

## Foothold and pwn

### Kerberoast Attack via GetUserSPNs.py

This is an impacket tool that can be used to see if there are SPNs (Service Principal Names) running under a user account. If so, then I can request a ticket, which comes in the form of a hash. If I can crack the hash, then I can escalate my privilege.

    GetUserSPNs.py -dc-ip active.htb active.htb/SVC_TGS:GPPstillStandingStrong2k18 -request
    
Now, I had some trouble with this because of a 'clock skew' error, I had to change the time on my computer to match more closely. But then it spit out the hash:

![](img/hash.png)

### Cracking with hashcat

I have a working hashcat instance on my other PC, which has a GPU, so I do all my password cracking on there.

    hashcat -m 13100 hash rockyou.txt
    
In 2 seconds I have the password:

![](img/hashcat.png)

Ticketmaster1968.

### crackmapexec

I use this quickly to check if I do indeed have the administrator credentials:

![](img/pwned.png)

I do! Now I can dump all of the hashes on the system with secretsdump (kind of unnecessary since we already have the admin password):

![hashdump](img/hashdump.png)

Anyway, let's get to having a SYSTEM shell on this box. For that I will use psexec.py, another Impacket tool:

    psexec.py active.htb/administrator:Ticketmaster1968@active.htb
    
And I am now SYSTEM:

![root](img/root-shell.png)

### Flags

User:

![user flag](img/user-flag.png)

Root:

![root flag](img/root-flag.png)

## Conclusion

This was a fun and relatively easy Active Directory style box. It demonstrates the pitfall of not removing a legacy implementation of password storage in the Groups.xml file; that implementation is no longer used after Microsoft leaked the key, but the file can still be there. Then there was a bit of enumeration until we could grab a hash via a kerberoasting attack, then it was quick work to crack that hash and escalate to SYSTEM.

Steps taken:

* Find Group Policy Preferences on SMB share, crack it
* Perform Kerberoasting attack to get Administrator hash, crack it
* Use psexec.py to get a SYSTEM shell
