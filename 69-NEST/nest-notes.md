# NEST | 10.10.10.178

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.178

### Ports and Services

![nmap scan](img/nmap.png)

I noted that port 4389 is also open when performing a full port scan.

#### 445 - SMB

When I tried smbmap on this, it errored out. But there are other ways to enumerate, mainly just simply using smbclient:

    smbclient -L \\\\10.10.10.178\\

![smbclient -L](img/smbclient.png)

In the 'Data' share, I only had permission to access the 'Shared' directory - the others were off limits:

![Data Share](img/smb-data.png)

Inside 'Shared' were more folders referring to Maintenance, HR and Marketing, and I grabbed the text files I found in those.

The next share I had access to was the 'Users' share, and this one had directories for the different users on this Windows system:

![Users Share](img/smb-users.png)

I am not allowed to view the contents of any of the folders, but this at least provides a handy list of users on the system.

Looking at one of the text files I pulled from the share, I find creds for the TempUser:

![HR Message](img/HR-message.png)

With these credentials I have slightly better access and can see what's in the 'IT' folder inside the 'Data' Share:

![TempUser access](img/tempuser-access.png)

Inside some of the folders are some xml and text files, I've grabbed them all and will note anything interesting.

The 'config.xml' file leaks some info about file paths in a user's folders:

![config.xml](img/config-xml.png)

The 'options.xml' file indicates that someone may be/may have used some kind of virtual PC software:

![options.xml](img/options-xml.png)

The 'RU_config.xml' file has credentials for the c.smith user, though they appear to be for LDAP:

![RU_config.xml](img/RU-config-xml.png)

The credentials do not work on SMB and look base64-encrypted, but when I decrypt it's gibberish, so that means there is more to it.

Trying the password 'welcome2019' on the rest of the usernames I found that it worked on two other users:

![welcome2019](img/cme-smb.png)

Even though that was the case, this got me nothing - they could not even access the folders bearing their names.

#### 4386 - HQK Server

Definitely a strange service to have running, but it appears to be some kind of old fashioned database. The nmap enum gave some clues regarding how to interact with it in terms of commands to issue:

![nmap enum of port 4386](img/port-4386.png)

A google search didn't tell me anything about how to connect to it, so I tried netcat and when that connected but didn't provide interaction, I tried good ol telnet:

![telnet connection to port 4386](img/telnet-hqk.png)

I was able to do limited navigation; there was a directory called 'comparisons' but I could not view the one file in there. I would get an invalid message back:

![runquery](img/runquery.png)

I then tried to use the debug command, and I input the LDAP password I found in the RU_config file, but it was not the correct password.

I found I can navigate backwards just like in bash, using `..` to move back (or up):

![setdir ..](img/setdir.png)

There is an LDAP directory with a configuration file in it, but again I am stymied by that invalid error and I can't read any of them.

However, I can keep climbing the directory tree and keep looking around, so that's something. But I couldn't actually access anything that mattered. I tried the paths I found in the Notepad++ config.xml but couldn't access them.

#### Trying SMB again

One thing I did not do at first was attempt to go directly to the directory `Secure$\IT\Carl\` - I just tried moving one level at a time. Access denied if I try to list the contents of `Secure$\IT`, but I can just go straight to `\Carl` since I know it exists:

![\Secure$\IT\Carl\](img/Carl.png)

So I'm in Carl's dir, and there are a lot of vbs files as it is some kind of project of his. I can see that parts of this project are interacting with the RU_config file from before (the one that has his encrypted password in it); the 'Module1.vb' file loads the RU_config file and runs a DecryptString function on it to decrypt the password.

![Module1](img/module1.png)

The decrypt function I found in the 'Utils.vb' file. In order to figure out how to do this without using Microsoft Visual Studio Code, I had to look it up. But what I could tell from looking at the code is that the string is decrypted into hex from base64, then decrypted from AES, but the AES key is derived from the passphrase ("N3st22") using PBKDF2:

![decrypt function](img/decrypt-function.png)

The salt is "88552299", there are 2 iterations and the key size is 256. I plugged this into [cyberchef](https://gchq.github.io/CyberChef/) and got a result of '76cdb9283585e05cf53d14658552f6a18285e49c63e9265078d4f0e8a477e8aa':

![AES key](img/cyberchef-aeskey.png)

That is the AES key. But now I need to take the base64 string I found in the RU_config file and convert it to hex:

![base64 to hex](img/b642hex.png)

Now I put the AES key in the correct spot, with the IV ("464R5DFA5DL6LE28", from 'Utils.vb'). The recipe in this case goes like this: Put the base64 string in 'input', then add a 'From Base64' ingredient, a 'To Hex' ingredient, then the 'AES Decrypt' ingredient. If all the parts are correct, I should get some kind of legible password:

![CyberChef recipe](img/recipe.png)

So C.Smith's password is 'xRxRxPANCAK3SxRxRx'. 

## SMB Access as C.Smith

I am able to access the 'C.Smith' share using these credentials and once connected I see the user.txt so I grab that:

![The user flag](img/user-txt.png)

Inside the "HQK Reporting" directory is an apparently empty file titled "Debug Mode Password". It's using ADS (Alternate Data Streams) so we can view what could be hidden by using the `allinfo` command in the smbclient:

![allinfo](img/allinfo.png)

In order to grab the file with the ADS we give the command `get "Debug Mode Password.txt:Password"`.

![Password](img/debug-pw.png)

Now I've gone back to the HQK console and entered the password; I have new commands available to me:

![debug mode](img/debug.png)

## Privilege Escalation - Administrator shell

### Further Enumeration

So now, I basically have to go through everything I went through before and do the 'showquery' command on them, to see if I can read something juicy from a file or something.

I pulled another encrypted string for the Administrator password out of 'HQK\LDAP\Ldap.conf'. Quickly I pasted that base64 string in the cyberchef recipe, but of course it won't be that easy. My guess is there is a similar encryption scheme going on in the 'HqkLdap.exe' program that is sitting in the same directory as the Ldap.conf. Luckily, in the 'AD Integration Module' directory (that I can access through SMB) there is the exact same program.

I opened it in ILSpy and sure enough, it is using the same method to encrypt/decrypt the string:

![Same scheme](img/encryptionscheme.png)

### Exploit method

So I should just need to supply the right info into the cyberchef recipe and it should spit out the administrator password and I can finally be done with this.

![admin password](img/recipe2.png)

'XtH4nkS4Pl4y1nGX' is the admin password. Let's get a shell as NT AUTHORITY using psexec:

![shell as NT AUTHORITY\SYSTEM](img/root-shell.png)

And here are both flags:

**User:**  
![user flag](img/user-flag.png)

**Root:**  
![root flag](img/root-flag.png)

## Conclusion

Didn't like this one, probably because I don't have a set workflow regarding decompiling/debugging .NET programs. The programs for Linux seem a bit buggy and limited. Also I do not find crypto to be interesting in the slightest, so realizing I have to parse out how to decrypt something makes me roll my eyes. At least I've had to learn how to do it two times, but I sure need work in understanding what is going on. Nevertheless, it was an interesting machine, only having port 445 to work with.

**Steps taken:**

* Find temp creds for TempUser on SMB
* Find config file with encrypted creds, decrypt them using info discovered in vbs project files
* Enumerate to find debug password for HQK service on port 4836
* Enumerate HQK service to find encrypted LDAP creds and LDAP app
* Decrypt creds to get administrator password, get root shell

### BONUS

![Nice.](img/nice.png)
