
<?php
if( ( isset($_SERVER['PHP_AUTH_USER'] ) && ( $_SERVER['PHP_AUTH_USER'] == "admin" ) ) AND
      ( isset($_SERVER['PHP_AUTH_PW'] ) && ( $_SERVER['PHP_AUTH_PW'] == "admin" )) )
    {
      if($_SERVER['REQUEST_METHOD']=="POST"){
        $target_dir = "C:\\firmwares\\";
        $target_file = $target_dir . basename($_FILES["firmware"]["name"]);
          if (move_uploaded_file($_FILES["firmware"]["tmp_name"], $target_file)) {
            header('Location: fw_up.php?msg=SUCCESS');
          }
          else {
            header('Location: fw_up.php?msg=ERR');
          }
      }
      else
        {?>
<!DOCTYPE html>
<html lang="en" >

<head>

  <meta charset="UTF-8">
  

  <title>MFP Firmware Update Center</title>
  
  
  
  
<style>
body {
  background-color: #1f232e;
}

.navbar-nav > li > .dropdown-menu {
  margin-top: 0px;
  border-radius: 0;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}

.navbar-default .navbar-nav > li > a {
  width: 200px;
  font-weight: bold;
}

.mega-dropdown-menu {
  box-shadow: none;
  -webkit-box-shadow: none;
  border: none;
}

.menu {
  margin: 0;
  list-style: none;
  padding: 0;
  height: 500px;
}

.menu li {
  text-align: center;
}
.menu li .submenu {
  display: none;
  height: 100%;
}

/* Menu item */
.menu li a {
  color: #000;
  padding: 10px;
  text-decoration: none;
  display: block;
  text-transform: uppercase;
  position: relative;
}
.menu li a:hover {
  color: #999;
}
.menu > li > a::after {
  border-left: 13px solid #fff;
  border-right: 13px solid transparent;
  border-top: 13px solid transparent;
  border-bottom: 13px solid transparent;
  content: "";
  display: none;
  bottom: 50%;
  height: 0;
  left: 100%;
  position: absolute;
  width: 0;
  z-index: 1;
}
.menu li:hover a::before,
.menu li:hover a::after {
  display: block;
}

/* Menu item */
.menu li:hover .submenu {
  display: flex;
}

.initial-show {
  display: flex !important;
}

.submenu {
  background: #f0f0f0;
  left: 100%;
  width: 952px;
  position: absolute;
  text-align: center;
  justify-content: center;
  align-items: center;
  top: 0;
  transition: all 0.3s linear 0s;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
}

.submenu ul {
  margin: 0;
  padding: 0;
}
.submenu ul li {
  display: inline-block;
  float: none;
  line-height: 1;
  margin: 0;
  padding: 2px 0;
  vertical-align: middle;
}

.submenu ul li span {
  display: block;
  margin-top: 5px;
}
table, td, th {
  border: 0px solid black;
}
td {
    padding-top: .5em;
    padding-bottom: .5em;
}
table {
  width: 50%;
  border-collapse: collapse;

}

.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  color: black;
  text-align: center;
}
</style>

  <script>
  window.console = window.console || function(t) {};
</script>

  
  
  <script>
  if (document.location.search.match(/type=embed/gi)) {
    window.parent.postMessage("resize", "*");
  }
</script>


</head>

<body translate="no" >
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">MFP Firmware Update Center</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">About</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="fw_up.php">Firmware Updates</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Drivers Updates</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Contact</a>
      </li>
    </ul>
    
  </div>
</nav>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  
<br/><p>&nbsp;&nbsp; Select printer model and upload the respective firmware update to our file share. Our testing team will review the uploads manually and initiates the testing soon. 
  <br/><form action="" method="post" enctype="multipart/form-data">
<br/><center> <table>
  <tr><td>Printer Model:</td><td><select name="printers" id="printers">
  <option value="HTB DesignJet">HTB DesignJet</option>
  <option value="HTB Ecotank">HTB Ecotank</option>
  <option value="HTB Laserjet Pro">HTB Laserjet Pro</option>
  <option value="HTB Mono">HTB Mono</option>
</select></td></tr>
<tr><td>Upload Firmware:</td><td><input type="file" id="firmware" name="firmware"></td>
  </tr>
  <tr>
    <td colspan="2"><input type="Submit" value="Submit"/></td></tr>
</table></center></form>

</body>
<div class="footer">
  <p>&#169; 2021 Driver Inc</p>
  <p><a href="mailto:support@driver.htb">support@driver.htb</a></p>
</div>
</html>
 <?php }}else
    {
        //Send headers to cause a browser to request
        //username and password from user
        header("WWW-Authenticate: " .
            "Basic realm=\"MFP Firmware Update Center\"");
        header("HTTP/1.0 401 Unauthorized");

        //Show failure text, which browsers usually
        //show only after several failed attempts
        print("Invalid Credentials");
    } ?>
