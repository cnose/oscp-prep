# DEVOOPS |  10.10.10.91

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.91

### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.2p2 Ubuntu
* 5000 - HTTP - Gunicorn 19.7.1

## Service Enumeration

### 22 - SSH

A standard SSH setup. We'll need valid credentials in order to proceed further here.

### 5000 - HTTP

Running version 19.7.1 of the Gunicorn HTTP server, which is a Python-based server for Unix/Linux systems. Searching the web I discover that this version is vulnerable to an [http request smuggling](https://snyk.io/vuln/SNYK-PYTHON-GUNICORN-541164) exploit, so I'll keep that in my back pocket. The main page is pretty plain, and indicates it is under construction:

![](img/port-5000.png)

Running a gobuster search finds me two other directories, the /upload one being the most interesting:

![](img/gobuster.png)
![](img/xml-upload.png)

I managed to upload a simple xml file that included the fields required, and it spat out a response, one that disclosed some juicy info regarding location of uploaded files, as well as a username:

![](img/post.png)

## Exploitation

In my searches for what to do about an xml upload form I searched for "xml reverse shell" and the first result was [this blog post](https://www.blackhillsinfosec.com/xml-external-entity-beyond-etcpasswd-fun-profit/) about XXE (XML External Entity). It is where I found the idea to try code execution. The initial exploit grabs the /etc/passwd file. I modified it slightly to fit with the parameters and I got a result:

![](img/etc-passwd.png)

I tried this with `/etc/shadow` as well but no dice. I was able to read the user flag though, which I agree is a bit of a cheat:

![](img/user-flag.png)

What I really want and need to do, is to leverage this for an attack that gives me shell access to the machine. I know that the machine is running SSH, so maybe I can use this attack to grab the private SSH key of the user roosa. I slightly modify the test file and upload it:

![](img/ssh-key.png)

Nice!

Let's try logging in:

![](img/user-shell.png)

OK, we have a user shell now. Let's begin enumeration.

## User Enumeration

I really don't like how `sudo -l` is so slow on this box, which is attributed to a network issue. But anyway, we don't have the user's password so we can't bother with that. I ran linpeas and pspy to see what I could find, and there was slim pickings. This user's home directory had a lot to go through though - which is actually pretty realistic. I `cat`ed out their bash history add noticed something interesting:

![](img/history.png)

Looks like the user accidentally pushed the wrong SSH key via a commit and had to revert. That means there should be traces of it in the git logs, which admittedly I had to look up to learn about. What I did was navigate into the `work/blogfeed` directory and used the command `git log -p` - then I was able to see the details of each commit and what was changed.

![](img/wrong-key.png)

One of those keys is the root key. I tried both just to be safe. Copied them over, made sure there were no stray characters, then did a chmod 600 on them and tried to log in as root via SSH:

![](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

This box was confusing for me for some reason. I guess it was the lack of specific public exploits available for the webserver - the fact that I had to know about general web app security. I will admit that I need to study harder about the OWASP Top Ten. Then, when I got on the box, at first I went through the typical motions of enumerating, using linpeas and my own queries, and kind of ignoring the user's home dir even though all the clues were there. I'll need to remember to look at everything methodically, and when I see something I don't understand right away, take notes and research it up.

Steps taken:

* Abuse an XXE (XML External Entity) vulnerability in the webserver via an exposed upload form to get SSH creds
* Login to the server with the credentials
* Enumerate the user and find out they accidentally committed the root SSH key to a github repo
* Search the git logs and grab the SSH key to escalate to root
