#  MIRAI | 10.10.10.48

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.48

### Open Ports and Their Services

* 22 - SSH OpenSSH 6.7p1 Debian (deb8u3)
* 53 - DOMAIN - dnsmasq 2.76
* 80 - HTTP - lighttpd 1.4.35
* 1184 - UPNP - Platinum UPnP 1.0.5.13
* 32400 - Plex Server
* 32469 - UPNP - Platinum UPnp 1.0.5.13

This setup almost reminds me of a home server setup. I'm actually running a Plex Server in my home, but for now it is not accessible from the outside.

## Service Enumeration

### 22 - SSH

Version 6.7p1 is really only vulnerable to a possible username enumeration, which might be useful if I can't find other ways to access this system.

### 53 - DOMAIN - dnsmasq

Version 2.76 is vulnerable to several types of DOS attacks, which is not useful for our purposes here.

### 80 - HTTP

Version 1.4.35 of the server that's running (lighttpd) is vulnerable to some kind of SQL injection/path traversal exploit that I don't fully comprehend. I will file that away until I am desperate.

I have found a python-based version of the exploit [here](https://github.com/sp4c30x1/uc_httpd_exploit/blob/master/vltz.py) that I will save for later.

This port is also running an instance of Pi-Hole, which is a dns-based ad-blocker that runs on a raspberry pi:

![](img/pihole-port-80.png)

There is a login screen there so I may have to bruteforce my way into that and see if I can reuse credentials elsewhere.

### 1184/32469 - UPNP

Both ports are running version 1.0.5.13, which as far as I can tell there are no known public exploits for it.

### 32400 - PLEX

Unlikely that this is a way in, as the authentication is handled non-locally by Plex, and there are probably not any exploits related to this.

## Exploitation

Since I once had a raspberry pi, I remembered that the default ssh credentials were username 'pi' and password 'raspberry' - which is why you should change that upon first boot-up, and also why I now have an SSH session as the user 'pi':

![](img/pi-shell.png)

### Flag

![](img/user-flag.png)

## User Enumeration

So obviously I started poking around after gaining initial access - I did a `cat /etc/passwd` to see what other users there were and noticed that the plex user has a login shell, which I guess is a common misconfiguration.

![](img/etc-passwd.png)

Then, a typical part of enumeration is seeing what kind of privileges your user has, so you type `sudo -l` and see what output you get:

![](img/sudo-l.png)

Wow. So, this means that the pi user is essentially root, in that they can run any command at all, without using a password.

## Privilege Escalation

So, now, I'll just say the magic words:

    sudo su root 

And now I am root:

![](img/root-shell.png)

### Flag

![](img/usb.png)

Dang.

![](img/james.png)

Dammit James!

OK so the guy deleted the 'files'. Let's find out what device this is attached to.

    lsblk

![](img/lsblk.png)

OK so it is located in /dev/sdb.

    cat /dev/sdb

![](img/sdb.png)

So there is the flag! OK I'm done.

## Conclusion

This was a fun box because it was illustrative of the danger of default passwords, misconfigurations and in general a lax security posture for Internet-Of-Things devices. Shows it's important to have several layers of security to at least slow the bastards down. That privesc was a joke.

Steps taken:

* Scan and find a service running that makes you think to try default credentials
* Log into SSH using default creds
* Trivially escalate privs due to terrible security policies
