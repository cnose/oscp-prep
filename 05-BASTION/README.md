# BASTION | 10.10.10.134

I already completed this box as part of the [Windows Privesc for Beginners](https://www.udemy.com/share/102YD8B0sYeVlXRQ==/) Course on Udemy.

The writeup can be found [here](https://gitlab.com/cnose/wpe-capstone/-/tree/master/4-BASTION).
