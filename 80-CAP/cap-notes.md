# CAP | 10.10.10.245

## Enumeration

```bash
sudo nmap -v -T4 -sCV -oN nmap-initial 10.10.10.245
sudo nmap -v -T4 -sUV --top-ports=20 -oN nmap-udp 10.10.10.245
```

### Ports and Services

![nmap scan](img/Pasted%20image%2020210610122956.png)

#### 21 - FTP - vsftpd 3.0.3

This version is vulnerable to a denial of service attack, but that it obvi not the attack vector we are looking for here. Anonymous login is not possible here.

#### 22 - SSH - OpenSSH 8.2p1

Your standard SSH setup. Like with FTP, I'll come back if/when I get valid creds.

#### 80 - HTTP - gunicorn

![homepage](img/Pasted%20image%2020210610121800.png)

This is a python-based web server that is hosting a simple network monitoring utility. Without providing credentials I have the ability to run `ifconfig`, `netstat` and even capture network traffic for 5 seconds (I guess it is calling `tcpdump`?).

I messed around for a while, analyzed pcaps (using wireshark) from my pings, FTP connections, and even quickly clicking other links after clicking the capture link. None of the pcaps had any juicy data. 


## Initial Foothold via cleartext credentials in accessible file

### Exploit Method

I noticed that after a time the pcap files are deleted, and they follow a simple increment from 1 onward. So I got the idea to see if there were any beyond mine. Incrementing past the most recent pcap just redirected me to the main page. However, I tried appending '0' to the URL and there was a pcap file there:

![PCAP data at position 0](img/Pasted%20image%2020210610132112.png)

So I analyzed this PCAP and found what I was looking for: credentials!

![FTP credentials](img/Pasted%20image%2020210610132452.png)

These credentials work for both FTP and SSH, so I just bypassed FTP and went straight for a shell (the FTP dir has the flag only).

![user shell](img/Pasted%20image%2020210610133121.png)

#### user flag

![user flag](img/Pasted%20image%2020210610133212.png)

## Privilege Escalation - nathan > root

### Enumeration

**strange code in website app.py (runs tcpdump elevated)**  
![app.py snippet](img/Pasted%20image%2020210610134211.png)

**python 3.8 capabilities**  
![python cap_setuid](img/Pasted%20image%2020210610134806.png)

### Exploit method

This does not really require any fancy footwork. Python can change UID to anything and our user is allowed to run python, so it is a simple command:

```bash
python3 -c 'import os; os.setuid(0); os.system("/bin/bash")'
```

![root shell](img/Pasted%20image%2020210610141458.png)

#### root flag

![root flag](img/Pasted%20image%2020210610141547.png)

## Conclusion

An easy box that is actually easy - just don't overthink it too much. To gain a foothold I just needed to observe what the webapp was doing. I went down a very brief rabbit-hole of trying basic command injection and then another one of trying to generate my own traffic to analyze. Getting root is very simple, just requires careful enumeration, and noticing something not typical.

**Steps taken:**

* Enumerate until finding hardcoded PCAP file
* Analyze PCAP to find cleartext user credentials, log in via SSH
* Enumerate until finding that python has a setuid capability
* Escalate to root using python
