# POISON | 10.10.10.84

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.84
    
### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.2 FreeBSD
* 80 - HTTP - Apache 2.4.29

Another FreeBSD machine

## Services Enumeration

### 22 - OpenSSH 7.2

A standard SSH setup, so I'll focus on the web server that's running first, then come back to this if I need to.

### 80 - Apache 2.4.29

Here is what is running on the webserver:

![](img/port-80.png)

Oh boy.

Right away I checked if I could view `/etc/passwd` and yes I could:

![](img/etc-passwd.png)

So the root user on the system is named Charlie, and there is a lower level user named 'charix'. Nice.

Also, one of the example php files, 'listfiles.php', when you access that one it leaks a bit of useful info:

![](img/listfiles.png)

So let's see if we can access that pwdbackup.txt file:

![](img/pwdbackup.png)

It looks to be base64 encoded, but I guess 13 times? After 13 decodings I ended up with what could be the SSH password:

    Charix!2#4%6&8(0
    
Let's try it out.

## Foothold - decoded SSH creds

![](img/user-shell.png)

OK we're in.

### Flag

![](img/user-flag.png)

## User Enumeration

OK this might be dicey, because I bet that privesc on a FreeBSD machine is not exactly the same as on a typical Linux box.

Already I see that sudo does not even exist, and I am not sure why I can't EXPORT any env variables either, but let's be systematic here.

### Home dir

![](img/homedir.png)

Hmmm, something stands out here.

The zip file is password-protected, which usually means it is crackable. I am able to bring it over to my machine for analysis because the remote system luckliy has the python2 SimpleHTTPServer so I can grab it easily with cURL.

I converted the file to a crackable hash via 'zip2john' but I had no hits when I ran it through john using the rockyou wordlist.

So I tried the same password I used to login to SSH and it extracted the file - 'secret' - but it is a bizarre character encoding and is just gibberish. I am not sure if the reason is that there is something weird on my system or if I simply have to figure out the correct encoding.

I also threw linpeas on the box (I was surprised it could run) but I did not find anything of note to my eyes, except perhaps for a VNC server running locally...

## Privilege Escalation via tightvnc over forwarded SSH session

I had to check out Ippsec's video on how to do this, and I was concerned that my non-Kali system that I've made for pentesting was not properly configured for this yet (do not have proxychains installed/configured).

I tried that cool trick for modifying connection configs while already connected (the "konami SSH code" referenced in the video), but it also did not work for me at first. Then it occurred to me to actually follow through, and it works! 

* Do a regular login via SSH
* Then hit the keys `~C` and you will get that 'ssh>' prompt
* From there I typed the dynamic SSH forwarding flag with port - `-D 1080`
* Next I added the local port forwarding rule: `-L 6801:127.0.0.1:5801`
* And one for the other port: `-L 6901:127.0.0.1:5901`

Then, I was able to connect to the remote server using vncviewer (using that strangely-encoded secret file as the password file):

    vncviewer 127.0.0.1::6901 -passwd secret
    
And then I was root (flag included):

![](img/root.png)

## Conclusion

This box was definitely a learning curve for me, once I gained initial access. I will want to read up on this a bit more, because I am not sure if the fact that I could SSH forward the tightvnc ports is a security issue, or if it was merely the fact that I had the 'secret' file that allowed the privesc. I suppose it was both, but it feels really crazy that a service running locally can be made to be remotely accessible with just a few SSH commands - I guess that's the magic of SSH forwarding that I don't fully comprehend.

Steps taken:

* find encoded credentials on server
* decode them 13 times to find SSH password, log in via SSH
* find credentials in password-protected zip file, use SSH password to access
* use SSH forwarding rules to access locally available vnc server as root
