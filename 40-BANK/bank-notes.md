# BANK | 10.10.10.29

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.29
    
### Open Ports and Their Services

* 22 - SSH - OpenSSH 6.6.1p1 Ubuntu
* 53 - DOMAIN - ISC BIND 9.9.5
* 80 - HTTP - Apache 2.4.7

## Services Enumeration

### 22 - OpenSSH 6.6.1p1

This version is vulnerable to username enumeration, like so many SSH versions. It (or rather the SFTP) part appears to be vulnerable to some kind of misconfiguration exploit that could be used to execute arbitrary commands; but it looks like you need to be an authenticated user so that does not help us yet.

### 53 - ISC BIND

Just means the machine is running a domain name server, which is a clue that there could be additional subdomains. Luckily I have just completed a different box that had a DNS service running, so I kind of know what to do:

![](img/bank-htb.png)

Adding those '.bank.htb' entries to my hosts file changes some things in regard to what webpages are visible. Let's move on to port 80.

### 80 - Apache 2.4.7

A searchsploit query turns up something potentially promising:

![](img/searchsploit-apache.png)

An [openssl bug](http://akat1.pl/?id=1) that allows for remote code execution, provided the version of PHP is 7, which I do not yet know. But there is lower hanging fruit, such as a login page:

![](img/bank-login.png)


Additionally, running a gobuster query on the URL finds a few other directories, but they are probably inaccessible unless logged in. A couple have some php files or js files, but none seem to be exploitable. So, we have the login screen but no hints for username or passwords, besides chris (but that may be an artifact of the person who made the box?). The lack of hints could point to an SQL injection, but there is one more curious thing, which is the 'support.php' page:

![](img/support.png)

It fails to properly redirect, so let's look at what's going on with BurpSuite.

![](img/burp-support.png)

Wow, we can actually access the page this way. That means the support.php is fully loaded before the redirection even occurs.

There is also a 'debug' message in the source code that hints if we want to upload a php file we should give it the '.htb' extension:

![](img/debug-message.png)

The only problem is that I needed to find out how to stop my browser from doing the redirect, because I could not figure out how to upload a file using Burp. So I dug into the documentation and found I could use the 'Match and Replace' option in the 'Proxy' tab, under 'Options'.

![](img/match-replace.png)

I added a rule that replaces the 302 redirect with a 200 OK code, and then I was actually able to navigate to the support page:

![](img/upload.png)

## Foothold

I've modified the standard php-reverse-shell that can be found on pentestmonkey or in the web-shells dir on a Kali machine. I've added my particulars and changed the extension to '.htb'.

Next, I've spun up a netcat listener on the specified port. Let's upload the shell:

![](img/upload-shell.png)

(I had to reload because I had my firewall enabled and couldn't get a connection)

Then I clicked on the 'Attachment' link and:

![](img/www-data-shell.png)

I have a shell as the 'www-data' user.

### User Flag

![](img/user-flag.png)

## User Enumeration

There is a single regular user on the machine, and it is 'chris' - so not an artifact. When you find yourself on a box as the www-data user, it is good practice to check out the `/var/www` directory to see what you could not access beforehand. I found a directory called 'balance_transfer' (note: this directory is actually publicly accessible but my gobuster wordlist did not have it):

![](img/var-www.png)

There is also the bankreports.txt file which contains plaintext creds:

![](img/creds.png)

If you sort all the files found in 'balance_transfer' by size, you'll find one file that is smaller than all others, and it contains the same info as the text file.

Also, I found more creds (for MySQL) in the 'user.php' file in `/var/www/bank/inc`, so now we have some leverage points. 

I quickly checked and I could not su to either chris or root using these passwords.

Wow, not sure why `/etc/passwd` is writable - that's a major misconfiguration:

![](img/etc-passwd.png)

## Privilege Escalation via writable /etc/passwd file

This probably never happens, but lets go with it. The first thing I did was to copy the original file somewhere, so I wouldn't mess things up too bad. Then I decided to become the chris user instead, as a test.

Even if the file is writable, you typically can't just remove the 'x' that signifies the encrypted password located in `/etc/shadow`. Instead, you should generate an encrypted password and insert it in place of the x:

    openssl passwd -1 -salt [user] password
    
The [user] is the salt value, since that's how the shadow file works. So you take the output from that command and put it in place of the x in the passwd file.

![](img/hash.png)

All I had to do at this point was `su chris` and type in the new password:

![](img/chris.png)

And luckily, the user had full sudo privs, so I was able to run `sudo bash` and I was root:

![](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

There may be other ways to get a foothold, and to privesc to root, but this is the way I discovered. I'm not sure how realistic any of this box is, but I did it anyway.

Steps taken:

* Enumerate DNS to find subdomains
* Detect malformed redirect using BurpSuite, find upload vuln, upload php reverse shell
* Enumerate to find writable /etc/passwd, escalate to root via edit
