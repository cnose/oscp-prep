# BOUNTYHUNTER | 10.10.11.100

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial IP

### Ports and Services

![nmap](img/nmap.png)

#### 22 - SSH - OpenSSH 8.2p1 Ubuntu

If I find valid credentials I will use this service to get a stable shell.

#### 80 - HTTP - Apache 2.2.41

The webpage is a template, customized to appear to be a cybersec company.

![port 80](img/port-80.png)

There is a contact form, but it doesn't send anything and makes a GET request to the root page, so nothing exploitable there.

There is a link to the PORTAL at the top right, it goes to a barebones page that says it is under development, and links to another page, 'log_submit.php'.

![portal](img/portal.png)

This tells us that the webserver is using PHP, so I will run a gobuster scan in the background while investigating the next webpage.

![gobuster log](img/gobuster.png)

The 'log_submit.php' page allows user input and may be exploitable somehow.

![log_submit.php](img/log_submit.png)

The webpage source links to two js libraries, one of which appears non-standard:

![source code](img/source.png)

Opening that one gives us another URL to examine, as well as letting us know that the script is using XML, which could mean it may be vulnerable to an XXE attack.

![bountylog.js](img/bountylog.png)

Filling in the form on the 'log_submit' page and intercepting the request with Burp we can also find that other URL, as well as the indication that XML is being used. The data we submit is base64-encoded as well.

![POST request](img/post.png)

![base64-encoded XML data](img/decoded.png)

All of this information leads me to believe that the web form is vulnerable to an XXE (XML External Entity) attack.

## Exploitation via XXE injection

An XXE is a type of custom entity whose definition is located outside of the DTD (Document Type Definition) where they are declared. They use a URL to load the value of the entity. We can load things from our attacker machine to hopefully gain code execution. For instance, if the application does not perform any checks or sanitization of requests then we can read files on the server with the simple injection:

    <!DOCTYPE pwn [ <!ENTITY xxe SYSTEM "file:///etc/passwd"> ]>

Then we need to modify the request to inject the payload as `&xxe;` and the response should be the `/etc/passwd` file.

### Exploit Method

First I've created the payload injection as above. Then I grab the decoded XML from the burp output and append it to the payload injection, and modify it slightly to inject the xxe:

![XXE payload](img/xxe.png)

The next step is to convert it into base64 and remove the spaces and line breaks. I did that by piping that xxe output into base64, then I eliminated the line breaks in vim.

Finally, I pasted the base64 into the burp request, replacing what was there, then URL-encoded it and sent it off.

It worked! The response included the requested file. We have code execution!

![Successful code execution](img/RCE.png)

Now I have to turn this into something that can get me a shell on the box. Here I messed around with the fact that I could tell the server to reach out to me, and I tried to get it to execute php payloads that could execute code but to no avail. For a while I didn't know what to do but I did have the ability via PHP wrappers to read the source code of PHP files that I could not normally, such as the 'db.php' file that appeared mysteriously empty in the gobuster scan.

In Burp I just modified the request to use the PHP wrapper like so:

    `<!DOCTYPE pwn [ <!ENTITY xxe SYSTEM "php://filter/convert.base64-encode/resource=db.php"> ]>`

That returned some output, base64-encoded:

![base64-encoded](img/base64.png)

Once I decoded it I had credentials.

![Database creds](img/creds.png)

With that credential, I had SSH access as the 'development' user (which we knew of from grabbing `/etc/passwd`):

![Shell as development](img/user-shell.png)

## Privilege Escalation - development > root

### Further Enumeration

First thing I do if I know the user's password is `sudo -l` to see if the user can run commands as root, and in this case they can:

![sudo -l](img/sudo-l.png)

For Hack the Box machines this is usually the route to privesc. But normally I would run linpeas or do some more manual enumeration here. OK actually I am going to run linpeas just in case. In fact there may be several ways to get root, but the one I am going to try is the intended route.

### Exploit method

We have to find some way to inject into the python script to execute code as root. Let's have a look through the script:

![python script](img/script.png)

Basically, if the file you input meets certain conditions then the Ticket Code will be evaluated with eval (which runs input as Python code). The specific vulnerable part is below:

![eval](img/eval.png)

If I can craft a ticket file that gets to that point then I can inject code that will call a bash shell. Here is what I made:

![exploit](img/exploit.png)

Last thing to do is to call the ticketvalidator script and give it the path of my malicious ticket:

![root shell](img/root-shell.png)

## Conclusion

This box should have been easier but I made a mistake when I did not look closely enough at my gobuster output to see db.php right away. I am familiar with XXE and have done them in the past but became stuck on what files I should try to exfiltrate. If I saw db.php right away I would have know to try to grab that immediately since it is a PHP file and blank when you navigate to it. Basically in this case we use the XXE injection to execute a Server Side Request Forgery (SSRF) and grab the code for that page.

After that the path to escalation was clear but again I had problems with figuring out how to exploit the weakness, because I really knew nothing about the eval function. However some research on my end would have figured it out eventually.

**Steps taken:**

* Discover XXE injection vuln in web form
* Use vuln to grab PHP file with credentials from server side, log in as user
* User sudo privs to exploit python script that runs as root to get root shell
