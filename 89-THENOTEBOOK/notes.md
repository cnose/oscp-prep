# THE NOTEBOOK | 10.10.10.230

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.230

### Ports and Services

![nmap](img/nmap.png)

This is a Linux machine, running Ubuntu 18.04.

#### 22 - SSH - OpenSSH 7.6p1

Your standard SSH setup. Perhaps I will be able to login as a user later on, leaving it for now.

#### 80 - HTTP - nginx 1.14.0

There is a simple webpage here, hosting a note-taking app.

![Main webpage](img/port-80.png)

I've signed up, and I am taken to a page where I can view and create notes. The URL seems to have a unique identifier in order to hinder/prevent bruteforcing of other accounts and their notes, perhaps?

![Notes page](img/notes.png)

I've opened BurpSuite to take a look at the request, and there are two cookies set: one is a long one called "auth", which appears to be a JWT (Json Web Token), and the other (called "uuid") is the same identifier that is in the URL for my notes page.

![Burp request](img/request.png)

#### Decoding the JWT

So I copied the auth cookie and took it to [JWT.io](https://jwt.io/), which breaks down and decodes the JWT so you can see what is being passed in it. I have highlighted some of the interesting parts, namely the location of a private key callout, the admin capability switch, and the public and private key requirements to be able to create your own valid JWTs.

![JWT](img/jwt.png)

## Exploitation

So with this information, we have a way to try to create an admin-level JWT, so that we can read all notes. I will need to modify the JWT to have the 'kid' in the header point to my own machine so it can grab a private key I create myself, and I will need to modify the 'admin_cap' switch in the payload, and finally I will need to see if I can sign the token using keys I create myself.

Let's try an experiment first, to see if I can modify the header and have it call out to me. At first I tried to edit the plaintext in the decoded section, but that did not work. But since we know the information is encoded as base64, we can decode, modify and encode in the terminal:

![Modifying the JWT header](img/modifyheader.png)

Note that I had to use the '-n' flag with echo as well as encapsulate the modified text in single quotes otherwise it would not encode properly and I would receive an error.

Then I started a netcat listener at port 7070 to see if the app would reach out after I pasted the modified header in the Burp request.

It worked!

![Connection received](img/connection.png)

I then tried modifying the payload in the same way, then pasting the base64 into the cookie on the website, but I would get 502 errors. Clearly at this point I would need to provide my own keys in order to move forward. So with some trial and error I was able to generate the correct type of keys required to actually sign my own JWT.

    ssh-keygen -t rsa -b 4096 -m PKCS8 -f notebook
    ssh-keygen -e -f notebook.pub -m PEM

Then I pasted the keys into the appropriate spots and bam, my token was verified. I made the changes to the header and payload and my token was still verified. Last thing to do was to rename the private key on my machine to privKey.key and run a server to host it.

I added the new signed JWT to the cookies in the devtools on the page, refreshed and suddenly I had admin access, exemplified by the new option to enter the Admin Panel:

![Admin](img/admin.png)

I could now view all notes saved to the application.

![Notes as admin](img/adminnotes.png)

Here is a sampling of the notes:

![Note by Noah](img/noah.png)

![Notebook Quotes](img/quotes.png)

![Backups](img/scheduledbackups.png)

![Fix config](img/config.png)

Obviously the last one is crucial, due to the fact that an admin can upload files:

![Admin Panel](img/adminpanel.png)

### Exploit Method

So now we have the workings of an exploit. I can upload a PHP reverse shell and it will be executed. I am using the pentestmonkey's PHP reverse shell since it works well enough. Once I get on the box I will see if I can grab some SSH keys for a more stable connection.

Step one is to make sure I continue to host the private key as with every web request I make the app looks for it.

Step two is to start a netcat listener on the port I indicated when I edited the PHP shell, in this case 6969.

Step three is to upload the shell:

![Uploaded malicious file](img/upload.png)

Lastly, just access the page, and then get a connection back.

![Shell as www-data](img/shell.png)

## Privilege Escalation - www-data > noah

I am the 'www-data' user, but I cannot read into noah's directory to see the flag, so that indicates I will have to escalate my privileges.

### Further Enumeration

Before uploading and running linpeas like I always do, I just did some quick manual enumeration, checking typical directories like `/tmp`, `/opt` and `/var`, and I got lucky this time. In `/var` was a backups dir and there was one called 'home.tar.gz' that I was allowed to read.

![Home directory backup?](img/backups.png)

I copied the backup and extracted the data, and lo and behold there was an SSH key in there for the user noah.

![Noah's home directory](img/noah-home.png)

Now I can login as noah.

![SSH session as noah](img/noah-shell.png)

### Privilege Escalation - noah > root

First thing I tried was `sudo -l` and there was something there:

![sudo -l](img/sudo-l.png)

So I can run this docker app as root without providing a password. After running the command and adding 'bash' at the end, I am dropped into the docker container as root, where I find the actual webapp that I exploited earlier.

![docker container](img/docker.png)

Poking around in the webapp files, you can find hashes for the admin and noah user, and could try cracking them, but instead it is a good idea to enumerate the docker version running, and see if it is exploitable. `docker --version` gives the version as 18.06.0-ce. Checking searchsploit gives us a hit. This version is vulnerable to CVE-2109-5736, which is a container breakout exploit that allows an attacker to overwrite the `runc` binary on the host from inside the container. The simplest version of this exploit is destructive because it overwrites the files, so I've made backups of the affected files.

I downloaded the exploit from [here](https://github.com/offensive-security/exploitdb-bin-sploits/raw/master/bin-sploits/46359.zip) and made the changes referred to in the readme file, before uploading them to the container.

### Exploit method

Next I have to compile the exploit on the container, then open another SSH connection as noah, and start a netcat listener. When I execute the docker command again, I should have a reverse shell as root on the host.

Had to revert the box as this exploit is unstable, but I was able to get a root shell in the end!

![root shell](img/exploit.png)

## Conclusion

This was a fun box once I wrapped my head around it. I tried this one a while back but got stuck on the JWT part - this was before I had done other boxes that used JWTs and before I found out about [jwt.io](https://jwt.io). Once I got that part figured out, getting a shell was easy.

The privesc was different for me in that I haven't done a container escape exploit before - I had done ones where you are allowed to run any docker app as root, and in those cases you can mount the host drive and get root that way. This version was a bit more complicated and with the exploit I chose, pretty unstable and destructive.

**Steps taken:**

* Enumerate webapp to find vulnerability in JWT/RSA implementation
* Use vuln to forge admin token, log in as admin
* Upload malicious PHP file, get reverse shell
* Move laterally by finding backup with SSH key
* Escalate privs via a Docker escape exploit
