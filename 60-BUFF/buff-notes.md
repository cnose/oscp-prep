# BUFF | 10.10.10.198

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.198
    
### Open Ports and Services

* 8080 - HTTP - Apache 2.4.43 Win64 OpenSSL 1.1.1g PHP 7.4.6
* 7680 - maybe Windows Update Delivery Optimization

## Service Enumeration

Another Windows box running xampp.

### 8080 - HTTP-ALT

It is a website for a gym:

![homepage](img/port-8080.png)

Gobuster is pulling up a lot of directories for me to look through, even though a lot of them deadend, and several throw errors. One such error does helpfully give me the directory path the server is in:

![path](img/upload-msg.png)

I think one significant finding is that there are two versions of this website: one at the web root, and another at `/ex`. And when I checked `/ex/upload.php` the page loaded, unlike the regular version:

![ex/upload.php](img/ex-upload.png)

I fired up BurpSuite to see what happens here, because the upload form is a bit weird - there are two blank fields and I don't know what to put in them, and then a file selector. When I observe the request in Burp, it becomes a little bit clearer:

![burp request](img/burp-upload.png)

It looks like the two fields are (respectively) "ext" for extension, and "name" for ... uh, name. I guess that gives me a better idea of what to put in them.

I also would have found this out if I had just looked at the source:

![ex/upload.php source](img/ex-upload-source.png)

Also it only appears to accept files of the type "image/*", which I might be able to bypass using BurpSuite. I could upload a php file but modify the "Content-Type" to say "image/png" or whatever.

I messed around a bit, but got no response from the server about whether my uploads were actually making it through. So I started doing another gobuster scan on the `/ex` dir and to my surprise it picked up the text file I had attempted to upload:

![hello](img/hello.png)

So it does work! And maybe I can upload a reverse shell or command shell. So far I have gotten limited command execution this way:

![buff shaun](img/code-ex.png)

I replaced the simple "whoami" command with a powershell command, to download and execute a reverse shell script. I got as far as seeing the script get pulled from my webserver, but for some reason the shell would not execute. I have tried several ways of execution using PHP, some of them work and the script gets downloaded, but often not. I did manage to grab the user flag this way though:

![user flag](img/user-flag.png)

Time to rethink. Let's try to download netcat to the box with one command, then execute another calling a reverse connection.

## Foothold via RCE in upload.php

The first step is to upload the simple webshell. First I made to upload a simple text file but intercepted the request in BurpSuite. I modified it by removing all content and replacing it with some simple PHP code. Then I changed the "ext" data to say "php", and made the "Content-Type" say image/jpg.

![Burp edits](img/rce-poc.png)

I sent that request away and the file was saved to `/ex/hello.php`. I could execute commands by appending `?cmd=` to the address bar. Then I used a powershell command in the address bar to download netcat from my machine, which had a server running:

![download command](img/download.png)

Next thing I did was make sure I had a listener running on my machine, ready to catch the upcoming connection.

Then, finally I executed another command via the address bar:

![execute reverse shell](img/execute.png)

And before I knew it, I had a shell on the box!

![shaun shell](img/user-shell.png)

## User Enumeration

Shaun is the only regular user on the box, and he does not have any special privileges. It is a Windows 10 Enterprise with no hotfixes installed, which could indicate I have a kernel-based privesc.

There are some services running that were not visible from outside, including MSRPC, SMB, MYSQL and something on port 8888 (both of which are running on localhost).

![listening ports](img/internal-ports.png)

Port 8888 is running some service called "CloudMe.exe" - I did a searchsploit query for it and it turns out there are several versions of buffer overflow exploits available for it:

![searchsploit](img/searchsploit.png)

I'll take the first one off the list and check it out.

## Privilege Escalation via buffer overflow in CloudMe.exe

![POC buffer overflow](img/buff-POC.png)

This one is pretty straightforward and has basically done all the legwork for us, I just need to replace the payload with my own. It does say the exploit was tested on x86 Windows 10, and the box is x64, so it may not work.

I'll add my own shellcode, one that connects back to me instead of spawning calc:

![msfvenom code](img/shellcode.png)

Another thing I have to do is forward the port that the program is listening on, since I want to run the exploit from my box and it is only listening on localhost. I have enough experience with doing that by now, using chisel.

I use my smbserver to copy over the exe, then I spin up the server instance on my end:

    ./chisel server -p 9999 --reverse
    
Then on the Windows box, I type `chisel.exe client 10.10.14.20:9999 R:8888:localhost:8888`.

Now, I should just be able to run the python code and it should return a shell to me (but I'm not counting on it working the first time - if it doesn't I'll have to reset the box).

![admin shell](img/admin-shell.png)

Wow, that actually worked like a charm. There was zero output from the exploit so I wasn't sure if it worked.

### Flag

![admin flag](img/admin-flag.png)

## Conclusion

I liked this box. I like how I figured out the foothold without consulting searchsploit (I did after and I saw the RCE); it was cool to work things out on my own. And the privesc was cool, though I would have liked to have to debug the application myself, which I know would have been difficult and taken me longer, but it's good practice. Maybe I'll grab the exe and work on it in my spare time.

**Steps taken:**

* Find upload form in kind of broken webpage
* Figure out extension filter bypass to upload simple php command executer
* Use webshell to download netcat & execute reverse shell
* Find app running on non-standard port on localhost
* Exploit buffer overflow vulnerability to get Administrator shell
