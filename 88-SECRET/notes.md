# SECRET | 10.10.11.120

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial IP

### Ports and Services

![nmap](img/nmap.png)

#### 22 - SSH - OpenSSH 8.2p1 Ubuntu

If/when I get valid credentials or a key, I will try them here.

#### 80 - HTTP - nginx 1.18.0 (also running on port 3000)

We have a webpage referring to Documentation of some kind of software. 

![port 80](img/port-80.png)

This page has links to register a user, login, and even download the source code of the application running in the background. In fact there is a lot of information on this page - it tells us a lot about what is running on the backend. It uses nodejs, mongodb, and JWT (Json Web Tokens) to authenticate.

![Page details](img/page-options.png)

Looking through the source code (specifically the js files in the 'routes' dir) I can see that the app is looking for a user with the name "theadmin" and if that matches then you can get access to the `/priv` endpoint. I managed to create a user for myself and then login, using Burp, and it gave me a JSON Web Token:

![create user](img/create-user.png)

![login as new user](img/login.png)

Running through the steps as my user, and trying to access the `/priv` endpoint, I am considered a normal user. However the source code and the documentation leak a lot of info. If I try to register as 'theadmin' and using the example email and password I see that at the very least the account already exists:

![theadmin already exists](img/admin.png)

Trying the password that is in the documentation doesn't work, so there is another way:

![Password is wrong](img/password.png)

Going back to the source code, there is a .env file that contains a secret that is used by 'auth.js' and 'verifytoken.js' to create or verify a token. With that secret I could create my own valid web tokens as any user. The file has the secret scrubbed for security reasons:

![.env with scrubbed secret](img/scrubbed-secret.png)

But there is also a `.git` directory which means there may be a history, and a point at which the secret was still there. `git log` shows there was such a change:

![git log](img/git-log.png)

Using the `git show` command with the commit hash actually shows us the original contents of .env, and the secret is there:

![git show](img/git-show.png)

I now have all I need to forge a valid token. I have been using [this site](https://jwt.io/) to decipher and create JWTs, and now that I have the secret, I can make a couple of changes to my valid token and create one that will allow me to be 'theadmin':

![forged JWT](img/forged.png)

Testing this in Burp actually works!

![valid admin token](img/success.png)

## Exploitation

OK, now that I can forge a valid admin token, I still need to find a vulnerability. So back to the source code of the application. Looking further into the 'private.js' file, there is a `/logs` endpoint that will grab the git logs if you are an admin. But there looks to be a potential command injection vulnerability because it takes user-supplied input and then executes it with 'exec':

![command injection vulnerability](img/vuln.png)

To test, I modified a request to the `/logs` endpoint and tried a basic injection technique - the semicolon. I was able to successfully get injection and view the `/etc/passwd` file:

![Injection POC](img/injection.png)

### Exploit Method

Now, I need to inject a reverse shell command. I had some difficulty with simple one-liners (though I will admit that I did not try them all). What I did have success with was a few commands in succession. A `wget` to download a simple bash script that contained the rev shell command; a `chmod` to make the file executable; then the execution command itself.

![simple script with revshell cmd](img/cmd.png)

Then I started a listener, served the script in a python web server, and executed the following on the server via Burp:

![getting a reverse shell](img/exploit.png)

The server grabbed my script, made it executable, then executed it and I had a shell.

![user shell](img/user-shell.png)

## Privilege Escalation - dasith > root

I am now on the box as the user 'dasith'. In order to grab the root flag I will need to escalate to the root user.

### Further Enumeration

I noticed there was info in the `.viminfo` file so I had a look and saw that there was once another hidden file called `.hidden`.

![viminfo](img/hidden.png)

The file no longer exists, however.

Moving on, I ran linpeas and there were some interesting results.

There is a chance I could get root by using a recent CVE against sudo, but I don't think that is the intended route:

![vulnerable sudo](img/peas1.png)

Linpeas also runs Linux Exploit Suggester, which had some probable suggestions; again, probably will not work but if you were doing a real pentest you would want to check these out:

![Linux Exploit Suggester](img/peas2.png)

And finally, linpeas noticed an SUID binary in the `/opt` directory:

![SUID](img/peas3.png)

Most likely this is the route to root.

### Investigating the Binary

It is called count, and when run it asks for a file. You can choose a file you wouldn't normally have access to, like `/root/.ssh/id_rsa` and it will read it. But it just grabs the stats like number of characters, words, lines. It won't display the contents. Then you can write the results, but it drops root privileges so you can't write anywhere you are not supposed to.

Looking at the code, especially where the binary drops root privs, there is a comment about enabling coredump generation, which is interesting.

![code from the count binary](img/count-code.png)

Apparently, because this flag is enabled, the binary will have the given file open as long as the binary is running. We would just need to grab the PID when we background the process. I can grab the PID by using `ps aux | grep count` and then I can check out the fd (file descriptor) in the `/proc` dir:

![file descriptor](img/fd.png)

Still can't read that file however, because I am trying to read one that doesn't have the correct permissions set. But I can read some files in the root directory using this method, even though I don't have access to it. Below are some files I can read using the binary:

![Readble files in /root](img/readable.png)

### Exploit method

Since I saw one in the user's home dir and read it, let's try reading the contents of `/root/.viminfo` and see if there is anything interesting in there:

![reading the .viminfo file](img/viminfo.png)

Scrolling down I find a private SSH key!

![SSH key in .viminfo](img/ssh.png)

The rest is history.

![root shell](img/root-shell.png)

## Conclusion

This is supposed to be an easy box, and I suppose the JWT stuff at the start is easy if you have worked with them before. Perhaps adding some code analysis makes it a bit more diffcult as well. Once you found the token getting a shell is an easy affair.

The privesc is something I have not dealt with before so I had to use a writeup. I have found the concept of file descriptors in Linux to be mind-boggling so I've avoided learning about them and what they are and what they do. So this was neat.

There is another way to privesc as well, it involves making the program crash which will dump it's memory to a file. Then you would decompress the blob, and you can read it with strings (since the blob is a binary file). In the output will be the contents of the file that you were reading (for example, root's SSH key).

**Steps taken:**

* Enumerate node.js app to figure out how to register
* read git logs to find secret key, forge JWT as admin
* exploit command injection vuln in app to get reverse shell as user
* abuse open file descriptor to read root's viminfo file to find SSH key
* login as root
