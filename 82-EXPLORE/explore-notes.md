# EXPLORE |10.129.133.200

## Enumeration

```bash
sudo nmap -v -T4 -sCV -oN nmap-initial <IP>
sudo nmap -v -T4 -sUV --top-ports=20 -oN nmap-udp <IP>
sudo nmap -v -T4 -sCV -p- -oN nmap-all <IP>
```

### Ports and Services

![nmap all ports 1](img/Pasted%20image%2020210628112334.png)  
![nmap all ports 2](img/Pasted%20image%2020210628112428.png)

#### 2222 - OpenSSH - Banana Studio?

Googled this to find out that it is an Android app, so we are dealing with an Android phone. Probably won't get too far with this as there do not seem to be any known vulns and I don't have any creds yet.

#### 42135 - HTTP - ES File Explorer Name Response httpd

This set off something in me, as I remembered reading about some kind of vulnerability with this app. Searchsploit does not have anything but simply googling "ES File Explorer exploit" gets some hits.

![ES File Explorer Vuln](img/Pasted%20image%2020210628113140.png)

Once the app is launched, it opens an http server on port 59777 (the nmap scan confirms the port is open). I checked it out via a web browser but there was just a simple error page:

![null response](img/Pasted%20image%2020210628113320.png)

(Lately I feel a number of new machines on Hack the Box have been flaky, so if you get an error page, it's a good idea to check the forums or Discord. As it turns out, I had to restart the box in order to move forward - there should not be an error page here.)

That is because in this case you talk to the server by sending it JSON requests, and there is a POC on github [here](https://github.com/fs0c131y/ESFileExplorerOpenPortVuln) that can enable you to gather information about the phone, such as installed apps, files, media, etc. It even allows you to grab files from the phone and launch installed apps.

![POC code snippet](img/Pasted%20image%2020210628113656.png)

Once I had the machine properly running I was able to use the POC to enumerate the device.

![getDeviceInfo](img/Pasted%20image%2020210628115618.png)

If this were a real phone it would tell me the make of it - in this case it is a VM.

I ran through several of the enum options, not finding much, until I tried listing the pictures on the device:

![listPics](img/Pasted%20image%2020210628115824.png)

Obviously a juicy filename. So I grabbed the jpg with the following command:

```bash
python3 poc.py -g /storage/emulated/0/DCIM/creds.jpg --host 10.129.19.119
```

It downloaded the file to my machine.

## Initial Foothold - SSH access via leaked credentials

### Exploit Method

I had a jpg file with the SSH creds written right on it:

![SSH creds](img/Pasted%20image%2020210628120722.png)

I could connect to the SSH server running on port 2222 using these credentials:

![access via SSH](img/Pasted%20image%2020210628120943.png)

I had to do some brief digging around to find the 'user' flag, but it is there:

![user flag](img/Pasted%20image%2020210628121425.png)

## Privilege Escalation - low-priv > root

I am user 'u0_a76', and only able to read from some files and directories. Additionally, using the `id` command I see that I am running in an 'untrusted' context which probably means I can't do anything that nefarious.

### Enumeration

For a joke I tried running linpeas and it told me I was already root, so I will just have to explore until I find something. While looking for `/etc/passwd` (it doesn't exist) I found a script called 'init.sh' so I looked inside. Near the bottom were some iptables rules, and they corresponded with the nmap output from before, which showed port 5555 being filtered:

![iptables rules](img/Pasted%20image%2020210628124738.png)

The 'phone' will only accept connections to that port from localhost. And I am now localhost. But I have no clue what service is running on that port. A quick google search tells me that port 5555 is for ADB - Android device bridge.

### Exploit method

So, taking all this in, we have ADB open on port 5555, but it can only be connected to via localhost. I've already checked and the Android-SDK is not installed on the phone, so that means I'll have to connect using my machine (I had to install it, in my case via the AUR). Starting with a local port forward via SSH.

```bash
ssh -L 5555:localhost:5555 kristi@10.129.19.119 -p2222
```

![ADB connection via port forward](img/Pasted%20image%2020210628131746.png)

Had to look through the contents of `adb --help` in order to find this, but eventually the command `adb -s localhost:5555 shell` got me a shell as 'shell' (adding the `-s localhost:5555` gets me past the error about there being more than one device).

![shell as shell](img/Pasted%20image%2020210628132214.png)

Then I typed `su` and I was root:

![root shell](img/Pasted%20image%2020210628132301.png)

#### root flag

It is located in the `/data` directory:

![root flag](img/Pasted%20image%2020210628132515.png)

## Conclusion

I suppose it's an easy machine, but having little knowledge of Android exploitation it was a bit of a learning curve. If you know what you are looking for you could root this very easily.

**Steps taken:**

* Find that ES Explorer is on the system and port 59777 is open, allowing you to read files on device
* Find SSH credentials stored in the DCIM folder, login via SSH
* Find iptables rules regarding port 5555
* Create local port forward via SSH to circumvent iptables rules
* Connect to device via ADB, get root shell
