# NIBBLES | 10.10.10.75

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial $IP

### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.2ps Ubuntu
* 80 - HTTP - Apache 2.4.18

## Service Enumeration

### SSH

Standard SSH setup; probably won't be able to do anything with this unless/until I find valid credentials.

### HTTP

The page is white, and with simple text at the top left of the page. Hitting `CTRL-u` reveals some information disclosure about a possible directory:

![](img/port-80-source.png)

Navigating to the the page shows it is indeed a blog, and it is super barebones.

![](img/nibbles.png)

Running gobuster gets me some more directories to look at:

    gobuster dir -u http://10.10.10.75/nibbleblog -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt -x php,html,txt

![](img/gobuster.png)

From the README directory I found out that the version of Nibbleblog (yes that's the name) is 4.0.3, which is vulnerable to arbitrary file upload, according to _searchsploit_:

![](img/searchsploit.png)

From the gobuster search I also found admin.php, so I went there and found a login page; on a lark I tried the credentials "admin:nibbles" and I actually was logged in.  
Cool.

## Exploitation

Wanting to avoid Metasploit, I do a simple google search for `nibbleblog 4.0.3 exploits` and I find a python based exploit from [here](https://github.com/TheRealHetfield/exploits/blob/master/nibbleBlog_fileUpload.py).

The exploit requires me to create a reverse perl shell and output it to a simple text file. Then in the python script I need to supply the URL and the login credentials.

![](img/exploit.png)

I make sure to have a netcat listener spun up, to catch the shell:

    rlwrap nc -nvlp 6969

And I run the exploit:

    ./upload.py

![](img/upload-py.png)
![](img/user-shell.png)

## User Enumeration

After getting that shell, I noticed that it was very limited and not that responsive. I decided to use the typical php-reverse-shell that you can find in Kali as the payload:

    cp /usr/share/webshells/php/php-reverse-shell.php ./niblet.txt

I had to strip out the lengthy comments at the start of the file in order to get the shell to upload.

### User flag

![](img/user-flag.png)

### Personal.zip

In the home dir of the user is a zipped file with a couple of directories and a script that appears to be a server monitor. Thing is, it runs as root, and I can write to the directory.

![](img/run-as-root.png)

## Privilege Escalation

So if I create my own script called 'monitor.sh' (one that contains a reverse shell), root will execute it for me and I'll have root access.

    echo "bash -i" > monitor.sh

Now, all that's left to do is to run it as root and I should become root:

    sudo /home/nibbler/personal/stuff/monitor.sh

After a lengthy pause (it appears the sudo command is trying to resolve a hostname?) I have succeeded:

![](img/root-shell.png)

And here is the flag:

![](img/root-flag.png)

## Conclusion

This was a nice and easy box, though I did spend too much time pfaffing around trying to upgrade my crappy shells, sometimes causing it to crash out. Otherwise, this was pretty standard stuff.

Steps taken:

* Find the blog on port 80
* Guess the login and password for the admin area of the blog
* Use an authenticated arbitrary file upload vuln to upload a reverse shell
* Enumerate the user to find a command that the user could run as root
* Modify the script to get a root reverse shell.
