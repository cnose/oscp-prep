# SPECTRA | 10.10.10.229

## Enumeration

    sudo nmap -v -T4 -sCV -oN nmap-initial 10.10.10.229

### Ports and Services

![nmap initial](img/nmap-initial.png)

#### 22 - SSH - OpenSSH 8.1

An up-to-date instance of SSH. If I find credentials I may be able to get a shell this way.

#### 80 - HTTP - nginx 1.17.4

![main page](img/homepage.png)

Here we have a plain looking page, referencing issue tracking, and directing visitors to other pages because Jira is not yet set up.

The "Software Issue Tracker" is in fact just a wordpress blog.

![WordPress](img/wp-page.png)

And the "testing" links to a page displaying a database error:

![Database Error](img/testing-page.png)

#### 3306 - MYSQL

There is a mysql server running on this port, but I am not allowed to connect to it remotely:

![Unauthorized](img/not-allowed.png)

### Enumerating WordPress

A good tool to use for this before I resort to brute-forcing the login is wpscan.

```bash
wpscan --url http://spectra.htb/main -e vp,vt,cb,dbe,u --plugins-detection aggressive --plugins-version-detection aggressive
```

The scan did not indicate much, but it found the username that I might have to bruteforce passwords for:

![WordPress User](img/wp-user.png)

Poking around the defunct `/testing` site, I was able to find a 'wp-config.php.save' file that appeared to contain the credentials to a mysql database:

![Database credentials](img/DB-creds.png)

I tried these credentials on SSH, with both devtest and administrator as usernames, both were rejected. However when I tried the password on the WordPress Login page, I had access:

![WordPress dashboard](img/dashboard.png)

With this kind of access, I should be able to get a shell relatively easily.

## Exploitation via WordPress admin access

If you have administrator access to the WordPress backend, you can then get a reverse shell on the machine by editing accessible PHP files.

### Exploit Method

First thing I tried was editing the 404.php page template - replacing what was there with the typical php-reverse-shell that you find from PentestMonkey, but that did not work at first - I kept getting an error so it would not save the template. But when that happens, it's always a good idea to try the other templates, even ones that are not in use. The twentytwenty template had issues, but it appears the twentyseventeen template could be properly edited and saved. Accessing the shell was another story. So I tried the twentynineteen 404.php page and that is the one that worked.

![Template edit](img/php-shell.png)

I did a curl command to the 404 page and a connection came back to my listener.

```bash
curl http://spectra.htb/main/wp-content/themes/twentynineteen/404.php
```

![nginx shell](img/nginx-shell.png)

## Privilege Escalation - nginx > katie

### Further Enumeration

Gotta say, this is definitely a strange box. I thought I couldn't upgrade my shell as typing `which python` errors out, but I could. The output of `cat /etc/passwd` seems to indicate some weird hybrid system, possibly ChromeOS.

#### Enum with linpeas

At first I couldn't execute linpeas, but realized I had to do `bash linpeas.sh` to get it to run. And the output confirms this is a ChromeOS box. It appears the `/home` directory is mounted on `/mnt` so there is a lot of doubled up output. It was difficult to find initially, but there is a file in the `/opt` folder that describes the autologin feature, and in the file is a hint for where to look for creds:

![autologin.conf.orig](img/autologin-conf.png)

When I go to the location, there is a file to read and within is what looks to be a password:

![autologin creds](img/autologin-creds.png)

I can SSH as the user 'katie' with this password:

![Katie SSH session](img/katie-shell.png)

#### User flag

![user flag](img/user-flag.png)

### Further Enumeration

Time to start from scratch, looking for a way to privesc to root. This user is in the 'developers' group, and seems to have permission over some crucial files, if the extreme colours in the linpeas output are to be believed:

![writeable init files](img/init.png)

Additionally, katie has permission to run `/sbin/initctl` as root, setting an environment variable as well:

![sudo -l](img/sudo-l.png)

I can use these privileges to get a root shell for myself.

### Exploit method

Though I am not exactly sure if this is the correct way (owing to the fact that I do not bother setting an environment variable), I can quickly edit a service conf file (katie has permissions over several) and then start that service using sudo. I will edit the conf file to make a copy of bash, and then make it SUID.

![edited init file](img/init-edit.png)

Then once I've saved that file I have just a few seconds to start the service before it's overwritten again:

```bash
sudo /sbin/initctl start -n test8
```

And I have a root shell once I execute `/bin/koopa -p`:

![root shell](img/root-shell.png)

#### root Flag

![root flag](img/root-flag.png)

## Conclusion

This was an interesting box, as it took a bit of sleuthing to find the 'config.php.save' file on the defunct test site, not to mention it appeared blank at first. Then, the lateral move from nginx to katie was a bit different as there was a lot to sift through owing to the bizarre filesystem setup of ChromeOS. Also there were some small restrictions in place to prevent me from executing binaries in certain part of the filesystem, I guess that is some additonal ChromeOS security. Finally, I'm not sure if the privesc was the intended way as it seems the 'nodetest.js' file was not really used and I kind of think it was supposed to be. I am guessing that you are supposed to set some kind of environment variable which would then help you escalate? I dunno, I'm curious to find out later.

**Steps taken:**

* Find hidden mysql credentials in unused and broken test site, login to main site wordpress with password
* Edit template to inject malicious PHP into 404 page, get reverse shell as nginx user
* Enumerate and find autologin credentials for katie user, login via SSH
* Escalate to root by abusing write permissions to legacy service files.
