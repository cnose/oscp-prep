# READY | 10.10.10.220

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.220

### Ports and Services

![nmap](img/nmap.png)

#### 22 - SSH - OpenSSH 8.2p1 Ubuntu

A standard SSH setup. Will try to use this service if I come across credentials.

#### 5080 - HTTP - nginx

A web page, which when navigated to is a GitLab instance with a 502 error. The nmap script gives us a bit more info as it found a `robots.txt` file.

![robots.txt](img/robots.png)

If I try to go to any of the pages that are disallowed, I am redirected to the sign-in page. Si I create a new account and see if I can poke around a bit.

I am able to find an existing project as a signed in user:

![GitLab project](img/projects.png)

The project appears to be for a website using Drupal as a backend. Poking around a bit I find database credentials, but right now there is no way to connect to the database:

![DB creds](img/db-creds.png)

Going to the help page lets me know that GitLab should be updated as soon as possible:

![Out of date GitLab version](img/version.png)

Searchsploit has two authenticated RCE exploits, so those are worth taking a look at.

## Exploitation

This version of GitLab is vulnerable to two different CVEs, which when chained together make the authenticated RCE. The first is a SSRF (CVE-2018-19571), in which an attacker can trick the application into making requests on their behalf. On the Import Project page, you can import a new repo by URL - it is here that GitLab will reach out to that URL, which in this case can be an internal URL for the server itself.

The second vulnerability is a CRLF Injection (CVE-2018-19585), which allows an attacker to add newlines into the URL, in which you can add a reverse shell command.

### Exploit Method

Most of the exploits available were using a netcat reverse shell, which did not call back for me, so I found an upgraded one which allowed me to select a shell language, from [here](https://github.com/Algafix/gitlab-RCE-11.4.7/blob/main/rce_script.py). I got my connect back using a bash reverse shell:

![shell as git user](img/shell.png)

The user flag was in dude's home directory.

## Privilege Escalation - Container Escape

### Further Enumeration

It turns out, I am inside a docker container. A quick way to find that out is to just `ls -la` the root dir, if there is a `.dockerenv` file there, then you are inside a docker container.

In doing so, I also noticed a file called `root_pass` there, so I tried to `su -` with it, but it is not the password.

Moving on to the `/opt/gitlab` directory, there is a `docker_compose.yml` file, which shows me that the root_pass is for the initial configuration, and not for the root user in the container. Good to know. Looking at the `gitlab_secrets.json` file there are a lot of keys, but in this context not useful.

So I grepped the `gitlab.rb` file for 'password' and found one, in this case the 'smtp_password':

![grepping gitlab.rb](img/gitlab-rb.png)

I tried that credential for the root user in the container, and luckily it did work.

![shell as root in container](img/docker-root.png)

So now we own the container, but we still need to break out somehow. Let's go back to the `docker_compose.yml` file. It shows that this container is run in 'privileged' mode, which disables all sorts of security protections:

![running in privileged mode](img/privileged.png)

There are a lot of things one can do while inside a privileged container, read about them [here](https://book.hacktricks.xyz/linux-unix/privilege-escalation/docker-breakout/docker-privileged). I am only interested in escaping the container and getting to the host filesystem.

### Exploit method

Checking the output of `lsblk` I see that `/dev/sda2` seems to be the main disk. So all I have to do is mount that:

    mount /dev/sda2 /mnt

And now I have complete access to the entire host filesystem, effectively root.

![root access via container escape](img/root.png)

## Conclusion

I suppose one could call this an easy medium box, as very basic enumeration can get you shell access, and then escalating is a matter of following some simple breadcrumbs. It is notable that there is more than one way to get root on the host, and you can look into [0xdf's writeup](https://0xdf.gitlab.io/2021/05/17/digging-into-cgroups.html) for an in-depth way to get root that is different from simply mounting the host disk. But, low-hanging fruit and all that.

**Steps taken:**

* Exploit vulnerable version of GitLab to get reverse shell as git user
* Escalate to root in container via reused credentials in gitlab deploy script
* Escape container by mounting host filesystem due to use of the `--privileged` flag
