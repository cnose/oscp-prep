# OPENADMIN | 10.10.10.171

## Initial Enumeration - nmap scan

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.171

### Open Ports and their Services

* 22 - SSH - OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
* 80 - HTTP - Apache httpd 2.4.29 ((Ubuntu))

### OS Discovery

Something tells me it's running Ubuntu...

## Service Enumeration

### 22 - SSH

Probably not going to get anywhere here without some credentials, as SSH typically does not have the kind of vulnerabilities that grant you access. So, on to the next service.

### 80 - HTTP

Right away I was presented with the default Apache start page:

![](img/port-80.png)

So that tells me some things:

1. The admin is lazy and/or careless
2. I should look for more directories

So I fired up gobuster to check for any dirs:

    gobuster dir -u http://10.10.10.171 -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt

And right away I got a hit:

![](img/music.png)

On that page was a login link, which linked to a directory called "/ona", so I clicked on that and ended up at an OpenNetAdmin page:

![](img/openadmin.png)

It also helpfully lets me know the version (18.1.1), so I can research this service to see if there are any vulnerabilities I could exploit to gain a shell.

![](img/searchsploit.png)

## Exploitation

So, there is a RCE available for this service; let's examine it:

    searchsploit -x php/webapps/47691.sh

![](img/rce-info.png)

Looks like I need to supply the IP of the machine as an argument, and then it is supposed to result in a shell (a way better explanation is [here](https://zacheller.dev/open-net-admin)).

    ./rce.sh http://10.10.10.171/ona

I ended up with what appeared to be a shell prompt, but no matter what I typed, there was no output, so I was on the hunt for another version of this exploit, non-Metasploit based.

![](img/sh-broken.png)

After a search I found [this](https://github.com/amriunix/ona-rce/blob/master/ona-rce.py) python-based exploit, that also has a way to check if the target is vulnerable.

    ./rce.py check http://10.10.10.171/ona

![](img/vulnerable.png)

    ./rce.py exploit http://10.10.10.171/ona

![](img/shell.png)

## User Enumeration

Although I have a shell, it appears to be quite limited: I can't run some commands (or there is no output from them), I can't change directories, I can't do that much.  
So I will have to be creative, and go through a lot of manual enumeration to find a way to privesc.

### Hostname

openadmin

### uname -a

Linux openadmin 4.15.0-70-generic #79-Ubuntu SMP Tue Nov 12 10:36:11 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux

So now I know the kernel version and architecture of the host. Time to check for files the www-data user has access to:

    find / -type f -user www-data

This spit out a lot, but one thing that stood out was a list of possible users:

![](img/users.png)

I confirmed this by cat-ing out the passwd file as well.

### netstat -tulpn

The output of this command shows me there is a DB instance running on port 3306 locally:

![](img/netstat.png)

### Reading files

Even though my user cannot navigate out of the current dir, they can still `ls` to check out the filesystem, and can also `cat` out files.

So I've been doing a lot of that, to start with in the `pwd`, just looking for clues, and I've run into some config files for ldap. After running

    cat config/auth_ldap.config.php

I was able to find a password = 'mysecretbindpassword'

![](img/ldap-pass.png)

Additionally, I was able to find another password, this time for the local instance of mysqli:

![](img/db-settings.png)

So, now I have two passwords. Looks like I need to try to connect to the database, or I can try using these passwords with the usernames I found above to log into SSH.

### Logging into SSH

I tried several times to connect to the local database using the credentials I found, to no avail. However, I was able to use the password 'n1nj4W4rri0R!' to log into the user jimmy's SSH, so now I have a real, stable user shell:

![](img/jimmy-shell.png)

After a lot of enumerating, I went through all these things that stood out a little:

* cron
* mysql again
* .gnupg
* /var/www/internal/

And it was this last thing that stuck out a lot. It appears to be a webpage, running internally (maybe on that other port from the netstat command, 52846).

The index.php page has a login form that looks for the correct credentials from jimmy only:

![](img/var-www-internal-index.png)

And the main.php page has a message from joanna to jimmy:

![](img/var-www-internal-main.png)

It appears that if the user jimmy successfully authenticates, joanna's ssh key will be output. 

I wanted to make sure the SHA512 hash in the php file was the same as jimmy's current password, so I tried to crack it in hashcat after adding his 'n1nj4W4rri0R!' password to the rockyou wordlist:

    hashcat -m 1700 jimmyhash ~/Documents/rockyou.txt -O

And I got nothing. So at least I know that the password for this internal site is different. Time to head to [Crackstation](https://crackstation.net):

![](img/revealed.png)

Nice!

### Making a POST request with cURL

I don't use cURL often, so I had to google "how to post using curl" and I found a resource [here](https://gist.github.com/subfuzion/08c5d85437d5d4f00e58) that gave me a starting point in regards to syntax.

    curl -X POST localhost:52846 -d "login&username=jimmy&password=Revealed" -L -v

This command makes a POST request to the page, -d is the data I had to send with the correct parameters and values, and -L tells cURL to follow the redirect to "index.php".

The login seemed to have succeeded, but the redirect didn't happen. I did manage to grab the session cookie though - I think I just need to pass that along to the main.php page with another cURL command.

![](img/cookie.png)

OK, so now I have to make a request to the main.php page, using the cookie as the header:

    curl localhost:52846/main.php -H "PHPSESSID=hfaad7ie4vh7svthragdquj1aj" -v

And I was greeted by the ssh key of joanna!

![](img/key.png)

Trying to log in with the key asked for a passphrase, so I used ssh2john to convert the key to a crackable hash, then used john to crack it:
    
    /usr/bin/john/ssh2john.py joanna-key > jk
    john --wordlist=/usr/share/wordlists/rockyou.txt jk

Now I have the passphrase for joanna's key:

![](img/john.png)

Logging in as joanna presents no problems:

![](img/joanna-shell.png)

And right away I grab the flag because it's been so long:

![](img/user-flag.png)

## Privilege Escalation

Pretty much as soon as I logged in as joanna I did a `sudo -l` to see if there was any privesc vector:

![](img/sudo-l.png)

Looks like I can escalate using nano. I quickly check [GTFObins](https://gtfobins.github.io/gtfobins/nano/) to see what I can do with nano:

![](img/gtfo-nano.png)

So if I open the /opt/priv file with nano using sudo, I can then hit CTRL-R to get into a menu to insert a file, then CTRL-X to be able to execute a command, then I type

    reset; bash 1>&0 2>&0

And suddenly I am the root user!

![](img/root-shell.png)

The final thing to do in this case is to grab the root flag:

![](img/root-flag.png)

## Conclusion

This was a bit of a slog for me, perhaps I am rusty at Linux exploitation since I've just been doing Windows machines for a while. But it helped me learn to look deeper and keep my eyes open for any anomalies. I also got to learn more about making http requests using cURL, so that is a plus.

Steps taken to root:

* Discover thru dir fuzzing an admin panel that was vulnerable to remote command execution
* Gain a very limited shell that allowed some limited enumeration
* Find plaintext passwords in DB files that user had access to
* Use one of the passwords to log in as a regular user through SSH
* Discover an internal website, visible only locally, that responds to http requests
* Crack a SHA512 hash to discover the correct password
* Make a POST request with cURL in order to grab a session cookie
* Use the session cookie in another POST request to display another user's private SSH key
* Crack the passphrase on the key and use it to login as them in SSH
* Exploit the root privs given to user to run nano and escape into a root shell.
