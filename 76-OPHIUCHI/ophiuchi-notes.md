# OPHIUCHI | 10.10.10.227

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.227

### Ports and Services

![nmap scan](img/nmap.png)

#### 22 - SSH - OpenSSH 8.2p1

A standard up-to-date SSH setup. I'll come back here if I get creds.

#### 8080 - HTTP - Apache Tomcat 9.0.38

![homepage](img/homepage.png)

It is a page that can parse yaml, which is a markup language. When I input most things, it takes me to another page that says the feature has been disabled due to security issues:

![feature on hold](img/servlet.png)

That is a big clue that there could be some vulnerability that I could take advantage of.

I messed around with different characters, trying to break the page and I managed to do it using just an asterisk:

![error page](img/error.png)

This leaks some useful info:

* The server is Apache Tomcat 9.0.38 (although the nmap scan revealed this earlier)
* The implementation of yaml being run is called snakeyaml
* The page uses java somehow

Searching for snakeyaml vulns does not have too much - one is a denial of service vulnerabilitiy that is obviously out of scope. However I find some information regarding java deserialization attacks which reference snakeyaml and resulting in remote code execution.

After lots of researching I found [this page](https://github.com/artsploit/yaml-payload) that has a yaml payload for snakeyaml. I cloned the repo and followed the instructions in the README, replacing the commands in the .java file with a simple ping command to my machine.

![ping command](img/ping-command.png)

I compiled it according to the instructions, served the file, and then pasted the java command in the parser, which resulted in a 500 error kind of like before, but with more to read.

![another 500 error](img/another-error.png)

Interestingly, the command I pasted in did work, as my server had contact with the target. This error was so long I read it in full, and found my problem:

![different versions](img/info.png)

I had to compile the script for an older version of the Java Runtime. A quick google search and I figured out how to do so:

```bash
javac --release 11 src/artsploit/AwesomeScriptEngineFactory.java
```

This compiled it for a version that was compatible with the target. I ran everything again, and made sure to run tcpdump to detect incoming ping (`sudo tcpdump -i tun0 -n icmp`). This resulted in a pingback!

![ping](img/icmp.png)

I now have some kind of command execution.

## Exploitation via java deserialization in snakeyaml

### Exploit Method

I tried a lot of ways to get a reverse shell using the payload, to no avail. Mind you, I did not try ALL of the possible ways, but I ended up moving on.

First off, I discovered I could use wget to upload data to my waiting netcat listener. The first thing I did was grab `/etc/passwd`, so I ended up learning about what users were on the box.

![/etc/passwd](img/etc-passwd.png)

This gave me the idea to try to grab the user 'admin' SSH keys, but whatever user I was running as did not have access to that directory (or the user did not use keys). So I moved on to the 'tomcat' user, or rather, to grabbing credentials from the `/opt/tomcat/conf/tomcat-users.xml` file. This was fresh in my mind as I had just done another box that referenced the file location. The credentials are stored in plain text. I edited the .java file to upload the file to my waiting netcat:

![wget command](img/command.png)

Served the file and bam, I had the tomcat file:

![tomcat credentials](img/tomcat.png)

These credentials work on the tomcat manager application login. But they also work for SSH:

![SSH session as admin](img/admin-shell.png)

#### Flag

![user flag](img/user-flag.png)

## Privilege Escalation - admin > root

`sudo -l`  
![sudo -l](img/sudo-l.png)

`/opt` folder  
![/opt](img/opt-folder.png)

So, this user can run that specific go command on that specific go file located in that specific directory. Lets have a look at the file:

![index.go](img/index-go.png)

Running the allowed command outputs 'Not ready to deploy' and exits. In the code we see that it reads in the "main.wasm" file, checks the result of the "info" export, and if it is not equal to 1, it prints "not ready" and exits. So, somehow I have to make the result of the info export be 1. But I also have to write into the deploy.sh file that is there.

### Exploitation - relative path abuse

So it took me a long time to realize that the "main.wasm" and "deploy.sh" files do not use their full paths, which means I can create these files elsewhere and control what they contain. I spent a lot of time trying to figure out how to compile and build a different "main.wasm" file but had no luck. Eventually I found out from [here](https://developer.mozilla.org/en-US/docs/WebAssembly/Text_format_to_wasm) about a tool that can convert wasm files to the actually readable (and writable) wat format. I installed the suite and transferred the main.wasm file to my machine. Converting was as simple as typing `wasm2wat main.wasm > main.wat`. Now I can edit the file:

![viewing the wasm file](img/wat0.png)

I simply had to change that 0 to a 1:

![edited file](img/wat1.png)

Then I sent the file over to the target machine, to `/dev/shm`. In that directory, I created the "deploy.sh" file using vim, first having it echo `whoami` just to test if I got this right:

![testing the privesc](img/deploy-1.png)

Then I ran the command with sudo and it worked:

![success](img/root.png)

OK, at this point I simply thought I could have it execute a reverse shell for me, and again I tried a few (but not all) methods, with no success. I also tried grabbing root's SSH key but it didn't exist. So finally I hit upon echoing my public SSH key into `/root/.ssh/authorized_keys` and then just logging in via SSH. This worked.

The deploy.sh file:

![deploy.sh](img/ssh-key.png)

This time, running the command added my key to the right place, and I was able to log in as root using SSH:

![root shell](img/root-shell.png)

#### root flag

![root flag](img/root-flag.png)

## Conclusion

This entire box was a learning process for me. I started it as soon as it was released, hit a wall, and stepped away for a few weeks. I came back determined to figure out the foothold and everntually worked my way in. I may have just skipped the initial foothold (shell as tomcat user) and ended up getting right to the admin user, but that was because I couldn't get any reverse shells to work so I had to try something else. Getting root was very difficult for me, because I failed to notice the use of relative paths in the go script that is run as root. Once I realized that, it was a matter of finding the right tool to edit a binary file.

**Steps taken:**

* Enumerate yaml parser until it leaked data about snakeyaml implementation
* Research java deserialization, find payload generator for snakeyaml, get RCE
* Use payload to access tomcat-users.xml file to find credentials for admin user, log in via SSH
* Escalate privileges to root by abusing relative paths in root-owned go script.
