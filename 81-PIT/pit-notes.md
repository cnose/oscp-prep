# PIT | 10.10.10.241

## Enumeration

    sudo nmap -v -T4 -sCV -oN nmap-initial 10.129.108.101

### Ports and Services

![nmap initial](img/0-enum-nmap-initial.png)

#### 22 - SSH - OpenSSH 8.0

Your typical SSH setup. Useless right now unless I get valid credentials.

#### 80 - HTTP - nginx 1.14.1

The default homepage once the web server is up and running. It leaks some useful infomation about the default locations of some files, as well as the underlying OS.

![default web page](img/0-enum-port80-homepage.png)

Running gobuster on this comes up with no useful hits. So let's check out the other port, which also happens to be running a webserver.

#### 9090 - HTTPS

![Port 9090](img/0-enum-port9090-login-page.png)

nmap kinda choked on this one, but looking at the source reveals it is a login screen for something called "Cockpit", which I guess tracks with the name of the box.

![Port 9090 source](img/0-enum-port9090-source.png)

The nmap scan did pick up another subdomain though (dms-pit.htb), so I've added that along with 'pit.htb' to my `/etc/hosts` file. Trying to access 'dms-pit.htb' on port 80 returns a 403 error, which means there is something there.

Checking searchsploit indicates there may be either an SSRF (Server Side Request Forgery) or RCE (Remote Code Execution) vulnerability here.

![searchsploit cockpit](img/0-enum-ss-cockpit.png)

I just completed a HTB machine that had an SSRF vuln on it, so I hope that helps me a bit. The first two, at least in their current form, don't seem to be an option because they are referring to Cockpit running PHP code, which in this case is not what the box is running. The third one, the unathenticated SSRF, may be what I need, so I've copied it over to my working directory to look at it more closely.

![SSRF exploit code snippet](img/0-exploit-ssrf-snippet.png)

This exploit allows an attacker to scan for internally open ports on a server, but really focussing on SSH. I ran this but received strange output. Namely, since I knew 22 was open, at least for me, the result I received was just an error for 22, but I got correct output for other ports. However, it does not look like this exploit is going to work for my purposes.

### UDP Scan with nmap

I ran into a wall so had to go back to the basics. When all else fails, it is a good idea to remember that other protocol, UDP. So I ran a UDP scan to see if maybe port 161 was open, which can sometimes be used to gather more info about internal services:

```bash
sudo nmap -v -T4 -sU -sV --top-ports=20 pit.htb
```
I got lucky:

![nmap UDP scan results](img/0-enum-nmap-udp.png)

#### 161 - SNMP

Now this was a big challenge for me. I spent too many hours looking at the same output, thinking I just couldn't see what was supposed to be right in front of me. It turns out that I wasn't making the correct query because I misunderstood how SNMP works. I had to get a hint from a very helpful user in the Hack the Box Discord.

Now to properly understand how to enumerate SNMP you need to understand OIDs (Object IDentifiers), and how they are arranged hierarchically. They are setup as a kind of tree.

![OID Tree](https://gblobscdn.gitbook.com/assets%2F-L_2uGJGU7AVNRcqRvEi%2F-MAwjRVVe_fE3LEMWuKt%2F-MAx-w1fbcPOpLV-IoIz%2FSNMP_OID_MIB_Tree.png)

I looked through the 'snmpwalk' man page, and consulted the help section to see how to make the best query I could, but I failed to realize I could append the proper OID at the end of the query, and in failing to do so, snmpwalk defaulted to making only a certain type of query. If you look at the numbers in brackets in the tree diagram, if you don't append the OID yourself, snmpwalk will default to '1.3.6.1.1', which is taking the tree on the left.

The left side can be thought of as basic system info, like ip related stuff, interfaces and directories. I was able to see all running processes this way, and sometimes you might find plaintext credentials there.

![snmpwalk output with default query](img/0-enum-snmpwalk-default.png)

That is not where I want to go in this case. Sometimes there is relevant info there, but not this time. To take the other side of the tree, namely the 'private' side, you have to use the right OID, like so:

```bash
snmpwalk -v 2c -c public 10.129.108.125 1.3.6.1.4.1
```
This will query other devices on the system, ones made by private companies and not under the purview of government.

From that command I received totally different output, and there were two things that I pulled from it that helped me move forward. The first was a path:

![snmpwalk query](img/0-enum-snmp-seeddms.png)

The second was a username:

![a new username](img/0-enum-username.png)

First I wasn't sure what to do with this new info, and how it related to the Cockpit service on port 9090, but then I looked back at my notes and remembered that I noted that although 'https://dms-pit.htb' resolves to the Cockpit login page, the 'http' version just gives me a 403 error, which means there is something there, I just don't have permission to access it that way. So I appended `/seeddms51x/seeddms` to `http://dms-pit.htb` and suddenly I was presented with a different login screen:

![seeddms login page](img/0-enum-seeddms-login.png)

Without knowing what else to do, I tried a username and password of 'michelle' and gained access:

![seeddms dashboard](img/0-enum-seeddms-dashboard.png)

Now I'm finally getting somewhere.

Inside is a note from the Administrator about upgrading the version due to security issues:

![Admin note](img/0-enum-security-note.png)

Which is helpful, so I can know what version I am dealing with. A check of searchsploit shows that previous versions indeed had an RCE, but that one ostensibly will not work on this one.

![searchsploit seeddms](img/0-enum-ss-seeddms.png)

The XSS one for version 5.1.18 does not appear to work either, or at least on this machine the ability to add an event does not work.

I decided to check out the RCE anyway as I was stuck. It is pretty basic in that you can upload a malicious PHP file, and just navigate to the file location and it should execute.

![RCE seeddms](img/0-exploit-rce-snippet.png)

From a previous engagement I chose a PHP webshell that allows me to put commands into a form rather than just appending the commands to the URL. The file uploaded just fine, but I had difficulty finding the path - the exploit note mentions a path of `/data/1048576/` but I am not finding these at `/seeddms51x/seeddms` which is where I assume they would be.

![Uploaded webshell.php](img/1-exploit-webshell-upload.png)

After a lot of trial and error, I find the path at `/seeddms51x/data/1048576/`, and my webshell is located at `<ID>/1.php`:

![Webshell location](img/1-exploit-whoami.png)

So I have code execution, finally.

## Exploitation - console webshell in Cockpit

So I have code execution, but it is pretty limited. I can navigate the filesystem and cat out some files, but I tried every way I knew how to get a reverse shell and none of them worked. So I figured I had to look for credentials to move forward.

### Exploit Method

From my research on seeddms, I know that there is a config file called 'settings.xml' that could have credentials in it, so I look for that. The webshell.php file gets deleted after a few minutes, and I have to keep re-uploading it, so while I look around that is a bit of an annoyance, but eventually I find the file I am looking for:

![mysql creds in settings.xml](img/1-enum-mysql-creds.png)

I tried this on SSH as michelle and was denied, so the next place I tried was the cockpit login page and I had access:

![Cockpit dashboard](img/1-enum-cockpit-dashboard.png)

Even better, this thing has a terminal in it:

![Cockpit terminal](img/1-cockpit-terminal.png)

#### user flag

![user flag](img/1-proof-user-flag.png)

## Privilege Escalation - michelle > root

The box does not have python installed, so I think I will just stay in the cockpit terminal and not try to get my own shell here.

### Enumeration

This machine has very different permissions, and my user is not able to do a lot of things. For example, to execute scripts I need to prepend `bash` in front of the script. But I was able to execute linpeas, and there is an interesting finding.

#### ACL on /usr/local/monitoring

![ACL](img/2-enum-linpeas-acls.png)

This user has the curious ability to write and execute scripts in this folder, but will not be able to read or delete anything in that folder. Curious indeed. Also interesting is that the folder name seems to correspond with certain output from the snmpwalk query.

![monitor script from snmpwalk](img/0-enum-snmp-monitor.png)

I am able to see the contents for this monitor script, but not write to it:

![monitor script](img/2-enum-monitor-script.png)

So it is simple. It checks the `/usr/local/monitoring/` dir for any scripts that have the name "check" in them, that end in "sh" and it just runs them. As root. Should be simple to exploit, right?

### Exploit method

I wanted to go my usual route, which is copying the bash binary somewhere and making it SUID so I could then execute it to become root, like so:

![exploit payload](img/3-exploit-payload.png)

But it just wouldn't work - I kept getting permission denied errors:

![permission denied](img/3-exploit-error.png)

I don't know why that occurs because isn't the script running as root? If there is some explanation as to why I am not allowed to do those things, namely copying and chmoding, I'd like to know. Anyway, I had to fall back on another way, one I don't like doing much, but it does work in this case, and that is echoing my public ssh key into `/root/.ssh/authorized_keys` and then logging in via SSH as root.

![echo command](img/3-exploit-ssh.png)

Then I had to run the snmpwalk query again, because that is how the monitor script gets executed as root. There was no suspect output so I quickly tried to SSH in as root:

![root shell](img/3-proof-root-shell.png)

#### root flag

![root flag](img/3-proof-root-flag.png)

## Conclusion

What a brainbuster. I needed a lot of clues and research and forum help to get this done. And it was very frustrating at parts. Because of this box I am going to add UDP scanning as a part of my typical nmap stuff. But I am glad to have been forced to learn more techniques, as well as to try different paths from typical, and to not always believe the Administrator when they say they have updated a webapp that is full of vulnerabilities. Definitely the hardest machine I've ever done.

**Steps taken:**

* Hit a wall in enumeration until doing a UDP scan to see snmp open
* Enumerate snmp, hit a wall
* Get a hint from discord to change my snmp query, find new www path and user
* Login to webapp with user creds
* Upload webshell to enumerate more, find mysql creds in settings file
* Use mysql password to login to cockpit, enumerate even more
* Trigger privesc with snmp query after creating SSH key echo script, become root.
