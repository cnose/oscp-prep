# LIGHTWEIGHT | 10.10.10.119

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.119

### Ports and Services

![nmap scan](img/nmap.png)

#### 22 - SSH - OpenSSH 7.4

With this service I usually say that if I get valid creds I'll come back and login, but after browsing the webpage it has told me that as soon as I connected to the IP I was given a username and password, consisting of my IP address.

![account info](img/account.png)

Sure enough, when I tried to login, I got an SSH session:

![ssh session](img/ssh.png)
#### 80 - HTTP - Apache httpd 3.4.6 CentOS OpenSSL/1.0.2k-fips mod_fcgid/2.3.9 PHP/5.4.16)

This looks like it is a simple website dedicated to some kind of plugin for a CMS, a fancy slider doohickey:

![homepage](img/homepage.png)

However, upon looking closer it appears to be something else (something approaching a CTF - ugh).

![info page](img/info.png)

And they aren't joking, because I tried to run gobuster and my IP was banned pretty much instantly, for 5 minutes. So I won't try that again.

But anyway, I have access via SSH, albeit very limited.

#### 389 - LDAP - OpenLDAP 2.2.X - 2.3.X


    nmap -n -sV -p389 --script "ldap* and not brute" 10.10.10.119

This actually got me a lot of info, including what looks to be the encrypted credentials of the other two "users" on the system:

![shadow passwords](img/shadowpasswords.png)

I grabbed those hashed creds and tried to crack them in my hashcat rig, but there were no hits.

### Further Enumeration on the Box

I ran an enumeration script, which noted that I could use tcpdump on the system - which is unusual.

![tcpdump allowed](img/tcpdump.png)

Combine that with the fact that when I first started connecting to the box that a user was created for me, there has got to be some kind of network activity going on.
## Exploitation - cleartext LDAP authentication

This box is only running LDAP on port 389 - which is the non-encrypted version. If it were running the secure server, it would be running on port 626. This means maybe we can grab creds since we have access to the machine and the ability to use tcpdump.
### Using tcpdump to capture packets on localhost

My networking bonafides are definitely lacking - I can't say I have ever used tcpdump. I've used wireshark in the past, but this will be a first for me.

I want to capture packets on the system itself, for tcp, and for port 389 only. I also want to print them out in something readable. So this is the command I came up with after some trial and error:

    tcpdump -i lo -Ann 'port 389'

I had to go to the reset page (`/reset.php`) and then wait a bit, but once I clicked over to the `/status.php` page I received something as output here. Mostly gibberish, but there was one thing that stood out:


That looks like a hash, but since this transaction takes place in the clear, and since with the previous LDAP enumeration I saw that the password is actually SHA-512 encrypted, this alphanumeric sequence is probably the actual password. Let's try `su - ldapuser2`:

![ldapuser2 shell](img/ldapuser2-shell.png)

The flag is in this user's directory, so that's good:

![user flag](img/user-flag.png)

## Privilege Escalation - ldapuser2 > ldapuser1

This user has a backup file in their home folder, owned by root, but readable by us.

![backup in home folder](img/backup.png)


I know what I have to do next. I need to use `7z2john` to get a hash out of the file, and then crack it. But my 7z2john is broken because I am lacking a specific perl module, which happens to be orphaned in the AUR at the moment. I'll install it to see if it works, and hope I don't break anything.

![hash](img/hash.png)

It worked, but I'm not happy about it. (Ok I am.)

Now let's (hopefully) crack it in john - `john --wordlist=rockyou.txt hash`:

![backup.7z password](img/password.png)

For a moment there it seemed like it wasn't going to find it, but it worked out.

OK, I now have all the files from the backup on my box:

![backup files](img/backup-files.png)

Time to take a look at them.
![ldapuser1 creds](img/ldap1-creds.png)

The relevant code was in the `/status.php` file.

### Exploit method

A simple `su - ldapuser 1` gets me a shell as that user:

![ldapuser1 shell](img/ldap1-shell.png)

## Privilege Escalation - ldapuser1 > root

### Enumeration

This user has the binaries `openssl` and `tcpdump` in their home folder, which is strange to say the least. Both of these are binaries with what are called 'capabilities', some of which may be leverageable for a privesc.

!["capable" binaries](img/getcaps.png)

For example, normally opening a network socket requires root permissions, but we were able to use tcpdump because it has the 'cap_net_admin' and 'cap_net_raw' flags - meaning it runs as root with the ability to open sockets.

Now, the openssl binary in the home folder here has the "empty" capability called '=ep', which effectively means it has all capabilities. This is dangerous and most likely how I can privesc.

![empty capability](img/empty-cap-openssl.png)

### Exploitation - Reverse shell with openssl? Nope.

OK. So I have openssl with full capabilities, which means it runs as root, and should be able to do setuid (and setgid) as 0 (root). I just need to figure out how.


Then I found a [blog post](https://int0x33.medium.com/day-44-linux-capabilities-privilege-escalation-via-openssl-with-selinux-enabled-and-enforced-74d2bec02099) that seemed right up my alley, and I followed things to the T, but it also didn't work because the binary is in the user's home dir and it needed to be run from the `/` dir for the exploit to work. It kept giving me an error if I tried to read files from outside the user's home dir.

I had to settle with using openssl to overwrite the `/etc/shadow` file with a blank root password.

First, I used openssl from the home dir to copy the shadow file:

    ./openssl base64 -in /etc/shadow | base64 -d > /dev/shm/s

Then I just deleted the password hash from the root entry. A better idea would be to generate a brand new hash to go in its place. But I am lazy.

Then I ran another openssl command to replace the `/etc/shadow` with mine:

    ./openssl base64 -in /dev/shm/s | base64 -d > /etc/shadow

Got a 'permission denied' with that one.

Instead I have to cat out the new file, pipe that into base64, then into openssl and use it's decrypting flag to directly decrypt and write the file, as that previous command calls the base64 binary as my user.

    cat /dev/shm/s | base64 | ./openssl enc -d -base64 -out /etc/shadow

Once I got that right, I just did `su -` and I was root:

![root shell](img/root-shell.png)

#### Flag

![root flag](img/root-flag.png)

## Conclusion

Definitely an interesting box. It started a bit CTF-like to me, because of the arbitrary nature of the website (creating an SSH user for you, 5 minute ban etc) but it was a good setup to helping you understand how to use tcpdump to grab cleartext LDAP communication. And maybe I don't fully grasp the box, because having a user have a special, full-capability version of openssl in their home directory seems a bit far-fetched. And I was disappointed I couldn't use it to get a reverse shell nor to make /bin/bash an SUID binary. I don't like overwriting sensitive files at all. Perhaps the idea was to also use openssl to create a new shadow password rather than just removing the one that existed.

**Steps taken:**

* Use free SSH session to access box
* Use tcpdump to sniff cleartext credentials for ldapuser2
* Find credentials for ldapuser1 in encrypted backup in home folder, crack pw for backup
* Leverage openssl with empty capabilities in home folder to modify `/etc/shadow` file, become root
