# POPCORN | 10.10.10.6

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.6

### Open Ports and Their Services

* 22 - SSH - OpenSSH 5.1p1 Debian
* 80 - HTTP - Apache 2.2.12 (Ubuntu)

## Service Enumeration

### 22 - SSH

This version is vulnerable to some DOS and username enumeration attacks, but we're probably looking for an exploit on anything running on port 80, so lets go there.

### 80 - HTTP

Upon navigating to the main page we get a generic page declaring the server is running:

![](img/port-80.png)

I have a nikto scan running and it gives me some interesting info, potentially one way to gain access:

![](img/nikto.png)

I also have a gobuster scan running and it has helped me find some directories, including one disclosing a lot of info about the server tools running (apache, php etc):

![](img/phpinfo.png) 

In that scan I think I have most likely found the route I need to take - via a torrent hosting site:

![](img/torrent.png)

I also run a gobuster on this particular dir and find a lot of other directories, some behind a login page - however there is one that is very interesting:

![](img/database.png)

In this file appears to be some encrypted credentials for the admin user:

![](img/dump.png)

I also find a readme file that reveals the default credentials:

![](img/readme.png)

Running the hashed password from the dump through hashcat reveals the password to be the default cred:

![](img/hashcat.png)

However, this password does not work for the website nor for SSH, so I'm looking for other ways to get in.

I haven't created an account yet, so I go and do that, which allows me to upload a torrent file. This reminded me to check searchsploit for a vulnerability and there was one called "Remount Upload", which looks to be some kind of upload vuln. Maybe I can upload a shell?
 
![](img/searchsploit.png)

The text file is pretty short on details, but it seems to imply you can upload a reverse shell (probably php) and then trigger it for pwnage.

So, what follows is my misadventures of trying to figure it out.

## Exploitation

I've decided that I need to upload a reverse php shell, so that is what I am going to try. I've copied a typical reverse shell from my `/usr/share/webshells/php` dir to my working directory, and changed what needed to be changed. First attempt will be simply to try to upload that as is.

![](img/notvalidtorrent.png)

I probably should have expected that. So I tried capturing the traffic in BurpSuite and changing some things - like changing the extension to '.torrent' and the Content-Type to 'x-bittorrent'. That didn't fool the website.

So now I've just gone an uploaded a real bonafide actual torrent, and I see I get the chance to edit it afterwards, for instance to add a screenshot.

![](img/edit.png)

This time I'll try to upload a shell as a screenshot. Sooooo devious.

![](img/upload-shell.png)

Except right there it tells me the allowed types. Dammit. So I've added a .png extension to see if that fools the system.

The shell seemed to upload successfully, but looks like it had it's filename changed (to a hash) and it is still considered a .png file. When I try to view it just errors out and I don't get a reverse shell.

So now I will do it all over again, but intercept the traffic and try changing things in Burp, like I attempted with the torrent file.

![](img/itworked.png)

I think it worked!

Time to sping up that listener to catch the shell! `rlwrap nc -nvlp 6969`

Hot damn I've a shell!

![](img/user-shell.png)

### Flag

![](img/user-flag.png)

## User Enumeration

* I don't have the password for this user so I can't check `sudo -l`
* Root is running a cron, must find out what it is
* The box is running an old version of sudo
* There is a MYSQL service running on localhost
* Found a plaintext password inside a PHP config file
* Running [Linux Exploit Suggester](https://github.com/mzet-/linux-exploit-suggester) gives us a lot of kernel exploit options (Dirty Cow, Half & Full Nelson)

## Privilege Escalation

### Full Nelson

It was the first exploit on the list, and so the most likely to succeed(?).

![](img/fullnelson.png)

I just need to grab the c file from [here](https://github.com/lucyoa/kernel-exploits/blob/master/full-nelson/full-nelson.c), then send it over to the machine, compile it and run it I guess.

The command to compile the exploit is luckily a simple one: `gcc fullnelson.c -o fullnelson`. It completed without errors.

![](img/root-shell.png)

And there it is folks.

### Flag

![](img/root-flag.png)

## Conclusion

I had a lot of trouble coming to the realization that I should mess with the upload functionality of the torrent site; I had to get help there since I could not understand what I was reading about the vuln. I hope that if I had just done more research that I would have gotten further on my own.

I did get to learn about using BurpSuite to circumvent some primitive filetype checks by tampering with the data in transit, so that was a neat trick.

Amusingly, when I finally did get a user shell, the system had already been compromised by someone that used (I think) the Dirtycow exploit - they left quite a mess and did not clean up. The `passwd` file was overwritten and the root user basically deleted. I had to reset the machine and get a new shell before I could privesc.

Steps taken:

* Scan/enumerate until an upload vuln is found in a torrent site (create an account to access the upload form)
* Upload a torrent file, then edit the upload to add a php reverse shell as a screenshot
* Bypass the filetype restriction by passing the traffic through BurpSuite proxy - change the filetype while in transit
* Once on the machine, use a kernel exploit to get a root shell (there were apparently many options)
