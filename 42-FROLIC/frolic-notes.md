# FROLIC | 10.10.10.111

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.111
    
### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.2p2
* 139/445 - SMB - smbd 4.3.11
* 1880 - Node.js
* 9999 - HTTP - nginx 1.10.3

## Services Enumeration

### 22 - OpenSSH 7.2p2

We'll wait and see if we get any credentials before returning to this service.

### 139/445 - smbd 4.3.11

Is vulnerable to the [Sambacry](https://github.com/opsxcq/exploit-CVE-2017-7494) exploit, but that is an authenticated exploit, so we will need valid creds to take advantage of it. Running smbmap shows that there is no anonymous access.

### 1880 - Node.js

This is the login page for a NodeRed server, which is a visual programming tool for connecting various hardware devices. This interface can be used to create javascript functions.

![](img/node-red.png)

Perhaps the user here has a weak password and it can be guessed, or there is some kind of SQLi vulnerability.

A gobuster scan gives me a few other directories, most of them inaccessible at this point:

![](img/gobuster-node.png)

Let's move on to port 9999.

### 9999 - nginx 1.10.3

This version does not appear to be vulnerable to any remote exploits, and it is a standard nginx start page:

![](img/port-9999.png)

Gobuster reports a handful of directories, and when I go to them, I begin to dread that I have spun up one of those annoying as hell CTF-type boxes with deliberate rabbit-holes. Honestly, I hate these types of machines.

![](img/test.png)

![](img/backup.png)

![](img/admin.png)

That login page has the password embedded in the js:

![](img/login-js.png)

So I can login and then I am presented with this crap:

![](img/success.png)

It is a programming language called Ook, apparently, and then I throw the text into a decoder and it gives me a directory to go to:

![](img/ook.png)

So I go there and wow, there is another encoded string. So fun:

![](img/newdir.png)

It is base64 encoded and decoding it shows it is a zip file, password protected. I 'zip2john' it into a hash and crack it with john, the password is literally 'password'.

![](img/password.png)

It extracts an index.php file that just has probably another code in it. I seriously regret spinning up this dumbfuck machine. Why is it on the OSCP list again?

It is encoded in hex, so I decode that and it is in base64 again. Please kill me. After decoding that I get another ciphertext, this time with dots, dashes, plus signs, square brackets and greater than/less than signs. It is the brainfuck language.

The decoded result is 'idkwhatispass', which is a password. Bravo.

Then the box maker wants you to recurse into all the previous directories you found just to find a clue to find another directory, which is `/playsms`. I hate this. Truth is, gobuster does not recurse into directories it finds, for that I will have to use another dirbusting tool I found called 'feroxbuster' (or dirsearch I suppose).

So I go to the /playsms dir and login with 'admin' and 'idkwhatispass' and it is a web-based SMS service thing. It has many potential vulnerabilities according to searchsploit, a few on Metasploit.

## Foothold

Because this box is so dumb, I'm going to use msfconsole for this one.

![](img/meterpreter.png)

I don't even care to show the flag.

## User Enumeration

I dropped into a shell and ran linpeas, and it found an SUID binary:

![](img/SUID.png)

It also found a hash in the 'settings.js' file in an accessible directory:

![](img/node-hash.png)

## Privilege Escalation

### SUID binary

The 'rop' binary appears to be vulnerable to a buffer overflow attack:

![](img/overflow.png)

Finally, something interesting!

So, let's go through the steps and see how we can exploit this for a root shell.

I ran `checksec` on it in gdb, and the 'no execute' (NX) bit is enabled, so that means I can't execute my own shellcode on the stack:

![](img/checksec.png)

But at least ASLR is disabled on the target:

![](img/aslr-disabled.png)

I'm just going to crib from my own previous work on other buffer overflows, and use my ['buffy'](https://gitlab.com/cnose/oscp-prep/-/blob/master/tools/linux/buffy.py) script, except I suppose this time I won't need the bruteforcing part.

#### find the EIP offset

I use an online tool since I am not on Kali, but the crash happens at 52 bytes in:

![](img/offset.png)

### Return to libc attack

Here, I just follow the commands in my buffy.py script, to find the libc base address, then for good measure I get the system and exit offsets. And finally, I find the shell offset, which will call /bin/sh for me.

Then I make the payload the 52 bytes of junk, plus all of the libc + shell address, and simply tell the script to print it out.

Then I upload the script to the victim machine.

All that is left to do is to mount the attack; it is simple:

    ./rop $(python /dev/shm/buffy.py)
    
![](img/root-shell.png)

And I win. Flag included.

## Conclusion

This was a dumbass box at the start, overstuffed with trolling, CTF bs that I don't care for. I understand that the rabbit holes and blind alleys are meant to get you to put your thinking cap on, but it is unneccesary in my opinion. Once I got on the box though, it's always nice to do a buffer overflow attack, even if they are simplistic to other more experienced binary reversers. 

Steps taken:

* After a bunch of BS, find vulnerable application and exploit one of it's many issues to get a php reverse shell
* Find vulnerable SUID binary, develop buffer overflow exploit
* Run the exploit for a root shell
