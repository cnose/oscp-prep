# SERVMON | 10.10.10.184

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.184
    
### Open Ports and Services

* 21 - FTP - Microsoft ftpd
* 22 - SSH - OpenSSH for Windows 7.7
* 80 - HTTP
* 135 - MSRPC
* 139/445 - SMB
* 5040
* 5666 - tcpwrapped
* 6063
* 7680
* 6699 - napster?
* 8443 - HTTPS-ALT

## Services Enumeration

### 21 - FTP

I am able to login as anon. Inside are two directories, 'Nadine' and 'Nathan'. Inside the Nadine folder is a note called "confidential.txt" and there is a "Notes to do" text file inside Nathan's dir.

**Confidential.txt**

![](img/confidential.png)

**Notes to do.txt**

![](img/todo.png)

I do not have write access to the FTP share.

### 22 - SSH

Without creds, I can't do anything with this right now.

### 135 - MSRPC

I cannot get a null session on this service, so there is nothing to get from it.

### 139/445 - SMB

Same here, without creds I can't get any info from it.

### 80 - HTTP

![NVMS-1000](img/port-80.png)

There seems to be some kind of DVR service running here, and it asks for credentials, which we do not yet have.

Searchsploit drums up a directory traversal exploit, which may be useful in grabbing that Passwords.txt file maybe.

### 8443 - HTTPS

This takes me to a slow and buggy-as-hell page for something called "NsClient++" that either hangs forever or presents me with a login screen:

![nsclient](img/nslogin.png)

Seems like the only viable option is trying the directory traversal exploit on the NVMS service, since the only exploits for the NSClient are authenticated ones.

## Foothold via directory traversal to expose passwords

I've grabbed an exploit written in python from [here](https://github.com/AleDiBen/NVMS1000-Exploit/blob/master/nvms.py), and I am going to try to grab the Passwords.txt file that was mentioned in the confidential note.

![passwords.txt](img/passwords.png)

What an annoying group of passwords. First let's use 'crackmapexec to see if any of these passwords are for an easy win:

    crackmapexec smb 10.10.10.184 -u users -p passwords.txt
    
And it looks like we have a winner:

![nadine smb](img/smb-nadine.png)

This should also mean we have SSH access:

![nadine ssh](img/nadine-ssh.png)

And here is the flag for good measure:

![nadine flag](img/user-flag.png)

## User Enumeration

Now, I've actually done the box up to this point before, without taking notes. But I do remember the privesc having something to do with the nsclient page, and it being a major pain in the ass, so I probably gave up. The machine kept resetting or people kept resetting the nsclient because that is one way to trigger the exploit. As it is, the page does not load properly for me in Firefox, so I have it in a Chromium-based one that seems to work a bit better.

Anyway, the privesc is the one that is in searchsploit, where local users can read the web admin's password in cleartext:

![](img/nsclient-privesc.png)

After a bit of sleuthing, I found the config file at C:\Program Files\NSClient++\nsclient.ini

![nsclient password](img/nsclient-pass.png)

OK let's try logging into the interface:

![forbidden](img/notallowed.png)

Luckily I just finished another box that taught me about a tool I can use to forward the port; it's called [chisel](https://github.com/jpillora/chisel). Once I upload the binary on to the machine, I run the Linux variant on my machine:

    ./chisel server -p 9999 --reverse
    
On Windows, I run `chisel.exe client 10.10.14.20:9999 R:8443:localhost:8443`.

Then, when I navigate to the interface on 'https://localhost:8443', I can input the password and gain access:

![](img/metrics.png)

## Privilege Escalation via external script execution

Now, this thing is annoying as hell, and I know that the exploit that's out there is more trouble than it's worth. So let's do it manually.

First I transfer a copy of netcat and a small .bat file over - the bat file executes the reverse shell command.

Next, I need to use the interface to add a new script, like so:

![script](img/addscript.png)

Then, I have to add an entry to the scheduler, like so:

![scheduler](img/sched.png)

After that I have to add another with slightly different inputs:

![scheduler2](img/sched2.png)

Finally I have a netcat listener waiting. And I wait....

And wait...

And wait....

And it does not work as advertised. If you make a single mistake that, say, starts to fill up the logs with errors, and then you hit the reload button, well you're fucked and you have to revert the box. And let me tell you, this whole privesc is prone to errors. 

The way I ended up doing it is similar to what I wrote above, except that you don't have to bother with the Scheduler part. Add the new script that runs your malicious .bat file, save the config. Then immediately reload and cross your goddamn fingers that it doesn't kill the box. 

If all went well, click on "Queries" and you should see your script there:

![no](img/no.png)

Then just click through until you see the "Run" option and click it. Make sure you have your listener running, otherwise you might be SOL.

Anyway, it worked after literal hours of trying:

![root shell](img/root-shell.png)

### Flag

![root flag](img/root-flag.png)

## Conclusion

Fuck this machine.
