# Cheatsheets

These are some cheatsheets that I have created based on others that I have found helpful in my journey to OSCP Completion. Starting small and building it up as I go.

## Contents

[Reverse Shell Cheatsheet](revshell.md) - some useful one-liners for reverse shell commands since I'm often forgetting them.

[File Transfer Commands](filetransfer.md) - Different methods for uploading and downloading files on Linux and Windows machines. Certainly incomplete.

[Linux Privesc Cheatsheet](linuxprivesc.md) - taken wholesale from PayloadsAllTheThings.

[Windows Privesc Cheatsheet](windowsprivesc.md) - also taken from PATT.

[Bind Shell Cheatsheet](bindshell.md)

[Command Injection](commandinjection.md)

[Directory Traversal](dirtraversal.md)

[File Inclusion](fileinclusion.md)

[SQL Injection](SQLinjection.md)

[Network Pivoting Techniques](networkpivoting.md)

[Linux Persistence](linuxpersistence.md)

[Windows Persistence](windowspersistence.md)
