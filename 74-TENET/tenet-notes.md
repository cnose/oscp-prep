# TENET | 10.10.10.223

## Enumeration

```bash
sudo nmap -v -T4 -sCV -oN nmap-initial 10.10.10.223
sudo nmap -v -T4 -sUV --top-ports=20 -oN nmap-udp 10.10.10.223
```

### Ports and Services

![nmap initial scan](img/Pasted image 20210521111501.png)

#### 22 - SSH - OpenSSH 7.6p1 Ubuntu

A normal SSH install. Hopefully I will find credentials that will allow me access to the box this way.

#### 80 - HTTP - Apache 2.4.29

![homepage](img/Pasted image 20210521110926.png)

A default page for an Apache web server. Running a dirsearch reveals a wordpress instance running:

![diresearch](img/Pasted image 20210521111015.png)

Once I updated my `/etc/hosts` file I was actually able to access the WordPress blog, which is located at the root `http://tenet.htb`:

![wordpress](img/Pasted image 20210521111600.png)

wpscan did not find much besides two users:

![wpscan](img/Pasted image 20210521112301.png)

On one post there is an interesting exchange by the users:

![blog post](img/Pasted image 20210521112526.png)

I guess that means I'm looking for some PHP, possibly (hopefully) called 'sator.php'? In case anyone didn't know, these names are based on characters from the movie Tenet. Also looking for a backup somewhere, although wpscan didn't find one in the usual places.

So far, searching manually I've found 'users.txt' and 'sator.php' - they are located at the IP address rather than the hostname, at the root:

**users.txt**  
![users.txt](img/Pasted image 20210521114425.png)

**sator.php**  
![sator.php](img/Pasted image 20210521114442.png)

Easy to see the two locations are related somehow.

Noticed another thing about that blog post that has the comment; the URL has a different name than usual (as the default is to have the post title):

![logs?](img/Pasted image 20210521114744.png)

Not sure if thats a clue but it may be worth noting.

In reference to a backup I tried looking for .bak files for a bit and came up empty. Then I appended .bak to 'sator.php' and hit paydirt:

![sator.php.bak](img/Pasted image 20210521121511.png)

I vaguely recognize this as PHP deserialization, which is essentially turning an object back into a string. In this script, there is a class called 'DatabaseExport'. Near the bottom of the file a new object called '$app' is created using the 'DatabaseExport' class, and then the 'update_db()' function is called.

The '\_\_destruct()' function is called after that, which writes 'data' into the 'user_file' (in this case 'users.txt'). I can also see that the script makes a GET request to 'arepo' ('opera' in reverse) and it is unserialized. So after reading up about this, I think if I pass a serialized object string to the $input variable, it will be unserialized and the $databaseupdate variable will be whatever we pass in via the GET request. (Honestly I'm just copying this verbatim from a blog post and I don't understand it yet, but hopefully soon I will.) Bottom line is I can get code execution this way.

## Exploitation - PHP deserialization attack to gain code execution

### Exploit Method

I have to first create the serialized object. I will need to write my own php script to create that, and then I will pass the object on to the target somehow? I dunno. I'm getting my info from [here](https://www.bootlesshacker.com/php-deserialization/), but there are probably better places that explain it better.

My PHP script is ready, and there is not much to it:

![PHP script](img/Pasted image 20210521125446.png)

I just need to run a PHP command on my system to generate the serialized string, fingers crossed I didn't make any mistakes.

```bash
php -q arepo.php
```

(I did make a mistake - I failed to correctly capitalize and spell the 'DatabaseExport' class in the echo command)

![PHP code](img/Pasted image 20210521130003.png)

It's ugly, but it seemed to have worked. So now I append that string to `http://10.10.10.223/sator.php?arepo=` and it _should_ write the simple backdoor to 'koopa.php'.

![Appended PHP](img/Pasted image 20210521130216.png)

Looks like I received a positive response as there is a second printing of 'Database updated' there. Let's check out `http://10.10.10.223/koopa.php` and hope for the best:

![Code execution](img/Pasted image 20210521130409.png)

Nice! Code execution! Time to get a shell. In my case netcat and bash didn't work, but PHP did. Guess that's fitting:

![user shell](img/Pasted image 20210521131919.png)

## Privilege Escalation - www-data > neil

I am unable to read the flag in Neil's directory, so I will have to escalate my privileges to neil or maybe I can skip that entirely and become root.

### Enumeration

**MySQL creds from /var/www/html/wordpress**  
![mysql creds](img/Pasted image 20210521132515.png)

Confirmed to not work on the wordpress login, but the password does work for neil on the box:

![su - neil](img/Pasted image 20210521132835.png)

SSH works as well.

#### user flag

![user flag](img/Pasted image 20210521132922.png)

## Privilege Escalation - neil > root

**Enumerating MySQL database**  
![mysql db](img/Pasted image 20210521133424.png)

**Enumerating wordpress database**  
![wordpress db](img/Pasted image 20210521133634.png)

### Enumeration

**linpeas - sudo -l**  
![sudo -l](img/Pasted image 20210521135631.png)

**linpeas - backup folder**  
![backup folder](img/Pasted image 20210521135847.png)

The backup folder is not what we want to look at. The fact that we can run this 'enableSSH.sh' script as root is interesting. For a moment I thought I could just replace the SSH key that was in the script and that would be that, but neil does not have write access to the script, so I can't edit it:

![script](img/Pasted image 20210521141014.png)

**No write access**  
![Can't write to the script](img/Pasted image 20210521141101.png)

So in this script 3 functions are defined. 'addKey' creates a temporary file and then echoes the $key into the tempfile. then it uses the 'checkFile' function to verify the key has been created. Finally, 'checkAdded' confirms that the specified key has been added to '/root/.ssh/authorized_keys'. Again, I am unable to write to this file; and the script uses the full paths of binaries that it calls, with the exception of 'touch' used within the 'addkey' function.

I researched the relationship between 'mktemp' and 'touch' and noted that there were several mentions of 'mktemp' being insecure in that an attacker could exploit a 'race condition' to basically access the tempfile before the script is finished with it. In this script, `mktemp -u` is called, which does not even create the file, just prints what it will be, and then in the next moment it is created by `touch`, which seems redundant because you should just use `mktemp` to create it and not add in a delay between the generation and the creation.

## Exploitation - privesc via race condition in root script

Basically, with the script that I can run as root there is a portion that determines what the temp file is named, then creates it, then adds content to it before moving that content into a restricted area. In those microseconds it is possible to gain access to the file and overwrite it.

### Exploit method

I had a LOT of trial and error on this one, but eventually made a dirty bash script that continuously loops through a find command on the '/tmp' dir and when it finds a candidate file (one named 'ssh-[8 random chars]') it quickly echoes my SSH key to it.

![dirty script](img/Pasted image 20210521181633.png)

I brought that script over to the target, ran it, then I also executed a command to run the 'enableSSH' script every 2 seconds:

``` bash
watch sudo /usr/local/bin/enableSSH.sh
```

I tested this on my machine and it worked, but it took a lot longer to work on the target (this is after many many attempts):

![Running the race](img/Pasted image 20210521182339.png)

But eventually I succeeded and quickly tried to login as root via SSH:

![root shell](img/Pasted image 20210521182258.png)

#### root flag

![root flag](img/Pasted image 20210521182531.png)

## Conclusion

What a ride. My first time exploiting a PHP deserialization vulnerability, and it was nice to learn something new. I'm going to dig into some Youtube videos and read more blog posts about the subject, because I know nothing about PHP so it was bewildering. The privesc to root was a trial. I know I'm not that great at scripting, or at least I am very trial and error. I'm sure a person more into scripting could have written the bash script in no time flat, but for me it was an exercise in trying one thing and having it not work, repeated ad nauseam, and then slowly building it out even though in the end it is just a few lines. It was super satisfying to have it actually work after all my effort.

**Steps taken:**

* Enumerate WordPress instance to find comment in blog post
* Find backup of PHP file to discover deserialization vulnerability
* Craft PHP string using custom PHP script, append string to URL to create PHP backdoor, get shell
* Move laterally by finding credentials in wordpress config files
* Escalate to root via race condition in root-owned script.
