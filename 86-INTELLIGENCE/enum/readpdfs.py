#!/usr/bin/python3

import os
import PyPDF2

directory = 'dox'

for file in os.listdir(directory):
    f = os.path.join(directory, file)
    pdfFileObj = open(f, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    pageObj = pdfReader.getPage(0)
    print(pageObj.extractText())
    pdfFileObj.close()
